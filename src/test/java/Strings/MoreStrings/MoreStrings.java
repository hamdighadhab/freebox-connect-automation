package Strings.MoreStrings;


import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.MorePages.MorePages;
import tests.ExtentReport.ExtentReportsFree;

public class MoreStrings {
	
	public static final String TestName1 = "Vérrouillage";
	public static final String TestName2 = "Nos applications : Files";
	public static final String TestName3 = "Nos applications : Home";
	public static final String TestName4 = "A propos";
	
	public static final String FonctionalityName = "Plus";


	//More tab-bar button Check 
	public static void moreTabBarButtonCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((MorePages.getMoreTabBarButtonText().equals("Plus")) && (MorePages.isMoreTabBarButtonDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'Plus' de la tab-bar s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'Plus' de la tab-bar s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ChevronConnectedDevice");
		}
	}

	//Click on More tab-bar
	public static void moreTabBarButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		MorePages.clickOnMoreTabBarButton();
		test.log(Status.INFO, "Clique sur Le bouton 'Plus' de la tab-bar");
	}

	//More page name Check 
	public static void morePageTitleCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((MorePages.getMorePageTitleText().equals("Plus")) && (MorePages.isMorePageTitleDiplayed() == true)) {
			test.log(Status.PASS, "La page 'Plus' s'affiche ");
		} else {
			test.log(Status.FAIL, "La page 'Plus' s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"MorePageTitle");
		}
	}
	
	//Ma Freebox section Check 
	public static void freeboxSectionTitleCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((MorePages.getFreeboxSectionTitleText().equals("Ma Freebox")) && (MorePages.isFreeboxSectionTitleDiplayed() == true)) {
			test.log(Status.PASS, "La section 'Ma Freebox' s'affiche ");
		} else {
			test.log(Status.FAIL, "La section 'Ma Freebox' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"FreeboxSectionTitle");
		}
	}

	//Informations section Check 
	public static void infosSectionTitleCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((MorePages.getInfosSectionTitleText().equals("Informations")) && (MorePages.isInfosSectionTitleDiplayed() == true)) {
			test.log(Status.PASS, "La section 'Ma Freebox' s'affiche ");
		} else {
			test.log(Status.FAIL, "La section 'Ma Freebox' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"InfosSectionTitle");
		}
	}

	
	//Lock element Check 
	public static void lockElementTitleCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((MorePages.getLockElementTitleText().equals("Verrouillage")) && (MorePages.isLockElementTitleDiplayed() == true)) {
			test.log(Status.PASS, "L'élémént 'Verrouillage' s'affiche ");
		} else {
			test.log(Status.FAIL, "L'élémént 'Verrouillage' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"LockElementTitle");
		}
	}

	//Click on lock element
	public static void lockElementTitleClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		MorePages.clickOnLockElementTitle();
		test.log(Status.INFO, "Clique sur L'élément 'Verrouillage'");
	}

	
	//Lock Page Check 
	public static void lockPageNameCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((MorePages.getLockPageNameText().equals("Verrouillage")) && (MorePages.isLockPageNameDiplayed() == true)) {
			test.log(Status.PASS, "La page 'Verrouillage' s'affiche ");
		} else {
			test.log(Status.FAIL, "La page 'Verrouillage' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"LockPageName");
		}
	}

	//Lock Page Description Check 
	public static void lockPageDescCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((MorePages.getLockPageDescText().equals("Verrouillez votre application avec un code ou l'authentification biométrique (empreinte / visage).")) && (MorePages.isLockPageDescDiplayed() == true)) {
			test.log(Status.PASS, "La description de la page 'Verrouillage' s'affiche ");
		} else {
			test.log(Status.FAIL, "La description de la page 'Verrouillage' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"LockPageDesc");
		}
	}

	
	//Activate Lock Button Check 
	public static void activateLockButtonCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((MorePages.getActivateLockButtonText().equals("Activer le verrouillage")) && (MorePages.isActivateLockButtonDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'Activer le verrouillage' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'Activer le verrouillage' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"LockElementTitle");
		}
	}

	//Click on Activate Lock Button
	public static void activateLockButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		MorePages.clickOnActivateLockButton();
		test.log(Status.INFO, "Clique sur le bouton 'Activer le verrouillage'");
	}
	
	
	//Ours App Element Check 
	public static void oursAppElementCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((MorePages.getOursAppElementText().equals("Nos applications")) && (MorePages.isOursAppElementDiplayed() == true)) {
			test.log(Status.PASS, "La cellule 'Nos applications' s'affiche ");
		} else {
			test.log(Status.FAIL, "La cellule 'Nos applications' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"OursAppElement");
		}
	}

	//Click on Ours App Element
	public static void oursAppElementClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		MorePages.clickOnOursAppElement();
		test.log(Status.INFO, "Clique sur la cellule 'Nos applications'");
	}

	//Ours Apps Page Title Check 
	public static void oursAppsPageTitleCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((MorePages.getOursAppsPageTitleText().equals("Nos applications")) && (MorePages.isOursAppsPageTitleDiplayed() == true)) {
			test.log(Status.PASS, "La page 'Nos applications' s'affiche ");
		} else {
			test.log(Status.FAIL, "La page 'Nos applications' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"OursAppsPageTitle");
		}
	}
	
	//Files App Name Check 
	public static void filesAppNameCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((MorePages.getFilesAppNameText().equals("Freebox Files")) && (MorePages.isFilesAppNameDiplayed() == true)) {
			test.log(Status.PASS, "L'application 'Freebox Files' s'affiche ");
		} else {
			test.log(Status.FAIL, "L'application 'Freebox Files' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"FilesAppName");
		}
	}

	//Files App Name Check 
	public static void filesAppDescCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((MorePages.getFilesAppDescText().equals("Lisez et téléchargez des fichiers sur votre Freebox.")) && (MorePages.isFilesAppDescDiplayed() == true)) {
			test.log(Status.PASS, "La description de l'application 'Freebox Files' s'affiche ");
		} else {
			test.log(Status.FAIL, "La description de l'application 'Freebox Files' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"FilesAppDesc");
		}
	}
	
	//Home App Name Check 
	public static void homeAppNameCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((MorePages.getHomeAppNameText().equals("Freebox Home")) && (MorePages.isHomeAppNameDiplayed() == true)) {
			test.log(Status.PASS, "L'application 'Freebox Home' s'affiche ");
		} else {
			test.log(Status.FAIL, "L'application 'Freebox Home' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"HomeAppName");
		}
	}

	//Home App Name Check 
	public static void homeAppDescCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((MorePages.getHomeAppDescText().equals("Gérez votre Pack Sécurité et vos objets connectés.")) && (MorePages.isHomeAppDescDiplayed() == true)) {
			test.log(Status.PASS, "La description de l'application 'Freebox Home' s'affiche ");
		} else {
			test.log(Status.FAIL, "La description de l'application 'Freebox Home' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"HomeAppDesc");
		}
	}
	//À propos Check 
	public static void aboutAppElementCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((MorePages.getAboutAppElementText().equals("À propos")) && (MorePages.isAboutAppElementDiplayed() == true)) {
			test.log(Status.PASS, "La cellule 'À propos' s'affiche ");
		} else {
			test.log(Status.FAIL, "La cellule 'À propos' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"AboutAppElement");
		}
	}

	//Click on À propos
	public static void aboutAppElementClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		MorePages.clickOnAboutAppElement();
		test.log(Status.INFO, "Clique sur la cellule 'À propos'");
	}

	//À propos page name Check 
	public static void aboutPageNameCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((MorePages.getAboutPageNameText().equals("À propos")) && (MorePages.isAboutPageNameDiplayed() == true)) {
			test.log(Status.PASS, "La page 'À propos' s'affiche ");
		} else {
			test.log(Status.FAIL, "La page 'À propos' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"AboutPageName");
		}
	}

	//Legal Notice Element Check 
	public static void legalNoticeElementCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((MorePages.getLegalNoticeElementText().equals("Mentions légales")) && (MorePages.isLegalNoticeElementDiplayed() == true)) {
			test.log(Status.PASS, "La cellule 'Mentions légales' s'affiche ");
		} else {
			test.log(Status.FAIL, "La cellule 'Mentions légales' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"LegalNoticeElement");
		}
	}

	//Used Library Element Check 
	public static void usedLibraryElementCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((MorePages.getUsedLibraryElementText().equals("Bibliothèques utilisées")) && (MorePages.isUsedLibraryElementDiplayed() == true)) {
			test.log(Status.PASS, "La cellule 'Bibliothèques utilisées' s'affiche ");
		} else {
			test.log(Status.FAIL, "La cellule 'Bibliothèques utilisées' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"UsedLibraryElement");
		}
	}

	//Click on Used Library Element
	public static void usedLibraryElementClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		MorePages.clickOnUsedLibraryElement();
		test.log(Status.INFO, "Clique sur la cellule 'Bibliothèques utilisées'");
	}

	//Used Library Page name Check 
	public static void usedLibraryPageCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((MorePages.getUsedLibraryPageText().equals("Bibliothèques utilisées")) && (MorePages.isUsedLibraryPageDiplayed() == true)) {
			test.log(Status.PASS, "La page 'Bibliothèques utilisées' s'affiche ");
		} else {
			test.log(Status.FAIL, "La page 'Bibliothèques utilisées' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"UsedLibraryPage");
		}
	}

}
