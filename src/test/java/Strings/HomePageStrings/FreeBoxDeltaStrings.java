package Strings.HomePageStrings;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.HomePagePages.ServerPage;
import tests.ExtentReport.ExtentReportsFree;

public class FreeBoxDeltaStrings {

	public static final String TestName1 = "Serveur Delta connecté";
	public static final String TestName2 = "Information Serveur Delta connecté";
	public static final String TestName3 = "Autres Informations Serveur Delta connecté";
	public static final String TestName4 = "Redémarrer la box Delta connecté";
	public static final String TestName5 = "Appareils connectés a ma box";
	public static final String FonctionalityName = "Delta Box";

	//Server Status Check
	public static void equipmentDetailStatusCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ServerPage.isEquipmentDetailStatusDiplayed() == true) {
			test.log(Status.PASS, "Le status de mon serveur s'affiche");
		} else {
			test.log(Status.FAIL, "Le status de mon serveur ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"EquipmentDetailStatus");
		}
	}
	public static void equipmentStatusCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ServerPage.getEquipmentStatus().equals(ServerPage.ServerStatus)) {
			test.log(Status.PASS, "Mon serveur est bien connecté");
		} else {
			test.log(Status.FAIL, "Mon serveur n'est pas connecté");
			ExtentReportsFree.takeScreenshot( driver,"EquipmentStatus");
		}
	}
	//Server Image Check
	public static void equipmentDetailImageCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ServerPage.isEquipmentDetailImageDiplayed() == true) {
			test.log(Status.PASS, "L'image de mon serveur s'affiche");
		} else {
			test.log(Status.FAIL, "L'image de mon serveur ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"EquipmentDetailImage");
		}
	}
	//Information Item Title Check
	public static void serverInformationsTitleCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ServerPage.isServerInformationsTitleDiplayed() == true) {
			test.log(Status.PASS, "Le titre Informations s'affiche");
		} else {
			test.log(Status.FAIL, "Le titre Informations ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ServerInformationsTitle");
		}
	}
	//Type Information Check
	public static void typeInfomationsElemenCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ServerPage.isTypeInfomationsElementDiplayed() == true) {
			test.log(Status.PASS, "L'élément Type s'affiche dans les informations du serveur");
		} else {
			test.log(Status.FAIL, "L'élément Type ne s'affiche pas dans les informations du serveur");
			ExtentReportsFree.takeScreenshot( driver,"TypeInfomationsElement");
		}
	}
	//Server Type Check
	public static void serverTypeValueCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ServerPage.getServerTypeValue().equals(ServerPage.DeltaServerType)) {
			test.log(Status.PASS, "Le Type de ma box est Delta");
		} else {
			test.log(Status.FAIL, "Le Type de ma box n'est pas Delta");
			ExtentReportsFree.takeScreenshot( driver,"ServerTypeValue");
		}
	}
	//Connected Device element check
	public static void connectedDeviceInfoElementCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ServerPage.isConnectedDeviceInfoElementDiplayed() == true) {
			test.log(Status.PASS, "L'élément Type s'affiche dans les informations du serveur");
		} else {
			test.log(Status.FAIL, "L'élément Type ne s'affiche pas dans les informations du serveur");
			ExtentReportsFree.takeScreenshot( driver,"ConnectedDeviceInfoElement");
		}
	}
	//Others Information Element Check 
	public static void deltaOtherInformationsCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ServerPage.isDeltaOtherInformationsDiplayed() == true) {
			test.log(Status.PASS, "L'élément Autres Informations s'affiche ");
		} else {
			test.log(Status.FAIL, "L'élément Autres Informations ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DeltaOtherInformations");
		}
	}
	//Click on Others Informations
	public static void deltaOtherInformationsClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ServerPage.clickOnDeltaOtherInformations();
		test.log(Status.INFO, "Clique autres informations de la Delta");
	}
	//Delta Model Check 
	public static void modeleDeltaCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ServerPage.isModeleDeltaDiplayed() == true) {
			test.log(Status.PASS, "Le modèle de la box Delta s'affiche ");
		} else {
			test.log(Status.FAIL, "Le modèle de la box Delta ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ModeleDelta");
		}
	}
	//Delta Model Check
	public static void modeleDeltaTextCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ServerPage.getModeleDeltaText().equals(ServerPage.DeltaModel)) {
			test.log(Status.PASS, "Le wording 'Freebox Server' s'affiche");
		} else {
			test.log(Status.FAIL, "Le wording 'Freebox Server' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ModeleDeltaText");
		}
	}
	//Logiciel Version Check 
	public static void versionLogicielleDeltaCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ServerPage.isVersionLogicielleDeltaDiplayed() == true) {
			test.log(Status.PASS, "La version logicielle de la box Delta s'affiche ");
		} else {
			test.log(Status.FAIL, "La version logicielle de la box Delta ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"VersionLogicielleDelta");
		}
	}
	//Serial Number Check 
	public static void numSerieDeltaCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ServerPage.isNumSerieDeltaDiplayed() == true) {
			test.log(Status.PASS, "Le Numèro de Serie de la box Delta s'affiche ");
		} else {
			test.log(Status.FAIL, "Le Numèro de Serie de la box Delta ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"NumSerieDelta");
		}
	}
	//MAC Adress Check 
	public static void adresseMACDeltaCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ServerPage.isAdresseMACDeltaDiplayed() == true) {
			test.log(Status.PASS, "L'adresse MAC de la box Delta s'affiche ");
		} else {
			test.log(Status.FAIL, "L'adresse MAC de la box Delta ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"AdresseMACDelta");
		}
	}
	//IPV4 Adress Check 
	public static void IPV4DeltaCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ServerPage.isIPV4DeltaDiplayed() == true) {
			test.log(Status.PASS, "L'adresse IPV4 de la box Delta s'affiche ");
		} else {
			test.log(Status.FAIL, "L'adresse IPV4 de la box Delta ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"IPV4Delta");
		}
	}
	//IPV4 Adress Check 
	public static void IPV6DeltaCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ServerPage.isIPV6DeltaDiplayed() == true) {
			test.log(Status.PASS, "L'adresse IPV6 de la box Delta s'affiche ");
		} else {
			test.log(Status.FAIL, "L'adresse IPV6 de la box Delta ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"IPV6Delta");
		}
	}
	//Allumé Depuis Check 
	public static void allumeDepuisDeltaCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ServerPage.isAllumeDepuisDeltaDiplayed() == true) {
			test.log(Status.PASS, "Allumé depuis de la box Delta s'affiche ");
		} else {
			test.log(Status.FAIL, "Allumé depuis de la box Delta ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"AllumeDepuisDelta");
		}
	}
	//Reboot button check
	public static void rebootButtonaCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ServerPage.isRebootButtonDiplayed() == true) {
			test.log(Status.PASS, "Le bouton 'Redémarer le Freebox Server' s'affiche");
		} else {
			test.log(Status.FAIL, "Le bouton 'Redémarer le Freebox Server' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"RebootButton");
		}
	}
	//Click on Reboot button
	public static void rebootButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ServerPage.clickOnRebootButton();
		test.log(Status.INFO, "Clique sur le bouton 'Redémarer le Freebox Server'");
	}
	//Reboot Page Title check
	public static void serverRebootTitleTextCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ServerPage.getServerRebootTitleText().equals(ServerPage.RebootPageTitle)) && (ServerPage.isServerRebootTitleDiplayed() == true)) {
			test.log(Status.PASS, "Le Titre de la page 'Redémarer le Freebox Server' s'affiche");
		} else {
			test.log(Status.FAIL, "Le Titre de la page 'Redémarer le Freebox Server' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ServerRebootTitleText");
		}
	}
	//Reboot Page Header check
	public static void serverRebootHeaderTextCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ServerPage.getServerRebootHeaderText().equals(ServerPage.RebootPageHeader)) && (ServerPage.isServerRebootHeaderDiplayed() == true)) {
			test.log(Status.PASS, "Le Header de la page 'Redémarer le Freebox Server' s'affiche");
		} else {
			test.log(Status.FAIL, "Le Header de la page 'Redémarer le Freebox Server' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ServerRebootHeaderText");
		}
	}
	//Reboot Page Header check
	public static void serverRebootDescriptionTextCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ServerPage.getServerRebootDescriptionText().equals(ServerPage.RebootPageDesc)) && (ServerPage.isServerRebootDescriptionDiplayed() == true)) {
			test.log(Status.PASS, "La Description de la page 'Redémarer le Freebox Server' s'affiche");
		} else {
			test.log(Status.FAIL, "La Description de la page 'Redémarer le Freebox Server' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ServerRebootDescriptionText");
		}
	}
	//Reboot Page Image check
	public static void serverRebootImageCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ServerPage.isServerRebootImageDiplayed() == true) {
			test.log(Status.PASS, "L'image de la page 'Redémarer le Freebox Server' s'affiche");
		} else {
			test.log(Status.FAIL, "L'image de la page 'Redémarer le Freebox Server' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ServerRebootImage");
		}
	}
	public static void rebootButton2Check(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ServerPage.isServerRebootButton2Diplayed() == true) {
			test.log(Status.PASS, "Le bouton 'Redémarer le Freebox Server' s'affiche");
		} else {
			test.log(Status.FAIL, "Le bouton 'Redémarer le Freebox Server' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"RebootButton2");
		}
	}
	//Connected devices element text Check
	public static void connectedDeltaDevicesTextCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ServerPage.getConnectedDeltaDevicesText().equals(ServerPage.ConnectedDeltaDevicesElementName)) && (ServerPage.isConnectedDeltaDevicesDiplayed() == true)) {
			test.log(Status.PASS, "L'élément 'Appareils Connectés' s'affiche");
		} else {
			test.log(Status.FAIL, "L'élément 'Appareils Connectés' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ConnectedDeltaDevicesText");
		}
	}
	//Number of Connected devices element Check
	public static void connectedDevicesNumberCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ServerPage.isConnectedDevicesNumberDiplayed() == true) {
			test.log(Status.PASS, "Le Nombre des appareils connectés s'affiche");
		} else {
			test.log(Status.FAIL, "Le Nombre des appareils connectés ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ConnectedDevicesNumber");
		}
	}
	//Click on Connected Devices
	public static void connectedDeltaDeviceClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ServerPage.clickOnConnectedDeltaDevices();
		test.log(Status.INFO, "Clique 'Appareils connectés'");
	}
	//Connected Devices Page Title
	public static void connectedDevicesPageTitleTextCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ServerPage.getConnectedDevicesPageTitleText().equals(ServerPage.ConnectedDeltaDevicesElementName)) && (ServerPage.isConnectedDevicesPageTitleDiplayed() == true)) {
			test.log(Status.PASS, "Le Titre de la page 'Appareils connectés' s'affiche");
		} else {
			test.log(Status.FAIL, "Le Titre de la page 'Appareils connectés' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ConnectedDevicesPageTitleText");
		}
	}
	//Device name Check
	public static void deviceNameCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ServerPage.isDeviceNameDiplayed() == true) {
			test.log(Status.PASS, "Le nom de l'appareil connecté s'affiche");
		} else {
			test.log(Status.FAIL, "Le nom de l'appareil connecté ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DeviceName");
		}
	}
	//Device Vendor Check
	public static void deviceVendorCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ServerPage.isDeviceVendorDiplayed() == true) {
			test.log(Status.PASS, "Le nom du vendeur de l'appareil connecté s'affiche");
		} else {
			test.log(Status.FAIL, "Le nom du vendeur de l'appareil connecté ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DeviceVendor");
		}
	}
	//Device Image Check
	public static void deviceImgCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ServerPage.isDeviceImgDiplayed() == true) {
			test.log(Status.PASS, "L'image de l'appareil connecté s'affiche");
		} else {
			test.log(Status.FAIL, "L'image de l'appareil connecté ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DeviceImg");
		}
	}
	//Device Connexion Type Check
	public static void deviceConnectionTypeCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ServerPage.isDeviceConnectionTypeDiplayed() == true) {
			test.log(Status.PASS, "Le type de connection de l'appareil s'affiche");
		} else {
			test.log(Status.FAIL, "Le type de connection de l'appareil ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DeviceConnectionType");
		}
	}
}
