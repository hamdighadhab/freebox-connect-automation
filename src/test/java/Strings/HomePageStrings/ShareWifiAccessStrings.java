package Strings.HomePageStrings;


import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.HomePagePages.ShareWifiAccessPage;
import tests.ExtentReport.ExtentReportsFree;



public class ShareWifiAccessStrings {

	//public static ExtentTest test;
	//public static AppiumDriver<MobileElement> driver;

	public static final String TestName1 = "Partage système du WiFi principal";
	public static final String TestName2 = "Partage système du WiFi principal via QR Code";
	public static final String TestName3 = "Créer un accès invité illimité / internet uniquement /Clé Automatique";
	public static final String TestName4 = "Partage système du WiFi invité";
	public static final String TestName5 = "Partage système du WiFi invité via QR Code";
	public static final String TestName6 = "Supprimer un accès invité";
	public static final String FonctionalityName = "WiFi Principal";



	//My WiFi access WiFi page verification
	public static void accessPageCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ShareWifiAccessPage.isNetworkCardDiplayed() == true) {
			test.log(Status.PASS, "La page Mes Accès WiFi s'affiche");
		} else {
			test.log(Status.FAIL, "La page Mes Accès WiFi ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"WiFiAccessPage");
		}
	}	
	//Display QR Code button display check
	public static void QRCodebuttonCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ShareWifiAccessPage.isShareQRCodeWifiButtonDiplayed() == true) {
			test.log(Status.PASS, "Le bouton Afficher le QR Code s'affiche");
		} else {
			test.log(Status.FAIL, "Le bouton Afficher le QR Code ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DisplayQRCode");
		}
	}
	//Click on share WiFi access button
	public static void shareWiFibuttonclick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ShareWifiAccessPage.clickOnShareWiFiButton();
		test.log(Status.INFO, "Clique sur le bouton Partager mes accès WiFi");
	}
	//Share button display check
	public static void shareKeyWifiButtonCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ShareWifiAccessPage.isShareKeyWifiButtonDiplayed() == true) {
			test.log(Status.PASS, "Le bouton Partager s'affiche");
		} else {
			test.log(Status.FAIL, "Le bouton Partager ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ShareButton");
		}
	}
	//Click on Share button
	public static void shareWifiKeyclick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ShareWifiAccessPage.clickOnShareWiFiKeyButton();
		test.log(Status.INFO, "Clique sur le bouton Partager");
	}
	//Click on Display QR Code button
	public static void shareWiFiQRCodeButtonclick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		test.log(Status.INFO, "Clique sur le bouton Afficher le QR Code");
		ShareWifiAccessPage.clickOnShareWiFiQRCodeButton();
	}
	//Display QR Code button display check
	public static void QRCodeCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ShareWifiAccessPage.isQRCodeDiplayed() == true) {
			test.log(Status.PASS, "Le QR Code s'affiche");
		} else {
			test.log(Status.FAIL, "Le QR Code ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"QRCode");
		}
	}
	//Click on share WiFi access button
	public static void GuestWiFiAccessTabclick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		test.log(Status.INFO, "Clique sur l'onglet Accès WiFi invité");
		ShareWifiAccessPage.clickOnGuestWiFiAccessTab();
	}
	//Create Guest Access button display check
	public static void createGuestWifiAccessCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ShareWifiAccessPage.isCreateGuestWifiAccessDiplayed() == true) {
			test.log(Status.PASS, "Le bouton Crèer un accès invité s'affiche");
		} else {
			test.log(Status.FAIL, "Le bouton Crèer un accès invité ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"CreateGuestWifiAccessButton");
		}
	}
	//Click on Create Guest Access button
	public static void createGuestWifiAccessTabclick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		test.log(Status.INFO, "Clique sur le bouton Crèer un accès invité");
		ShareWifiAccessPage.clickOnCreateGuestWifiAccessTab();
	}
	//Guest WiFi Access Name Field display check
	public static void guestWiFiAccessNameFieldCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ShareWifiAccessPage.isGuestWiFiAccessNameFieldDiplayed() == true) {
			test.log(Status.PASS, "Le champ nom d'un nouvel invité s'affiche");
		} else {
			test.log(Status.FAIL, "Le champ nom d'un nouvel invité ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"GuestWiFiAccessNameField");
		}
	}
	//Guest WiFi Access Name Field display check
	public static void validateGuestWiFiAccessButtonCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ShareWifiAccessPage.isValidateGuestWiFiAccessButtonDiplayed() == true) {
			test.log(Status.PASS, "Le bouton VALIDER s'affiche");
		} else {
			test.log(Status.FAIL, "Le bouton VALIDER ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ValidateGuestWiFiAccessButton");
		}
	}
	//Click on Validate Guest WiFi Access Button
	public static void validateGuestWiFiAccessButtonclick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ShareWifiAccessPage.clickOnValidateGuestWiFiAccessButton();
		test.log(Status.INFO, "Valider l'accès invité");
	}

	//verification of the addition of guest WiFi access 
	public static void GuestWiFiNameCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ShareWifiAccessPage.getGuestWiFiName().equals("Anniversaire")) {
			test.log(Status.PASS, "L'accès WiFi invité est bien ajouté");
		} else {
			test.log(Status.FAIL, "L'accès WiFi invité n'est pas bien ajouté");
			ExtentReportsFree.takeScreenshot( driver,"GuestWiFiName");
		}
	}
	//Share button display check
	public static void shareWifiGuestKeyButtonCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ShareWifiAccessPage.isShareWifiGuestKeyButtonDiplayed() == true) {
			test.log(Status.PASS, "Le bouton Partager s'affiche");
		} else {
			test.log(Status.FAIL, "Le bouton Partager ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ShareWifiGuestKeyButton");
		}
	}
	//Click on Share button
	public static void shareWifiGuestKeyButtonclick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		test.log(Status.INFO, "Clique sur le bouton Partager");
		ShareWifiAccessPage.clickOnShareWifiGuestKeyButton();
	}
	//Display QR Code button display check
	public static void shareWifiGuestQRCodeButtonCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ShareWifiAccessPage.isShareWifiGuestQRCodeButtonDiplayed() == true) {
			test.log(Status.PASS, "Le bouton Afficher le QR Code s'affiche");
		} else {
			test.log(Status.FAIL, "Le bouton Afficher le QR Code ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ShareWifiGuestQRCodeButton");
		}
	}
	//Delete button display check
	public static void DeleteGuestWiFiAccessButtonCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ShareWifiAccessPage.isDeleteGuestWiFiAccessButtonDiplayed() == true) {
			test.log(Status.PASS, "Le bouton de suppression d'un accès WiFi invité s'affiche");
		} else {
			test.log(Status.FAIL, "Le bouton de suppression d'un accès WiFi invité ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DeleteGuestWiFiAccessButton");
		}
	}
	//Click on Delete Guest WiFi Access button
	public static void deleteGuestWiFiAccessButtonclick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ShareWifiAccessPage.clickOnDeleteGuestWiFiAccessButton();
		test.log(Status.INFO, "Clique sur le bouton de suppression d'un accés invité");
	}

	//Display QR Code button display check
	public static void ComfirmationDeleteGuestWiFiAccessCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ShareWifiAccessPage.isDComfirmationDeleteGuestWiFiAccessDiplayed() == true) {
			test.log(Status.PASS, "Le bouton de confirmation de la suppression d'un accès WiFi invité s'affiche");
		} else {
			test.log(Status.FAIL, "Le bouton de confirmation de la suppression d'un accès WiFi invité ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ComfirmationDeleteGuestWiFiAccess");
		}
	}
	public static void comfirmationDeleteGuestWiFiAccessButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ShareWifiAccessPage.clickOnComfirmationDeleteGuestWiFiAccessButton();
		test.log(Status.INFO, "Clique sur le bouton de confirmation de la suppression d'un accés invité");
	}
	//WiFi Guest Tab check
	public static void wiFiGuestTabCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ShareWifiAccessPage.isWiFiGuestTabDiplayed() == true) {
			test.log(Status.PASS, "L'onglet 'WiFi invités' s'affiche");
		} else {
			test.log(Status.FAIL, "L'onglet 'WiFi invités' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"WiFiGuestTab");
		}
	}
	//Click on WiFi Guest Tab
	public static void wiFiGuestTabClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		test.log(Status.INFO, "Clique sur l'onglet 'WiFi invités'");
		ShareWifiAccessPage.clickOnWiFiGuestTab();
	}
	
	
	
	
	
	
	//WiFi Guest Note check
	public static void wiFiGuestNoteCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ShareWifiAccessPage.isWiFiGuestNoteDiplayed() == true) {
			test.log(Status.PASS, "l'élément 'Note' s'affiche");
		} else {
			test.log(Status.FAIL, "l'élément 'Note' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"WiFiGuestNote");
		}
	}
	//Click on WiFi Guest Note
	public static void WiFiGuestNoteClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		test.log(Status.INFO, "Clique sur 'Note'");
		ShareWifiAccessPage.clickOnWiFiGuestNote();
	}
	
	//WiFi Guest Note Field check
	public static void wiFiGuestNoteFieldCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ShareWifiAccessPage.isWiFiGuestNoteFieldDiplayed() == true) {
			test.log(Status.PASS, "Le champ 'Note' s'affiche");
		} else {
			test.log(Status.FAIL, "Le champ 'Note' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"WiFiGuestNoteField");
		}
	}

	//Wifi Note Validate check
	public static void wifiNoteValidateCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ShareWifiAccessPage.isWifiNoteValidateDiplayed() == true) {
			test.log(Status.PASS, "l'élément 'Note' s'affiche");
		} else {
			test.log(Status.FAIL, "l'élément 'Note' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"WifiNoteValidate");
		}
	}
	//Click on Wifi Note Validate
	public static void wifiNoteValidateClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		test.log(Status.INFO, "Clique sur 'Note'");
		ShareWifiAccessPage.clickOnWifiNoteValidate();
	}

}
