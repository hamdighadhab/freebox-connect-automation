package Strings.HomePageStrings;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.HomePagePages.PlayerPage;
import tests.ExtentReport.ExtentReportsFree;

public class PlayersStrings {

	public static final String TestName1 = "Player Delta Connecté";
	public static final String TestName2 = "Player Delta informations";
	public static final String TestName3 = "Redemarrer le Player Delta";
	public static final String TestName4 = "Autres Informations du Player Delta";

	public static final String FonctionalityName = "Player Delta";


	//Delta player status check
	public static void deltaPlayerStatusCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((PlayerPage.getDeltaPlayerStatusText().equals(PlayerPage.DeltaPlayerStatus)) && (PlayerPage.isDeltaPlayerStatusDiplayed() == true)) {
			test.log(Status.PASS, "Le status 'Connecté' s'affiche");
		} else {
			test.log(Status.FAIL, "Le status 'Connecté' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DeltaPlayerStatus");
		}
	}

	//Delta player status check
	public static void deltaPlayerPictoCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (PlayerPage.isDeltaPlayerPictoDiplayed() == true) {
			test.log(Status.PASS, "Le picto du player Delta s'affiche");
		}
	}


	//Delta player status check
	public static void indormationsPlayerSectionCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((PlayerPage.getIndormationsPlayerSectionText().equals(PlayerPage.IndormationsPlayerSection)) && (PlayerPage.isIndormationsPlayerSectionDiplayed() == true)) {
			test.log(Status.PASS, "La section 'Informations' s'affiche");
		} else {
			test.log(Status.FAIL, "La section 'Informations' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DeltaPlayerStatus");
		}
	}

	//Player Connection Element check
	public static void playerConnectionElementCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((PlayerPage.getPlayerConnectionElementText().equals(PlayerPage.PlayerConnectionElement)) && (PlayerPage.isPlayerConnectionElementDiplayed() == true)) {
			test.log(Status.PASS, "L'élément 'Connection' dans la section 'Information' s'affiche");
		} else {
			test.log(Status.FAIL, "L'élément 'Connection' dans la section 'Information' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"PlayerConnectionElement");
		}
	}

	//Player Connection Type check
	public static void playerConnectionTypeCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((PlayerPage.getPlayerConnectionTypeText().equals(PlayerPage.PlayerConnectionType)) && (PlayerPage.isPlayerConnectionTypeDiplayed() == true)) {
			test.log(Status.PASS, "Le type de la connexion du player s'affiche");
		} else {
			test.log(Status.FAIL, "Le type de la connexion du player ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"PlayerConnectionType");
		}
	}	

	//Alert reboot player Title check
	public static void rebootPlayerAlertTitleCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((PlayerPage.getRebootPlayerAlertTitleText().equals(PlayerPage.RebootPlayerAlertTitle)) && (PlayerPage.isRebootPlayerAlertTitleDiplayed() == true)) {
			test.log(Status.PASS, "Le Titre de l'alert de redémarrage du player s'affiche");
		} else {
			test.log(Status.FAIL, "Le Titre de l'alert de redémarrage du player ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"RebootPlayerAlertTitle");
		}
	}	

	//Delta player reboot check
	public static void rebootPlayerButtonCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((PlayerPage.getRebootPlayerButtonText().equals(PlayerPage.RebootPlayerButton)) && (PlayerPage.isRebootPlayerButtonDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'Redémarrer le Freebox Player' s'affiche");
		} else {
			test.log(Status.FAIL, "Le bouton 'Redémarrer le Freebox Player' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"RebootPlayerButton");
		}
	}

	//Delta player reboot Click
	public static void rebootPlayerButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		PlayerPage.clickOnRebootPlayerButton();
		test.log(Status.INFO, "Clique sur 'Redémarrer le Freebox Player'");
	}

	//Other informations player check
	public static void othersInformationsDeltaPlayerCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((PlayerPage.getOthersInformationsDeltaPlayerText().equals(PlayerPage.OthersInformationsDeltaPlayer)) && (PlayerPage.isOthersInformationsDeltaPlayerDiplayed() == true)) {
			test.log(Status.PASS, "'Autres Informations' s'affiche");
		} else {
			test.log(Status.FAIL, "'Autres Informations' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"OthersInformationsDeltaPlayer");
		}
	}

	//Other informations player Click
	public static void OthersInformationsDeltaPlayerClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		PlayerPage.clickOnOthersInformationsDeltaPlayer();
		test.log(Status.INFO, "Clique sur 'Autres Informations'");
	}

	//Other informations player page title check
	public static void playerOtherInformationsTitleCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((PlayerPage.getPlayerOtherInformationsTitleText().equals(PlayerPage.PlayerOtherInformationsTitle)) && (PlayerPage.isPlayerOtherInformationsTitleDiplayed() == true)) {
			test.log(Status.PASS, "Le titre 'Autres Informations' s'affiche");
		} else {
			test.log(Status.FAIL, "Le titre 'Autres Informations' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"PlayerOtherInformationsTitle");
		}
	}

	//Model Element check
	public static void playerModelElementCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((PlayerPage.getPlayerModelElementText().equals(PlayerPage.PlayerModelElement)) && (PlayerPage.isPlayerModelElementDiplayed() == true)) {
			test.log(Status.PASS, "L'élément 'Modéle' s'affiche dans la liste des autres informations");
		} else {
			test.log(Status.FAIL, "L'élément 'Modéle' ne s'affiche pas dans la liste des autres informations");
			ExtentReportsFree.takeScreenshot( driver,"PlayerModelElement");
		}
	}

	//Delta Player Model Check
	public static void deltaPlayerModelValueCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (PlayerPage.getDeltaPlayerModelValueText().equals(PlayerPage.DeltaPlayerModelValue)) {
			test.log(Status.PASS, "Le Type de mon player est Delta");
		} else {
			test.log(Status.FAIL, "Le Type de mon player n'est pas Delta");
			ExtentReportsFree.takeScreenshot( driver,"DeltaPlayerModelValue");
		}
	}

	//Software Version Element check
	public static void playerLogicielVersionElementCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((PlayerPage.getPlayerLogicielVersionElementText().equals(PlayerPage.PlayerLogicielVersionElement)) && (PlayerPage.isPlayerLogicielVersionElementDiplayed() == true)) {
			test.log(Status.PASS, "L'élément 'Version logicielle' s'affiche dans la liste des autres informations");
		} else {
			test.log(Status.FAIL, "L'élément 'Version logicielle' ne s'affiche pas dans la liste des autres informations");
			ExtentReportsFree.takeScreenshot( driver,"PlayerLogicielVersionElement");
		}
	}

	//Serial Number Element check
	public static void playerSerielNumberElementCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((PlayerPage.getPlayerSerielNumberElementText().equals(PlayerPage.PlayerSerielNumberElement)) && (PlayerPage.isPlayerSerielNumberElementDiplayed() == true)) {
			test.log(Status.PASS, "L'élément 'Numéro de série' s'affiche dans la liste des autres informations");
		} else {
			test.log(Status.FAIL, "L'élément 'Numéro de série' ne s'affiche pas dans la liste des autres informations");
			ExtentReportsFree.takeScreenshot( driver,"PlayerSerielNumberElement");
		}
	}

	//MAC Address Element check
	public static void playerMACAdressElementCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((PlayerPage.getPlayerMACAdressElementText().equals(PlayerPage.PlayerMACAdressElement)) && (PlayerPage.isPlayerMACAdressElementDiplayed() == true)) {
			test.log(Status.PASS, "L'élément 'Adresse MAC' s'affiche dans la liste des autres informations");
		} else {
			test.log(Status.FAIL, "L'élément 'Adresse MAC' ne s'affiche pas dans la liste des autres informations");
			ExtentReportsFree.takeScreenshot( driver,"PlayerMACAdressElement");
		}
	}

	//Turned on Element check
	public static void playerTurnedONElementCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((PlayerPage.getPlayerTurnedONElementText().equals(PlayerPage.PlayerTurnedONElement)) && (PlayerPage.isPlayerTurnedONElementDiplayed() == true)) {
			test.log(Status.PASS, "L'élément 'Allumé depuis' s'affiche dans la liste des autres informations");
		} else {
			test.log(Status.FAIL, "L'élément 'Allumé depuis' ne s'affiche pas dans la liste des autres informations");
			ExtentReportsFree.takeScreenshot( driver,"PlayerTurnedONElement");
		}
	}

}
