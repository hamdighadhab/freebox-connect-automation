package Strings.HomePageStrings;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import elements.CommonElements;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.HomePagePages.HomeConnectionStatus;
import tests.ExtentReport.ExtentReportsFree;

public class ConnexionStatusStrings {

	public static final String TestName1 = "Information Serveur Delta connecté";
	public static final String FonctionalityName = "Voir mon débit internet";

	//Vérification de la nom de la page 
	public static void connectionStatusPageCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (CommonElements.getConnectionStatusPageName().equals(CommonElements.ConnectionStatusPageName)) {
			test.log(Status.PASS, "Le nom de la page de l'état de ma connexion s'affiche");
		} else {
			test.log(Status.FAIL, "Le nom de la page de l'état de ma connexion ne s'affiche pas et le nom de la page affiché est : "+ CommonElements.getConnectionStatusPageName());
			ExtentReportsFree.takeScreenshot( driver,"ConnectionStatusPage");
		}
	}
	//Vérification de la nom de la page 
	public static void connectionStatusCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (CommonElements.getConnectionStatus().equals(CommonElements.ConnectionStatus)) {
			test.log(Status.PASS, "Etat de ma connexion : Connecté à internet");
		} else {
			test.log(Status.FAIL, "Etat de ma connexion : Pas de connexion internet");
			ExtentReportsFree.takeScreenshot( driver,"ConnectionStatus");
		}
	}
	//Vérification si ma box est un DELTA ou bien POP 
	public static void deltaBoxCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (HomeConnectionStatus.modeleBox() == true) {
			test.log(Status.INFO, "Je possède la Box Delta");
			if (CommonElements.getAgregationSwitchStatus().equals(CommonElements.AgregationSwitchActivated)) {
				test.log(Status.INFO, "Le Switch Agrégation 4G est à ON");
				if (CommonElements.getModem4GStatus().equals(CommonElements.Modem4GConnected)) {
					test.log(Status.PASS, "L'état du modem 4G est Connecté ");
				} else {
					test.log(Status.FAIL, "L'état du modem 4G ne s'affiche pas correctement");
					ExtentReportsFree.takeScreenshot( driver,"4GModemStatus");
				}
			} else {
				test.log(Status.INFO, "Le Switch Agrégation 4G est à OFF");
				if (CommonElements.getModem4GStatus().equals(CommonElements.Modem4GNotConnected)) {
					test.log(Status.PASS, "L'état du modem 4G est Désactivé ");
				} else {
					test.log(Status.FAIL, "L'état du modem 4G ne s'affiche pas correctement");
					ExtentReportsFree.takeScreenshot( driver,"4GModemStatus");
				}
			}
		} else {
			test.log(Status.INFO, "Je possède la Box Pop");
			ExtentReportsFree.takeScreenshot( driver,"PopNotDelta");
		}
	}
	
	//Wan rate Download Check
	public static void wanRateDownloadCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (HomeConnectionStatus.isWanRateDownloadDiplayed() == true) {
			test.log(Status.PASS, "Le débit en réception s'affiche ");
		} else {
			test.log(Status.FAIL, "Le débit en réception ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"wanRateDownloadCheck");
		}
	}
	//Wan rate upload Check
	public static void wanRateUploadCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (HomeConnectionStatus.isWanRateUploadDiplayed() == true) {
			test.log(Status.PASS, "Le débit d'envoi s'affiche ");
		} else {
			test.log(Status.FAIL, "Le débit d'envoi ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"wanRateUploadCheck");
		}
	}

}
