package Strings.HomePageStrings;


import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.HomePagePages.RepeaterPage;
import pages.HomePagePages.ServerPage;
import tests.ExtentReport.ExtentReportsFree;

public class RepeaterStrings {

	public static final String TestName1 = "Répéteur connecté";
	public static final String TestName2 = "Information répéteur connecté";
	public static final String TestName3 = "Appareils connectés au répéteur";
	public static final String TestName4 = "Voyant lumineux du répéteur";
	public static final String TestName5 = "Redémarrer un répéteur";
	public static final String TestName6 = "Supprimer un répéteur";
	public static final String TestName7 = "Eloignez votre répéteur";
	public static final String TestName8 = "Autres Informations répéteur connecté";
	public static final String TestName9 = "Changer le nom du répéteur";
	public static final String TestName10 = "Améliorer ma connexion";
	public static final String FonctionalityName = "Répéteur";

	//Repeater Status Check
	public static void equipmentDetailStatusCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ServerPage.isEquipmentDetailStatusDiplayed() == true) {
			test.log(Status.PASS, "Le status de mon Répéteur s'affiche");
		} else {
			test.log(Status.FAIL, "Le status de mon Répéteur ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"EquipmentDetailStatus");
		}
	}
	//Repeater Status Text Check
	public static void EquipmentStatusCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ServerPage.getEquipmentStatus().equals(ServerPage.ServerStatus)) {
			test.log(Status.PASS, "Mon Répéteur est bien connecté");
		} else {
			test.log(Status.FAIL, "Mon Répéteur n'est pas connecté");
			ExtentReportsFree.takeScreenshot( driver,"EquipmentStatus");
		}
	}
	//Repeater Image Check
	public static void equipmentDetailImageCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ServerPage.isEquipmentDetailImageDiplayed() == true) {
			test.log(Status.PASS, "L'image de mon Répéteur s'affiche dans la page informations ");
		} else {
			test.log(Status.FAIL, "L'image de mon Répéteur ne s'affiche pas dans la page informations");
			ExtentReportsFree.takeScreenshot( driver,"EquipmentDetailImage");
		}
	}
	//Information Item Title Check
	public static void serverInformationsTitleCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ServerPage.isServerInformationsTitleDiplayed() == true) {
			test.log(Status.PASS, "Le titre Informations s'affiche");
		} else {
			test.log(Status.FAIL, "Le titre Informations ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ServerInformationsTitle");
		}
	}
	//Type Information Check
	public static void typeInfomationsElementCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ServerPage.isTypeInfomationsElementDiplayed() == true) {
			test.log(Status.PASS, "L'élément Type s'affiche dans les informations du répéteur");
		} else {
			test.log(Status.FAIL, "L'élément Type ne s'affiche pas dans les informations du répéteur");
			ExtentReportsFree.takeScreenshot( driver,"EquipmentDetailImage");
		}
	}
	//Server Type Check
	public static void serverTypeValueCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ServerPage.getServerTypeValue().equals(RepeaterPage.RepeaterType)) {
			test.log(Status.PASS, "Le Type est bien 'Répéteur'");
		} else {
			test.log(Status.FAIL, "Le Type n'est pas 'Répéteur'");
			ExtentReportsFree.takeScreenshot( driver,"EquipmentDetailImage");
		}
	}
	//Connected devices element text Check
	public static void connectedRepeaterDevicesTextCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RepeaterPage.getConnectedRepeaterDevicesText().equals(RepeaterPage.ConnectedRepeaterDevicesElementName)) && (RepeaterPage.isConnectedRepeaterDevicesDiplayed() == true)) {
			test.log(Status.PASS, "L'élément 'Appareils Connectés' s'affiche");
		} else {
			test.log(Status.FAIL, "L'élément 'Appareils Connectés' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ConnectedRepeaterDevicesText");
		}
	}
	//Number of Connected devices element Check
	public static void repeaterConnectedDevicesNumberCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (RepeaterPage.isRepeaterConnectedDevicesNumberDiplayed() == true) {
			test.log(Status.PASS, "Le Nombre des appareils connectés s'affiche");
		} else {
			test.log(Status.FAIL, "Le Nombre des appareils connectés ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"RepeaterConnectedDevicesNumber");
		}
	}
	//Click on Connected Devices
	public static void connectedRepeaterDevicesClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		RepeaterPage.clickOnConnectedRepeaterDevices();
		test.log(Status.INFO, "Clique sur 'Appareils connectés'");
	}
	//Connected Devices Page Title
	public static void connectedDevicesPageTitleTextCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ServerPage.getConnectedDevicesPageTitleText().equals(ServerPage.ConnectedDeltaDevicesElementName)) && (ServerPage.isConnectedDevicesPageTitleDiplayed() == true)) {
			test.log(Status.PASS, "Le Titre de la page 'Appareils connectés' s'affiche");
		} else {
			test.log(Status.FAIL, "Le Titre de la page 'Appareils connectés' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ConnectedDevicesPageTitleText");

		}
	}
	//Device name Check
	public static void deviceNameCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ServerPage.isDeviceNameDiplayed() == true) {
			test.log(Status.PASS, "Le nom de l'appareil connecté s'affiche");
		} else {
			test.log(Status.FAIL, "Le nom de l'appareil connecté ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DeviceName");
		}
	}
	//Device Vendor Check
	public static void deviceVendorCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ServerPage.isDeviceVendorDiplayed() == true) {
			test.log(Status.PASS, "Le nom du vendeur de l'appareil connecté s'affiche");
		} else {
			test.log(Status.FAIL, "Le nom du vendeur de l'appareil connecté ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DeviceVendor");
		}
	}
	//Device Image Check
	public static void deviceImgCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ServerPage.isDeviceImgDiplayed() == true) {
			test.log(Status.PASS, "L'image de l'appareil connecté s'affiche");
		} else {
			test.log(Status.FAIL, "L'image de l'appareil connecté ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DeviceImg");
		}
	}
	//Device Connexion Type Check
	public static void DeviceConnectionTypeCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ServerPage.isDeviceConnectionTypeDiplayed() == true) {
			test.log(Status.PASS, "Le type de connection de l'appareil s'affiche");
		} else {
			test.log(Status.FAIL, "Le type de connection de l'appareil ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DeviceConnectionType");
		}
	}
	//No connected device Description
	public static void noDeviceDescTextCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RepeaterPage.getNoDeviceDescText().equals(RepeaterPage.NoDeviceDescription)) && (RepeaterPage.isNoDeviceDescDiplayed() == true)) {
			test.log(Status.PASS, "La page d'aucun appareils connectés s'affiche");
		} else {
			test.log(Status.FAIL, "La page d'aucun appareils connectés ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"NoDeviceDescText");
		}
	}
	public static void lightIndicatorTextCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RepeaterPage.getLightIndicatorText().equals(RepeaterPage.IndicatorLightText)) && (RepeaterPage.isLightIndicatorDiplayed() == true)) {
			test.log(Status.PASS, "L'élément 'Voyant Lumineux' s'affiche");
		} else {
			test.log(Status.FAIL, "L'élément 'Voyant Lumineux' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"LightIndicatorText");
		}
	}
	//Indicator light Switch
	public static void lightIndicatorSwitchCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (RepeaterPage.isLightIndicatorSwitchDiplayed() == true) {
			test.log(Status.PASS, "Le Switch du voyant lumineux s'affiche");
		} else {
			test.log(Status.FAIL, "Le Switch du voyant lumineux ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"LightIndicatorSwitch");
		}
	}
	public static void lightIndicatorSwitchONCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		test.log(Status.INFO, "Le Switch est a ON");
		RepeaterPage.clickOnLightIndicatorSwitch();
		test.log(Status.INFO, "Changer l'état du switch");
		if (RepeaterPage.getLightIndicatorSwitchChecked().equals("false")) {
			test.log(Status.PASS, "Le Switch du voyant lumineux Passe de ON a OFF");
		}else {
			test.log(Status.FAIL, "Le Switch du voyant lumineux ne passe pas a OFF");
			ExtentReportsFree.takeScreenshot( driver,"LightIndicatorSwitchON");

		}
	}
	public static void lightIndicatorSwitchOFFCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		test.log(Status.INFO, "Le Switch est a OFF");
		RepeaterPage.clickOnLightIndicatorSwitch();
		test.log(Status.INFO, "Changer l'état du switch");
		if (RepeaterPage.getLightIndicatorSwitchChecked().equals("true")) {
			test.log(Status.PASS, "Le Switch du voyant lumineux Passe de OFF a ON");
		}else {
			test.log(Status.FAIL, "Le Switch du voyant lumineux ne passe pas a ON");
			ExtentReportsFree.takeScreenshot( driver,"LightIndicatorSwitchOFF");
		}
	}
	//Reboot button check
	public static void rebootRepeaterButtonTextCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RepeaterPage.getRebootRepeaterButtonText().equals(RepeaterPage.RebootRepeaterButton)) && (RepeaterPage.isRebootRepeaterButtonDiplayed() == true)) {
			test.log(Status.PASS, "le bouton 'Redémarrer le répéteur' s'affiche ");
		} else {
			test.log(Status.FAIL, "le bouton 'Redémarrer le répéteur ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"RebootRepeaterButtonText");
		}
	}
	//Click on reboot repeater button
	public static void rebootRepeaterButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		RepeaterPage.clickOnRebootRepeaterButton();
		test.log(Status.INFO, "Clique sur le bouton 'Redémarrer le répéteur'");
	}
	//Reboot Repeater Alert Title check
	public static void rebootRepeaterAlertTitleTextCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RepeaterPage.getRebootRepeaterAlertTitleText().equals(RepeaterPage.RebootRepeaterAlertTitle)) && (RepeaterPage.isRebootRepeaterAlertTitleDiplayed() == true)) {
			test.log(Status.PASS, "le titre de l'alerte s'affiche ");
		} else {
			test.log(Status.FAIL, "le titre de l'alerte ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"RebootRepeaterAlertTitleText");
		}
	}
	//Reboot Repeater Alert Message check
	public static void rebootRepeaterAlertMessageTextCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RepeaterPage.getRebootRepeaterAlertMessageText().equals(RepeaterPage.RebootRepeaterAlertMessage)) && (RepeaterPage.isRebootRepeaterAlertMessageDiplayed() == true)) {
			test.log(Status.PASS, "le message de l'alerte s'affiche ");
		} else {
			test.log(Status.FAIL, "le message de l'alerte ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"RebootRepeaterAlertMessageText");
		}
	}
	//Cancel Reboot Repeater 
	public static void cancelRebootButtonTextCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RepeaterPage.getCancelRebootButtonText().equals(RepeaterPage.CancelReboot)) && (RepeaterPage.isCancelRebootButtonDiplayed() == true)) {
			test.log(Status.PASS, "le bouton 'Annuler' de l'alerte s'affiche ");
		} else {
			test.log(Status.FAIL, "le bouton 'Annuler' de l'alerte ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"CancelRebootButtonText");
		}
	}
	//Cancel Reboot
	public static void cancelRebootButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		RepeaterPage.clickOnCancelRebootButton();
		test.log(Status.INFO, "Annuler le redémarrage'");
	}
	//Delete Repeater button check
	public static void deleteRepeaterButtonTextCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RepeaterPage.getDeleteRepeaterButtonText().equals(RepeaterPage.DeleteRepeaterButton)) && (RepeaterPage.isDeleteRepeaterButtonDiplayed() == true)) {
			test.log(Status.PASS, "le bouton 'Supprimer de mes équipements' s'affiche ");
		} else {
			test.log(Status.FAIL, "le bouton 'Supprimer de mes équipements' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DeleteRepeaterButtonText");
		}
	}
	//Click on delete repeater button
	public static void deleteRepeaterButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		RepeaterPage.clickOnDeleteRepeaterButton();
		test.log(Status.INFO, "Clique sur le bouton 'Supprimer de mes équipements'");
	}
	//Move your Repeater button check
	public static void repeaterWarningButtonTextCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RepeaterPage.getrepeaterWarningButtonText().equals(RepeaterPage.repeaterWarningButton)) && (RepeaterPage.isrepeaterWarningButtonDiplayed() == true)) {
			test.log(Status.PASS, "le bouton 'Éloignez votre répéteur' s'affiche ");
		} else {
			test.log(Status.FAIL, "le bouton 'Éloignez votre répéteur' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"repeaterWarningButtonText");
		}
	}
	//Click on move your repeater button
	public static void repeaterWarningButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		RepeaterPage.clickOnrepeaterWarningButton();
		test.log(Status.INFO, "Clique sur 'Éloignez votre répéteur'");
	}
	//Move your Repeater title page check
	public static void moveRepeaterPageTitleTextCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RepeaterPage.getMoveRepeaterPageTitleText().equals(RepeaterPage.MoveRepeaterPageTitle)) && (RepeaterPage.isMoveRepeaterPageTitleDiplayed() == true)) {
			test.log(Status.PASS, "La page 'Éloignez ce répéteur' s'affiche ");
		} else {
			test.log(Status.FAIL, "La page 'Éloignez ce répéteur' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"MoveRepeaterPageTitleText");
		}
	}
	//Move repeater Image check Check
	public static void repeaterWarningImageCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (RepeaterPage.isRepeaterWarningImageDiplayed() == true) {
			test.log(Status.PASS, "L'image de la page 'Éloignez ce répéteur' s'affiche");
		} else {
			test.log(Status.FAIL, "L'image de la page 'Éloignez ce répéteur' ne s'affiche pas ");
		}
	}
	//Warning title check
	public static void repeaterWarningTitleTextCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RepeaterPage.getRepeaterWarningTitleText().equals(RepeaterPage.RepeaterWarningTitle)) && (RepeaterPage.isRepeaterWarningTitleDiplayed() == true)) {
			test.log(Status.PASS, "Le Warning s'affiche ");
		} else {
			test.log(Status.FAIL, "Le Warning ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DeleteRepeaterButtonText");
		}	
	}
	//Close Warning button check
	public static void repeaterWarningCloseButtonTextCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RepeaterPage.getRepeaterWarningCloseButtonText().equals(RepeaterPage.RepeaterWarningCloseButton)) && (RepeaterPage.isRepeaterWarningCloseButtonDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'Fermer' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'Fermer' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DeleteRepeaterButtonText");
		}	
	}
	//Close Warning page
	public static void deleteRepeaterButtonTextClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		RepeaterPage.clickOnRepeaterWarningCloseButton();
		test.log(Status.INFO, "Fermer la page du warning");
	}
	//Others Information Element Check 
	public static void repeaterOthersInformationTextCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RepeaterPage.getRepeaterOthersInformationText().equals(RepeaterPage.RepeaterOthersInformation)) && (RepeaterPage.isRepeaterOthersInformationDiplayed() == true)) {
			test.log(Status.PASS, "L'élément Autres Informations s'affiche ");
		} else {
			test.log(Status.FAIL, "L'élément Autres Informations ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"RepeaterOthersInformationText");
		}
	}
	//Click on Others Informations
	public static void repeaterOthersInformationClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		RepeaterPage.clickOnRepeaterOthersInformation();
		test.log(Status.INFO, "Clique autres informations de mon répéteur");
	}

	//Software version Check 
	public static void repeaterLogicielVersionTextCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RepeaterPage.getRepeaterLogicielVersionText().equals(RepeaterPage.RepeaterLogicielVersion)) && (RepeaterPage.isRepeaterLogicielVersionDiplayed() == true)) {
			test.log(Status.PASS, "La version logicielle s'affiche ");
		} else {
			test.log(Status.FAIL, "La version logicielle ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"RepeaterLogicielVersionText");
		}
	}
	//Serial number Check 
	public static void repeaterSerialNumberTextCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RepeaterPage.getRepeaterSerialNumberText().equals(RepeaterPage.RepeaterSerialNumber)) && (RepeaterPage.isRepeaterSerialNumberDiplayed() == true)) {
			test.log(Status.PASS, "Le numéro de série du répéteur s'affiche ");
		} else {
			test.log(Status.FAIL, "Le numéro de série du répéteur ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"RepeaterSerialNumberText");
		}
	}
	//MAC adress Check 
	public static void repeaterMACAdressTextCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RepeaterPage.getRepeaterMACAdressText().equals(RepeaterPage.RepeaterMACAdress)) && (RepeaterPage.isRepeaterMACAdressDiplayed() == true)) {
			test.log(Status.PASS, "L'adresse MAC du répéteur s'affiche ");
		} else {
			test.log(Status.FAIL, "L'adresse MAC du répéteur ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"RepeaterMACAdressText");
		}
	}
	//IPV4 adress Check 
	public static void repeaterIPV4AdressTextCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RepeaterPage.getRepeaterIPV4AdressText().equals(RepeaterPage.RepeaterIPV4Adress)) && (RepeaterPage.isRepeaterIPV4AdressDiplayed() == true)) {
			test.log(Status.PASS, "L'adresse IPV4 du répéteur s'affiche ");
		} else {
			test.log(Status.FAIL, "L'adresse IPV4 du répéteur ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"RepeaterIPV4AdressText");
		}
	}
	//IPV6 adress Check 
	public static void repeaterIPV6AdressTextCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RepeaterPage.getRepeaterIPV6AdressText().equals(RepeaterPage.RepeaterIPV6Adress)) && (RepeaterPage.isRepeaterIPV6AdressDiplayed() == true)) {
			test.log(Status.PASS, "L'adresse IPV6 du répéteur s'affiche ");
		} else {
			test.log(Status.FAIL, "L'adresse IPV6 du répéteur ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DeleteRepeaterButtonText");
		}
	}
	//WiFi 2.4GHz Check 
	public static void repeaterWiFi2GHzTextCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RepeaterPage.getRepeaterWiFi2GHzText().equals(RepeaterPage.RepeaterWiFi2GHz)) && (RepeaterPage.isRepeaterWiFi2GHzDiplayed() == true)) {
			test.log(Status.PASS, "Le WiFi BSSID 2.4GHZ s'affiche ");
		} else {
			test.log(Status.FAIL, "Le WiFi BSSID 2.4GHZ ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"RepeaterWiFi2GHzText");
		}
	}
	//WiFi 5GHz Check 
	public static void repeaterWiFi5GHzTextCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RepeaterPage.getRepeaterWiFi5GHzText().equals(RepeaterPage.RepeaterWiFi5GHz)) && (RepeaterPage.isRepeaterWiFi5GHzDiplayed() == true)) {
			test.log(Status.PASS, "Le WiFi BSSID 5GHZ s'affiche ");
		} else {
			test.log(Status.FAIL, "Le WiFi BSSID 5GHZ ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"RepeaterWiFi5GHzText");
		}
	}
	//Turn on from check 
	public static void repeaterTurnOnTextCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RepeaterPage.getRepeaterTurnOnText().equals(RepeaterPage.RepeaterTurnOn)) && (RepeaterPage.isRepeaterTurnOnDiplayed() == true)) {
			test.log(Status.PASS, "Allumé depuis s'affiche ");
		} else {
			test.log(Status.FAIL, "Allumé depuis ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DeleteRepeaterButtonText");
		}
	}
	//Room element check 
	public static void roomrepeaterElementTextCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RepeaterPage.getRoomrepeaterElementText().equals(RepeaterPage.RoomrepeaterElement)) && (RepeaterPage.isRoomrepeaterElementDiplayed() == true)) {
			test.log(Status.PASS, "L'élément Pièce s'affiche ");
		} else {
			test.log(Status.FAIL, "L'élément Pièce ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"RoomrepeaterElementText");
		}
	}
	//Room Name check 
	public static void repeaterRoomNameTextCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RepeaterPage.getRepeaterRoomNameText().equals(RepeaterPage.RepeaterRoomName)) && (RepeaterPage.isRepeaterRoomNameDiplayed() == true)) {
			test.log(Status.PASS, "Le nom de Pièce s'affiche ");
		} else {
			test.log(Status.FAIL, "Le nom de Pièce ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"RepeaterRoomNameText");
		}
	}

	//Click on Room Element
	public static void roomrepeaterElementClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		RepeaterPage.clickOnRoomrepeaterElement();
		test.log(Status.INFO, "Clique sur Pièce");
	}
	//Select Chambre
	public static void newRepeaterNameClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		RepeaterPage.clickOnNewRepeaterName();
		test.log(Status.INFO, "Séléctionner 'Chambre'");
	}
	//Select Chambre
	public static void oldRepeaterNameClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		RepeaterPage.clickOnOldRepeaterName();
		test.log(Status.INFO, "Séléctionner 'Bureau'");
	}
	//Advice button check 
	public static void repeaterWarningHelpTextCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RepeaterPage.getRepeaterWarningHelpText().equals(RepeaterPage.RepeaterWarningHelp)) && (RepeaterPage.isRepeaterWarningHelpDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'Conseils?' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'Conseils?' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"RepeaterWarningHelpText");
		}
	}
	//Click on Advice button
	public static void repeaterWarningHelpClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		RepeaterPage.clickOnRepeaterWarningHelp();
		test.log(Status.INFO, "Clique sur le bouton 'Conseils'");
	}
	//Onboarding first page check 
	public static void onboardingFirstPageCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RepeaterPage.getImproveMyConnectionTitlePageText().equals(RepeaterPage.ImproveMyConnectionTitlePage)) && (RepeaterPage.isImproveMyConnectionTitlePageDiplayed() == true)) {
			test.log(Status.PASS, "Le nom de la première page 'Améliorer ma connexion' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le nom de la première page 'Améliorer ma connexion' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ImproveMyConnectionTitlePageText");
		}

		if ((RepeaterPage.getSetupRepeaterAdviceHeaderText().equals(RepeaterPage.SetupRepeaterAdviceHeaderFirstPage)) && (RepeaterPage.isSetupRepeaterAdviceHeaderDiplayed() == true)) {
			test.log(Status.PASS, "Le Header de la première page 'Améliorer ma connexion' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le Header de la page 'Améliorer ma connexion' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"RepeaterAdviceHeaderText");
		}

		if ((RepeaterPage.getSetupRepeaterAdviceDescriptionText().equals(RepeaterPage.SetupRepeaterAdviceDescriptionFirstPage)) && (RepeaterPage.isSetupRepeaterAdviceDescriptionDiplayed() == true)) {
			test.log(Status.PASS, "La description de la première page 'Améliorer ma connexion' s'affiche ");
		} else {
			test.log(Status.FAIL, "La description de la page 'Améliorer ma connexion' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"RepeaterAdviceDescriptionText");
		}

		if ((RepeaterPage.getNextAdviceButtonText().equals(RepeaterPage.NextAdviceButton)) && (RepeaterPage.isNextAdviceButtonDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'Conseil suivant' de la première page 'Améliorer ma connexion' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'Conseil suivant' de la première page 'Améliorer ma connexion' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"NextAdviceButtonText");
		}
	}
	//Click Next Advice button
	public static void nextAdviceButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		RepeaterPage.clickOnNextAdviceButton();
		test.log(Status.INFO, "Clique sur le bouton 'Conseil suivant'");
	}
	//Onboarding Second page check 
	public static void onboardingSecondPageCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RepeaterPage.getImproveMyConnectionTitlePageText().equals(RepeaterPage.ImproveMyConnectionTitlePage)) && (RepeaterPage.isImproveMyConnectionTitlePageDiplayed() == true)) {
			test.log(Status.PASS, "Le nom de la deuxième page 'Améliorer ma connexion' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le nom de la deuxième page 'Améliorer ma connexion' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"Page2ImproveMyConnectionTitlePageText");
		}
		if ((RepeaterPage.getSetupRepeaterAdviceHeaderText().equals(RepeaterPage.SetupRepeaterAdviceHeaderSecondPage)) && (RepeaterPage.isSetupRepeaterAdviceHeaderDiplayed() == true)) {
			test.log(Status.PASS, "Le Header de la deuxième page 'Améliorer ma connexion' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le Header de la deuxième page 'Améliorer ma connexion' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"Page2SetupRepeaterAdviceHeaderText");
		}

		if ((RepeaterPage.getSetupRepeaterAdviceDescriptionText().equals(RepeaterPage.SetupRepeaterAdviceDescriptionSecondPage)) && (RepeaterPage.isSetupRepeaterAdviceDescriptionDiplayed() == true)) {
			test.log(Status.PASS, "La description de la deuxième page 'Améliorer ma connexion' s'affiche ");
		} else {
			test.log(Status.FAIL, "La description de la deuxième page 'Améliorer ma connexion' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"Page2SetupRepeaterAdviceDescriptionText");
		}
		if ((RepeaterPage.getNextAdviceButtonText().equals(RepeaterPage.NextAdviceButton)) && (RepeaterPage.isNextAdviceButtonDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'Conseil suivant' de la deuxième page 'Améliorer ma connexion' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'Conseil suivant' de la deuxième page 'Améliorer ma connexion' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"Page2NextAdviceButtonText");
		}
	}
	//Onboarding Third page check 
	public static void onboardingThirdPageCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RepeaterPage.getImproveMyConnectionTitlePageText().equals(RepeaterPage.ImproveMyConnectionTitlePage)) && (RepeaterPage.isImproveMyConnectionTitlePageDiplayed() == true)) {
			test.log(Status.PASS, "Le nom de la troixième page 'Améliorer ma connexion' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le nom de la troixième page 'Améliorer ma connexion' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"Page3ImproveMyConnectionTitlePageText");
		}

		if ((RepeaterPage.getSetupRepeaterAdviceHeaderText().equals(RepeaterPage.SetupRepeaterAdviceHeaderThirdPage)) && (RepeaterPage.isSetupRepeaterAdviceHeaderDiplayed() == true)) {
			test.log(Status.PASS, "Le Header de la troixième page 'Améliorer ma connexion' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le Header de la troixième page 'Améliorer ma connexion' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"Page3SetupRepeaterAdviceHeaderText");
		}

		if ((RepeaterPage.getSetupRepeaterAdviceDescriptionText().equals(RepeaterPage.SetupRepeaterAdviceDescriptionThirdPage)) && (RepeaterPage.isSetupRepeaterAdviceDescriptionDiplayed() == true)) {
			test.log(Status.PASS, "La description de la troixième page 'Améliorer ma connexion' s'affiche ");
		} else {
			test.log(Status.FAIL, "La description de la troixième page 'Améliorer ma connexion' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"Page3SetupRepeaterAdviceDescriptionText");
		}

		if ((RepeaterPage.getNextAdviceButtonText().equals(RepeaterPage.NextAdviceButton)) && (RepeaterPage.isNextAdviceButtonDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'Conseil suivant' de la troixième page 'Améliorer ma connexion' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'Conseil suivant' de la troixième page 'Améliorer ma connexion' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"Page3NextAdviceButtonText");
		}
	}
	//Onboarding Fourth page check 
	public static void onboardingFourthPageCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RepeaterPage.getImproveMyConnectionTitlePageText().equals(RepeaterPage.ImproveMyConnectionTitlePage)) && (RepeaterPage.isImproveMyConnectionTitlePageDiplayed() == true)) {
			test.log(Status.PASS, "Le nom de la quatrième page 'Améliorer ma connexion' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le nom de la quatrième page 'Améliorer ma connexion' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"Page4ImproveMyConnectionTitlePageText");
		}

		if ((RepeaterPage.getSetupRepeaterAdviceHeaderText().equals(RepeaterPage.SetupRepeaterAdviceHeaderFourthPage)) && (RepeaterPage.isSetupRepeaterAdviceHeaderDiplayed() == true)) {
			test.log(Status.PASS, "Le Header de la quatrième page 'Améliorer ma connexion' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le Header de la quatrième page 'Améliorer ma connexion' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"Page4SetupRepeaterAdviceHeaderText");
		}

		if ((RepeaterPage.getSetupRepeaterAdviceDescriptionText().equals(RepeaterPage.SetupRepeaterAdviceDescriptionFourthPage)) && (RepeaterPage.isSetupRepeaterAdviceDescriptionDiplayed() == true)) {
			test.log(Status.PASS, "La description de la quatrième page 'Améliorer ma connexion' s'affiche ");
		} else {
			test.log(Status.FAIL, "La description de la quatrième page 'Améliorer ma connexion' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"Page4SetupRepeaterAdviceDescriptionText");
		}

		if ((RepeaterPage.getNextAdviceButtonText().equals("Terminer")) && (RepeaterPage.isNextAdviceButtonDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'Conseil suivant' de la quatrième page 'Améliorer ma connexion' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'Conseil suivant' de la quatrième page 'Améliorer ma connexion' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"Page4NextAdviceButtonText");
		}
	}
}
