package Strings.ProfilesStrings;



import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import tests.ExtentReport.ExtentReportsFree;
import pages.ProfilesPages.ProfilesPages;


public class ProfilesStrings {

	public static final String TestName1 = "Page Profils vide";
	public static final String TestName2 = "Profil / Aucun appareil connecté / Aucune pause";
	public static final String TestName3 = "Associé un appareil a un profil";
	public static final String TestName4 = "Associé les vacances scolaire a un profil";
	public static final String TestName5 = "Modification d'un Profil";
	public static final String TestName6 = "Pause jusqu'à l'arrêt a partir de la liste des profils";
	public static final String TestName7 = "Pause de 30 Minutes a partir de la liste des profils";
	public static final String TestName8 = "Pause D'une heure a partir de la liste des profils";
	public static final String TestName9 = "Pause De deux heures a partir de la liste des profils";
	public static final String TestName10 = "Pause De deux heures a partir d'un profil";
	public static final String TestName11 = "Pause de 30 Minutes a partir d'un profil";
	public static final String TestName12 = "Pause D'une heure a partir d'un profil";
	public static final String TestName13 = "Pause De deux heures a partir d'un profil";
	public static final String TestName14 = "Créer une pause planifiée";
	public static final String TestName15 = "Désactiver une pause planifiée";
	public static final String TestName16 = "Supprimer une pause planifiée";
	public static final String TestName17 = "Supprimer un profil";

	public static final String FonctionalityName = "Profiles";

	//Profile tab bar button check 
	public static void profilesTabBarButtonCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ProfilesPages.getProfilesTabBarButtonText().equals(ProfilesPages.ProfilesTabBarButton)) && (ProfilesPages.isProfilesTabBarButtonDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'Profils de la Tab Bar s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'Profils de la Tab Bar ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ProfilesTabBarButton");
		}
	}

	//Profile tab bar button click 
	public static void profilesTabBarButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ProfilesPages.clickOnProfilesTabBarButton();
		test.log(Status.INFO, "Clique 'Profils' dans la Tab Bar");
	}	


	//Profile number check 
	public static void profileNumberCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ProfilesPages.getProfileNumberText().equals(ProfilesPages.EmptyProfile)) && (ProfilesPages.isProfileNumberDiplayed() == true)) {
			test.log(Status.PASS, "L'information '0 Profil' s'affiche");
		} else {
			test.log(Status.FAIL, "L'information '0 Profil' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"EmptyProfile");
		}
	}

	//Profile empty list image check 
	public static void profilesEmptyListImageCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ProfilesPages.isProfilesEmptyListImageDiplayed() == true) {
			test.log(Status.PASS, "L'image de la liste des profils vide s'affiche ");
		}
	}

	//Profile empty list description check 
	public static void profilesEmptyListDescCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ProfilesPages.getProfilesEmptyListDescText().equals(ProfilesPages.ProfilesEmptyListDesc)) && (ProfilesPages.isProfilesEmptyListDescDiplayed() == true)) {
			test.log(Status.PASS, "La description de la liste des profils vide s'affiche ");
		} else {
			test.log(Status.FAIL, "La description de la liste des profils vide ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ProfilesEmptyListDesc");
		}
	}

	//Profile empty list button check 
	public static void profilesEmptyListButtonCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ProfilesPages.getProfilesEmptyListButtonText().equals(ProfilesPages.ProfilesEmptyListButton)) && (ProfilesPages.isProfilesEmptyListButtonDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'Créer mon premier profil' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'Créer mon premier profil' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ProfilesEmptyListButton");
		}
	}
	//Profile empty list button click 
	public static void profilesEmptyListButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ProfilesPages.clickOnProfilesEmptyListButton();
		test.log(Status.INFO, "Clique sur le bouton 'Créer mon premier profil'");
	}	

	//Profile empty list button check 
	public static void newProfilePageCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ProfilesPages.getNewProfilePageText().equals(ProfilesPages.NewProfilePage)) && (ProfilesPages.isNewProfilePageDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'Créer mon premier profil' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'Créer mon premier profil' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"NewProfilePage");
		}
	}
	//Profile name description check 
	public static void profileNameDescCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ProfilesPages.getProfileNameDescText().equals(ProfilesPages.ProfileNameDesc)) && (ProfilesPages.isProfileNameDescDiplayed() == true)) {
			test.log(Status.PASS, "La description du champ nom du profil s'affiche ");
		} else {
			test.log(Status.FAIL, "La description du champ nom du profil ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ProfileNameDesc");
		}
	}
	//Profile icon description check 
	public static void profileIconDescCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ProfilesPages.getProfileIconDescText().equals(ProfilesPages.ProfileIconDesc)) && (ProfilesPages.isProfileIconDescDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'Créer mon premier profil' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'Créer mon premier profil' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ProfileIconDesc");
		}
	}
	//Enter first profile name
	public static void enterProfileName(ExtentTest test, AppiumDriver<MobileElement> driver, String ProfileName) {
		ProfilesPages.enterProfileNameField(ProfileName);
		test.log(Status.INFO, "La saisie du nom du nouveau profil ");
	}	

	//Profile icon check 
	public static void profileSecondIconSelectCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ProfilesPages.isProfileSecondIconSelectDiplayed() == true)) {
			test.log(Status.PASS, "La deuxième icône s'affiche ");
		}
	}
	//Profile icon select 
	public static void profileSecondIconSelectSelect(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ProfilesPages.clickOnProfileSecondIconSelectButton();
		test.log(Status.INFO, "Clique sur la deuxième icône");
	}	

	//Profile empty list button check 
	public static void nextButtonNewProfilesCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ProfilesPages.getNextButtonNewProfilesText().equals(ProfilesPages.NextButtonNewProfiles)) && (ProfilesPages.isNextButtonNewProfilesDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'SUIVANT' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'SUIVANT' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"NextButtonNewProfiles");
		}
	}
	//Profile empty list button click 
	public static void nextButtonNewProfilesClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ProfilesPages.clickOnNextButtonNewProfiles();
		test.log(Status.INFO, "Clique sur le bouton 'SUIVANT'");
	}	

	//Associate Devices page check 
	public static void associateDevicesPageCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ProfilesPages.getAssociateDevicesPageText().equals(ProfilesPages.AssociateDevicesPage)) && (ProfilesPages.isAssociateDevicesPageDiplayed() == true)) {
			test.log(Status.PASS, "La page 'Appareil(0)' s'affiche ");
		} else {
			test.log(Status.FAIL, "La page 'Appareil(0)' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"AssociateDevicesPage");
		}
	}

	//Profile number check 
	public static void oneProfileNumberCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ProfilesPages.getProfileNumberText().equals(ProfilesPages.OneProfile)) && (ProfilesPages.isProfileNumberDiplayed() == true)) {
			test.log(Status.PASS, "L'information '1 Profil' s'affiche");
		} else {
			test.log(Status.FAIL, "L'information '1 Profil' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"OneProfile");
		}
	}

	//Profile number check 
	public static void profileListItemNameCheck(ExtentTest test, AppiumDriver<MobileElement> driver, String EditedProfile) {
		if ((ProfilesPages.getProfileListItemNameText().equals(EditedProfile)) && (ProfilesPages.isProfileListItemNameDiplayed() == true)) {
			test.log(Status.PASS, "Le profil créé s'affiche");
		} else {
			test.log(Status.FAIL, "Le profil créé ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"FirstProfile");
		}
	}

	//Device associeted to profile number check 
	public static void associetedDeviceNumberCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ProfilesPages.getAssocietedDeviceNumberText().equals(ProfilesPages.AssocietedDeviceNumber)) && (ProfilesPages.isAssocietedDeviceNumberDiplayed() == true)) {
			test.log(Status.PASS, "Le nombre de divice associé est 'O'");
		} else {
			test.log(Status.FAIL, "Le nombre de divice associé n'est pas 'O'");
			ExtentReportsFree.takeScreenshot( driver,"AssocietedDeviceNumber");
		}
	}

	//Profile Status check 
	public static void profilStatusCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ProfilesPages.getProfilStatusText().equals(ProfilesPages.WithoutBreakProfilStatus)) && (ProfilesPages.isProfilStatusDiplayed() == true)) {
			test.log(Status.PASS, "Le status 'Aucune pause à venir' s'affiche");
		} else {
			test.log(Status.FAIL, "Le status 'Aucune pause à venir' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ProfilStatus");
		}
	}

	//Detail Profile button check 
	public static void detailsProfilButtonCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ProfilesPages.getDetailsProfilButtonText().equals(ProfilesPages.DetailsProfilButton)) && (ProfilesPages.isDetailsProfilButtonDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'Détails' dans Profils s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'Détails' dans Profils ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DetailsProfilButton");
		}
	}

	//Detail Profile button click 
	public static void detailsProfilButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ProfilesPages.clickOnDetailsProfilButton();
		test.log(Status.INFO, "Clique sur le bouton 'Détails' dans Profils");
	}	

	//Element associated device check 
	public static void profilesAssociatedDeviceElementCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ProfilesPages.getProfilesAssociatedDeviceElementText().equals(ProfilesPages.ProfilesAssociatedDeviceElement)) && (ProfilesPages.isProfilesAssociatedDeviceElementDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'Détails' dans Profils s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'Détails' dans Profils ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ProfilesAssociatedDeviceElement");
		}
	}

	//Element associated device click 
	public static void profilesAssociatedDeviceElementClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ProfilesPages.clickOnProfilesAssociatedDeviceElement();
		test.log(Status.INFO, "Clique sur le bouton 'Détails' dans Profils");
	}	

	//Number of associated device to profile check 
	public static void profilesAssociatedDeviceNumberCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ProfilesPages.getProfilesAssociatedDeviceNumberText().equals(ProfilesPages.AssocietedDeviceNumber)) && (ProfilesPages.isProfilesAssociatedDeviceNumberDiplayed() == true)) {
			test.log(Status.PASS, "Le Nombre des appareil associés est bien 0");
		} else {
			test.log(Status.FAIL, "Le Nombre des appareil associés n'est pas égale à 0");
			ExtentReportsFree.takeScreenshot( driver,"ProfilesAssociatedDevice");
		}
	}

	//Add associated device check 
	public static void noDeviceButtonCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ProfilesPages.getNoDeviceButtonText().equals(ProfilesPages.NoDeviceButton)) && (ProfilesPages.isNoDeviceButtonDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'Ajouter des appareils' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'Ajouter des appareils' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"NoDeviceButton");
		}
	}

	//Add associated device click 
	public static void noDeviceButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ProfilesPages.clickOnNoDeviceButton();
		test.log(Status.INFO, "Clique sur le bouton 'Ajouter des appareils' dans Profils");
	}

	//Select first device
	public static void firstDeviceProfileSelect(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ProfilesPages.clickOnFirstDeviceProfile();
		test.log(Status.INFO, "Selection du premier Appareil");
	}

	//Validate Associated Device
	public static void validateDeviceAssociated(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ProfilesPages.clickOnValidateDeviceAssociated();
		test.log(Status.INFO, "Cliquer sur le bouton 'VALIDER'");
	}

	//One associated device to profile check 
	public static void profilesAssociatedOneDeviceCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ProfilesPages.getProfilesAssociatedDeviceNumberText().equals(ProfilesPages.OneAssocietedDeviceNumber)) && (ProfilesPages.isProfilesAssociatedDeviceNumberDiplayed() == true)) {
			test.log(Status.PASS, "Le Nombre des appareil associés est bien 1");
		} else {
			test.log(Status.FAIL, "Le Nombre des appareil associés n'est pas égale à 1");
			ExtentReportsFree.takeScreenshot( driver,"ProfilesAssociatedDevice");
		}
	}

	//School holiday element check 
	public static void profileHolidaySchoolElementCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ProfilesPages.getProfileHolidaySchoolElementText().equals(ProfilesPages.ProfileHolidaySchoolElement)) && (ProfilesPages.isProfileHolidaySchoolElementDiplayed() == true)) {
			test.log(Status.PASS, "L'element 'Vacances scolaire' s'affiche ");
		} else {
			test.log(Status.FAIL, "L'element 'Vacances scolaire' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ProfileHolidaySchoolElement");
		}
	}

	//School holiday element click 
	public static void profileHolidaySchoolElementClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ProfilesPages.clickOnProfileHolidaySchoolElement();
		test.log(Status.INFO, "Clique sur l'element 'Vacances scolaire' dans Profils");
	}

	//School holiday Value check
	public static void profileHolidaySchoolValueCheck(ExtentTest test, AppiumDriver<MobileElement> driver, String ProfileHolidaySchoolValue) {
		if ((ProfilesPages.getProfileHolidaySchoolValueText().equals(ProfileHolidaySchoolValue)) && (ProfilesPages.isProfileHolidaySchoolValueDiplayed() == true)) {
			test.log(Status.PASS, "La valeur de la vacance scolaire est : "+ ProfileHolidaySchoolValue);
		} else {
			test.log(Status.FAIL, "La valeur de la vacance scolaire devrait être : "+ ProfileHolidaySchoolValue);
			ExtentReportsFree.takeScreenshot( driver,"ProfileHolidaySchoolElement");
		}
	}

	//Holiday Zone A element check 
	public static void holidayZoneAProfileCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ProfilesPages.getHolidayZoneAProfileText().equals(ProfilesPages.HolidayZoneAProfile)) && (ProfilesPages.isHolidayZoneAProfileDiplayed() == true)) {
			test.log(Status.PASS, "L'element 'Vacance scolaire zone A' s'affiche ");
		} else {
			test.log(Status.FAIL, "L'element 'Vacance scolaire zone A' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"HolidayZoneAProfile");
		}
	}

	//Holiday Zone A element click 
	public static void holidayZoneAProfileClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ProfilesPages.clickOnHolidayZoneAProfile();
		test.log(Status.INFO, "Clique sur une Vacance scolaire de la liste");
	}

	//Holiday Zone B element check 
	public static void holidayZoneBProfileCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ProfilesPages.getHolidayZoneBProfileText().equals(ProfilesPages.HolidayZoneBProfile)) && (ProfilesPages.isHolidayZoneBProfileDiplayed() == true)) {
			test.log(Status.PASS, "L'element 'Vacance scolaire zone B' s'affiche ");
		} else {
			test.log(Status.FAIL, "L'element 'Vacance scolaire zone B' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"HolidayZoneBProfile");
		}
	}

	//Holiday Zone B element click 
	public static void holidayZoneBProfileClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ProfilesPages.clickOnHolidayZoneBProfile();
		test.log(Status.INFO, "Clique sur 'Vacance scolaire zone A");
	}

	//Holiday Zone C element check 
	public static void holidayZoneCProfileCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ProfilesPages.getHolidayZoneCProfileText().equals(ProfilesPages.HolidayZoneCProfile)) && (ProfilesPages.isHolidayZoneCProfileDiplayed() == true)) {
			test.log(Status.PASS, "L'element 'Vacance scolaire zone C' s'affiche ");
		} else {
			test.log(Status.FAIL, "L'element 'Vacance scolaire zone C' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"HolidayZoneCProfile");
		}
	}

	//Holiday Zone C element click 
	public static void holidayZoneCProfileClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ProfilesPages.clickOnHolidayZoneCProfile();
		test.log(Status.INFO, "Clique sur 'Vacance scolaire zone A");
	}

	//Holiday Corse element check 
	public static void holidayCorseProfileCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ProfilesPages.getHolidayCorseProfileText().equals(ProfilesPages.HolidayCorseProfile)) && (ProfilesPages.isHolidayCorseProfileDiplayed() == true)) {
			test.log(Status.PASS, "L'element 'Vacance scolaire zone Corse' s'affiche ");
		} else {
			test.log(Status.FAIL, "L'element 'Vacance scolaire zone Corse' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"HolidayCorseProfile");
		}
	}

	//Holiday Corse element click 
	public static void holidayCorseProfileClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ProfilesPages.clickOnHolidayCorseProfile();
		test.log(Status.INFO, "Clique sur 'Vacance scolaire zone A");
	}
	
	//None Holiday element check 
	public static void holidayAucuneProfileCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ProfilesPages.getHolidayAucuneProfileText().equals("Aucune")) && (ProfilesPages.isHolidayAucuneProfileDiplayed() == true)) {
			test.log(Status.PASS, "L'element 'Aucune' s'affiche ");
		} else {
			test.log(Status.FAIL, "L'element 'Aucune' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"HolidayAucuneProfile");
		}
	}

	//None Holiday element click 
	public static void holidayAucuneProfileClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ProfilesPages.clickOnHolidayAucuneProfile();
		test.log(Status.INFO, "Clique sur 'Aucune");
	}

	//Edit Profile button check 
	public static void editProfileButtonCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ProfilesPages.getEditProfileButtonText().equals(ProfilesPages.EditProfileButton)) && (ProfilesPages.isEditProfileButtonDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'MODIFIER' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'MODIFIER' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"EditProfileButton");
		}
	}

	//Edit Profile button click 
	public static void editProfileButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ProfilesPages.clickOnEditProfileButton();
		test.log(Status.INFO, "Clique sur 'MODIFIER'");
	}


	//Pause from Profile List button check 
	public static void pauseProfileButtonCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ProfilesPages.getPauseProfileButtonText().equals(ProfilesPages.PauseProfileButton)) && (ProfilesPages.isPauseProfileButtonDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'Mettre en pause' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'Mettre en pause' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"PauseProfileButton");
		}
	}

	//Pause from Profile List button click 
	public static void pauseProfileButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver, String StartStopPause) {
		ProfilesPages.clickOnPauseProfileButton();
		test.log(Status.INFO, "Clique sur '"+StartStopPause+"'");
	}

	//Profile Status check 
	public static void profileStatusCheck(ExtentTest test, AppiumDriver<MobileElement> driver, String ProfileStatus) {
		if ((ProfilesPages.getProfileStatusText().equals(ProfileStatus)) && (ProfilesPages.isProfileStatusDiplayed() == true)) {
			test.log(Status.PASS, "Le Status '"+ProfileStatus+"' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le Status '"+ProfileStatus+"' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ProfileStatus");
		}
	}
	//List Profile Pause click 
	public static void selectPause(ExtentTest test, AppiumDriver<MobileElement> driver, String Pause) {

		if (Pause.equals("1")) {
			if ((ProfilesPages.getPause30MinText().equals(ProfilesPages.Pause30Min)) && (ProfilesPages.isPause30MinDiplayed() == true)) {
				test.log(Status.PASS, "L'élément '"+ProfilesPages.Pause30Min+"' s'affiche ");
				ProfilesPages.clickOnPause30Min();
				test.log(Status.INFO, "Clique sur '30 Minutes'");
			} else {
				test.log(Status.FAIL, "L'élément '"+ProfilesPages.Pause30Min+"' ne s'affiche pas");
				ExtentReportsFree.takeScreenshot( driver,"Pause30Min");
			}
		}else if (Pause.equals("2")) {
			if ((ProfilesPages.getPause1HourText().equals(ProfilesPages.Pause1Hour)) && (ProfilesPages.isPause1HourDiplayed() == true)) {
				test.log(Status.PASS, "L'élément '"+ProfilesPages.Pause1Hour+"' s'affiche ");
				ProfilesPages.clickOnPause1Hour();
				test.log(Status.INFO, "Clique sur '1 heure'");
			} else {
				test.log(Status.FAIL, "L'élément '"+ProfilesPages.Pause1Hour+"' ne s'affiche pas");
				ExtentReportsFree.takeScreenshot( driver,"Pause1Hour");
			}
		}else if (Pause.equals("3")) {
			if ((ProfilesPages.getPause2HoursText().equals(ProfilesPages.Pause2Hours)) && (ProfilesPages.isPause2HoursDiplayed() == true)) {
				test.log(Status.PASS, "L'élément '"+ProfilesPages.Pause2Hours+"' s'affiche ");
				ProfilesPages.clickOnPause2Hours();
				test.log(Status.INFO, "Clique sur '2 heure'");
			} else {
				test.log(Status.FAIL, "L'élément '"+ProfilesPages.Pause2Hours+"' ne s'affiche pas");
				ExtentReportsFree.takeScreenshot( driver,"Pause2Hours");
			}
		}else if (Pause.equals("4")) {
			if ((ProfilesPages.getPauseInfiniteText().equals(ProfilesPages.PauseInfinite)) && (ProfilesPages.isPauseInfiniteDiplayed() == true)) {
				test.log(Status.PASS, "L'élément '"+ProfilesPages.PauseInfinite+"' s'affiche ");
				ProfilesPages.clickOnPauseInfinite();
				test.log(Status.INFO, "Clique sur "+ProfilesPages.PauseInfinite+"'");
			} else {
				test.log(Status.FAIL, "L'élément '"+ProfilesPages.PauseInfinite+"' ne s'affiche pas");
				ExtentReportsFree.takeScreenshot( driver,"PauseInfinite");
			}
		}
		//Submit pause button click 
		ProfilesPages.clickOnApplicatePauseButton();
		test.log(Status.INFO, "Clique sur 'Appliquer'");
	}
	
	//Pause from Profile button click 
	public static void setPauseProfileButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver, String StartStopPauseProfile) {
		ProfilesPages.clickOnSetPauseProfileButton();
		test.log(Status.INFO, "Clique sur le bouton '"+StartStopPauseProfile+"'");
	}

	//Profile Status check 
	public static void detailProfileStatusCheck(ExtentTest test, AppiumDriver<MobileElement> driver, String DetailProfileStatus) {
		if ((ProfilesPages.getDetailProfileStatusText().equals(DetailProfileStatus)) && (ProfilesPages.isDetailProfileStatusDiplayed() == true)) {
			test.log(Status.PASS, "Le Status '"+DetailProfileStatus+"' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le Status '"+DetailProfileStatus+"' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DetailProfileStatus");
		}
	}

	//Profile Delete Button check 
	public static void profileDeleteButtonCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ProfilesPages.getProfileDeleteButtonText().equals("Supprimer le profil")) && (ProfilesPages.isProfileDeleteButtonDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'Supprimer le profil' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'Supprimer le profil' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ProfileDeleteButton");
		}
	}

	//Profile Delete Button click 
	public static void profileDeleteButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ProfilesPages.clickOnProfileDeleteButton();
		test.log(Status.INFO, "Clique sur 'Supprimer le profil'");
	}

	//Profile Delete Button check 
	public static void profileConfirmDeleteButtonCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ProfilesPages.getProfileConfirmDeleteButtonText().equals("Supprimer")) && (ProfilesPages.isProfileConfirmDeleteButtonDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'Supprimer' de la pop-up de confirmation s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'Supprimer' de la pop-up de confirmation ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ProfileConfirmDeleteButton");
		}
	}

	//Profile Delete Button click 
	public static void profileConfirmDeleteButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ProfilesPages.clickOnProfileConfirmDeleteButton();
		test.log(Status.INFO, "Clique sur 'Supprimer'");
	}
	
	
	//Scheduled Break Button check 
	public static void scheduledBreakButtonCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ProfilesPages.getScheduledBreakButtonText().equals("Créer une pause planifiée")) && (ProfilesPages.isScheduledBreakButtonDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'Créer une pause planifiée' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'Créer une pause planifiée' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ProfileDeleteButton");
		}
	}

	//Scheduled Break Button click 
	public static void scheduledBreakButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ProfilesPages.clickOnScheduledBreakButton();
		test.log(Status.INFO, "Clique sur 'Créer une pause planifiée'");
	}

	
	//New Break Page Title check 
	public static void newBreakPageTitleCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ProfilesPages.getNewBreakPageTitleText().equals("Nouvelle pause")) && (ProfilesPages.isNewBreakPageTitleDiplayed() == true)) {
			test.log(Status.PASS, "Le nom de la page 'Nouvelle pause' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le nom de la page 'Nouvelle pause' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"NewBreakPageTitle");
		}
	}

	//New Break Page Title check 
	public static void breakNameElementCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ProfilesPages.getBreakNameElementText().equals("Nom de la pause")) && (ProfilesPages.isBreakNameElementDiplayed() == true)) {
			test.log(Status.PASS, "Le titre du champ d'édition de la pause 'Nom de la pause' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le titre du champ d'édition de la pause 'Nom de la pause'  ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"BreakNameElement");
		}
	}
		
	
	//Hour Start Element check 
	public static void hourStartElementCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ProfilesPages.getHourStartElementText().equals("Heure de début")) && (ProfilesPages.isHourStartElementDiplayed() == true)) {
			test.log(Status.PASS, "Le header 'Heure de début' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le header 'Heure de début'  ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"HourStartElement");
		}
	}

	//Hour Start Element check 
	public static void hourEndElementCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ProfilesPages.getHourEndElementText().equals("Heure de fin")) && (ProfilesPages.isHourEndElementDiplayed() == true)) {
			test.log(Status.PASS, "Le header 'Heure de fin' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le header 'Heure de fin'  ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"HourEndElement");
		}
	}

	//Week Days Header check 
	public static void weekDaysElementCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ProfilesPages.getWeekDaysElementText().equals("Jours de la semaine")) && (ProfilesPages.isWeekDaysElementDiplayed() == true)) {
			test.log(Status.PASS, "Le header 'Jours de la semaine' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le header 'Jours de la semaine' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"WeekDaysElement");
		}
	}

	//Profile Pause Edit Summary check 
	public static void profilePauseEditSummaryCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ProfilesPages.getProfilePauseEditSummaryText().equals("La pause commencera chaque jour sélectionné à 20h00 et finira le lendemain à 6h00.")) && (ProfilesPages.isProfilePauseEditSummaryDiplayed() == true)) {
			test.log(Status.PASS, "La description de la pause planifiée s'affiche ");
		} else {
			test.log(Status.FAIL, "La description de la pause planifiée ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ProfilePauseEditSummary");
		}
	}
	
	//Pause Edit Friday check 
	public static void pauseEditFridayCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ProfilesPages.getPauseEditFridayText().equals("V")) && (ProfilesPages.isPauseEditFridayDiplayed() == true)) {
			test.log(Status.PASS, "Le jour de la semaine 'V' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le jour de la semaine 'V' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"PauseEditFriday");
		}
	}

	//Pause Edit Friday click 
	public static void pauseEditFridayClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ProfilesPages.clickOnPauseEditFriday();
		test.log(Status.INFO, "Séléctionnier le vendredi");
	}

	//Pause Edit Friday check 
	public static void pauseEditSaturdayCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ProfilesPages.getPauseEditSaturdayText().equals("S")) && (ProfilesPages.isPauseEditSaturdayDiplayed() == true)) {
			test.log(Status.PASS, "Le jour de la semaine 'S' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le jour de la semaine 'S' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"PauseEditSaturday");
		}
	}

	//Pause Edit Friday click 
	public static void pauseEditSaturdayClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ProfilesPages.clickOnPauseEditSaturday();
		test.log(Status.INFO, "Séléctionnier le samedi");
	}

	//Enter Scheduled Pause Name
	public static void enterScheduledPauseName(ExtentTest test, AppiumDriver<MobileElement> driver, String ScheduledPauseName) {
		ProfilesPages.enterScheduledPauseNameField(ScheduledPauseName);
		test.log(Status.INFO, "Saisir un nom pour la pause planifiée ");
	}	

	//Validate Scheduled Pause button click 
	public static void validateScheduledPauseClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ProfilesPages.clickOnValidateScheduledPause();
		test.log(Status.INFO, "Valider la pause planifiée");
	}

	//Scheduled pause switch click 
	public static void scheduledPauseOFFClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ProfilesPages.clickOnScheduledPauseOFF();
		test.log(Status.INFO, "Désactiver la pause planifiée");
	}

	//Scheduled Pause Item click 
	public static void scheduledPauseItemClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ProfilesPages.clickOnScheduledPauseItem();
		test.log(Status.INFO, "Séléctionnier la pause planifiée");
	}
	
	//Scheduled Pause Delete Button check 
	public static void scheduledPauseDeleteButtonCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ProfilesPages.getScheduledPauseDeleteButtonText().equals("Supprimer la pause")) && (ProfilesPages.isScheduledPauseDeleteButtonDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'Supprimer la pause' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'Supprimer la pause' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ScheduledPauseDeleteButton");
		}
	}

	//Scheduled Pause Delete Button click 
	public static void scheduledPauseDeleteButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ProfilesPages.clickOnScheduledPauseDeleteButton();
		test.log(Status.INFO, "Clique sur le bouton 'Supprimer la pause'");
	}

	
	//Scheduled Pause Item click 
	public static void scheduledPauseDeleteConfirmationButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ProfilesPages.clickOnScheduledPauseDeleteConfirmationButton();
		test.log(Status.INFO, "Confirmation de la suppression");
	}
}
