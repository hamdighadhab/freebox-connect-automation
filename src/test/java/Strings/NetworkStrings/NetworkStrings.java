package Strings.NetworkStrings;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.NetworkPages.NetworkPages;
import tests.ExtentReport.ExtentReportsFree;

public class NetworkStrings {

	public static final String TestName1 = "Sécurité WiFi";
	public static final String TestName2 = "Visibilité du WiFi";
	public static final String TestName3 = "Largeur de bande";
	public static final String TestName4 = "Canaux";
	public static final String TestName5 = "Etat du Wifi";
	public static final String TestName6 = "Portée du WiFi";
	//******************************************************
	public static final String TestName7 = "Bloquer l'accès / Sans appareil";
	public static final String TestName8 = "Bloquer l'accès / Ajouter un appareil";
	public static final String TestName9 = "Bloquer l'accès / Supprimer un appareil";
	public static final String TestName10 = "Autoriser l'accès / Sans appareil";
	public static final String TestName11 = "Autoriser l'accès / Ajouter appareil";
	public static final String TestName12 = "Autoriser l'accès / Supprimer un appareil";
	//******************************************************
	public static final String TestName13 = "Aucune coupure planifiée";
	public static final String TestName14 = "Coupure planifiée : Plage horaire : 00h-00h30";
	public static final String TestName15 = "Réinitialiser les plages horraires";
	public static final String TestName16 = "Coupure planifiée : Plage horaire : 23h30-00h";
	//******************************************************
	public static final String TestName17 = "Afficher ma clé";
	public static final String TestName18 = "Modifier Mon SSID";
	public static final String TestName19 = "Modifier ma clé";
	public static final String TestName20 = "Modifier la sécurité WiFi";
	public static final String TestName21 = "Modifier le type de configuration WiFi";

	//******************************************************
	//******************************************************
	//******************************************************

	public static final String FonctionalityName = "Diagnostic WiFi";
	public static final String FonctionalityName2 = "Restrictions WiFi";
	public static final String FonctionalityName3 = "Coupure planifiée du WiFi";
	public static final String FonctionalityName4 = "Mon Réseau";


	//Network tab-bar button Check 
	public static void networkTabBarButtonsCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getNetworkTabBarButtonText().equals("Réseau")) && (NetworkPages.isNetworkTabBarButtonDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'Réseau' de la TabBar s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'Réseau' de la TabBar ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"NetworkTabBarButton");
		}
	}
	//Click on Network tab-bar
	public static void networkTabBarButtonsClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnNetworkTabBarButton();
		test.log(Status.INFO, "Clique sur le bouton 'Réseau' de la TabBar");
	}

	//Diag WiFi button Check 
	public static void diagWiFiButtonCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getDiagWiFiButtonText().equals("Diagnostic WiFi")) && (NetworkPages.isDiagWiFiButtonDiplayed() == true)) {
			test.log(Status.PASS, "L'élément 'Diagnostic WiFi' s'affiche ");
		} else {
			test.log(Status.FAIL, "L'élément 'Diagnostic WiFi' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DiagWiFiButton");
		}
	}
	//Click on Diag WiFi
	public static void diagWiFiButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnDiagWiFiButton();
		test.log(Status.INFO, "Clique sur le bouton 'Réseau' de la TabBar");
	}

	//Start Diag button Check 
	public static void diagStartButtonCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getDiagStartButtonText().equals("Démarrer le diagnostic")) && (NetworkPages.isDiagStartButtonDiplayed() == true)) {
			test.log(Status.PASS, "L'élément 'Diagnostic WiFi' s'affiche ");
		} else {
			test.log(Status.FAIL, "L'élément 'Diagnostic WiFi' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DiagStartButton");
		}
	}
	//Click on Start Diag button
	public static void diagStartButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnDiagStartButton();
		test.log(Status.INFO, "Clique sur le bouton 'Réseau' de la TabBar");
	}

	// Diag page name Check 
	public static void diagWiFiTitleCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getDiagWiFiTitleText().equals("Diagnostic WiFi")) && (NetworkPages.isDiagWiFiTitleDiplayed() == true)) {
			test.log(Status.PASS, "L'élément 'Diagnostic WiFi' s'affiche ");
		} else {
			test.log(Status.FAIL, "L'élément 'Diagnostic WiFi' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DiagWiFiTitle");
		}
	}

	// Diag page name Check 
	public static void diagWiFiDescCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getDiagWiFiDescText().equals("Tous vos paramètres WiFi sont optimaux !")) && (NetworkPages.isDiagWiFiDescDiplayed() == true)) {
			test.log(Status.PASS, "La description du diag 'Tous vos paramètres WiFi sont optimaux !' s'affiche ");
		} else {
			test.log(Status.FAIL, "La description du diag 'Tous vos paramètres WiFi sont optimaux !' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DiagWiFiDesc");
		}
	}

	// Diag Params Check 
	public static void diagParamVerifCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getDiagParamVerifText().equals("6 paramètres vérifiés")) && (NetworkPages.isDiagParamVerifDiplayed() == true)) {
			test.log(Status.PASS, "L'élément '6 paramètres vérifiés' s'affiche ");
		} else {
			test.log(Status.FAIL, "L'élément '6 paramètres vérifiés' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DiagParamVerif");
		}
	}

	// Diag status Check 
	public static void diagVerifStatusCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getDiagVerifStatusText().equals("Rien à signaler")) && (NetworkPages.isDiagVerifStatusDiplayed() == true)) {
			test.log(Status.PASS, "L'élément 'Rien à signaler' s'affiche ");
		} else {
			test.log(Status.FAIL, "L'élément 'Rien à signaler' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DiagVerifStatus");
		}
	}

	//Click on detail Diag setting button
	public static void detailsSettingsButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnDetailsSettingsButton();
		test.log(Status.INFO, "Clique sur le bouton pour afficher les paramètre du diag");
	}


	// Diag Settings page name Check 
	public static void verifedSettingsPageCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getVerifedSettingsPageText().equals("Paramètres vérifiés")) && (NetworkPages.isVerifedSettingsPageDiplayed() == true)) {
			test.log(Status.PASS, "La page  'Paramètres vérifiés' s'affiche ");
		} else {
			test.log(Status.FAIL, "La page  'Paramètres vérifiés' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"VerifedSettingsPage");
		}
	}

	// Setting Secutity WiFi Check 
	public static void settingSecutityWiFiCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getSettingSecutityWiFiText().equals("Sécurité WiFi")) && (NetworkPages.isSettingSecutityWiFiDiplayed() == true)) {
			test.log(Status.PASS, "L'élément 'Sécurité WiFi' s'affiche ");
		} else {
			test.log(Status.FAIL, "L'élément 'Sécurité WiFi' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DiagVerifStatus");
		}
	}

	//Click on Setting Secutity WiFi
	public static void settingSecutityWiFiButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnSettingSecutityWiFiButton();
		test.log(Status.INFO, "Clique sur 'Sécurité WiFi'");
	}


	// Diag Secutity Detail Check 
	public static void diagSecutityDetailCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getDiagSecutityDetailText().equals("Votre sécurité WiFi est optimale !")) && (NetworkPages.isDiagSecutityDetailDiplayed() == true)) {
			test.log(Status.PASS, "Le Statut du paramètre 'Sécurité WiFi' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le Statut du paramètre 'Sécurité WiFi' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DiagSecutityDetail");
		}
	}

	// Diag Secutity Desc Check 
	public static void diagSecutityDescCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.isDiagSecutityDescDiplayed() == true)) {
			test.log(Status.PASS, "La description du paramètre 'Sécurité WiFi' s'affiche ");
		} else {
			test.log(Status.FAIL, "La description du paramètre 'Sécurité WiFi' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DiagSecutityDesc");
		}
	}

	// OK button Check 
	public static void diagnosticDetailButtonCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getDiagnosticDetailButtonText().equals("OK")) && (NetworkPages.isDiagnosticDetailButtonDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'OK' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'OK' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DiagnosticDetailButton");
		}
	}


	//Click on OK button
	public static void diagnosticDetailButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnDiagnosticDetailButton();
		test.log(Status.INFO, "Clique sur Le bouton 'OK'");
	}

	// Visibility WiFi element Check 
	public static void settingVisibilityWiFiCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getSettingVisibilityWiFiText().equals("Visibilité du WiFi")) && (NetworkPages.isSettingVisibilityWiFiDiplayed() == true)) {
			test.log(Status.PASS, "L'élément 'Visibilité du WiFi' s'affiche ");
		} else {
			test.log(Status.FAIL, "L'élément 'Visibilité du WiFi' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"SettingVisibilityWiFi");
		}
	}


	//Click on Visibility WiFi button
	public static void settingVisibilityWiFiClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnSettingVisibilityWiFiButton();
		test.log(Status.INFO, "Clique sur 'Visibilité du WiFi'");
	}

	// Diag Visibility Detail Check 
	public static void diagVisibilityDetailCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getDiagVisibilityDetailText().equals("Votre réseau personnel est activé")) && (NetworkPages.isDiagVisibilityDetailDiplayed() == true)) {
			test.log(Status.PASS, "Le Statut du paramètre 'Visibility WiFi' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le Statut du paramètre 'Visibility WiFi' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DiagVisibilityDetail");
		}
	}

	// Diag Visibility Desc Check 
	public static void diagVisibilityDescCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.isDiagVisibilityDescDiplayed() == true)) {
			test.log(Status.PASS, "La description du paramètre 'Visibility WiFi' s'affiche ");
		} else {
			test.log(Status.FAIL, "La description du paramètre 'Visibility WiFi' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DiagVisibilityDesc");
		}
	}

	// Bandwidth element Check 
	public static void settingBandwidthWiFiCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getSettingBandwidthWiFiText().equals("Largeur de bande")) && (NetworkPages.isSettingBandwidthWiFiDiplayed() == true)) {
			test.log(Status.PASS, "L'élément 'Largeur de bande' s'affiche ");
		} else {
			test.log(Status.FAIL, "L'élément 'Largeur de bande' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"SettingBandwidthWiFi");
		}
	}

	//Click on Bandwidth element
	public static void settingBandwidthWiFiClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnSettingBandwidthWiFiButton();
		test.log(Status.INFO, "Clique sur 'Largeur de bande'");
	}

	// Diag Bandwidth Detail Check 
	public static void diagBandwidthDetailCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getDiagBandwidthDetailText().equals("Votre largeur de bande est optimale !")) && (NetworkPages.isDiagBandwidthDetailDiplayed() == true)) {
			test.log(Status.PASS, "Le Statut du paramètre 'Largeur de bande' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le Statut du paramètre 'Largeur de bande' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DiagBandwidthDetail");
		}
	}

	// Diag Bandwidth Desc Check 
	public static void diagBandwidthDescCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.isDiagBandwidthDescDiplayed() == true)) {
			test.log(Status.PASS, "La description du paramètre 'Largeur de bande' s'affiche ");
		} else {
			test.log(Status.FAIL, "La description du paramètre 'Largeur de bande' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DiagBandwidthDesc");
		}
	}

	// Canals element Check 
	public static void settingCanalsWiFiCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getSettingCanalsWiFiText().equals("Canaux")) && (NetworkPages.isSettingCanalsWiFiDiplayed() == true)) {
			test.log(Status.PASS, "L'élément 'Canaux' s'affiche ");
		} else {
			test.log(Status.FAIL, "L'élément 'Canaux' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"SettingCanalsWiFi");
		}
	}

	//Click on Canals element
	public static void settingCanalsWiFiClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnSettingCanalsWiFiButton();
		test.log(Status.INFO, "Clique sur 'Largeur de bande'");
	}
	// Diag Canals Detail Check 
	public static void diagCanalsDetailCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getDiagCanalsDetailText().equals("Vos canaux sont actuellement choisis automatiquement")) && (NetworkPages.isDiagCanalsDetailDiplayed() == true)) {
			test.log(Status.PASS, "Le Statut du paramètre 'Canaux' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le Statut du paramètre 'Canaux' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DiagCanalsDetail");
		}
	}

	// Diag Canals Desc Check 
	public static void diagCanalsDescCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.isDiagCanalsDescDiplayed() == true)) {
			test.log(Status.PASS, "La description du paramètre 'Canaux' s'affiche ");
		} else {
			test.log(Status.FAIL, "La description du paramètre 'Canaux' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DiagCanalsDesc");
		}
	}

	// WiFiStatus element Check 
	public static void settingStatusWiFiCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getSettingStatusWiFiText().equals("État du WiFi")) && (NetworkPages.isSettingStatusWiFiDiplayed() == true)) {
			test.log(Status.PASS, "L'élément 'État du WiFi' s'affiche ");
		} else {
			test.log(Status.FAIL, "L'élément 'État du WiFi' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"SettingStatusWiFi");
		}
	}

	//Click on WiFiStatus element
	public static void settingStatusWiFiClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnSettingStatusWiFiButton();
		test.log(Status.INFO, "Clique sur 'État du WiFi'");
	}
	// Diag WiFiStatus Detail Check 
	public static void diagStatusDetailCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getDiagStatusDetailText().equals("Votre WiFi est opérationnel !")) && (NetworkPages.isDiagStatusDetailDiplayed() == true)) {
			test.log(Status.PASS, "Le Statut du paramètre 'État du WiFi' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le Statut du paramètre 'État du WiFi' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DiagStatusDetail");
		}
	}

	// Diag WiFiStatus Desc Check 
	public static void diagStatusDescCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.isDiagStatusDescDiplayed() == true)) {
			test.log(Status.PASS, "La description du paramètre 'État du WiFi' s'affiche ");
		} else {
			test.log(Status.FAIL, "La description du paramètre 'État du WiFi' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DiagStatusDesc");
		}
	}

	// WiFiRange element Check 
	public static void settingRangeWiFiCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getSettingRangeWiFiText().equals("Portée du WiFi")) && (NetworkPages.isSettingRangeWiFiDiplayed() == true)) {
			test.log(Status.PASS, "L'élément 'Portée du WiFi' s'affiche ");
		} else {
			test.log(Status.FAIL, "L'élément 'Portée du WiFi' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"SettingRangeWiFi");
		}
	}

	//Click on WiFiRange element
	public static void settingRangeWiFiClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnSettingRangeWiFiButton();
		test.log(Status.INFO, "Clique sur 'Portée du WiFi'");
	}
	
	// Diag WiFiRange Detail Check 
	public static void diagRangeDetailCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getDiagRangeDetailText().equals("Aucun appareil n'est en limite de portée de votre WiFi")) && (NetworkPages.isDiagRangeDetailDiplayed() == true)) {
			test.log(Status.PASS, "Le Statut du paramètre 'Portée du WiFi' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le Statut du paramètre 'Portée du WiFi' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DiagRangeDetail");
		}
	}

	// Diag WiFiRange Desc Check 
	public static void diagRangeDescCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.isDiagRangeDescDiplayed() == true)) {
			test.log(Status.PASS, "La description du paramètre 'Portée du WiFi' s'affiche ");
		} else {
			test.log(Status.FAIL, "La description du paramètre 'Portée du WiFi' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DiagRangeDesc");
		}
	}

	// Restriction WiFi boutton Check 
	public static void restrictionsWiFiButtonCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getRestrictionsWiFiButtonText().equals("Restrictions WiFi")) && (NetworkPages.isRestrictionsWiFiButtonDiplayed() == true)) {
			test.log(Status.PASS, "L'élément 'Restrictions WiFi' s'affiche ");
		} else {
			test.log(Status.FAIL, "L'élément 'Restrictions WiFi' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"RestrictionsWiFiButton");
		}
	}

	//Click on Restriction WiFi button
	public static void RestrictionsWiFiButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnRestrictionsWiFiButton();
		test.log(Status.INFO, "Clique sur 'Portée du WiFi'");
	}

	// Restriction WiFi page name Check 
	public static void restrictionsWiFiPageNameCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getRestrictionsWiFiPageNameText().equals("Restrictions WiFi")) && (NetworkPages.isRestrictionsWiFiPageNameDiplayed() == true)) {
			test.log(Status.PASS, "La page 'Restrictions WiFi' s'affiche ");
		} else {
			test.log(Status.FAIL, "La page 'Restrictions WiFi' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"RestrictionsWiFiPageName");
		}
	}

	// Restriction WiFi description Check 
	public static void restrictionsWiFiDescCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getRestrictionsWiFiDescText().equals("Les restrictions WiFi vous permettent de choisir les appareils qui peuvent se connecter à votre WiFi.")) && (NetworkPages.isRestrictionsWiFiDescDiplayed() == true)) {
			test.log(Status.PASS, "La description de la page 'Restrictions WiFi' s'affiche ");
		} else {
			test.log(Status.FAIL, "La description de la page 'Restrictions WiFi' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"RestrictionsWiFiDesc");
		}
	}

	// Restriction Type Check 
	public static void restrictionsTypeElementCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getRestrictionsTypeElementText().equals("Type de restrictions")) && (NetworkPages.isRestrictionsTypeElementDiplayed() == true)) {
			test.log(Status.PASS, "L'élément 'Type de restrictions' s'affiche ");
		} else {
			test.log(Status.FAIL, "L'élément 'Type de restrictions' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"RestrictionsTypeElement");
		}
	}

	//Restriction Type Value
	public static void BlockAcessTypeValueCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getRestrictionsTypeValueText().equals("Bloquer l'accès")) && (NetworkPages.isRestrictionsTypeValueDiplayed() == true)) {
			test.log(Status.PASS, "La valeur 'Bloquer l'accès' de l'élément 'Type de restrictions' s'affiche ");
		} else {
			test.log(Status.FAIL, "La valeur 'Bloquer l'accès' de l'élément 'Type de restrictions' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"RestrictionsTypeValue");
		}
	}
	
	//Restriction Type Value
	public static void AllowAcessTypeValueCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getRestrictionsTypeValueText().equals("Autoriser l'accès")) && (NetworkPages.isRestrictionsTypeValueDiplayed() == true)) {
			test.log(Status.PASS, "La valeur 'Bloquer l'accès' de l'élément 'Type de restrictions' s'affiche ");
		} else {
			test.log(Status.FAIL, "La valeur 'Bloquer l'accès' de l'élément 'Type de restrictions' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"RestrictionsTypeValue");
		}
	}


	//Click on Restriction Type Value
	public static void restrictionsTypeValueClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnRestrictionsTypeValue();
		test.log(Status.INFO, "Clique sur le type de la restriction");
	}
	
	//Select "Bloquer l'accès" Restriction Type Value
	public static void accessBlockElementClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnAccessBlockElement();
		test.log(Status.INFO, "Sélectionner 'Bloquer l'accès'");
	}

	//Switch WiFi Restrection Click
	public static void restrictionsWiFiSwitchClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnRestrictionsWiFiSwitch();
		test.log(Status.INFO, "Activer la restriction WiFi'");
	}
	
	// Activation Restriction element Check 
	public static void activationRestrictionsElementCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getActivationRestrictionsElementText().equals("Activer les restrictions WiFi")) && (NetworkPages.isActivationRestrictionsElementDiplayed() == true)) {
			test.log(Status.PASS, "L'élément 'Activer les restrictions WiFi' s'affiche ");
		} else {
			test.log(Status.FAIL, "L'élément 'Type de Activer les restrictions WiFi' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ActivationRestrictionsElement");
		}
	}
	
	// No Device Restriction desc Check 
	public static void restrictionsNoDeviceDescCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.isRestrictionsNoDeviceDescDiplayed() == true)) {
			test.log(Status.PASS, "La description 'Vous n'avez actuellement aucun appareil autorisé' s'affiche ");
		} else {
			test.log(Status.FAIL, "La description 'Vous n'avez actuellement aucun appareil autorisé' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"RestrictionsNoDeviceDesc");
		}
	}

	
	//Add Device button check
	public static void addDeviceButtonCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getAddDeviceButtonText().equals("Ajouter un appareil")) && (NetworkPages.isAddDeviceButtonDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'Ajouter un appareil' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'Ajouter un appareil' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"AddDeviceButton");
		}
	}

	//Click on Add Device button
	public static void addDeviceButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnAddDeviceButton();
		test.log(Status.INFO, "Clique sur le type de la restriction");
	}
	
	//Validate button check
	public static void restrictionsValidatebuttonCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getRestrictionsValidatebuttonText().equals("VALIDER")) && (NetworkPages.isRestrictionsValidatebuttonDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'Valider' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'Valider' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"RestrictionsValidatebutton");
		}
	}

	//Click on Validate button
	public static void restrictionsValidatebuttonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnRestrictionsValidatebutton();
		test.log(Status.INFO, "Clique sur le bouton 'Valider'");
	}
	
	//Select Device List button check
	public static void selectListDevicesButtonCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getSelectListDevicesButtonText().equals("Sélectionner dans la liste d'appareils")) && (NetworkPages.isSelectListDevicesButtonDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'Valider' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'Valider' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"SelectListDevicesButton");
		}
	}
	
	//Click on Select Device List button
	public static void selectListDevicesButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnSelectListDevicesButton();
		test.log(Status.INFO, "Clique sur la corbeille");
	}


	//Click to delete device
	public static void deleteDeviceButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnDeleteDeviceButton();
		test.log(Status.INFO, "Clique sur la corbeille");
	}
	
	//Select "Autoriser l'accès" Restriction Type Value
	public static void accessAlowElementClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnAccessAlowElement();
		test.log(Status.INFO, "Sélectionner 'Autoriser l'accès'");
	}
	
	//Show password button check
	public static void ShowPasswordButtonCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getShowHidePasswordButtonText().equals("Afficher ma clé")) && (NetworkPages.isShowHidePasswordButtonDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'Afficher ma clé' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'Afficher ma clé' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ShowHidePasswordButton");
		}
	}

	//Hide password button check
	public static void HidePasswordButtonCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getShowHidePasswordButtonText().equals("Masquer ma clé")) && (NetworkPages.isShowHidePasswordButtonDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'Masquer ma clé' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'Masquer ma clé' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ShowHidePasswordButton");
		}
	}

	//Click on Select Device List button
	public static void showHidePasswordButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnShowHidePasswordButton();
		test.log(Status.INFO, "Clique sur 'Afficher/Masqué ma clé'");
	}
	
	
	//Edit network button check
	public static void editNetworkButtonCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getEditNetworkButtonText().equals("Modifier")) && (NetworkPages.isEditNetworkButtonDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'Modifier' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'Modifier' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"EditNetworkButton");
		}
	}
	
	//Click on Edit network button
	public static void editNetworkButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnEditNetworkButton();
		test.log(Status.INFO, "Clique sur le bouton 'Modifier'");
	}

	//Edit network Title page check
	public static void editNetworkTitlePageCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getEditNetworkTitlePageText().equals("Modifier mon WiFi")) && (NetworkPages.isEditNetworkTitlePageDiplayed() == true)) {
			test.log(Status.PASS, "La page 'Modifier mon WiFi' s'affiche ");
		} else {
			test.log(Status.FAIL, "La page 'Modifier mon WiFi' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"EditNetworkTitlePage");
		}
	}

	//Edit networks Title page check
	public static void editNetworksTitlePageCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getEditNetworkTitlePageText().equals("Modifier mes WiFi")) && (NetworkPages.isEditNetworkTitlePageDiplayed() == true)) {
			test.log(Status.PASS, "La page 'Modifier mes WiFi' s'affiche ");
		} else {
			test.log(Status.FAIL, "La page 'Modifier mes WiFi' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"EditNetworksTitlePage");
		}
	}

	
	//Network WiFi element check
	public static void SSIDNetworkElementCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getSSIDNetworkElementText().equals("Réseau WiFi")) && (NetworkPages.isSSIDNetworkElementDiplayed() == true)) {
			test.log(Status.PASS, "L'élément 'Réseau WiFi' s'affiche ");
		} else {
			test.log(Status.FAIL, "L'élément 'Réseau WiFi' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"SSIDNetworkElement");
		}
	}

	//Select SSID
	public static void SSIDNetworkElementClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnSSIDNetworkElement();
		test.log(Status.INFO, "Clique sur le SSID du réseau Wifi");
	}

	//SSID name field check
	public static void SSIDNetworkFieldCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.isSSIDNetworkFieldDiplayed() == true)) {
			test.log(Status.PASS, "LE champs 'Nom de mon réseau WiFi' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le champs 'Nom de mon réseau WiFi' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"SSIDNetworkField");
		}
	}

	//Select name field
	public static void SSIDNetworkFieldClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnSSIDNetworkField();
		test.log(Status.INFO, "Clique sur le SSID du réseau Wifi");
	}
	
	//clear field
	public static void ClearSSIDFieldClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnClearSSIDField();
		test.log(Status.INFO, "Supprimer le nom su réseau WiFi");
	}

	//Network WiFi element check
	public static void validateButtonCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getValidateButtonText().equals("Valider")) && (NetworkPages.isValidateButtonDiplayed() == true)) {
			test.log(Status.PASS, "L'élément 'Réseau WiFi' s'affiche ");
		} else {
			test.log(Status.FAIL, "L'élément 'Réseau WiFi' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ValidateButton");
		}
	}

	//Click on Edit network button
	public static void validateButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnValidateButton();
		test.log(Status.INFO, "Clique sur le bouton 'Valider'");
	}

	//WiFi Key element check
	public static void networkKeyElementCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getNetworkKeyElementText().equals("Clé WiFi")) && (NetworkPages.isNetworkKeyElementDiplayed() == true)) {
			test.log(Status.PASS, "L'élément 'Clé WiFi' s'affiche ");
		} else {
			test.log(Status.FAIL, "L'élément 'Clé WiFi' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"NetworkKeyElement");
		}
	}

	//Click on WiFi Key button
	public static void networkKeyElementClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnNetworkKeyElement();
		test.log(Status.INFO, "Clique sur l'élément 'Clé WiFi'");
	}
	
	
	//*******************************************
	
	//WiFi Key Page name check
	public static void wiFiKeyTitlePageCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getWiFiKeyTitlePageText().equals("Clé WiFi")) && (NetworkPages.isWiFiKeyTitlePageDiplayed() == true)) {
			test.log(Status.PASS, "La page 'Clé WiFi' s'affiche ");
		} else {
			test.log(Status.FAIL, "La page 'Clé WiFi' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"WiFiKeyTitlePage");
		}
	}
	//WiFi Key Page Description check
	public static void wiFiKeyPageDescCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getWiFiKeyPageDescText().equals("Astuce : pour définir une clé puissante et facile à retenir, composez-la de quatre à cinq mots sans liens entre eux. Ex : table-jour-lapin-fleur-sourire")) && (NetworkPages.isWiFiKeyPageDescDiplayed() == true)) {
			test.log(Status.PASS, "La description de la page 'Clé WiFi' s'affiche ");
		} else {
			test.log(Status.FAIL, "La description de la page ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"WiFiKeyPageDesc");
		}
	}

	//WiFi Key field name check
	public static void wiFiKeyFieldNameCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getWiFiKeyFieldNameText().equals("Clé WiFi")) && (NetworkPages.isWiFiKeyFieldNameDiplayed() == true)) {
			test.log(Status.PASS, "Le nom du champ 'Clé WiFi' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le nom du champ ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"WiFiKeyFieldName");
		}
	}

	//Edit WiFi Key
	public static void WiFiKeyFieldEdit(ExtentTest test, AppiumDriver<MobileElement> driver, String WiFiKey) {
		NetworkPages.enterWiFiKeyField(WiFiKey);
		test.log(Status.INFO, "Saisir la Clé WiFi");
	}
	
	//WiFi Key Status check
	public static void wiFiKeyStatusCheck(ExtentTest test, AppiumDriver<MobileElement> driver, String KeyStatus) {
		if ((NetworkPages.getWiFiKeyStatusText().equals(KeyStatus)) && (NetworkPages.isWiFiKeyStatusDiplayed() == true)) {
			test.log(Status.PASS, "Le message de l'encart s'affiche bien ");
		} else {
			test.log(Status.FAIL, "Le message de l'encart ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"WiFiKeyStatus");
		}
	}

	//WiFi Key element check
	public static void securityWiFiElementCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getSecurityWiFiElementText().equals("Sécurité WiFi")) && (NetworkPages.isSecurityWiFiElementDiplayed() == true)) {
			test.log(Status.PASS, "L'élément 'Sécurité WiFi' s'affiche ");
		} else {
			test.log(Status.FAIL, "L'élément 'Sécurité WiFi' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"SecurityWiFiElement");
		}
	}

	//Click on WiFi Key button
	public static void securityWiFiElementClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnSecurityWiFiElement();
		test.log(Status.INFO, "Clique sur l'élément 'Clé WiFi'");
	}

	//Click on recommended WiFi security element
	public static void recommendedSecurityElementClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnRecommendedSecurityElement();
		test.log(Status.INFO, "Séléctionner 'WPA2 - AES'");
	}

	//Click on Notrecommended WiFi security element
	public static void notrecommendedSecurityElementClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnNotRecommendedSecurityElement();
		test.log(Status.INFO, "Séléctionner 'WPA2 - AES/TKIP'");
	}
	
	//Pop-up WiFi Security check
	public static void popupTitleCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getPopupTitleText().equals("Sécurité WiFi obsolète")) && (NetworkPages.isPopupTitleDiplayed() == true)) {
			test.log(Status.PASS, "Popup 'Sécurité WiFi obsolète' s'affiche ");
		} else {
			test.log(Status.FAIL, "Popup 'Sécurité WiFi obsolète' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"PopupTitle");
		}
	}

	//WiFi Key element check
	public static void editWiFiSecurityButtonCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getEditWiFiSecurityButtonText().equals("Modifier")) && (NetworkPages.isEditWiFiSecurityButtonDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'Modifier' de la popoup WiFi obsolète s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'Modifier' de la popoup WiFi obsolète ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"EditWiFiSecurityButton");
		}
	}

	//Click on WiFi Key button
	public static void editWiFiSecurityButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnEditWiFiSecurityButton();
		test.log(Status.INFO, "Clique sur le bouton 'Modifier' de la popoup WiFi obsolète");
	}

	//WiFi Conf Type element check
	public static void confTypeElementCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getConfTypeElementText().equals("Type de configuration WiFi")) && (NetworkPages.isConfTypeElementDiplayed() == true)) {
			test.log(Status.PASS, "L'élément 'Type de configuration WiFi' s'affiche ");
		} else {
			test.log(Status.FAIL, "L'élément 'Type de configuration WiFi' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ConfTypeElement");
		}
	}

	//Click on WiFi Conf Type
	public static void confTypeElementClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnConfTypeElement();
		test.log(Status.INFO, "Clique sur 'Type de configuration WiFi'");
	}

	//WiFi Conf Type Page check
	public static void confWiFiTypePageCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getConfWiFiTypePageText().equals("Type de configuration WiFi")) && (NetworkPages.isConfWiFiTypePageDiplayed() == true)) {
			test.log(Status.PASS, "La page 'ConfWiFiTypePage' s'affiche ");
		} else {
			test.log(Status.FAIL, "LA page 'ConfWiFiTypePage' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ConfWiFiTypePage");
		}
	}

	//Separated Conf Type element check
	public static void separatedConfElementCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getSeparatedConfElementText().equals("Séparée")) && (NetworkPages.isSeparatedConfElementDiplayed() == true)) {
			test.log(Status.PASS, "L'élément 'Séparée' s'affiche ");
		} else {
			test.log(Status.FAIL, "L'élément 'Séparée' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"SeparatedConfElement");
		}
	}

	//Click On Separated
	public static void separatedConfElementClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnSeparatedConfElement();
		test.log(Status.INFO, "Clique sur 'Séparée'");
	}

	//Separated Conf Type element check
	public static void uniqueConfElementCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getUniqueConfElementText().equals("Unique")) && (NetworkPages.isUniqueConfElementDiplayed() == true)) {
			test.log(Status.PASS, "L'élément 'Unique' s'affiche ");
		} else {
			test.log(Status.FAIL, "L'élément 'Unique' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"UniqueConfElement");
		}
	}

	//Click On Separated
	public static void uniqueConfElementClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnUniqueConfElement();
		test.log(Status.INFO, "Clique sur 'Séparée'");
	}
	
	//Label element check
	public static void wiFiInfoPagCheck(ExtentTest test, AppiumDriver<MobileElement> driver, String Etiquette) {
		if ((NetworkPages.getWiFiInfoPagText().equals(Etiquette)) && (NetworkPages.isWiFiInfoPagDiplayed() == true)) {
			test.log(Status.PASS, "L'étiquette "+Etiquette+" s'affiche ");
		} else {
			test.log(Status.FAIL, "L'étiquette "+Etiquette+" ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"WiFiInfoPag");
		}
	}

	//Click On 2.4GHz
	public static void tab2GHzClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnTab2GHz();
		test.log(Status.INFO, "Clique sur l'onglet '2.4GHz'");
	}

	//Confirm conf check
	public static void confirmTypeButtonCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getConfirmTypeButtonText().equals("Confirmer")) && (NetworkPages.isConfirmTypeButtonDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'Confirmer' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'Confirmer' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ConfirmTypeButton");
		}
	}

	//Click On Confirm button
	public static void confirmTypeButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnConfirmTypeButton();
		test.log(Status.INFO, "Clique sur Le bouton'Confirmer'");
	}
	
	//Planned WiFi OFF Element check
	public static void plannedWiFiOFFElementCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getPlannedWiFiOFFElementText().equals("Coupure planifiée du WiFi")) && (NetworkPages.isPlannedWiFiOFFElementDiplayed() == true)) {
			test.log(Status.PASS, "L'élément 'Coupure planifiée du WiFi' s'affiche ");
		} else {
			test.log(Status.FAIL, "L'élément 'Coupure planifiée du WiFi' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"PlannedWiFiOFFElement");
		}
	}

	//Click On Planned WiFi OFF Element
	public static void plannedWiFiOFFElementClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnPlannedWiFiOFFElement();
		test.log(Status.INFO, "Clique sur 'Coupure planifiée du WiFi'");
	}
	
	//WiFi Planning Page check
	public static void plannedWiFiOFFPageNameCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getPlannedWiFiOFFPageNameText().equals("Coupure planifiée du WiFi")) && (NetworkPages.isPlannedWiFiOFFPageNameDiplayed() == true)) {
			test.log(Status.PASS, "La page 'Coupure planifiée du WiFi' s'affiche ");
		} else {
			test.log(Status.FAIL, "La page 'Coupure planifiée du WiFi' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"PlannedWiFiOFFPageName");
		}
	}

	//Wifi Planning Message check
	public static void wifiPlanningMessageCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getWifiPlanningMessageText().equals("Sélectionnez les plages horaires durant lesquelles vous souhaitez que votre WiFi soit désactivé.")) && (NetworkPages.isWifiPlanningMessageDiplayed() == true)) {
			test.log(Status.PASS, "La description de la page 'Coupure planifiée du WiFi' s'affiche ");
		} else {
			test.log(Status.FAIL, "La description de la page 'Coupure planifiée du WiFi' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"WifiPlanningMessage");
		}
	}
	
	//Wifi Planning Validate Button check
	public static void wifiPlanningValidateButtonCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getWifiPlanningValidateButtonText().equals("Valider les plages horaires")) && (NetworkPages.isWifiPlanningValidateButtonDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'Valider les plages horaires' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'Valider les plages horaires' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"WifiPlanningValidateButton");
		}
	}

	//Click On Wifi Planning Validate Button
	public static void wifiPlanningValidateButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnWifiPlanningValidateButton();
		test.log(Status.INFO, "Clique sur 'Valider les plages horaires'");
	}
	
	//Wifi State Subtitle check
	public static void wifiStateSubtitleCheck(ExtentTest test, AppiumDriver<MobileElement> driver, String WiFiState) {
		if ((NetworkPages.getWifiStateSubtitleText().equals(WiFiState)) && (NetworkPages.isWifiStateSubtitleDiplayed() == true)) {
			test.log(Status.PASS, "Le status WiFi est "+WiFiState+" ");
		} else {
			test.log(Status.FAIL, "Le status WiFi "+WiFiState+" ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"WifiStateSubtitle");
		}
	}

	//Wifi Planning Reset Button check
	public static void wifiPlanningResetButtonCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getWifiPlanningResetButtonText().equals("Réinitialiser les plages horaires")) && (NetworkPages.isWifiPlanningResetButtonDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'Réinitialiser les plages horaires' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'Réinitialiser les plages horaires' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"WifiPlanningResetButton");
		}
	}

	//Click On Wifi Planning Reset Button
	public static void wifiPlanningResetButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnWifiPlanningResetButton();
		test.log(Status.INFO, "Clique sur 'Réinitialiser les plages horaires'");
	}

	//WifiPlanningResetConfirmation check
	public static void wifiPlanningResetConfirmationCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((NetworkPages.getWifiPlanningResetConfirmationText().equals("Réinitialiser")) && (NetworkPages.isWifiPlanningResetConfirmationDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'Réinitialiser' de la popup de confirmation s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'Réinitialiser' de la popup de confirmation ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"WifiPlanningResetConfirmation");
		}
	}

	//Click On WifiPlanningResetConfirmation
	public static void wifiPlanningResetConfirmationClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		NetworkPages.clickOnWifiPlanningResetConfirmation();
		test.log(Status.INFO, "Clique sur 'Réinitialiser les plages horaires'");
	}

}
