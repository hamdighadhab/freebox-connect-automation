package Strings.DevicesStrings;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.DevicesPages.ConnectedDevicesPages;
import tests.ExtentReport.ExtentReportsFree;

public class ConnectedDevicesStrings {

	
	public static final String TestName1 = "Liste des appareils connectés";
	public static final String TestName2 = "Information de connextion d'un appareil connecté";
	public static final String TestName3 = "Détails d'un appareil connecté";
	public static final String TestName4 = "Appareil connecté : Autres informations ";
	public static final String TestName5 = "Modifier un appareil connecté";

	public static final String FonctionalityName = "Appareils";

	//Chevron check
	public static void chevronConnectedDeviceCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ConnectedDevicesPages.isChevronConnectedDeviceDiplayed() == true)) {
			test.log(Status.PASS, "Le Chevron des appareils connectés s'affiche ");
		} else {
			test.log(Status.FAIL, "Le Chevron des appareils connectés s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ChevronConnectedDevice");
		}
	}

	//Click on Chevron
	public static void chevronConnectedDeviceClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ConnectedDevicesPages.clickOnChevronConnectedDevice();
		test.log(Status.INFO, "Clique sur Chevron des appareils connectés");
	}

	//connected list
	public static void connectedDevicesSectionCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ConnectedDevicesPages.getConnectedDevicesSectionText().equals("Actuellement connectés")) && (ConnectedDevicesPages.isConnectedDevicesSectionDiplayed() == true)) {
			test.log(Status.PASS, "La section 'Actuellement connectés' s'affiche ");
		} else {
			test.log(Status.FAIL, "La section 'Actuellement connectés' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ConnectedDevicesSection");
		}
	}


	//Devices Connection Type check
	public static void devicesConnectionTypeCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ConnectedDevicesPages.isDevicesConnectionTypeDiplayed() == true)) {
			test.log(Status.PASS, "Le type de connexion des appareils connectés s'affiche ");
		} else {
			test.log(Status.FAIL, "Le type de connexion des appareils connectés s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DevicesConnectionType");
		}
	}

	//Device Up Rate check
	public static void deviceUpRateCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ConnectedDevicesPages.isDeviceUpRateDiplayed() == true)) {
			test.log(Status.PASS, "Le débit d'envoi de l'appareil connectés s'affiche ");
		} else {
			test.log(Status.FAIL, "Le débit d'envoi de l'appareil connectés ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DeviceUpRate");
		}
	}

	//Device Down Rate check
	public static void deviceDownRateCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ConnectedDevicesPages.isDeviceDownRateDiplayed() == true)) {
			test.log(Status.PASS, "Le débit de réception de l'appareil connectés s'affiche ");
		} else {
			test.log(Status.FAIL, "Le débit de réception de l'appareil connectés ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DeviceDownRate");
		}
	}

	//Device Rate Unit check
	public static void deviceRateUnitCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ConnectedDevicesPages.isDeviceRateUnitDiplayed() == true)) {
			test.log(Status.PASS, "L'unité 'Kb/s' de l'appareil connectés s'affiche ");
		} else {
			test.log(Status.FAIL, "L'unité 'Kb/s' de l'appareil connectés ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DeviceRateUnit");
		}
	}


	//First Picto Device check
	public static void firstPictoDeviceCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ConnectedDevicesPages.isFirstPictoDeviceDiplayed() == true)) {
			test.log(Status.PASS, "Le picto de l'appareil connectés s'affiche ");
		} else {
			test.log(Status.FAIL, "Le picto de l'appareil connectés ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"FirstPictoDevice");
		}
	}

	//Click on first device
	public static void firstPictoDeviceClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ConnectedDevicesPages.clickOnFirstPictoDevice();
		test.log(Status.INFO, "Clique sur le premier appareil connecté");
	}


	//Device Detail Image check
	public static void deviceDetailImagecheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ConnectedDevicesPages.isDeviceDetailImageDiplayed() == true)) {
			test.log(Status.PASS, "L'mage dans la page de détail d'un appareil connecté s'affiche ");
		} else {
			test.log(Status.FAIL, "L'mage dans la page de détail d'un appareil connecté ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DeviceDetailImage");
		}
	}

	//Device Detail Status check 
	public static void deviceDetailStatusCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ConnectedDevicesPages.getDeviceDetailStatusText().equals("Connecté")) && (ConnectedDevicesPages.isDeviceDetailStatusDiplayed() == true)) {
			test.log(Status.PASS, "Le status 'Connecté' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le status 'Connecté' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DeviceDetailStatus");
		}
	}
	
	//Device Detail Status check 
	public static void recentlyDeviceDetailStatusCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ConnectedDevicesPages.isDeviceDetailStatusDiplayed() == true)) {
			test.log(Status.PASS, "Le status 'Connecté' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le status 'Connecté' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DeviceDetailStatus");
		}
	}


	//Device Detail Up Rate check
	public static void deviceDetailUpRatecheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ConnectedDevicesPages.isDeviceDetailUpRateDiplayed() == true)) {
			test.log(Status.PASS, "Le débit d'envoi dans la page de détail d'un appareil connecté s'affiche ");
		} else {
			test.log(Status.FAIL, "Le débit d'envoi dans la page de détail d'un appareil connecté ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DeviceDetailUpRate");
		}
	}

	//Device Detail Down Rate check
	public static void deviceDetailDownRatecheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ConnectedDevicesPages.isDeviceDetailDownRateDiplayed() == true)) {
			test.log(Status.PASS, "Le débit de réception dans la page de détail d'un appareil connecté s'affiche ");
		} else {
			test.log(Status.FAIL, "Le débit de réception dans la page de détail d'un appareil connecté ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DeviceDetailDownRate");
		}
	}

	//Device Detail Down Rate Unit check
	public static void deviceDetailDownRateUnitcheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ConnectedDevicesPages.isDeviceDetailDownRateUnitDiplayed() == true)) {
			test.log(Status.PASS, "L'unité 'Kb/s' dans la page de détail d'un appareil connecté s'affiche ");
		} else {
			test.log(Status.FAIL, "L'unité 'Kb/s' dans la page de détail d'un appareil connecté ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DeviceDetailDownRateUnit");
		}
	}

	//Device Detail Gateway Icon check
	public static void deviceDetailGatewayIconcheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ConnectedDevicesPages.isDeviceDetailGatewayIconDiplayed() == true)) {
			test.log(Status.PASS, "L'icône getway dans la page de détail d'un appareil connecté s'affiche ");
		} else {
			test.log(Status.FAIL, "L'icône getway dans la page de détail d'un appareil connecté ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DeviceDetailGatewayIcon");
		}
	}

	//Device Detail Gateway Name check 
	public static void deviceDetailGatewayNameCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ConnectedDevicesPages.isDeviceDetailGatewayNameDiplayed() == true)) {
			test.log(Status.PASS, "Le nom de la getway s'affiche ");
		} else {
			test.log(Status.FAIL, "Le nom de la getway ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DeviceDetailGatewayName");
		}
	}

	//Device Detail Name check 
	public static void deviceDetailNameCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ConnectedDevicesPages.getDeviceDetailNameText().equals("Nom d’hôte")) && (ConnectedDevicesPages.isDeviceDetailNameDiplayed() == true)) {
			test.log(Status.PASS, "L'élément 'Nom d’hôte' s'affiche ");
		} else {
			test.log(Status.FAIL, "L'élément 'Nom d’hôte' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DeviceDetailName");
		}
	}

	//Device Detail Constructor check 
	public static void deviceDetailConstructorCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ConnectedDevicesPages.getDeviceDetailConstructorText().equals("Constructeur")) && (ConnectedDevicesPages.isDeviceDetailConstructorDiplayed() == true)) {
			test.log(Status.PASS, "L'élément 'Constructeur' s'affiche ");
		} else {
			test.log(Status.FAIL, "L'élément 'Constructeur' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DeviceDetailConstructor");
		}
	}

	//Device Detail Constructor check 
	public static void deviceDetailAssociatedProfileCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ConnectedDevicesPages.getDeviceDetailAssociatedProfileText().equals("Profil associé")) && (ConnectedDevicesPages.isDeviceDetailAssociatedProfileDiplayed() == true)) {
			test.log(Status.PASS, "L'élément 'Profil associé' s'affiche ");
		} else {
			test.log(Status.FAIL, "L'élément 'Profil associé' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DeviceDetailAssociatedProfile");
		}
	}

	//Device Detail Others Informations check 
	public static void deviceDetailOthersInformationsCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ConnectedDevicesPages.getDeviceDetailOthersInformationsText().equals("Autres informations")) && (ConnectedDevicesPages.isDeviceDetailOthersInformationsDiplayed() == true)) {
			test.log(Status.PASS, "L'élément 'Autres informations' s'affiche ");
		} else {
			test.log(Status.FAIL, "L'élément 'Autres informations' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DeviceDetailOthersInformations");
		}
	}

	//Click on Others Informations
	public static void deviceDetailOthersInformationsClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ConnectedDevicesPages.clickOnDeviceDetailOthersInformations();
		test.log(Status.INFO, "Clique sur 'Autres informations'");
	}

	//IPV4 element check 
	public static void deviceIPV4AdressCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ConnectedDevicesPages.getDeviceIPV4AdressText().equals("Adresse IPv4")) && (ConnectedDevicesPages.isDeviceIPV4AdressDiplayed() == true)) {
			test.log(Status.PASS, "L'élément 'Adresse IPv4' s'affiche ");
		} else {
			test.log(Status.FAIL, "L'élément 'Adresse IPv4' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DeviceIPV4Adress");
		}
	}

	//IPV6 element check 
	public static void deviceIPV6AdressCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ConnectedDevicesPages.getDeviceIPV6AdressText().equals("Adresse IPv6")) && (ConnectedDevicesPages.isDeviceIPV6AdressDiplayed() == true)) {
			test.log(Status.PASS, "L'élément 'Adresse IPv6' s'affiche ");
		} else {
			test.log(Status.FAIL, "L'élément 'Adresse IPv6' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DeviceIPV6Adress");
		}
	}

	//MAC element check 
	public static void deviceMACAdressCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ConnectedDevicesPages.getDeviceMACAdressText().equals("Adresse MAC")) && (ConnectedDevicesPages.isDeviceMACAdressDiplayed() == true)) {
			test.log(Status.PASS, "L'élément 'Adresse MAC' s'affiche ");
		} else {
			test.log(Status.FAIL, "L'élément 'Adresse MAC' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DeviceMACAdress");
		}
	}

	//Modify button check 
	public static void modifyConnectedDeviceButtonCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ConnectedDevicesPages.getModifyConnectedDeviceButtonText().equals("MODIFIER")) && (ConnectedDevicesPages.isModifyConnectedDeviceButtonDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'Modifier' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'Modifier' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ModifyConnectedDeviceButton");
		}
	}

	//Click on Modify button
	public static void modifyConnectedDeviceButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ConnectedDevicesPages.clickOnModifyConnectedDeviceButton();
		test.log(Status.INFO, "Clique sur 'Modifier'");
	}


	//Modify Connected Device Page Name check 	
	public static void modifyConnectedDevicePageNameCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ConnectedDevicesPages.getModifyConnectedDevicePageNameText().equals("Modifier l’appareil")) && (ConnectedDevicesPages.isModifyConnectedDevicePageNameDiplayed() == true)) {
			test.log(Status.PASS, "La page 'Modifier l’appareil' s'affiche ");
		} else {
		test.log(Status.FAIL, "La page 'Modifier l’appareil' ne s'affiche pas");
		ExtentReportsFree.takeScreenshot( driver,"ModifyConnectedDevicePageName");
		}
    }
	
	//Field Modify Device Description check 	
	public static void fieldModifyDeviceDescCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ConnectedDevicesPages.getFieldModifyDeviceDescText().equals("Saisissez un nom pour cet appareil")) && (ConnectedDevicesPages.isFieldModifyDeviceDescDiplayed() == true)) {
			test.log(Status.PASS, "La description 'Saisissez un nom pour cet appareil' s'affiche ");
		} else {
		test.log(Status.FAIL, "La description 'Saisissez un nom pour cet appareil' ne s'affiche pas");
		ExtentReportsFree.takeScreenshot( driver,"FieldModifyDeviceDesc");
		}
    }
	
	//Field Modify Device Description check 	
	public static void typeConnectedDeviceDescCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ConnectedDevicesPages.getTypeConnectedDeviceDescText().equals("Sélectionnez un type pour cet appareil :")) && (ConnectedDevicesPages.isTypeConnectedDeviceDescDiplayed() == true)) {
			test.log(Status.PASS, "La description 'Sélectionnez un type pour cet appareil :' s'affiche ");
		} else {
		test.log(Status.FAIL, "La description 'Sélectionnez un type pour cet appareil :' ne s'affiche pas");
		ExtentReportsFree.takeScreenshot( driver,"TypeConnectedDeviceDesc");
		}
    }
	
	//Modify Connected Device Field check 	
	public static void modifyConnectedDeviceFieldCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ConnectedDevicesPages.isModifyConnectedDeviceFieldDiplayed() == true)) {
			test.log(Status.PASS, "La champ de saisie du nom d'un appareil s'affiche ");
		} else {
		test.log(Status.FAIL, "La champ de saisie du nom d'un appareil ne s'affiche pas");
		ExtentReportsFree.takeScreenshot( driver,"ModifyConnectedDeviceField");
		}
    }
	
	//Click on Modify Connected Device Field
	public static void modifyConnectedDeviceFieldClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ConnectedDevicesPages.clickOnModifyConnectedDeviceField();
		test.log(Status.INFO, "Selectionner le champ de saisie du nom d'un appareil");
	}
	
	public static String InitialDeviceName = "";
	public static String initialDeviceName() {
		return InitialDeviceName = ConnectedDevicesPages.getModifyConnectedDeviceFieldText();
	}
	
	//Validate Button check 	
	public static void validateModifButtonCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ConnectedDevicesPages.getValidateModifButtonText().equals("VALIDER")) &&(ConnectedDevicesPages.isValidateModifButtonDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'VALIDER' s'affiche ");
		} else {
		test.log(Status.FAIL, "Le bouton 'VALIDER' ne s'affiche pas");
		ExtentReportsFree.takeScreenshot( driver,"ValidateModifButton");
		}
    }

	//Click on VALIDATE button
	public static void validateModifButtonClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ConnectedDevicesPages.clickOnValidateModifButton();
		test.log(Status.INFO, "Clique sur le bouton 'VALIDER'");
	}
	
	//Initial device name check 	
	public static void initialdeviceNameValueCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ConnectedDevicesPages.getDeviceNameValueText().equals(InitialDeviceName)) &&(ConnectedDevicesPages.isDeviceNameValueDiplayed() == true)) {
			test.log(Status.PASS, "Le nom initial de la device s'affiche ");
		} else {
		test.log(Status.FAIL, "Le nom initial de la device ne s'affiche pas");
		ExtentReportsFree.takeScreenshot( driver,"DeviceNameValue");
		}
    }

	//New device name check 	
	public static void newdeviceNameValueCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((ConnectedDevicesPages.getDeviceNameValueText().equals("ConnectedDeviceAutomatedName")) &&(ConnectedDevicesPages.isDeviceNameValueDiplayed() == true)) {
			test.log(Status.PASS, "Le nom modifié de la device s'affiche ");
		} else {
		test.log(Status.FAIL, "Le nom modifié de la device ne s'affiche pas");
		ExtentReportsFree.takeScreenshot( driver,"DeviceNameValue");
		}
    }
}

