package Strings.DevicesStrings;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;


import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import tests.ExtentReport.ExtentReportsFree;
import pages.DevicesPages.RecentlyLoggedDevicesPages;

public class RecentlyLoggedDevicesStrings {
	
	public static final String TestName1 = "Liste des appareils récemments connectés";
	public static final String TestName2 = "Détails d'un appareil connecté";
	public static final String TestName3 = "Plus d'information d'un appareil récemment connecté ";
	public static final String TestName4 = "Modifier un appareil récemment connecté ";
	public static final String TestName5 = "Supprimer un appareil récemment connecté";

	public static final String FonctionalityName = "Appareils";

	//Header with number of connected device check 
	public static void recentlyLoggedWithoutDeviceConnectedCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RecentlyLoggedDevicesPages.getRecentlyLoggedWithoutDeviceConnectedText().equals(RecentlyLoggedDevicesPages.RecentlyLoggedDevice)) && (RecentlyLoggedDevicesPages.isRecentlyLoggedWithoutDeviceConnectedDiplayed() == true)) {
			test.log(Status.PASS, "Le nombre d'appareils connectés dans le header s'affiche ");
		} else {
			test.log(Status.FAIL, "Le nombre d'appareils connectés dans le header ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"RecentlyLoggedWithoutDeviceConnected");
		}
	}
	//Chevron without connected device check 
	public static void chevronRecentlyLoggedWithoutDeviceConnectedCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RecentlyLoggedDevicesPages.isChevronRecentlyLoggedWithoutDeviceConnectedDiplayed() == true)) {
			test.log(Status.PASS, "Le nombre d'appareils connectés dans le header s'affiche ");
		}
	}
	//chevron without connected device click 
	public static void chevronRecentlyLoggedWithoutDeviceConnectedClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		RecentlyLoggedDevicesPages.clickOnChevronRecentlyLoggedWithoutDeviceConnected();
		test.log(Status.INFO, "Clique sur chevron");
	}	
	
	//Header with number of connected device check 
	public static void recentlyLoggedWithDeviceConnectedCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RecentlyLoggedDevicesPages.getRecentlyLoggedWithDeviceConnectedText().equals(RecentlyLoggedDevicesPages.RecentlyLoggedDevice)) && (RecentlyLoggedDevicesPages.isRecentlyLoggedWithDeviceConnectedDiplayed() == true)) {
			test.log(Status.PASS, "Le nombre d'appareils connectés dans le header s'affiche ");
		} else {
			test.log(Status.FAIL, "Le nombre d'appareils connectés dans le header ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"RecentlyLoggedWithDeviceConnected");
		}
	}

	
	//Chevron with connected device check 
	public static void chevronRecentlyLoggedWithDeviceConnectedCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RecentlyLoggedDevicesPages.isChevronRecentlyLoggedWithDeviceConnectedDiplayed() == true)) {
			test.log(Status.PASS, "Le nombre d'appareils connectés dans le header s'affiche ");
		}
	}
	//chevron with connected device click 
	public static void chevronRecentlyLoggedWithDeviceConnectedClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		RecentlyLoggedDevicesPages.clickOnChevronRecentlyLoggedWithDeviceConnected();
		test.log(Status.INFO, "Clique sur chevron");
	}

	
	//First device check 
	public static void firstDeviceListCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RecentlyLoggedDevicesPages.isFirstDeviceListDiplayed() == true)) {
			test.log(Status.PASS, "L'appareil récemment connecté  s'affiche ");
		} else {
			test.log(Status.FAIL, "L'appareil récemment connecté ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"FirstDeviceList");
		}
	}

	//Click on the first device 
	public static void firstDeviceListClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		RecentlyLoggedDevicesPages.clickOnFirstDeviceList();
		test.log(Status.INFO, "Clique sur l'appareil récemment connecté");
	}

	//Second device check 
	public static void secondDeviceListCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RecentlyLoggedDevicesPages.isSecondDeviceListDiplayed() == true)) {
			test.log(Status.PASS, "L'appareil récemment connecté  s'affiche ");
		} else {
			test.log(Status.FAIL, "L'appareil récemment connecté ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"SecondDeviceList");
		}
	}

	//Click on the Second device 
	public static void secondDeviceListClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		RecentlyLoggedDevicesPages.clickOnSecondDeviceList();
		test.log(Status.INFO, "Clique sur l'appareil récemment connecté");
	}

	//Third device check 
	public static void thirdDeviceListCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RecentlyLoggedDevicesPages.isThirdDeviceListDiplayed() == true)) {
			test.log(Status.PASS, "L'appareil récemment connecté  s'affiche ");
		} else {
			test.log(Status.FAIL, "L'appareil récemment connecté ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"ThirdDeviceList");
		}
	}

	//Click on the Third device 
	public static void thirdDeviceListClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		RecentlyLoggedDevicesPages.clickOnThirdDeviceList();
		test.log(Status.INFO, "Clique sur l'appareil récemment connecté");
	}

	//Fourth device check 
	public static void fourthDeviceListCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RecentlyLoggedDevicesPages.isFourthDeviceListDiplayed() == true)) {
			test.log(Status.PASS, "L'appareil récemment connecté  s'affiche ");
		} else {
			test.log(Status.FAIL, "L'appareil récemment connecté ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"FourthDeviceList");
		}
	}

	//Click on the Fourth device 
	public static void fourthDeviceListClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		RecentlyLoggedDevicesPages.clickOnFourthDeviceList();
		test.log(Status.INFO, "Clique sur l'appareil récemment connecté");
	}
	
	//Device remove button check 
	public static void deviceDetailRemoveCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RecentlyLoggedDevicesPages.getDeviceDetailRemoveText().equals("Oublier cet appareil")) && (RecentlyLoggedDevicesPages.isDeviceDetailRemoveDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'Oublier cet appareil' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'Oublier cet appareil' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DeviceDetailRemove");
		}
	}

	//Click on device remove 
	public static void deviceDetailRemoveClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		RecentlyLoggedDevicesPages.clickOnDeviceDetailRemove();
		test.log(Status.INFO, "Clique sur 'Oublier cet appareil'");
	}

	//Alert Device Title check 
	public static void alertDeviceTitleCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RecentlyLoggedDevicesPages.getAlertDeviceTitleText().equals("Oublier cet appareil?")) && (RecentlyLoggedDevicesPages.isAlertDeviceTitleDiplayed() == true)) {
			test.log(Status.PASS, "Le titre de la popup 'Oublier cet appareil' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le titre de la popup 'Oublier cet appareil' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"AlertDeviceTitle");
		}
	}

	//Alert Device Description check 
	public static void alertDeviceDescCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RecentlyLoggedDevicesPages.getAlertDeviceDescText().equals("Êtes-vous sûr de vouloir retirer cet appareil de votre liste ?")) && (RecentlyLoggedDevicesPages.isAlertDeviceDescDiplayed() == true)) {
			test.log(Status.PASS, "La description de la popup 'Oublier cet appareil' s'affiche ");
		} else {
			test.log(Status.FAIL, "La description de la popup 'Oublier cet appareil' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"AlertDeviceDesc");
		}
	}
	
	//Alert decline button check 
	public static void alertDeviceDeclineCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((RecentlyLoggedDevicesPages.getAlertDeviceDeclineText().equals("Annuler")) && (RecentlyLoggedDevicesPages.isAlertDeviceDeclineDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'Annuler' s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'Oublier cet appareil' ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"AlertDeviceDecline");
		}
	}

	//Click on alert decline button
	public static void alertDeviceDeclineClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		RecentlyLoggedDevicesPages.clickOnAlertDeviceDecline();
		test.log(Status.INFO, "Clique sur 'Annuler'");
	}
}
