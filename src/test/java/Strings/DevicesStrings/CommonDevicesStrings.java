package Strings.DevicesStrings;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.DevicesPages.CommonDevicesPages;
import tests.ExtentReport.ExtentReportsFree;

public class CommonDevicesStrings {

	//Device tab-bar button Check 
	public static void devicesTabBarButtonsCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((CommonDevicesPages.getDevicesTabBarButtonText().equals("Appareils")) && (CommonDevicesPages.isDevicesTabBarButtonDiplayed() == true)) {
			test.log(Status.PASS, "Le bouton 'Appareils' de la TabBar s'affiche ");
		} else {
			test.log(Status.FAIL, "Le bouton 'Appareils' de la TabBar ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"DevicesTabBarButton");
		}
	}
	//Click on Device tab-bar
	public static void devicesTabBarButtonsClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		CommonDevicesPages.clickOnDevicesTabBarButton();
		test.log(Status.INFO, "Clique sur le bouton 'Appareils' de la TabBar");
	}

	
	
	

}
