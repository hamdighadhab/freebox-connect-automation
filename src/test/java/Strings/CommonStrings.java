package Strings;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import elements.CommonElements;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.HomePagePages.RepeaterPage;
import pages.HomePagePages.ServerPage;
import tests.ExtentReport.ExtentReportsFree;

public class CommonStrings {


	//Home page verification
	public static void homePageCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if ((CommonElements.isHomeLogoDisplayed() == true) && (CommonElements.isConnexionStatusBoutonDisplayed() == true) && (CommonElements.isShareWifiBoutonDisplayed() == true) && (CommonElements.isMuDevicesHomeDisplayed() == true)) {
			test.log(Status.PASS, "La page Home s'affiche");
		} else {
			test.fail("La page Home ne s'affiche pas");
			ExtentReportsFree.takeScreenshot( driver,"homePage");
		}
	}
	//First Equipment Check
	public static void firstEquipmentCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ServerPage.isFirstEquipmentnDiplayed() == true) {
			test.log(Status.PASS, "Ma Box s'affiche dans la liste de mes équipements");
		} else {
			test.log(Status.FAIL, "Ma Box ne s'affiche pas dans la liste de mes équipements");
			ExtentReportsFree.takeScreenshot( driver,"FirstEquipment");
		}
	}
	//Click on First Equipment
	public static void firstEquipmentClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ServerPage.clickOnFirstEquipment();
		test.log(Status.INFO, "Clique sur ma box");
	}
	//Second Equipment Check
	public static void secondEquipmentCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (ServerPage.isSecondEquipmentnDiplayed() == true) {
			test.log(Status.PASS, "Mon Répéteur s'affiche dans la liste de mes équipements");
		} else {
			test.log(Status.FAIL, "Mon Répéteur ne s'affiche pas dans la liste de mes équipements");
			ExtentReportsFree.takeScreenshot( driver,"SecondEquipment");
		}
	}
	//Click on Third Equipment
	public static void secondEquipmentClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		ServerPage.clickOnSecondEquipment();
		test.log(Status.INFO, "Clique sur mon Répéteur");
	}

	//Third Equipment Check
	public static void thirdEquipmentCheck(ExtentTest test, AppiumDriver<MobileElement> driver) {
		if (RepeaterPage.isThirdEquipmentnDiplayed() == true) {
			test.log(Status.PASS, "Mon Répéteur s'affiche dans la liste de mes équipements");
		} else {
			test.log(Status.FAIL, "Mon Répéteur ne s'affiche pas dans la liste de mes équipements");
			ExtentReportsFree.takeScreenshot( driver,"SecondEquipment");
		}
	}
	//Click on Second Equipment
	public static void thirdEquipmentClick(ExtentTest test, AppiumDriver<MobileElement> driver) {
		RepeaterPage.clickOnThirdEquipment();
		test.log(Status.INFO, "Clique sur mon Répéteur");
	}

	//End Test
	public static void endTest(AppiumDriver<MobileElement> driver) {
		driver.closeApp();
		driver.quit();
	}


}
