package pages.HomePagePages;


import elements.CommonElements;
import io.appium.java_client.MobileElement;
import tests.Base.BaseClass;


public class RepeaterPage extends BaseClass {

	public static String RepeaterType = "Répéteur";
	public static String SettingSectionName = "Réglages";
	public static String ConnectedRepeaterDevicesElementName = "Appareils connectés";
	public static String NoDeviceDescription = "Il n'y a actuellement aucun appareil connecté à Bureau";
	public static String IndicatorLightText = "Voyant lumineux";
	public static String RebootRepeaterButton = "Redémarrer le répéteur";
	public static String RebootRepeaterAlertTitle = "Êtes-vous sûr de vouloir redémarrer ce répéteur ?";
	public static String RebootRepeaterAlertMessage = "La couverture de votre WiFi sera réduite pendant environ 1minute.";
	public static String CancelReboot = "Annuler";
	public static String DeleteRepeaterButton = "Supprimer de mes équipements";
	public static String repeaterWarningButton = "Éloignez ce répéteur";
	public static String MoveRepeaterPageTitle = "Éloignez ce répéteur";
	public static String RepeaterWarningTitle = "Votre répéteur semble trop proche de votre Freebox";
	public static String RepeaterWarningCloseButton = "Fermer";
	public static String RepeaterOthersInformation = "Autres informations";
	public static String RepeaterLogicielVersion = "Version logicielle";
	public static String RepeaterSerialNumber = "Numéro de série";
	public static String RepeaterMACAdress = "Adresse MAC";
	public static String RepeaterIPV4Adress = "Adresse IPv4";
	public static String RepeaterIPV6Adress = "Adresse IPv6";
	public static String RepeaterWiFi2GHz = "WiFi BSSID 2.4GHz";
	public static String RepeaterWiFi5GHz = "WiFi BSSID 5GHz";
	public static String RepeaterTurnOn = "Allumé depuis";
	public static String RoomrepeaterElement = "Pièce";
	public static String RepeaterRoomName = "Bureau";
	public static String RepeaterWarningHelp = "Conseils";
	public static String ImproveMyConnectionTitlePage = "Améliorer ma connexion";
	public static String SetupRepeaterAdviceHeaderFirstPage = "Placez votre répéteur là où vous captez, mais éloigné de votre Freebox";
	public static String SetupRepeaterAdviceHeaderSecondPage = "Tenez votre répéteur à l'écart des gros appareils électroniques";
	public static String SetupRepeaterAdviceHeaderThirdPage = "Placez votre répéteur dans un espace ouvert";
	public static String SetupRepeaterAdviceHeaderFourthPage = "Installez autant de répéteur que nécessaire pour avoir du WiFi partout";
	public static String SetupRepeaterAdviceDescriptionFirstPage = "Vous devez le placer dans une pièce où vous captez le WiFi habituellement, différente de celle où est votre Freebox Server. Dans l'idéal, placez-le à 2 pièces de votre Freebox ou d'un autre répéteur.";
	public static String SetupRepeaterAdviceDescriptionSecondPage = "Évitez de placer votre répéteur trop proche d'une télévision ou d'un ordinateur car le métal bloque le signal WiFi.";
	public static String SetupRepeaterAdviceDescriptionThirdPage = "Afin d'optimiser la performance du WiFi, nous vous recommandons de le placer en hauteur dans un espace ouvert. Evitez de le mettre dans un placard ou un espace clos.";
	public static String SetupRepeaterAdviceDescriptionFourthPage = "Si votre répéteur ne suffit pas pour avoir du WiFi dans toutes vos pièces, vous pouvez en commander plus dans la boutique Free.";
	public static String NextAdviceButton = "Conseil suivant";


	public static MobileElement getSecondEquipment()
	{
		return driver.findElement(CommonElements.Second_equipement);
	}
	public static boolean isSecondEquipmentnDiplayed() {
		return driver.findElement(CommonElements.Second_equipement).isDisplayed();
	}
	public static void clickOnSecondEquipment() {
		driver.findElement(CommonElements.Second_equipement).click();		
	}

	public static MobileElement getThirdEquipment()
	{
		return driver.findElement(CommonElements.Troisieme_equipement);
	}
	public static boolean isThirdEquipmentnDiplayed() {
		return driver.findElement(CommonElements.Troisieme_equipement).isDisplayed();
	}
	public static void clickOnThirdEquipment() {
		driver.findElement(CommonElements.Troisieme_equipement).click();		
	}


	public static MobileElement getSettingSection()
	{
		return driver.findElement(CommonElements.Setting_Section);
	}
	public static String getSettingSectionName() {
		return driver.findElement(CommonElements.Setting_Section).getText();
	}

	public static MobileElement getConnectedRepeaterDevices()
	{
		return driver.findElement(CommonElements.Connected_Repeater_Devices);
	}
	public static boolean isConnectedRepeaterDevicesDiplayed() {
		return driver.findElement(CommonElements.Connected_Repeater_Devices).isDisplayed();
	}
	public static void clickOnConnectedRepeaterDevices() {
		driver.findElement(CommonElements.Connected_Repeater_Devices).click();		
	}
	public static String getConnectedRepeaterDevicesText() {
		return driver.findElement(CommonElements.Connected_Repeater_Devices).getText();
	}	

	public static MobileElement getRepeaterConnectedDevicesNumber()
	{
		return driver.findElement(CommonElements.Repeater_Connected_Devices_Number);
	}
	public static boolean isRepeaterConnectedDevicesNumberDiplayed() {
		return driver.findElement(CommonElements.Repeater_Connected_Devices_Number).isDisplayed();
	}
	public static String getRepeaterConnectedDevicesValue() {
		return driver.findElement(CommonElements.Repeater_Connected_Devices_Number).getText();
	}

	public static MobileElement getNoDeviceDesc()
	{
		return driver.findElement(CommonElements.No_Device_Desc);
	}
	public static boolean isNoDeviceDescDiplayed() {
		return driver.findElement(CommonElements.No_Device_Desc).isDisplayed();
	}
	public static String getNoDeviceDescText() {
		return driver.findElement(CommonElements.No_Device_Desc).getText();
	}	

	public static MobileElement getLightIndicator()
	{
		return driver.findElement(CommonElements.Light_Indicator);
	}
	public static boolean isLightIndicatorDiplayed() {
		return driver.findElement(CommonElements.Light_Indicator).isDisplayed();
	}
	public static String getLightIndicatorText() {
		return driver.findElement(CommonElements.Light_Indicator).getText();
	}	

	public static MobileElement getLightIndicatorSwitch()
	{
		return driver.findElement(CommonElements.Light_Indicator_Switch);
	}
	public static boolean isLightIndicatorSwitchDiplayed() {
		return driver.findElement(CommonElements.Light_Indicator_Switch).isDisplayed();
	}
	public static String getLightIndicatorSwitchChecked() {
		return driver.findElement(CommonElements.Light_Indicator_Switch).getAttribute("checked");
	}	
	public static void clickOnLightIndicatorSwitch() {
		driver.findElement(CommonElements.Light_Indicator_Switch).click();		
	}


	public static MobileElement getRebootRepeaterButton()
	{
		return driver.findElement(CommonElements.RebootRepeaterButton);
	}
	public static boolean isRebootRepeaterButtonDiplayed() {
		return driver.findElement(CommonElements.RebootRepeaterButton).isDisplayed();
	}
	public static String getRebootRepeaterButtonText() {
		return driver.findElement(CommonElements.RebootRepeaterButton).getText();
	}	
	public static void clickOnRebootRepeaterButton() {
		driver.findElement(CommonElements.RebootRepeaterButton).click();		
	}

	public static MobileElement getRebootRepeaterAlertTitle()
	{
		return driver.findElement(CommonElements.Reboot_Repeater_Alert_Title);
	}
	public static boolean isRebootRepeaterAlertTitleDiplayed() {
		return driver.findElement(CommonElements.Reboot_Repeater_Alert_Title).isDisplayed();
	}
	public static String getRebootRepeaterAlertTitleText() {
		return driver.findElement(CommonElements.Reboot_Repeater_Alert_Title).getText();
	}	

	public static MobileElement getRebootRepeaterAlertMessage()
	{
		return driver.findElement(CommonElements.Reboot_Repeater_Alert_Message);
	}
	public static boolean isRebootRepeaterAlertMessageDiplayed() {
		return driver.findElement(CommonElements.Reboot_Repeater_Alert_Message).isDisplayed();
	}
	public static String getRebootRepeaterAlertMessageText() {
		return driver.findElement(CommonElements.Reboot_Repeater_Alert_Message).getText();
	}	

	public static MobileElement getCancelRebootButton()
	{
		return driver.findElement(CommonElements.Cancel_Reboot);
	}
	public static boolean isCancelRebootButtonDiplayed() {
		return driver.findElement(CommonElements.Cancel_Reboot).isDisplayed();
	}
	public static String getCancelRebootButtonText() {
		return driver.findElement(CommonElements.Cancel_Reboot).getText();
	}	
	public static void clickOnCancelRebootButton() {
		driver.findElement(CommonElements.Cancel_Reboot).click();		
	}

	public static MobileElement getDeleteRepeaterButton()
	{
		return driver.findElement(CommonElements.Delete_Repeater_Button);
	}
	public static boolean isDeleteRepeaterButtonDiplayed() {
		return driver.findElement(CommonElements.Delete_Repeater_Button).isDisplayed();
	}
	public static String getDeleteRepeaterButtonText() {
		return driver.findElement(CommonElements.Delete_Repeater_Button).getText();
	}	
	public static void clickOnDeleteRepeaterButton() {
		driver.findElement(CommonElements.Delete_Repeater_Button).click();		
	}


	public static MobileElement getrepeaterWarningButton()
	{
		return driver.findElement(CommonElements.repeater_Warning_Button);
	}
	public static boolean isrepeaterWarningButtonDiplayed() {
		return driver.findElement(CommonElements.repeater_Warning_Button).isDisplayed();
	}
	public static String getrepeaterWarningButtonText() {
		return driver.findElement(CommonElements.repeater_Warning_Button).getText();
	}	
	public static void clickOnrepeaterWarningButton() {
		driver.findElement(CommonElements.repeater_Warning_Button).click();		
	}


	public static MobileElement getMoveRepeaterPageTitle()
	{
		return driver.findElement(CommonElements.Move_Repeater_Page_Title);
	}
	public static boolean isMoveRepeaterPageTitleDiplayed() {
		return driver.findElement(CommonElements.Move_Repeater_Page_Title).isDisplayed();
	}
	public static String getMoveRepeaterPageTitleText() {
		return driver.findElement(CommonElements.Move_Repeater_Page_Title).getText();
	}	


	public static MobileElement getRepeaterWarningImage()
	{
		return driver.findElement(CommonElements.Repeater_Warning_Image);
	}
	public static boolean isRepeaterWarningImageDiplayed() {
		return driver.findElement(CommonElements.Repeater_Warning_Image).isDisplayed();
	}

	public static MobileElement getRepeaterWarningTitle()
	{
		return driver.findElement(CommonElements.Repeater_Warning_Title);
	}
	public static boolean isRepeaterWarningTitleDiplayed() {
		return driver.findElement(CommonElements.Repeater_Warning_Title).isDisplayed();
	}
	public static String getRepeaterWarningTitleText() {
		return driver.findElement(CommonElements.Repeater_Warning_Title).getText();
	}	

	public static MobileElement getRepeaterWarningCloseButton()
	{
		return driver.findElement(CommonElements.Repeater_Warning_Close_Button);
	}
	public static boolean isRepeaterWarningCloseButtonDiplayed() {
		return driver.findElement(CommonElements.Repeater_Warning_Close_Button).isDisplayed();
	}
	public static String getRepeaterWarningCloseButtonText() {
		return driver.findElement(CommonElements.Repeater_Warning_Close_Button).getText();
	}	
	public static void clickOnRepeaterWarningCloseButton() {
		driver.findElement(CommonElements.Repeater_Warning_Close_Button).click();		
	}

	public static MobileElement getRepeaterOthersInformation()
	{
		return driver.findElement(CommonElements.Repeater_Others_Information);
	}
	public static boolean isRepeaterOthersInformationDiplayed() {
		return driver.findElement(CommonElements.Repeater_Others_Information).isDisplayed();
	}
	public static String getRepeaterOthersInformationText() {
		return driver.findElement(CommonElements.Repeater_Others_Information).getText();
	}	
	public static void clickOnRepeaterOthersInformation() {
		driver.findElement(CommonElements.Repeater_Others_Information).click();		
	}

	//*******

	public static MobileElement getRepeaterLogicielVersion()
	{
		return driver.findElement(CommonElements.Repeater_Logiciel_Version);
	}
	public static boolean isRepeaterLogicielVersionDiplayed() {
		return driver.findElement(CommonElements.Repeater_Logiciel_Version).isDisplayed();
	}
	public static String getRepeaterLogicielVersionText() {
		return driver.findElement(CommonElements.Repeater_Logiciel_Version).getText();
	}	

	public static MobileElement getRepeaterSerialNumber()
	{
		return driver.findElement(CommonElements.Repeater_Serial_Number);
	}
	public static boolean isRepeaterSerialNumberDiplayed() {
		return driver.findElement(CommonElements.Repeater_Serial_Number).isDisplayed();
	}
	public static String getRepeaterSerialNumberText() {
		return driver.findElement(CommonElements.Repeater_Serial_Number).getText();
	}	

	public static MobileElement getRepeaterMACAdress()
	{
		return driver.findElement(CommonElements.Repeater_MAC_Adress);
	}
	public static boolean isRepeaterMACAdressDiplayed() {
		return driver.findElement(CommonElements.Repeater_MAC_Adress).isDisplayed();
	}
	public static String getRepeaterMACAdressText() {
		return driver.findElement(CommonElements.Repeater_MAC_Adress).getText();
	}	

	public static MobileElement getRepeaterIPV4Adress()
	{
		return driver.findElement(CommonElements.Repeater_IPV4_Adress);
	}
	public static boolean isRepeaterIPV4AdressDiplayed() {
		return driver.findElement(CommonElements.Repeater_IPV4_Adress).isDisplayed();
	}
	public static String getRepeaterIPV4AdressText() {
		return driver.findElement(CommonElements.Repeater_IPV4_Adress).getText();
	}	

	public static MobileElement getRepeaterIPV6Adress()
	{
		return driver.findElement(CommonElements.Repeater_IPV6_Adress);
	}
	public static boolean isRepeaterIPV6AdressDiplayed() {
		return driver.findElement(CommonElements.Repeater_IPV6_Adress).isDisplayed();
	}
	public static String getRepeaterIPV6AdressText() {
		return driver.findElement(CommonElements.Repeater_IPV6_Adress).getText();
	}	

	public static MobileElement getRepeaterWiFi2GHz()
	{
		return driver.findElement(CommonElements.Repeater_WiFi_2GHz);
	}
	public static boolean isRepeaterWiFi2GHzDiplayed() {
		return driver.findElement(CommonElements.Repeater_WiFi_2GHz).isDisplayed();
	}
	public static String getRepeaterWiFi2GHzText() {
		return driver.findElement(CommonElements.Repeater_WiFi_2GHz).getText();
	}	

	public static MobileElement getRepeaterWiFi5GHz()
	{
		return driver.findElement(CommonElements.Repeater_WiFi_5GHz);
	}
	public static boolean isRepeaterWiFi5GHzDiplayed() {
		return driver.findElement(CommonElements.Repeater_WiFi_5GHz).isDisplayed();
	}
	public static String getRepeaterWiFi5GHzText() {
		return driver.findElement(CommonElements.Repeater_WiFi_5GHz).getText();
	}	

	public static MobileElement getRepeaterTurnOn()
	{
		return driver.findElement(CommonElements.Repeater_Turn_On);
	}
	public static boolean isRepeaterTurnOnDiplayed() {
		return driver.findElement(CommonElements.Repeater_Turn_On).isDisplayed();
	}
	public static String getRepeaterTurnOnText() {
		return driver.findElement(CommonElements.Repeater_Turn_On).getText();
	}	

	public static MobileElement getRoomrepeaterElement()
	{
		return driver.findElement(CommonElements.Room_repeater_Element);
	}
	public static boolean isRoomrepeaterElementDiplayed() {
		return driver.findElement(CommonElements.Room_repeater_Element).isDisplayed();
	}
	public static String getRoomrepeaterElementText() {
		return driver.findElement(CommonElements.Room_repeater_Element).getText();
	}	
	public static void clickOnRoomrepeaterElement() {
		driver.findElement(CommonElements.Room_repeater_Element).click();		
	}


	public static MobileElement getRepeaterRoomName()
	{
		return driver.findElement(CommonElements.Repeater_Room_Name);
	}
	public static boolean isRepeaterRoomNameDiplayed() {
		return driver.findElement(CommonElements.Repeater_Room_Name).isDisplayed();
	}
	public static String getRepeaterRoomNameText() {
		return driver.findElement(CommonElements.Repeater_Room_Name).getText();
	}	

	public static MobileElement getNewRepeaterName()
	{
		return driver.findElement(CommonElements.New_Repeater_Name);
	}
	public static void clickOnNewRepeaterName() {
		driver.findElement(CommonElements.New_Repeater_Name).click();		
	}

	public static MobileElement getOldRepeaterName()
	{
		return driver.findElement(CommonElements.Old_Repeater_Name);
	}
	public static void clickOnOldRepeaterName() {
		driver.findElement(CommonElements.Old_Repeater_Name).click();		
	}

	public static MobileElement getRepeaterWarningHelp()
	{
		return driver.findElement(CommonElements.Repeater_Warning_Help);
	}
	public static boolean isRepeaterWarningHelpDiplayed() {
		return driver.findElement(CommonElements.Repeater_Warning_Help).isDisplayed();
	}
	public static String getRepeaterWarningHelpText() {
		return driver.findElement(CommonElements.Repeater_Warning_Help).getText();
	}	
	public static void clickOnRepeaterWarningHelp() {
		driver.findElement(CommonElements.Repeater_Warning_Help).click();		
	}

	public static MobileElement getImproveMyConnectionTitlePage()
	{
		return driver.findElement(CommonElements.Improve_My_Connection_Title_Page);
	}
	public static boolean isImproveMyConnectionTitlePageDiplayed() {
		return driver.findElement(CommonElements.Improve_My_Connection_Title_Page).isDisplayed();
	}
	public static String getImproveMyConnectionTitlePageText() {
		return driver.findElement(CommonElements.Improve_My_Connection_Title_Page).getText();
	}	

	public static MobileElement getSetupRepeaterAdviceHeader()
	{
		return driver.findElement(CommonElements.Setup_Repeater_Advice_Header);
	}
	public static boolean isSetupRepeaterAdviceHeaderDiplayed() {
		return driver.findElement(CommonElements.Setup_Repeater_Advice_Header).isDisplayed();
	}
	public static String getSetupRepeaterAdviceHeaderText() {
		return driver.findElement(CommonElements.Setup_Repeater_Advice_Header).getText();
	}	

	public static MobileElement getSetupRepeaterAdviceDescription()
	{
		return driver.findElement(CommonElements.Setup_Repeater_Advice_Description);
	}
	public static boolean isSetupRepeaterAdviceDescriptionDiplayed() {
		return driver.findElement(CommonElements.Setup_Repeater_Advice_Description).isDisplayed();
	}
	public static String getSetupRepeaterAdviceDescriptionText() {
		return driver.findElement(CommonElements.Setup_Repeater_Advice_Description).getText();
	}	

	public static MobileElement getNextAdviceButton()
	{
		return driver.findElement(CommonElements.Next_Advice_Button);
	}
	public static boolean isNextAdviceButtonDiplayed() {
		return driver.findElement(CommonElements.Next_Advice_Button).isDisplayed();
	}
	public static String getNextAdviceButtonText() {
		return driver.findElement(CommonElements.Next_Advice_Button).getText();
	}	
	public static void clickOnNextAdviceButton() {
		driver.findElement(CommonElements.Next_Advice_Button).click();		
	}


}






