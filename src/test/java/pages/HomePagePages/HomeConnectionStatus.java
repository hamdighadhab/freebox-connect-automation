package pages.HomePagePages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;



import elements.CommonElements;
import io.appium.java_client.MobileElement;
import tests.Base.BaseClass;

public class HomeConnectionStatus extends BaseClass{

	public static String freeBoxName = "Freebox#Hamdi";
	public static String freeBoxPwd = "freeboxqa90";
	public static void checkStartingApp() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		System.out.println("Start Test");
		Thread.sleep(5000);
		//TouchAction touchAction = new TouchAction(driver);
		//touchAction.tap(PointOption.point(1000, 1000)).perform();
		//touchAction.tap(PointOption.point(1000, 1000)).perform();


		driver.navigate().back();
		driver.navigate().back();

		if (driver.findElement(CommonElements.Bouton_Commencer_Bienvenue).isDisplayed()) {
			wait.until(ExpectedConditions.visibilityOf(CommonElements.getBoutonCommencerBienvenue()));
			driver.findElement(CommonElements.Bouton_Commencer_Bienvenue).click();
			Thread.sleep(2000);


			if (CommonElements.getFirstFree().equals(freeBoxName)) {
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				driver.findElement(CommonElements.Premier_Freebox_Associe).click();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			}else if (CommonElements.getSecondFree().equals(freeBoxName)) {
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				driver.findElement(CommonElements.Deuxieme_Freebox_Associe).click();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			}else if (CommonElements.getThirdFree().equals(freeBoxName)) {
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				driver.findElement(CommonElements.Troixieme_Freebox_Associe).click();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			}

			if (driver.findElement(CommonElements.Box_Password_Field).isDisplayed() == true) {
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				wait.until(ExpectedConditions.visibilityOf(CommonElements.getBoxPasswordField()));
				driver.findElement(CommonElements.Box_Password_Field).click();
				driver.findElement(CommonElements.Box_Password_Field).sendKeys(freeBoxPwd);
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			} 


			if (driver.findElement(CommonElements.ValidatePwdBoxBouton).isDisplayed() == true) {
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				Thread.sleep(2000);
				wait.until(ExpectedConditions.visibilityOf(CommonElements.getValidatePwdBoxBouton()));
				driver.findElement(CommonElements.ValidatePwdBoxBouton).click();
				Thread.sleep(2000);
			} 
		}	
	}
	public static void clickOnConnectionStatusButton() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOf(CommonElements.getConnexionStatusbutton()));
		driver.findElement(CommonElements.Connexion_Status_button).click();		
	}

	public static boolean modeleBox() {
		return driver.findElement(CommonElements.AgregationSwitch).isDisplayed();
	}

	public static MobileElement getQuatriemeFreeboxAssocie()
	{
		return driver.findElement(CommonElements.Quatrieme_equipement);
	}

	public static MobileElement getWanRateDownload()
	{
		return driver.findElement(CommonElements.wan_rate_download);
	}
	public static boolean isWanRateDownloadDiplayed() {
		return driver.findElement(CommonElements.wan_rate_download).isDisplayed();
	}

	public static MobileElement getWanRateUpload()
	{
		return driver.findElement(CommonElements.wan_rate_upload);
	}
	public static boolean isWanRateUploadDiplayed() {
		return driver.findElement(CommonElements.wan_rate_upload).isDisplayed();
	}

}

