package pages.HomePagePages;



import elements.CommonElements;
import io.appium.java_client.MobileElement;
import tests.Base.BaseClass;

public class ShareWifiAccessPage extends BaseClass {

	public static void clickOnShareWiFiButton() {
		driver.findElement(CommonElements.Share_WiFi_button).click();		
	}

	public static boolean isNetworkCardDiplayed() {
		return driver.findElement(CommonElements.Network_Card).isDisplayed();
	}

	public static boolean isShareKeyWifiButtonDiplayed() {
		return driver.findElement(CommonElements.Share_Wifi_Key).isDisplayed();
	}

	public static void clickOnShareWiFiKeyButton() {
		driver.findElement(CommonElements.Share_Wifi_Key).click();		
	}

	public static boolean isShareQRCodeWifiButtonDiplayed() {
		return driver.findElement(CommonElements.Share_Wifi_QRCode).isDisplayed();
	}

	public static void clickOnShareWiFiQRCodeButton() {
		driver.findElement(CommonElements.Share_Wifi_QRCode).click();		
	}

	public static boolean isQRCodeDiplayed() {
		return driver.findElement(CommonElements.QRCode).isDisplayed();
	}

	public static void clickOnGuestWiFiAccessTab() {
		driver.findElement(CommonElements.Guest_WiFi_Access).click();		
	}

	public static boolean isCreateGuestWifiAccessDiplayed() {
		return driver.findElement(CommonElements.Create_Guest_WiFi_Access).isDisplayed();
	}

	public static void clickOnCreateGuestWifiAccessTab() {
		driver.findElement(CommonElements.Create_Guest_WiFi_Access).click();		
	}

	public static boolean isGuestWiFiAccessNameFieldDiplayed() {
		return driver.findElement(CommonElements.Guest_WiFi_Access_Name_Field).isDisplayed();
	}

	public static void clickOnWiFiAccessNameField() {
		driver.findElement(CommonElements.Guest_WiFi_Access_Name_Field).click();
	}


	//public static String GuestName01 = "Anniversaire";
	public static void enterGuestWiFiAccessName() {
		driver.findElement(CommonElements.Guest_WiFi_Access_Name_Field).sendKeys("Anniversaire");		
	}

	public static boolean isValidateGuestWiFiAccessButtonDiplayed() {
		return driver.findElement(CommonElements.Validate_Guest_WiFi_Access_Button).isDisplayed();
	}
	public static MobileElement getValidateGuestWiFiAccessButton()
	{
		return driver.findElement(CommonElements.Validate_Guest_WiFi_Access_Button);
	}

	public static void clickOnValidateGuestWiFiAccessButton() {
		driver.findElement(CommonElements.Validate_Guest_WiFi_Access_Button).click();		
	}

	public static String getGuestWiFiName() {
		return driver.findElement(CommonElements.Guest_WiFi_Access_Added).getText();
	}

	public static MobileElement getDeleteGuestWiFiAccessButton()
	{
		return driver.findElement(CommonElements.Delete_Guest_WiFi_Access_Button);
	}

	public static boolean isDeleteGuestWiFiAccessButtonDiplayed() {
		return driver.findElement(CommonElements.Delete_Guest_WiFi_Access_Button).isDisplayed();
	}

	public static void clickOnDeleteGuestWiFiAccessButton() {
		driver.findElement(CommonElements.Delete_Guest_WiFi_Access_Button).click();		
	}

	public static boolean isShareWifiGuestKeyButtonDiplayed() {
		return driver.findElement(CommonElements.Share_Wifi_Guest_Key).isDisplayed();
	}

	public static void clickOnShareWifiGuestKeyButton() {
		driver.findElement(CommonElements.Share_Wifi_Guest_Key).click();		
	}

	public static boolean isShareWifiGuestQRCodeButtonDiplayed() {
		return driver.findElement(CommonElements.Share_Wifi_Guest_QRCode).isDisplayed();
	}

	public static void clickOnShareWifiGuestQRCodeButton() {
		driver.findElement(CommonElements.Share_Wifi_Guest_QRCode).click();		
	}

	public static MobileElement getComfirmationDeleteGuestWiFiAccess()
	{
		return driver.findElement(CommonElements.ComfirmationDeleteGuestWiFiAccess);
	}

	public static boolean isDComfirmationDeleteGuestWiFiAccessDiplayed() {
		return driver.findElement(CommonElements.ComfirmationDeleteGuestWiFiAccess).isDisplayed();
	}

	public static void clickOnComfirmationDeleteGuestWiFiAccessButton() {
		driver.findElement(CommonElements.ComfirmationDeleteGuestWiFiAccess).click();		
	}
	
	public static MobileElement getWiFiGuestTab()
	{
		return driver.findElement(CommonElements.WiFi_Guest_Tab);
	}

	public static boolean isWiFiGuestTabDiplayed() {
		return driver.findElement(CommonElements.WiFi_Guest_Tab).isDisplayed();
	}

	public static void clickOnWiFiGuestTab() {
		driver.findElement(CommonElements.WiFi_Guest_Tab).click();		
	}

	public static MobileElement getWiFiGuestNote()
	{
		return driver.findElement(CommonElements.WiFi_Guest_Note);
	}

	public static boolean isWiFiGuestNoteDiplayed() {
		return driver.findElement(CommonElements.WiFi_Guest_Note).isDisplayed();
	}

	public static void clickOnWiFiGuestNote() {
		driver.findElement(CommonElements.WiFi_Guest_Note).click();		
	}

	public static MobileElement getWiFiGuestNoteField()
	{
		return driver.findElement(CommonElements.WiFiGuest_Note_Field);
	}

	public static boolean isWiFiGuestNoteFieldDiplayed() {
		return driver.findElement(CommonElements.WiFiGuest_Note_Field).isDisplayed();
	}

	public static void clickOnWiFiGuestNoteField() {
		driver.findElement(CommonElements.WiFiGuest_Note_Field).click();		
	}

	public static void enterGuestWiFiAccessNote() {
		driver.findElement(CommonElements.WiFiGuest_Note_Field).sendKeys("Anniversaire");		
	}

	
	public static MobileElement getWifiNoteValidate()
	{
		return driver.findElement(CommonElements.Wifi_Note_Validate);
	}

	public static boolean isWifiNoteValidateDiplayed() {
		return driver.findElement(CommonElements.Wifi_Note_Validate).isDisplayed();
	}

	public static void clickOnWifiNoteValidate() {
		driver.findElement(CommonElements.Wifi_Note_Validate).click();		
	}



}
