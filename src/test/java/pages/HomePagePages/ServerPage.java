package pages.HomePagePages;

import elements.CommonElements;
import io.appium.java_client.MobileElement;
import tests.Base.BaseClass;

public class ServerPage extends BaseClass {
	public static String ServerStatus = "Connecté";
	public static String DeltaServerType = "Delta";
	public static String DeltaModel = "Freebox Server";
	public static String RebootPageTitle = "Redémarrer le Freebox Server";
	public static String RebootPageHeader = "Vous êtes sur le point de redémarrer votre Freebox Server";
	public static String RebootPageDesc = "Tous les appareils connectés au réseau par câble ou en WiFi, y compris votre Freebox Player si vous en avez un, seront déconnectés pendant quelques minutes.";
	public static String RebootPageHeaderLive = "Votre Freebox Server est en train de redémarrer…";
	public static String RebootPageHeaderDone = "Votre Freebox Server a fini son redémarrage !";
	public static String ConnectedDeltaDevicesElementName = "Appareils connectés";




	public static MobileElement getFirstEquipment()
	{
		return driver.findElement(CommonElements.Premier_equipement);
	}
	public static boolean isFirstEquipmentnDiplayed() {
		return driver.findElement(CommonElements.Premier_equipement).isDisplayed();
	}
	public static void clickOnFirstEquipment() {
		driver.findElement(CommonElements.Premier_equipement).click();		
	}

	public static MobileElement getSecondEquipment()
	{
		return driver.findElement(CommonElements.Deuxieme_equipement);
	}
	public static boolean isSecondEquipmentnDiplayed() {
		return driver.findElement(CommonElements.Deuxieme_equipement).isDisplayed();
	}
	public static void clickOnSecondEquipment() {
		driver.findElement(CommonElements.Deuxieme_equipement).click();		
	}

	public static MobileElement getThirdEquipment()
	{
		return driver.findElement(CommonElements.Troisieme_equipement);
	}
	public static boolean isThirdEquipmentDiplayed() {
		return driver.findElement(CommonElements.Troisieme_equipement).isDisplayed();
	}
	public static void clickOnThirdEquipment() {
		driver.findElement(CommonElements.Troisieme_equipement).click();		
	}

	public static MobileElement getEquipmentDetailStatus()
	{
		return driver.findElement(CommonElements.Equipment_Detail_Status);
	}
	public static boolean isEquipmentDetailStatusDiplayed() {
		return driver.findElement(CommonElements.Equipment_Detail_Status).isDisplayed();
	}
	public static String getEquipmentStatus() {
		return driver.findElement(CommonElements.Equipment_Detail_Status).getText();
	}

	public static MobileElement getEquipmentDetailImage()
	{
		return driver.findElement(CommonElements.Equipment_Detail_Image);
	}
	public static boolean isEquipmentDetailImageDiplayed() {
		return driver.findElement(CommonElements.Equipment_Detail_Image).isDisplayed();
	}


	public static MobileElement getServerInformationsTitle()
	{
		return driver.findElement(CommonElements.Server_Info_Title);
	}
	public static boolean isServerInformationsTitleDiplayed() {
		return driver.findElement(CommonElements.Server_Info_Title).isDisplayed();
	}


	public static MobileElement getTypeInfomationsElement()
	{
		return driver.findElement(CommonElements.Type_Info_Element);
	}
	public static boolean isTypeInfomationsElementDiplayed() {
		return driver.findElement(CommonElements.Type_Info_Element).isDisplayed();
	}

	public static MobileElement getConnectedDeviceInfoElement()
	{
		return driver.findElement(CommonElements.Connected_Device_Info_Element);
	}
	public static boolean isConnectedDeviceInfoElementDiplayed() {
		return driver.findElement(CommonElements.Connected_Device_Info_Element).isDisplayed();
	}

	public static MobileElement getServerType()
	{
		return driver.findElement(CommonElements.Server_Type);
	}
	public static boolean isServerTypeDiplayed() {
		return driver.findElement(CommonElements.Server_Type).isDisplayed();
	}
	public static String getServerTypeValue() {
		return driver.findElement(CommonElements.Server_Type).getText();
	}


	public static MobileElement getDeltaOtherInformations()
	{
		return driver.findElement(CommonElements.Delta_Other_Informations);
	}
	public static boolean isDeltaOtherInformationsDiplayed() {
		return driver.findElement(CommonElements.Delta_Other_Informations).isDisplayed();
	}
	public static void clickOnDeltaOtherInformations() {
		driver.findElement(CommonElements.Delta_Other_Informations).click();		
	}

	public static MobileElement getModeleDelta()
	{
		return driver.findElement(CommonElements.Modele_Delta);
	}
	public static boolean isModeleDeltaDiplayed() {
		return driver.findElement(CommonElements.Modele_Delta).isDisplayed();
	}

	public static MobileElement getVersionLogicielleDelta()
	{
		return driver.findElement(CommonElements.Version_Logicielle_Delta);
	}
	public static boolean isVersionLogicielleDeltaDiplayed() {
		return driver.findElement(CommonElements.Version_Logicielle_Delta).isDisplayed();
	}

	public static MobileElement getNumSerieDelta()
	{
		return driver.findElement(CommonElements.Num_Serie_Delta);
	}
	public static boolean isNumSerieDeltaDiplayed() {
		return driver.findElement(CommonElements.Num_Serie_Delta).isDisplayed();
	}

	public static MobileElement getAdresseMACDelta()
	{
		return driver.findElement(CommonElements.Adresse_MAC_Delta);
	}
	public static boolean isAdresseMACDeltaDiplayed() {
		return driver.findElement(CommonElements.Adresse_MAC_Delta).isDisplayed();
	}

	public static MobileElement getIPV4Delta()
	{
		return driver.findElement(CommonElements.IPV4_Delta);
	}
	public static boolean isIPV4DeltaDiplayed() {
		return driver.findElement(CommonElements.IPV4_Delta).isDisplayed();
	}


	public static MobileElement getIPV6Delta()
	{
		return driver.findElement(CommonElements.IPV6_Delta);
	}
	public static boolean isIPV6DeltaDiplayed() {
		return driver.findElement(CommonElements.IPV6_Delta).isDisplayed();
	}

	public static MobileElement getAllumeDepuisDelta()
	{
		return driver.findElement(CommonElements.Allume_Depuis_Delta);
	}
	public static boolean isAllumeDepuisDeltaDiplayed() {
		return driver.findElement(CommonElements.Allume_Depuis_Delta).isDisplayed();
	}

	public static MobileElement getModeleDeltaValue()
	{
		return driver.findElement(CommonElements.Modele_Delta_Value);
	}
	public static boolean isModeleDeltaValueDiplayed() {
		return driver.findElement(CommonElements.Modele_Delta_Value).isDisplayed();
	}
	public static String getModeleDeltaText() {
		return driver.findElement(CommonElements.Modele_Delta_Value).getText();
	}	


	public static MobileElement getRebootButton()
	{
		return driver.findElement(CommonElements.RebootButoon);
	}
	public static boolean isRebootButtonDiplayed() {
		return driver.findElement(CommonElements.RebootButoon).isDisplayed();
	}
	public static void clickOnRebootButton() {
		driver.findElement(CommonElements.RebootButoon).click();		
	}


	public static MobileElement getServerRebootTitle()
	{
		return driver.findElement(CommonElements.Server_Reboot_Title);
	}
	public static boolean isServerRebootTitleDiplayed() {
		return driver.findElement(CommonElements.Server_Reboot_Title).isDisplayed();
	}
	public static String getServerRebootTitleText() {
		return driver.findElement(CommonElements.Server_Reboot_Title).getText();
	}	


	public static MobileElement getServerRebootHeader()
	{
		return driver.findElement(CommonElements.Server_Reboot_Header);
	}
	public static boolean isServerRebootHeaderDiplayed() {
		return driver.findElement(CommonElements.Server_Reboot_Header).isDisplayed();
	}
	public static String getServerRebootHeaderText() {
		return driver.findElement(CommonElements.Server_Reboot_Header).getText();
	}	


	public static MobileElement getServerRebootDescription()
	{
		return driver.findElement(CommonElements.Server_Reboot_Description);
	}
	public static boolean isServerRebootDescriptionDiplayed() {
		return driver.findElement(CommonElements.Server_Reboot_Description).isDisplayed();
	}
	public static String getServerRebootDescriptionText() {
		return driver.findElement(CommonElements.Server_Reboot_Description).getText();
	}	

	public static MobileElement getServerRebootImage()
	{
		return driver.findElement(CommonElements.Server_Reboot_Image);
	}
	public static boolean isServerRebootImageDiplayed() {
		return driver.findElement(CommonElements.Server_Reboot_Image).isDisplayed();
	}

	public static MobileElement getServerRebootButton2()
	{
		return driver.findElement(CommonElements.Server_Reboot_Button2);
	}
	public static boolean isServerRebootButton2Diplayed() {
		return driver.findElement(CommonElements.Server_Reboot_Button2).isDisplayed();
	}
	public static void clickOnServerRebootButton2() {
		driver.findElement(CommonElements.Server_Reboot_Button2).click();		
	}


	public static MobileElement getServerRebootIndicator()
	{
		return driver.findElement(CommonElements.Server_Reboot_Indicator);
	}
	public static boolean isServerRebootIndicatorDiplayed() {
		return driver.findElement(CommonElements.Server_Reboot_Indicator).isDisplayed();
	}

	public static MobileElement getRebootSpinner()
	{
		return driver.findElement(CommonElements.Reboot_Spinner);
	}
	public static boolean isRebootSpinnerDiplayed() {
		return driver.findElement(CommonElements.Reboot_Spinner).isDisplayed();
	}

	public static MobileElement getEndRebootButton()
	{
		return driver.findElement(CommonElements.End_Reboot_Button);
	}
	public static boolean isEndRebootButtonDiplayed() {
		return driver.findElement(CommonElements.End_Reboot_Button).isDisplayed();
	}
	public static void clickOnEndRebootButton() {
		driver.findElement(CommonElements.End_Reboot_Button).click();		
	}

	public static MobileElement getConnectedDeltaDevices()
	{
		return driver.findElement(CommonElements.Connected_Delta_Devices);
	}
	public static boolean isConnectedDeltaDevicesDiplayed() {
		return driver.findElement(CommonElements.Connected_Delta_Devices).isDisplayed();
	}
	public static void clickOnConnectedDeltaDevices() {
		driver.findElement(CommonElements.Connected_Delta_Devices).click();		
	}
	public static String getConnectedDeltaDevicesText() {
		return driver.findElement(CommonElements.Connected_Delta_Devices).getText();
	}	

	public static MobileElement getConnectedDevicesNumber()
	{
		return driver.findElement(CommonElements.Connected_Devices_Number);
	}
	public static boolean isConnectedDevicesNumberDiplayed() {
		return driver.findElement(CommonElements.Connected_Devices_Number).isDisplayed();
	}
	public static String getConnectedDevicesValue() {
		return driver.findElement(CommonElements.Connected_Devices_Number).getText();
	}

	public static MobileElement getConnectedDevicesPageTitle()
	{
		return driver.findElement(CommonElements.Connected_Devices_Page_Title);
	}
	public static boolean isConnectedDevicesPageTitleDiplayed() {
		return driver.findElement(CommonElements.Connected_Devices_Page_Title).isDisplayed();
	}
	public static String getConnectedDevicesPageTitleText() {
		return driver.findElement(CommonElements.Connected_Devices_Page_Title).getText();
	}	

	public static MobileElement getDeviceName()
	{
		return driver.findElement(CommonElements.Device_Name);
	}
	public static boolean isDeviceNameDiplayed() {
		return driver.findElement(CommonElements.Device_Name).isDisplayed();
	}

	public static MobileElement getDeviceVendor()
	{
		return driver.findElement(CommonElements.Device_Vendor);
	}
	public static boolean isDeviceVendorDiplayed() {
		return driver.findElement(CommonElements.Device_Vendor).isDisplayed();
	}

	public static MobileElement getDeviceImg()
	{
		return driver.findElement(CommonElements.Device_Img);
	}
	public static boolean isDeviceImgDiplayed() {
		return driver.findElement(CommonElements.Device_Img).isDisplayed();
	}

	public static MobileElement getDeviceConnectionType()
	{
		return driver.findElement(CommonElements.Device_Connection_Type);
	}
	public static boolean isDeviceConnectionTypeDiplayed() {
		return driver.findElement(CommonElements.Device_Connection_Type).isDisplayed();
	}

}
