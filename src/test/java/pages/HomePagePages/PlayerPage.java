package pages.HomePagePages;

import elements.CommonElements;
import io.appium.java_client.MobileElement;
import tests.Base.BaseClass;

public class PlayerPage extends BaseClass{


	public static String DeltaPlayerStatus = "Connecté";
	public static String IndormationsPlayerSection = "Informations";
	public static String PlayerConnectionElement = "Connexion";
	public static String PlayerConnectionType = "Ethernet";
	public static String RebootPlayerButton = "Redémarrer le Freebox Player";
	public static String RebootPlayerAlertTitle = "Êtes-vous sûr de vouloir redémarrer votre Player ?";
	public static String OthersInformationsDeltaPlayer = "Autres informations";
	public static String PlayerOtherInformationsTitle = "Autres informations";
	public static String PlayerModelElement = "Modèle";
	public static String DeltaPlayerModelValue = "Delta";
	public static String PlayerLogicielVersionElement = "Version logicielle";
	public static String PlayerSerielNumberElement = "Numéro de série";
	public static String PlayerMACAdressElement = "Adresse MAC";
	public static String PlayerTurnedONElement = "Allumé depuis";


	public static MobileElement getDeltaPlayerStatus()
	{
		return driver.findElement(CommonElements.Delta_Player_Status);
	}
	public static boolean isDeltaPlayerStatusDiplayed() {
		return driver.findElement(CommonElements.Delta_Player_Status).isDisplayed();
	}
	public static String getDeltaPlayerStatusText() {
		return driver.findElement(CommonElements.Delta_Player_Status).getText();
	}	


	public static MobileElement getDeltaPlayerPicto()
	{
		return driver.findElement(CommonElements.Delta_Player_Picto);
	}
	public static boolean isDeltaPlayerPictoDiplayed() {
		return driver.findElement(CommonElements.Delta_Player_Picto).isDisplayed();
	}


	public static MobileElement getIndormationsPlayerSection()
	{
		return driver.findElement(CommonElements.Indormations_Player_Section);
	}
	public static boolean isIndormationsPlayerSectionDiplayed() {
		return driver.findElement(CommonElements.Indormations_Player_Section).isDisplayed();
	}
	public static String getIndormationsPlayerSectionText() {
		return driver.findElement(CommonElements.Indormations_Player_Section).getText();
	}	


	public static MobileElement getPlayerConnectionElement()
	{
		return driver.findElement(CommonElements.Player_Connection_Element);
	}
	public static boolean isPlayerConnectionElementDiplayed() {
		return driver.findElement(CommonElements.Player_Connection_Element).isDisplayed();
	}
	public static String getPlayerConnectionElementText() {
		return driver.findElement(CommonElements.Player_Connection_Element).getText();
	}	


	public static MobileElement getPlayerConnectionType()
	{
		return driver.findElement(CommonElements.Player_Connection_Type);
	}
	public static boolean isPlayerConnectionTypeDiplayed() {
		return driver.findElement(CommonElements.Player_Connection_Type).isDisplayed();
	}
	public static String getPlayerConnectionTypeText() {
		return driver.findElement(CommonElements.Player_Connection_Type).getText();
	}	

	public static MobileElement getRebootPlayerButton()
	{
		return driver.findElement(CommonElements.Reboot_Player_Button);
	}
	public static boolean isRebootPlayerButtonDiplayed() {
		return driver.findElement(CommonElements.Reboot_Player_Button).isDisplayed();
	}
	public static String getRebootPlayerButtonText() {
		return driver.findElement(CommonElements.Reboot_Player_Button).getText();
	}	
	public static void clickOnRebootPlayerButton() {
		driver.findElement(CommonElements.Reboot_Player_Button).click();		
	}

	public static MobileElement getRebootPlayerAlertTitle()
	{
		return driver.findElement(CommonElements.Reboot_Player_Alert_Title);
	}
	public static boolean isRebootPlayerAlertTitleDiplayed() {
		return driver.findElement(CommonElements.Reboot_Player_Alert_Title).isDisplayed();
	}
	public static String getRebootPlayerAlertTitleText() {
		return driver.findElement(CommonElements.Reboot_Player_Alert_Title).getText();
	}	


	public static MobileElement getOthersInformationsDeltaPlayer()
	{
		return driver.findElement(CommonElements.Others_Informations_Delta_Player);
	}
	public static boolean isOthersInformationsDeltaPlayerDiplayed() {
		return driver.findElement(CommonElements.Others_Informations_Delta_Player).isDisplayed();
	}
	public static String getOthersInformationsDeltaPlayerText() {
		return driver.findElement(CommonElements.Others_Informations_Delta_Player).getText();
	}	
	public static void clickOnOthersInformationsDeltaPlayer() {
		driver.findElement(CommonElements.Others_Informations_Delta_Player).click();		
	}

	public static MobileElement getPlayerOtherInformationsTitle()
	{
		return driver.findElement(CommonElements.Player_Other_Informations_Title);
	}
	public static boolean isPlayerOtherInformationsTitleDiplayed() {
		return driver.findElement(CommonElements.Player_Other_Informations_Title).isDisplayed();
	}
	public static String getPlayerOtherInformationsTitleText() {
		return driver.findElement(CommonElements.Player_Other_Informations_Title).getText();
	}	

	public static MobileElement getPlayerModelElement()
	{
		return driver.findElement(CommonElements.Player_Model_Element);
	}
	public static boolean isPlayerModelElementDiplayed() {
		return driver.findElement(CommonElements.Player_Model_Element).isDisplayed();
	}
	public static String getPlayerModelElementText() {
		return driver.findElement(CommonElements.Player_Model_Element).getText();
	}	

	public static MobileElement getDeltaPlayerModelValue()
	{
		return driver.findElement(CommonElements.Delta_Player_Model_Value);
	}
	public static boolean isDeltaPlayerModelValueDiplayed() {
		return driver.findElement(CommonElements.Delta_Player_Model_Value).isDisplayed();
	}
	public static String getDeltaPlayerModelValueText() {
		return driver.findElement(CommonElements.Delta_Player_Model_Value).getText();
	}	


	public static MobileElement getPlayerLogicielVersionElement()
	{
		return driver.findElement(CommonElements.Player_Logiciel_Version_Element);
	}
	public static boolean isPlayerLogicielVersionElementDiplayed() {
		return driver.findElement(CommonElements.Player_Logiciel_Version_Element).isDisplayed();
	}
	public static String getPlayerLogicielVersionElementText() {
		return driver.findElement(CommonElements.Player_Logiciel_Version_Element).getText();
	}	

	public static MobileElement getPlayerSerielNumberElement()
	{
		return driver.findElement(CommonElements.Player_Seriel_Number_Element);
	}
	public static boolean isPlayerSerielNumberElementDiplayed() {
		return driver.findElement(CommonElements.Player_Seriel_Number_Element).isDisplayed();
	}
	public static String getPlayerSerielNumberElementText() {
		return driver.findElement(CommonElements.Player_Seriel_Number_Element).getText();
	}		


	public static MobileElement getPlayerMACAdressElement()
	{
		return driver.findElement(CommonElements.Player_MAC_Adress_Element);
	}
	public static boolean isPlayerMACAdressElementDiplayed() {
		return driver.findElement(CommonElements.Player_MAC_Adress_Element).isDisplayed();
	}
	public static String getPlayerMACAdressElementText() {
		return driver.findElement(CommonElements.Player_MAC_Adress_Element).getText();
	}		

	public static MobileElement getPlayerTurnedONElement()
	{
		return driver.findElement(CommonElements.Player_Turned_ON_Element);
	}
	public static boolean isPlayerTurnedONElementDiplayed() {
		return driver.findElement(CommonElements.Player_Turned_ON_Element).isDisplayed();
	}
	public static String getPlayerTurnedONElementText() {
		return driver.findElement(CommonElements.Player_Turned_ON_Element).getText();
	}		

}
