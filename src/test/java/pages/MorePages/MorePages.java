package pages.MorePages;


import elements.CommonElements;
import io.appium.java_client.MobileElement;
import tests.Base.BaseClass;

public class MorePages extends BaseClass {

	
	public static MobileElement getMoreTabBarButton()
	{
		return driver.findElement(CommonElements.More_TabBar_Button);
	}
	public static boolean isMoreTabBarButtonDiplayed() {
		return driver.findElement(CommonElements.More_TabBar_Button).isDisplayed();
	}
	public static String getMoreTabBarButtonText() {
		return driver.findElement(CommonElements.More_TabBar_Button).getText();
	}	
	public static void clickOnMoreTabBarButton() {
		driver.findElement(CommonElements.More_TabBar_Button).click();		
	}

	public static MobileElement getMorePageTitle()
	{
		return driver.findElement(CommonElements.More_Page_Title);
	}
	public static boolean isMorePageTitleDiplayed() {
		return driver.findElement(CommonElements.More_Page_Title).isDisplayed();
	}
	public static String getMorePageTitleText() {
		return driver.findElement(CommonElements.More_Page_Title).getText();
	}	

	public static MobileElement getFreeboxSectionTitle()
	{
		return driver.findElement(CommonElements.Freebox_Section_Title);
	}
	public static boolean isFreeboxSectionTitleDiplayed() {
		return driver.findElement(CommonElements.Freebox_Section_Title).isDisplayed();
	}
	public static String getFreeboxSectionTitleText() {
		return driver.findElement(CommonElements.Freebox_Section_Title).getText();
	}	

	public static MobileElement getInfosSectionTitle()
	{
		return driver.findElement(CommonElements.Infos_Section_Title);
	}
	public static boolean isInfosSectionTitleDiplayed() {
		return driver.findElement(CommonElements.Infos_Section_Title).isDisplayed();
	}
	public static String getInfosSectionTitleText() {
		return driver.findElement(CommonElements.Infos_Section_Title).getText();
	}	

	public static MobileElement getLockElementTitle()
	{
		return driver.findElement(CommonElements.Lock_Element_Title);
	}
	public static boolean isLockElementTitleDiplayed() {
		return driver.findElement(CommonElements.Lock_Element_Title).isDisplayed();
	}
	public static String getLockElementTitleText() {
		return driver.findElement(CommonElements.Lock_Element_Title).getText();
	}	
	public static void clickOnLockElementTitle() {
		driver.findElement(CommonElements.Lock_Element_Title).click();		
	}


	public static MobileElement getLockPageName()
	{
		return driver.findElement(CommonElements.Lock_Page_Name);
	}
	public static boolean isLockPageNameDiplayed() {
		return driver.findElement(CommonElements.Lock_Page_Name).isDisplayed();
	}
	public static String getLockPageNameText() {
		return driver.findElement(CommonElements.Lock_Page_Name).getText();
	}	
	
	public static MobileElement getLockPageDesc()
	{
		return driver.findElement(CommonElements.Lock_Page_Desc);
	}
	public static boolean isLockPageDescDiplayed() {
		return driver.findElement(CommonElements.Lock_Page_Desc).isDisplayed();
	}
	public static String getLockPageDescText() {
		return driver.findElement(CommonElements.Lock_Page_Desc).getText();
	}	

	public static MobileElement getActivateLockButton()
	{
		return driver.findElement(CommonElements.Activate_Lock_Button);
	}
	public static boolean isActivateLockButtonDiplayed() {
		return driver.findElement(CommonElements.Activate_Lock_Button).isDisplayed();
	}
	public static String getActivateLockButtonText() {
		return driver.findElement(CommonElements.Activate_Lock_Button).getText();
	}	
	public static void clickOnActivateLockButton() {
		driver.findElement(CommonElements.Activate_Lock_Button).click();		
	}


	public static MobileElement getOursAppElement()
	{
		return driver.findElement(CommonElements.Ours_App_Element);
	}
	public static boolean isOursAppElementDiplayed() {
		return driver.findElement(CommonElements.Ours_App_Element).isDisplayed();
	}
	public static String getOursAppElementText() {
		return driver.findElement(CommonElements.Ours_App_Element).getText();
	}	
	public static void clickOnOursAppElement() {
		driver.findElement(CommonElements.Ours_App_Element).click();		
	}
	
	public static MobileElement getOursAppsPageTitle()
	{
		return driver.findElement(CommonElements.OursApps_Page_Title);
	}
	public static boolean isOursAppsPageTitleDiplayed() {
		return driver.findElement(CommonElements.OursApps_Page_Title).isDisplayed();
	}
	public static String getOursAppsPageTitleText() {
		return driver.findElement(CommonElements.OursApps_Page_Title).getText();
	}	

	public static MobileElement getFilesAppName()
	{
		return driver.findElement(CommonElements.Files_App_Name);
	}
	public static boolean isFilesAppNameDiplayed() {
		return driver.findElement(CommonElements.Files_App_Name).isDisplayed();
	}
	public static String getFilesAppNameText() {
		return driver.findElement(CommonElements.Files_App_Name).getText();
	}	

	public static MobileElement getFilesAppDesc()
	{
		return driver.findElement(CommonElements.Files_App_Desc);
	}
	public static boolean isFilesAppDescDiplayed() {
		return driver.findElement(CommonElements.Files_App_Desc).isDisplayed();
	}
	public static String getFilesAppDescText() {
		return driver.findElement(CommonElements.Files_App_Desc).getText();
	}	

	public static MobileElement getHomeAppName()
	{
		return driver.findElement(CommonElements.Home_App_Name);
	}
	public static boolean isHomeAppNameDiplayed() {
		return driver.findElement(CommonElements.Home_App_Name).isDisplayed();
	}
	public static String getHomeAppNameText() {
		return driver.findElement(CommonElements.Home_App_Name).getText();
	}	

	public static MobileElement getHomeAppDesc()
	{
		return driver.findElement(CommonElements.Home_App_Desc);
	}
	public static boolean isHomeAppDescDiplayed() {
		return driver.findElement(CommonElements.Home_App_Desc).isDisplayed();
	}
	public static String getHomeAppDescText() {
		return driver.findElement(CommonElements.Home_App_Desc).getText();
	}	

	public static MobileElement getAboutAppElement()
	{
		return driver.findElement(CommonElements.About_App_Element);
	}
	public static boolean isAboutAppElementDiplayed() {
		return driver.findElement(CommonElements.About_App_Element).isDisplayed();
	}
	public static String getAboutAppElementText() {
		return driver.findElement(CommonElements.About_App_Element).getText();
	}	
	public static void clickOnAboutAppElement() {
		driver.findElement(CommonElements.About_App_Element).click();		
	}
	
	public static MobileElement getAboutPageName()
	{
		return driver.findElement(CommonElements.About_Page_Name);
	}
	public static boolean isAboutPageNameDiplayed() {
		return driver.findElement(CommonElements.About_Page_Name).isDisplayed();
	}
	public static String getAboutPageNameText() {
		return driver.findElement(CommonElements.About_Page_Name).getText();
	}	

	public static MobileElement getLegalNoticeElement()
	{
		return driver.findElement(CommonElements.Legal_Notice_Element);
	}
	public static boolean isLegalNoticeElementDiplayed() {
		return driver.findElement(CommonElements.Legal_Notice_Element).isDisplayed();
	}
	public static String getLegalNoticeElementText() {
		return driver.findElement(CommonElements.Legal_Notice_Element).getText();
	}	

	public static MobileElement getUsedLibraryElement()
	{
		return driver.findElement(CommonElements.Used_Library_Element);
	}
	public static boolean isUsedLibraryElementDiplayed() {
		return driver.findElement(CommonElements.Used_Library_Element).isDisplayed();
	}
	public static String getUsedLibraryElementText() {
		return driver.findElement(CommonElements.Used_Library_Element).getText();
	}	
	public static void clickOnUsedLibraryElement() {
		driver.findElement(CommonElements.Used_Library_Element).click();		
	}

	public static MobileElement getUsedLibraryPage()
	{
		return driver.findElement(CommonElements.Used_Library_Page);
	}
	public static boolean isUsedLibraryPageDiplayed() {
		return driver.findElement(CommonElements.Used_Library_Page).isDisplayed();
	}
	public static String getUsedLibraryPageText() {
		return driver.findElement(CommonElements.Used_Library_Page).getText();
	}	

}
