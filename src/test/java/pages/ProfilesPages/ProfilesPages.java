package pages.ProfilesPages;



import elements.CommonElements;
import io.appium.java_client.MobileElement;
import tests.Base.BaseClass;

public class ProfilesPages extends BaseClass{

	public static String ProfilesTabBarButton = "Profils";
	public static String EmptyProfile = "0 profil";
	public static String ProfilesEmptyListDesc = "Créez des profils pour les membres de votre famille pour pouvoir mettre en pause internet et programmer des pauses planifiées sur tous leurs appareils.";
	public static String ProfilesEmptyListButton = "Créer mon premier profil";
	public static String NewProfilePage = "Nouveau profil";
	public static String ProfileNameDesc = "Saisissez un nom pour ce profil";
	public static String ProfileIconDesc = "Choisissez une icône pour ce profil";
	public static String FirstProfile = "FirstProfile";
	public static String NextButtonNewProfiles = "SUIVANT";
	public static String AssociateDevicesPage = "Appareils (0)";
	public static String OneProfile = "1 profil";
	public static String AssocietedDeviceNumber = "0";
	public static String WithoutBreakProfilStatus = "Aucune pause à venir";
	public static String DetailsProfilButton = "Détails";
	public static String ProfilesAssociatedDeviceElement = "Appareils associés";
	public static String NoDeviceButton = "Ajouter des appareils";
	public static String OneAssocietedDeviceNumber = "1";
	public static String ProfileHolidaySchoolElement = "Vacances scolaires";
	public static String HolidayZoneAProfile = "Vacances scolaires zone A";
	public static String HolidayZoneBProfile = "Vacances scolaires zone B";
	public static String HolidayZoneCProfile = "Vacances scolaires zone C";
	public static String HolidayCorseProfile = "Vacances scolaires zone Corse";
	public static String EditProfileButton = "MODIFIER";
	public static String PauseProfileButton = "Mettre en pause";
	public static String Pause30Min = "30 minutes";
	public static String Pause1Hour = "1 heure";
	public static String Pause2Hours = "2 heures";
	public static String PauseInfinite = "Jusqu'à ce que j'arrête la pause";
	public static String ApplicatePauseButton = "Appliquer";
	public static String SetPauseProfileButton = "Mettre en pause";

	
	public static MobileElement getProfilesTabBarButton()
	{
		return driver.findElement(CommonElements.Profiles_TabBar_Button);
	}
	public static boolean isProfilesTabBarButtonDiplayed() {
		return driver.findElement(CommonElements.Profiles_TabBar_Button).isDisplayed();
	}
	public static String getProfilesTabBarButtonText() {
		return driver.findElement(CommonElements.Profiles_TabBar_Button).getText();
	}	
	public static void clickOnProfilesTabBarButton() {
		driver.findElement(CommonElements.Profiles_TabBar_Button).click();		
	}

	public static MobileElement getProfileNumber()
	{
		return driver.findElement(CommonElements.Profile_Number);
	}
	public static boolean isProfileNumberDiplayed() {
		return driver.findElement(CommonElements.Profile_Number).isDisplayed();
	}
	public static String getProfileNumberText() {
		return driver.findElement(CommonElements.Profile_Number).getText();
	}	


	public static MobileElement getProfilesEmptyListImage()
	{
		return driver.findElement(CommonElements.Profiles_EmptyList_Image);
	}
	public static boolean isProfilesEmptyListImageDiplayed() {
		return driver.findElement(CommonElements.Profiles_EmptyList_Image).isDisplayed();
	}	

	public static MobileElement getProfilesEmptyListDesc()
	{
		return driver.findElement(CommonElements.Profiles_EmptyList_Desc);
	}
	public static boolean isProfilesEmptyListDescDiplayed() {
		return driver.findElement(CommonElements.Profiles_EmptyList_Desc).isDisplayed();
	}
	public static String getProfilesEmptyListDescText() {
		return driver.findElement(CommonElements.Profiles_EmptyList_Desc).getText();
	}	

	public static MobileElement getProfilesEmptyListButton()
	{
		return driver.findElement(CommonElements.Profiles_EmptyList_Button);
	}
	public static boolean isProfilesEmptyListButtonDiplayed() {
		return driver.findElement(CommonElements.Profiles_EmptyList_Button).isDisplayed();
	}
	public static String getProfilesEmptyListButtonText() {
		return driver.findElement(CommonElements.Profiles_EmptyList_Button).getText();
	}
	public static void clickOnProfilesEmptyListButton() {
		driver.findElement(CommonElements.Profiles_EmptyList_Button).click();		
	}

	public static MobileElement getNewProfilePage()
	{
		return driver.findElement(CommonElements.New_Profile_Page);
	}
	public static boolean isNewProfilePageDiplayed() {
		return driver.findElement(CommonElements.New_Profile_Page).isDisplayed();
	}
	public static String getNewProfilePageText() {
		return driver.findElement(CommonElements.New_Profile_Page).getText();
	}

	public static MobileElement getProfileNameDesc()
	{
		return driver.findElement(CommonElements.Profile_Name_Desc);
	}
	public static boolean isProfileNameDescDiplayed() {
		return driver.findElement(CommonElements.Profile_Name_Desc).isDisplayed();
	}
	public static String getProfileNameDescText() {
		return driver.findElement(CommonElements.Profile_Name_Desc).getText();
	}
	public static MobileElement getProfileIconDesc()
	{
		return driver.findElement(CommonElements.Profile_Icon_Desc);
	}
	public static boolean isProfileIconDescDiplayed() {
		return driver.findElement(CommonElements.Profile_Icon_Desc).isDisplayed();
	}
	public static String getProfileIconDescText() {
		return driver.findElement(CommonElements.Profile_Icon_Desc).getText();
	}

	public static void enterProfileNameField(String ProfileName) {
		driver.findElement(CommonElements.Profile_Name_Field).sendKeys(ProfileName);		
	}
	public static MobileElement getProfileSecondIconSelect()
	{
		return driver.findElement(CommonElements.Profile_Second_Icon_Select);
	}
	public static boolean isProfileSecondIconSelectDiplayed() {
		return driver.findElement(CommonElements.Profile_Second_Icon_Select).isDisplayed();
	}
	public static void clickOnProfileSecondIconSelectButton() {
		driver.findElement(CommonElements.Profile_Second_Icon_Select).click();		
	}
	
	public static MobileElement getNextButtonNewProfiles()
	{
		return driver.findElement(CommonElements.Next_Button_New_Profiles);
	}
	public static boolean isNextButtonNewProfilesDiplayed() {
		return driver.findElement(CommonElements.Next_Button_New_Profiles).isDisplayed();
	}
	public static String getNextButtonNewProfilesText() {
		return driver.findElement(CommonElements.Next_Button_New_Profiles).getText();
	}
	public static void clickOnNextButtonNewProfiles() {
		driver.findElement(CommonElements.Next_Button_New_Profiles).click();		
	}
	
	public static MobileElement getAssociateDevicesPage()
	{
		return driver.findElement(CommonElements.Associate_Devices_Page);
	}
	public static boolean isAssociateDevicesPageDiplayed() {
		return driver.findElement(CommonElements.Associate_Devices_Page).isDisplayed();
	}
	public static String getAssociateDevicesPageText() {
		return driver.findElement(CommonElements.Associate_Devices_Page).getText();
	}
	
	public static MobileElement getProfileListItemName()
	{
		return driver.findElement(CommonElements.profile_ListItem_Name);
	}
	public static boolean isProfileListItemNameDiplayed() {
		return driver.findElement(CommonElements.profile_ListItem_Name).isDisplayed();
	}
	public static String getProfileListItemNameText() {
		return driver.findElement(CommonElements.profile_ListItem_Name).getText();
	}
	
	public static MobileElement getAssocietedDeviceNumber()
	{
		return driver.findElement(CommonElements.Associeted_Device_Number);
	}
	public static boolean isAssocietedDeviceNumberDiplayed() {
		return driver.findElement(CommonElements.Associeted_Device_Number).isDisplayed();
	}
	public static String getAssocietedDeviceNumberText() {
		return driver.findElement(CommonElements.Associeted_Device_Number).getText();
	}
	
	public static MobileElement getProfilStatus()
	{
		return driver.findElement(CommonElements.Profil_Status);
	}
	public static boolean isProfilStatusDiplayed() {
		return driver.findElement(CommonElements.Profil_Status).isDisplayed();
	}
	public static String getProfilStatusText() {
		return driver.findElement(CommonElements.Profil_Status).getText();
	}

	public static MobileElement getDetailsProfilButton()
	{
		return driver.findElement(CommonElements.Details_Profil_Button);
	}
	public static boolean isDetailsProfilButtonDiplayed() {
		return driver.findElement(CommonElements.Details_Profil_Button).isDisplayed();
	}
	public static String getDetailsProfilButtonText() {
		return driver.findElement(CommonElements.Details_Profil_Button).getText();
	}
	public static void clickOnDetailsProfilButton() {
		driver.findElement(CommonElements.Details_Profil_Button).click();		
	}

	
	public static MobileElement getProfilesAssociatedDeviceElement()
	{
		return driver.findElement(CommonElements.Profiles_Associated_Device_Element);
	}
	public static boolean isProfilesAssociatedDeviceElementDiplayed() {
		return driver.findElement(CommonElements.Profiles_Associated_Device_Element).isDisplayed();
	}
	public static String getProfilesAssociatedDeviceElementText() {
		return driver.findElement(CommonElements.Profiles_Associated_Device_Element).getText();
	}
	public static void clickOnProfilesAssociatedDeviceElement() {
		driver.findElement(CommonElements.Profiles_Associated_Device_Element).click();		
	}

	public static MobileElement getProfilesAssociatedDeviceNumber()
	{
		return driver.findElement(CommonElements.Profiles_Associated_Device_Number);
	}
	public static boolean isProfilesAssociatedDeviceNumberDiplayed() {
		return driver.findElement(CommonElements.Profiles_Associated_Device_Number).isDisplayed();
	}
	public static String getProfilesAssociatedDeviceNumberText() {
		return driver.findElement(CommonElements.Profiles_Associated_Device_Number).getText();
	}
	
	public static MobileElement getNoDeviceButton()
	{
		return driver.findElement(CommonElements.No_Device_Button);
	}
	public static boolean isNoDeviceButtonDiplayed() {
		return driver.findElement(CommonElements.No_Device_Button).isDisplayed();
	}
	public static String getNoDeviceButtonText() {
		return driver.findElement(CommonElements.No_Device_Button).getText();
	}
	public static void clickOnNoDeviceButton() {
		driver.findElement(CommonElements.No_Device_Button).click();		
	}
	
	public static MobileElement getFirstDeviceProfile()
	{
		return driver.findElement(CommonElements.First_Device_Profile);
	}
	public static void clickOnFirstDeviceProfile() {
		driver.findElement(CommonElements.First_Device_Profile).click();		
	}

	public static MobileElement getValidateDeviceAssociated()
	{
		return driver.findElement(CommonElements.Validate_Device_Associated);
	}
	public static void clickOnValidateDeviceAssociated() {
		driver.findElement(CommonElements.Validate_Device_Associated).click();		
	}

	public static MobileElement getProfileHolidaySchoolElement()
	{
		return driver.findElement(CommonElements.Profile_Holiday_School_Element);
	}
	public static boolean isProfileHolidaySchoolElementDiplayed() {
		return driver.findElement(CommonElements.Profile_Holiday_School_Element).isDisplayed();
	}
	public static String getProfileHolidaySchoolElementText() {
		return driver.findElement(CommonElements.Profile_Holiday_School_Element).getText();
	}
	public static void clickOnProfileHolidaySchoolElement() {
		driver.findElement(CommonElements.Profile_Holiday_School_Element).click();		
	}
	
	
	public static MobileElement getProfileHolidaySchoolValue()
	{
		return driver.findElement(CommonElements.Profile_Holiday_School_Value);
	}
	public static boolean isProfileHolidaySchoolValueDiplayed() {
		return driver.findElement(CommonElements.Profile_Holiday_School_Value).isDisplayed();
	}
	public static String getProfileHolidaySchoolValueText() {
		return driver.findElement(CommonElements.Profile_Holiday_School_Value).getText();
	}

	public static MobileElement getHolidayZoneAProfile()
	{
		return driver.findElement(CommonElements.Holiday_ZoneA_Profile);
	}
	public static boolean isHolidayZoneAProfileDiplayed() {
		return driver.findElement(CommonElements.Holiday_ZoneA_Profile).isDisplayed();
	}
	public static String getHolidayZoneAProfileText() {
		return driver.findElement(CommonElements.Holiday_ZoneA_Profile).getText();
	}
	public static void clickOnHolidayZoneAProfile() {
		driver.findElement(CommonElements.Holiday_ZoneA_Profile).click();		
	}
	
	public static MobileElement getHolidayZoneBProfile()
	{
		return driver.findElement(CommonElements.Holiday_ZoneB_Profile);
	}
	public static boolean isHolidayZoneBProfileDiplayed() {
		return driver.findElement(CommonElements.Holiday_ZoneB_Profile).isDisplayed();
	}
	public static String getHolidayZoneBProfileText() {
		return driver.findElement(CommonElements.Holiday_ZoneB_Profile).getText();
	}
	public static void clickOnHolidayZoneBProfile() {
		driver.findElement(CommonElements.Holiday_ZoneB_Profile).click();		
	}
	
	public static MobileElement getHolidayZoneCProfile()
	{
		return driver.findElement(CommonElements.Holiday_ZoneC_Profile);
	}
	public static boolean isHolidayZoneCProfileDiplayed() {
		return driver.findElement(CommonElements.Holiday_ZoneC_Profile).isDisplayed();
	}
	public static String getHolidayZoneCProfileText() {
		return driver.findElement(CommonElements.Holiday_ZoneC_Profile).getText();
	}
	public static void clickOnHolidayZoneCProfile() {
		driver.findElement(CommonElements.Holiday_ZoneC_Profile).click();		
	}

	public static MobileElement getHolidayCorseProfile()
	{
		return driver.findElement(CommonElements.Holiday_Corse_Profile);
	}
	public static boolean isHolidayCorseProfileDiplayed() {
		return driver.findElement(CommonElements.Holiday_Corse_Profile).isDisplayed();
	}
	public static String getHolidayCorseProfileText() {
		return driver.findElement(CommonElements.Holiday_Corse_Profile).getText();
	}
	public static void clickOnHolidayCorseProfile() {
		driver.findElement(CommonElements.Holiday_Corse_Profile).click();		
	}
	
	public static MobileElement getHolidayAucuneProfile()
	{
		return driver.findElement(CommonElements.Holiday_Aucune_Profile);
	}
	public static boolean isHolidayAucuneProfileDiplayed() {
		return driver.findElement(CommonElements.Holiday_Aucune_Profile).isDisplayed();
	}
	public static String getHolidayAucuneProfileText() {
		return driver.findElement(CommonElements.Holiday_Aucune_Profile).getText();
	}
	public static void clickOnHolidayAucuneProfile() {
		driver.findElement(CommonElements.Holiday_Aucune_Profile).click();		
	}

	
	public static MobileElement getEditProfileButton()
	{
		return driver.findElement(CommonElements.Edit_Profile_Button);
	}
	public static boolean isEditProfileButtonDiplayed() {
		return driver.findElement(CommonElements.Edit_Profile_Button).isDisplayed();
	}
	public static String getEditProfileButtonText() {
		return driver.findElement(CommonElements.Edit_Profile_Button).getText();
	}
	public static void clickOnEditProfileButton() {
		driver.findElement(CommonElements.Edit_Profile_Button).click();		
	}

	public static MobileElement getPauseProfileButton()
	{
		return driver.findElement(CommonElements.Pause_Profile_Button);
	}
	public static boolean isPauseProfileButtonDiplayed() {
		return driver.findElement(CommonElements.Pause_Profile_Button).isDisplayed();
	}
	public static String getPauseProfileButtonText() {
		return driver.findElement(CommonElements.Pause_Profile_Button).getText();
	}
	public static void clickOnPauseProfileButton() {
		driver.findElement(CommonElements.Pause_Profile_Button).click();		
	}

	
	public static MobileElement getProfileStatus()
	{
		return driver.findElement(CommonElements.Profile_Status);
	}
	public static boolean isProfileStatusDiplayed() {
		return driver.findElement(CommonElements.Profile_Status).isDisplayed();
	}
	public static String getProfileStatusText() {
		return driver.findElement(CommonElements.Profile_Status).getText();
	}
	public static void clickOnProfileStatus() {
		driver.findElement(CommonElements.Profile_Status).click();		
	}

	public static MobileElement getPause30Min()
	{
		return driver.findElement(CommonElements.Pause_30Min);
	}
	public static boolean isPause30MinDiplayed() {
		return driver.findElement(CommonElements.Pause_30Min).isDisplayed();
	}
	public static String getPause30MinText() {
		return driver.findElement(CommonElements.Pause_30Min).getText();
	}
	public static void clickOnPause30Min() {
		driver.findElement(CommonElements.Pause_30Min).click();		
	}
	
	public static MobileElement getPause1Hour()
	{
		return driver.findElement(CommonElements.Pause_1Hour);
	}
	public static boolean isPause1HourDiplayed() {
		return driver.findElement(CommonElements.Pause_1Hour).isDisplayed();
	}
	public static String getPause1HourText() {
		return driver.findElement(CommonElements.Pause_1Hour).getText();
	}
	public static void clickOnPause1Hour() {
		driver.findElement(CommonElements.Pause_1Hour).click();		
	}

	public static MobileElement getPause2Hours()
	{
		return driver.findElement(CommonElements.Pause_2Hours);
	}
	public static boolean isPause2HoursDiplayed() {
		return driver.findElement(CommonElements.Pause_2Hours).isDisplayed();
	}
	public static String getPause2HoursText() {
		return driver.findElement(CommonElements.Pause_2Hours).getText();
	}
	public static void clickOnPause2Hours() {
		driver.findElement(CommonElements.Pause_2Hours).click();		
	}

	public static MobileElement getPauseInfinite()
	{
		return driver.findElement(CommonElements.Pause_Infinite);
	}
	public static boolean isPauseInfiniteDiplayed() {
		return driver.findElement(CommonElements.Pause_Infinite).isDisplayed();
	}
	public static String getPauseInfiniteText() {
		return driver.findElement(CommonElements.Pause_Infinite).getText();
	}
	public static void clickOnPauseInfinite() {
		driver.findElement(CommonElements.Pause_Infinite).click();		
	}

	public static MobileElement getApplicatePauseButton()
	{
		return driver.findElement(CommonElements.Applicate_Pause_Button);
	}
	public static boolean isApplicatePauseButtonDiplayed() {
		return driver.findElement(CommonElements.Applicate_Pause_Button).isDisplayed();
	}
	public static String getApplicatePauseButtonText() {
		return driver.findElement(CommonElements.Applicate_Pause_Button).getText();
	}
	public static void clickOnApplicatePauseButton() {
		driver.findElement(CommonElements.Applicate_Pause_Button).click();		
	}

	public static MobileElement getSetPauseProfileButton()
	{
		return driver.findElement(CommonElements.Set_Pause_Profile_Button);
	}
	public static boolean isSetPauseProfileButtonDiplayed() {
		return driver.findElement(CommonElements.Set_Pause_Profile_Button).isDisplayed();
	}
	public static String getSetPauseProfileButtonText() {
		return driver.findElement(CommonElements.Set_Pause_Profile_Button).getText();
	}
	public static void clickOnSetPauseProfileButton() {
		driver.findElement(CommonElements.Set_Pause_Profile_Button).click();		
	}

	public static MobileElement getDetailProfileStatus()
	{
		return driver.findElement(CommonElements.Detail_Profile_Status);
	}
	public static boolean isDetailProfileStatusDiplayed() {
		return driver.findElement(CommonElements.Detail_Profile_Status).isDisplayed();
	}
	public static String getDetailProfileStatusText() {
		return driver.findElement(CommonElements.Detail_Profile_Status).getText();
	}

	public static MobileElement getProfileDeleteButton()
	{
		return driver.findElement(CommonElements.Profile_Delete_Button);
	}
	public static boolean isProfileDeleteButtonDiplayed() {
		return driver.findElement(CommonElements.Profile_Delete_Button).isDisplayed();
	}
	public static String getProfileDeleteButtonText() {
		return driver.findElement(CommonElements.Profile_Delete_Button).getText();
	}
	public static void clickOnProfileDeleteButton() {
		driver.findElement(CommonElements.Profile_Delete_Button).click();		
	}

	public static MobileElement getProfileConfirmDeleteButton()
	{
		return driver.findElement(CommonElements.Profile_ConfirmDelete_Button);
	}
	public static boolean isProfileConfirmDeleteButtonDiplayed() {
		return driver.findElement(CommonElements.Profile_ConfirmDelete_Button).isDisplayed();
	}
	public static String getProfileConfirmDeleteButtonText() {
		return driver.findElement(CommonElements.Profile_ConfirmDelete_Button).getText();
	}
	public static void clickOnProfileConfirmDeleteButton() {
		driver.findElement(CommonElements.Profile_ConfirmDelete_Button).click();		
	}

	public static MobileElement getScheduledBreakButton()
	{
		return driver.findElement(CommonElements.Scheduled_Break_Button);
	}
	public static boolean isScheduledBreakButtonDiplayed() {
		return driver.findElement(CommonElements.Scheduled_Break_Button).isDisplayed();
	}
	public static String getScheduledBreakButtonText() {
		return driver.findElement(CommonElements.Scheduled_Break_Button).getText();
	}
	public static void clickOnScheduledBreakButton() {
		driver.findElement(CommonElements.Scheduled_Break_Button).click();		
	}

	public static MobileElement getNewBreakPageTitle()
	{
		return driver.findElement(CommonElements.NewBreak_Page_Title);
	}
	public static boolean isNewBreakPageTitleDiplayed() {
		return driver.findElement(CommonElements.NewBreak_Page_Title).isDisplayed();
	}
	public static String getNewBreakPageTitleText() {
		return driver.findElement(CommonElements.NewBreak_Page_Title).getText();
	}

	public static MobileElement getBreakNameElement()
	{
		return driver.findElement(CommonElements.Break_Name_Element);
	}
	public static boolean isBreakNameElementDiplayed() {
		return driver.findElement(CommonElements.Break_Name_Element).isDisplayed();
	}
	public static String getBreakNameElementText() {
		return driver.findElement(CommonElements.Break_Name_Element).getText();
	}

	public static MobileElement getHourStartElement()
	{
		return driver.findElement(CommonElements.Hour_Start_Element);
	}
	public static boolean isHourStartElementDiplayed() {
		return driver.findElement(CommonElements.Hour_Start_Element).isDisplayed();
	}
	public static String getHourStartElementText() {
		return driver.findElement(CommonElements.Hour_Start_Element).getText();
	}

	public static MobileElement getHourEndElement()
	{
		return driver.findElement(CommonElements.Hour_End_Element);
	}
	public static boolean isHourEndElementDiplayed() {
		return driver.findElement(CommonElements.Hour_End_Element).isDisplayed();
	}
	public static String getHourEndElementText() {
		return driver.findElement(CommonElements.Hour_End_Element).getText();
	}

	public static MobileElement getWeekDaysElement()
	{
		return driver.findElement(CommonElements.Week_Days_Element);
	}
	public static boolean isWeekDaysElementDiplayed() {
		return driver.findElement(CommonElements.Week_Days_Element).isDisplayed();
	}
	public static String getWeekDaysElementText() {
		return driver.findElement(CommonElements.Week_Days_Element).getText();
	}

	public static MobileElement getProfilePauseEditSummary()
	{
		return driver.findElement(CommonElements.ProfilePause_Edit_Summary);
	}
	public static boolean isProfilePauseEditSummaryDiplayed() {
		return driver.findElement(CommonElements.ProfilePause_Edit_Summary).isDisplayed();
	}
	public static String getProfilePauseEditSummaryText() {
		return driver.findElement(CommonElements.ProfilePause_Edit_Summary).getText();
	}

	public static MobileElement getPauseEditFriday()
	{
		return driver.findElement(CommonElements.Pause_Edit_Friday);
	}
	public static boolean isPauseEditFridayDiplayed() {
		return driver.findElement(CommonElements.Pause_Edit_Friday).isDisplayed();
	}
	public static String getPauseEditFridayText() {
		return driver.findElement(CommonElements.Pause_Edit_Friday).getText();
	}
	public static void clickOnPauseEditFriday() {
		driver.findElement(CommonElements.Pause_Edit_Friday).click();		
	}
	
	public static MobileElement getPauseEditSaturday()
	{
		return driver.findElement(CommonElements.Pause_Edit_Saturday);
	}
	public static boolean isPauseEditSaturdayDiplayed() {
		return driver.findElement(CommonElements.Pause_Edit_Saturday).isDisplayed();
	}
	public static String getPauseEditSaturdayText() {
		return driver.findElement(CommonElements.Pause_Edit_Saturday).getText();
	}
	public static void clickOnPauseEditSaturday() {
		driver.findElement(CommonElements.Pause_Edit_Saturday).click();		
	}

	public static void enterScheduledPauseNameField(String ScheduledPauseName) {
		driver.findElement(CommonElements.Scheduled_Pause_Name_Field).sendKeys(ScheduledPauseName);		
	}
	
	public static void clickOnValidateScheduledPause() {
		driver.findElement(CommonElements.Validate_Scheduled_Pause).click();		
	}

	public static MobileElement getScheduledPauseOFF()
	{
		return driver.findElement(CommonElements.Scheduled_Pause_OFF);
	}

	public static void clickOnScheduledPauseOFF() {
		driver.findElement(CommonElements.Scheduled_Pause_OFF).click();		
	}

	public static MobileElement getScheduledPauseItem()
	{
		return driver.findElement(CommonElements.Scheduled_Pause_Item);
	}

	public static void clickOnScheduledPauseItem() {
		driver.findElement(CommonElements.Scheduled_Pause_Item).click();		
	}
	
	public static MobileElement getScheduledPauseDeleteButton()
	{
		return driver.findElement(CommonElements.Scheduled_Pause_Delete_Button);
	}
	public static boolean isScheduledPauseDeleteButtonDiplayed() {
		return driver.findElement(CommonElements.Scheduled_Pause_Delete_Button).isDisplayed();
	}
	public static String getScheduledPauseDeleteButtonText() {
		return driver.findElement(CommonElements.Scheduled_Pause_Delete_Button).getText();
	}
	public static void clickOnScheduledPauseDeleteButton() {
		driver.findElement(CommonElements.Scheduled_Pause_Delete_Button).click();		
	}

	public static MobileElement getScheduledPauseDeleteConfirmationButton()
	{
		return driver.findElement(CommonElements.Scheduled_Pause_Delete_ConfirmationButton);
	}

	public static void clickOnScheduledPauseDeleteConfirmationButton() {
		driver.findElement(CommonElements.Scheduled_Pause_Delete_ConfirmationButton).click();		
	}



		
}
