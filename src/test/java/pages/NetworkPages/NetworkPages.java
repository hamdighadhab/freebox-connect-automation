package pages.NetworkPages;


import elements.CommonElements;
import io.appium.java_client.MobileElement;
import tests.Base.BaseClass;

public class NetworkPages extends BaseClass {

	public static String DiagSecutityDesc = "Le mode de sécurité actuel de votre WiFi est WPA2 - AES.\n Il s'agit du protocole le plus sécurisé, fiable et performant pour votre réseau.";

	public static MobileElement getNetworkTabBarButton()
	{
		return driver.findElement(CommonElements.Network_TabBar_Button);
	}
	public static boolean isNetworkTabBarButtonDiplayed() {
		return driver.findElement(CommonElements.Network_TabBar_Button).isDisplayed();
	}
	public static void clickOnNetworkTabBarButton() {
		driver.findElement(CommonElements.Network_TabBar_Button).click();		
	}
	public static String getNetworkTabBarButtonText() {
		return driver.findElement(CommonElements.Network_TabBar_Button).getText();
	}	

	
	public static MobileElement getDiagWiFiButton()
	{
		return driver.findElement(CommonElements.Diag_WiFi_Button);
	}
	public static boolean isDiagWiFiButtonDiplayed() {
		return driver.findElement(CommonElements.Diag_WiFi_Button).isDisplayed();
	}
	public static void clickOnDiagWiFiButton() {
		driver.findElement(CommonElements.Diag_WiFi_Button).click();		
	}
	public static String getDiagWiFiButtonText() {
		return driver.findElement(CommonElements.Diag_WiFi_Button).getText();
	}	

	
	public static MobileElement getDiagStartButton()
	{
		return driver.findElement(CommonElements.Diag_Start_Button);
	}
	public static boolean isDiagStartButtonDiplayed() {
		return driver.findElement(CommonElements.Diag_Start_Button).isDisplayed();
	}
	public static void clickOnDiagStartButton() {
		driver.findElement(CommonElements.Diag_Start_Button).click();		
	}
	public static String getDiagStartButtonText() {
		return driver.findElement(CommonElements.Diag_Start_Button).getText();
	}	
	
	//*********************************
	
	public static MobileElement getDiagWiFiTitle()
	{
		return driver.findElement(CommonElements.Diag_WiFi_Title);
	}
	public static boolean isDiagWiFiTitleDiplayed() {
		return driver.findElement(CommonElements.Diag_WiFi_Title).isDisplayed();
	}
	public static String getDiagWiFiTitleText() {
		return driver.findElement(CommonElements.Diag_WiFi_Title).getText();
	}	
	
	public static MobileElement getDiagWiFiDesc()
	{
		return driver.findElement(CommonElements.Diag_WiFi_Desc);
	}
	public static boolean isDiagWiFiDescDiplayed() {
		return driver.findElement(CommonElements.Diag_WiFi_Desc).isDisplayed();
	}
	public static String getDiagWiFiDescText() {
		return driver.findElement(CommonElements.Diag_WiFi_Desc).getText();
	}	

	public static MobileElement getDiagParamVerif()
	{
		return driver.findElement(CommonElements.Diag_Param_Verif);
	}
	public static boolean isDiagParamVerifDiplayed() {
		return driver.findElement(CommonElements.Diag_Param_Verif).isDisplayed();
	}
	public static String getDiagParamVerifText() {
		return driver.findElement(CommonElements.Diag_Param_Verif).getText();
	}	

	public static MobileElement getDiagVerifStatus()
	{
		return driver.findElement(CommonElements.Diag_Verif_Status);
	}
	public static boolean isDiagVerifStatusDiplayed() {
		return driver.findElement(CommonElements.Diag_Verif_Status).isDisplayed();
	}
	public static String getDiagVerifStatusText() {
		return driver.findElement(CommonElements.Diag_Verif_Status).getText();
	}	

	public static void clickOnDetailsSettingsButton() {
		driver.findElement(CommonElements.Details_Settings_Button).click();		
	}

	
	public static MobileElement getVerifedSettingsPage()
	{
		return driver.findElement(CommonElements.Verifed_Settings_Page);
	}
	public static boolean isVerifedSettingsPageDiplayed() {
		return driver.findElement(CommonElements.Verifed_Settings_Page).isDisplayed();
	}
	public static String getVerifedSettingsPageText() {
		return driver.findElement(CommonElements.Verifed_Settings_Page).getText();
	}	

	public static MobileElement getSettingSecutityWiFi()
	{
		return driver.findElement(CommonElements.Setting_Secutity_WiFi);
	}
	public static boolean isSettingSecutityWiFiDiplayed() {
		return driver.findElement(CommonElements.Setting_Secutity_WiFi).isDisplayed();
	}
	public static String getSettingSecutityWiFiText() {
		return driver.findElement(CommonElements.Setting_Secutity_WiFi).getText();
	}	

	public static void clickOnSettingSecutityWiFiButton() {
		driver.findElement(CommonElements.Setting_Secutity_WiFi).click();		
	}

	public static MobileElement getDiagSecutityDetail()
	{
		return driver.findElement(CommonElements.Diag_Secutity_Detail);
	}
	public static boolean isDiagSecutityDetailDiplayed() {
		return driver.findElement(CommonElements.Diag_Secutity_Detail).isDisplayed();
	}
	public static String getDiagSecutityDetailText() {
		return driver.findElement(CommonElements.Diag_Secutity_Detail).getText();
	}	

	public static MobileElement getDiagSecutityDesc()
	{
		return driver.findElement(CommonElements.Diag_Secutity_Desc);
	}
	public static boolean isDiagSecutityDescDiplayed() {
		return driver.findElement(CommonElements.Diag_Secutity_Desc).isDisplayed();
	}
	public static String getDiagSecutityDescText() {
		return driver.findElement(CommonElements.Diag_Secutity_Desc).getText();
	}	

	public static MobileElement getDiagnosticDetailButton()
	{
		return driver.findElement(CommonElements.Diagnostic_Detail_Button);
	}
	public static boolean isDiagnosticDetailButtonDiplayed() {
		return driver.findElement(CommonElements.Diagnostic_Detail_Button).isDisplayed();
	}
	public static String getDiagnosticDetailButtonText() {
		return driver.findElement(CommonElements.Diagnostic_Detail_Button).getText();
	}	

	public static void clickOnDiagnosticDetailButton() {
		driver.findElement(CommonElements.Diagnostic_Detail_Button).click();		
	}

	
	public static MobileElement getSettingVisibilityWiFi()
	{
		return driver.findElement(CommonElements.Setting_Visibility_WiFi);
	}
	public static boolean isSettingVisibilityWiFiDiplayed() {
		return driver.findElement(CommonElements.Setting_Visibility_WiFi).isDisplayed();
	}
	public static String getSettingVisibilityWiFiText() {
		return driver.findElement(CommonElements.Setting_Visibility_WiFi).getText();
	}	

	public static void clickOnSettingVisibilityWiFiButton() {
		driver.findElement(CommonElements.Setting_Visibility_WiFi).click();		
	}

	public static MobileElement getDiagVisibilityDetail()
	{
		return driver.findElement(CommonElements.Diag_Visibility_Detail);
	}
	public static boolean isDiagVisibilityDetailDiplayed() {
		return driver.findElement(CommonElements.Diag_Visibility_Detail).isDisplayed();
	}
	public static String getDiagVisibilityDetailText() {
		return driver.findElement(CommonElements.Diag_Visibility_Detail).getText();
	}	

	public static MobileElement getDiagVisibilityDesc()
	{
		return driver.findElement(CommonElements.Diag_Visibility_Desc);
	}
	public static boolean isDiagVisibilityDescDiplayed() {
		return driver.findElement(CommonElements.Diag_Visibility_Desc).isDisplayed();
	}
	public static String getDiagVisibilityDescText() {
		return driver.findElement(CommonElements.Diag_Visibility_Desc).getText();
	}	

	public static MobileElement getSettingBandwidthWiFi()
	{
		return driver.findElement(CommonElements.Setting_Bandwidth_WiFi);
	}
	public static boolean isSettingBandwidthWiFiDiplayed() {
		return driver.findElement(CommonElements.Setting_Bandwidth_WiFi).isDisplayed();
	}
	public static String getSettingBandwidthWiFiText() {
		return driver.findElement(CommonElements.Setting_Bandwidth_WiFi).getText();
	}	

	public static void clickOnSettingBandwidthWiFiButton() {
		driver.findElement(CommonElements.Setting_Bandwidth_WiFi).click();		
	}

	public static MobileElement getDiagBandwidthDetail()
	{
		return driver.findElement(CommonElements.Diag_Bandwidth_Detail);
	}
	public static boolean isDiagBandwidthDetailDiplayed() {
		return driver.findElement(CommonElements.Diag_Bandwidth_Detail).isDisplayed();
	}
	public static String getDiagBandwidthDetailText() {
		return driver.findElement(CommonElements.Diag_Bandwidth_Detail).getText();
	}	

	public static MobileElement getDiagBandwidthDesc()
	{
		return driver.findElement(CommonElements.Diag_Bandwidth_Desc);
	}
	public static boolean isDiagBandwidthDescDiplayed() {
		return driver.findElement(CommonElements.Diag_Bandwidth_Desc).isDisplayed();
	}
	public static String getDiagBandwidthDescText() {
		return driver.findElement(CommonElements.Diag_Bandwidth_Desc).getText();
	}	
	
	public static MobileElement getSettingCanalsWiFi()
	{
		return driver.findElement(CommonElements.Setting_Canals_WiFi);
	}
	public static boolean isSettingCanalsWiFiDiplayed() {
		return driver.findElement(CommonElements.Setting_Canals_WiFi).isDisplayed();
	}
	public static String getSettingCanalsWiFiText() {
		return driver.findElement(CommonElements.Setting_Canals_WiFi).getText();
	}	

	public static void clickOnSettingCanalsWiFiButton() {
		driver.findElement(CommonElements.Setting_Canals_WiFi).click();		
	}

	public static MobileElement getDiagCanalsDetail()
	{
		return driver.findElement(CommonElements.Diag_Canals_Detail);
	}
	public static boolean isDiagCanalsDetailDiplayed() {
		return driver.findElement(CommonElements.Diag_Canals_Detail).isDisplayed();
	}
	public static String getDiagCanalsDetailText() {
		return driver.findElement(CommonElements.Diag_Canals_Detail).getText();
	}	

	public static MobileElement getDiagCanalsDesc()
	{
		return driver.findElement(CommonElements.Diag_Canals_Desc);
	}
	public static boolean isDiagCanalsDescDiplayed() {
		return driver.findElement(CommonElements.Diag_Canals_Desc).isDisplayed();
	}
	public static String getDiagCanalsDescText() {
		return driver.findElement(CommonElements.Diag_Canals_Desc).getText();
	}	

	public static MobileElement getStatusWiFi()
	{
		return driver.findElement(CommonElements.Setting_Status_WiFi);
	}
	public static boolean isSettingStatusWiFiDiplayed() {
		return driver.findElement(CommonElements.Setting_Status_WiFi).isDisplayed();
	}
	public static String getSettingStatusWiFiText() {
		return driver.findElement(CommonElements.Setting_Status_WiFi).getText();
	}	

	public static void clickOnSettingStatusWiFiButton() {
		driver.findElement(CommonElements.Setting_Status_WiFi).click();		
	}

	public static MobileElement getDiagStatusDetail()
	{
		return driver.findElement(CommonElements.Diag_Status_Detail);
	}
	public static boolean isDiagStatusDetailDiplayed() {
		return driver.findElement(CommonElements.Diag_Status_Detail).isDisplayed();
	}
	public static String getDiagStatusDetailText() {
		return driver.findElement(CommonElements.Diag_Status_Detail).getText();
	}	

	public static MobileElement getDiagStatusDesc()
	{
		return driver.findElement(CommonElements.Diag_Status_Desc);
	}
	public static boolean isDiagStatusDescDiplayed() {
		return driver.findElement(CommonElements.Diag_Status_Desc).isDisplayed();
	}
	public static String getDiagStatusDescText() {
		return driver.findElement(CommonElements.Diag_Status_Desc).getText();
	}	

	
	public static MobileElement getRangeWiFi()
	{
		return driver.findElement(CommonElements.Setting_Range_WiFi);
	}
	public static boolean isSettingRangeWiFiDiplayed() {
		return driver.findElement(CommonElements.Setting_Range_WiFi).isDisplayed();
	}
	public static String getSettingRangeWiFiText() {
		return driver.findElement(CommonElements.Setting_Range_WiFi).getText();
	}	

	public static void clickOnSettingRangeWiFiButton() {
		driver.findElement(CommonElements.Setting_Range_WiFi).click();		
	}

	public static MobileElement getDiagRangeDetail()
	{
		return driver.findElement(CommonElements.Diag_Range_Detail);
	}
	public static boolean isDiagRangeDetailDiplayed() {
		return driver.findElement(CommonElements.Diag_Range_Detail).isDisplayed();
	}
	public static String getDiagRangeDetailText() {
		return driver.findElement(CommonElements.Diag_Range_Detail).getText();
	}	

	public static MobileElement getDiagRangeDesc()
	{
		return driver.findElement(CommonElements.Diag_Range_Desc);
	}
	public static boolean isDiagRangeDescDiplayed() {
		return driver.findElement(CommonElements.Diag_Range_Desc).isDisplayed();
	}
	public static String getDiagRangeDescText() {
		return driver.findElement(CommonElements.Diag_Range_Desc).getText();
	}	

	public static MobileElement getRestrictionsWiFiButton()
	{
		return driver.findElement(CommonElements.Restrictions_WiFi_Button);
	}
	public static boolean isRestrictionsWiFiButtonDiplayed() {
		return driver.findElement(CommonElements.Restrictions_WiFi_Button).isDisplayed();
	}
	public static String getRestrictionsWiFiButtonText() {
		return driver.findElement(CommonElements.Restrictions_WiFi_Button).getText();
	}	

	public static void clickOnRestrictionsWiFiButton() {
		driver.findElement(CommonElements.Restrictions_WiFi_Button).click();		
	}

	public static MobileElement getRestrictionsWiFiPageName()
	{
		return driver.findElement(CommonElements.Restrictions_WiFi_PageName);
	}
	public static boolean isRestrictionsWiFiPageNameDiplayed() {
		return driver.findElement(CommonElements.Restrictions_WiFi_PageName).isDisplayed();
	}
	public static String getRestrictionsWiFiPageNameText() {
		return driver.findElement(CommonElements.Restrictions_WiFi_PageName).getText();
	}	
	
	
	public static MobileElement getRestrictionsWiFiDesc()
	{
		return driver.findElement(CommonElements.Restrictions_WiFi_Desc);
	}
	public static boolean isRestrictionsWiFiDescDiplayed() {
		return driver.findElement(CommonElements.Restrictions_WiFi_Desc).isDisplayed();
	}
	public static String getRestrictionsWiFiDescText() {
		return driver.findElement(CommonElements.Restrictions_WiFi_Desc).getText();
	}	

	public static MobileElement getRestrictionsTypeElement()
	{
		return driver.findElement(CommonElements.Restrictions_Type_Element);
	}
	public static boolean isRestrictionsTypeElementDiplayed() {
		return driver.findElement(CommonElements.Restrictions_Type_Element).isDisplayed();
	}
	public static String getRestrictionsTypeElementText() {
		return driver.findElement(CommonElements.Restrictions_Type_Element).getText();
	}	

	
	public static MobileElement getRestrictionsTypeValue()
	{
		return driver.findElement(CommonElements.Restrictions_Type_Value);
	}
	public static boolean isRestrictionsTypeValueDiplayed() {
		return driver.findElement(CommonElements.Restrictions_Type_Value).isDisplayed();
	}
	public static String getRestrictionsTypeValueText() {
		return driver.findElement(CommonElements.Restrictions_Type_Value).getText();
	}	

	public static void clickOnRestrictionsTypeValue() {
		driver.findElement(CommonElements.Restrictions_Type_Value).click();		
	}

	public static void clickOnAccessBlockElement() {
		driver.findElement(CommonElements.Access_Block_Element).click();		
	}

	public static void clickOnRestrictionsWiFiSwitch() {
		driver.findElement(CommonElements.Restrictions_WiFi_Switch).click();		
	}

	public static MobileElement getActivationRestrictionsElement()
	{
		return driver.findElement(CommonElements.Activation_Restrictions_Element);
	}
	public static boolean isActivationRestrictionsElementDiplayed() {
		return driver.findElement(CommonElements.Activation_Restrictions_Element).isDisplayed();
	}
	public static String getActivationRestrictionsElementText() {
		return driver.findElement(CommonElements.Activation_Restrictions_Element).getText();
	}	

	
	public static MobileElement getRestrictionsNoDeviceDesc()
	{
		return driver.findElement(CommonElements.Restrictions_NoDevice_Desc);
	}
	public static boolean isRestrictionsNoDeviceDescDiplayed() {
		return driver.findElement(CommonElements.Restrictions_NoDevice_Desc).isDisplayed();
	}
	public static String getRestrictionsNoDeviceDescText() {
		return driver.findElement(CommonElements.Restrictions_NoDevice_Desc).getText();
	}	

	public static MobileElement getAddDeviceButton()
	{
		return driver.findElement(CommonElements.Add_Device_Button);
	}
	public static boolean isAddDeviceButtonDiplayed() {
		return driver.findElement(CommonElements.Add_Device_Button).isDisplayed();
	}
	public static String getAddDeviceButtonText() {
		return driver.findElement(CommonElements.Add_Device_Button).getText();
	}	

	public static void clickOnAddDeviceButton() {
		driver.findElement(CommonElements.Add_Device_Button).click();		
	}

	public static MobileElement getRestrictionsValidatebutton()
	{
		return driver.findElement(CommonElements.Restrictions_Validate_button);
	}
	public static boolean isRestrictionsValidatebuttonDiplayed() {
		return driver.findElement(CommonElements.Restrictions_Validate_button).isDisplayed();
	}
	public static String getRestrictionsValidatebuttonText() {
		return driver.findElement(CommonElements.Restrictions_Validate_button).getText();
	}	

	public static void clickOnRestrictionsValidatebutton() {
		driver.findElement(CommonElements.Restrictions_Validate_button).click();		
	}

	
	public static MobileElement getSelectListDevicesButton()
	{
		return driver.findElement(CommonElements.Select_ListDevices_Button);
	}
	public static boolean isSelectListDevicesButtonDiplayed() {
		return driver.findElement(CommonElements.Select_ListDevices_Button).isDisplayed();
	}
	public static String getSelectListDevicesButtonText() {
		return driver.findElement(CommonElements.Select_ListDevices_Button).getText();
	}	
	public static void clickOnSelectListDevicesButton() {
		driver.findElement(CommonElements.Select_ListDevices_Button).click();		
	}

	

	public static void clickOnDeleteDeviceButton() {
		driver.findElement(CommonElements.Delete_Device_Button).click();		
	}

	public static MobileElement getAccessAlowElement()
	{
		return driver.findElement(CommonElements.Access_Alow_Element);
	}
	public static boolean isAccessAlowElementDiplayed() {
		return driver.findElement(CommonElements.Access_Alow_Element).isDisplayed();
	}
	public static String getAccessAlowElementText() {
		return driver.findElement(CommonElements.Access_Alow_Element).getText();
	}	
	public static void clickOnAccessAlowElement() {
		driver.findElement(CommonElements.Access_Alow_Element).click();		
	}
	
	public static MobileElement getShowHidePasswordButton()
	{
		return driver.findElement(CommonElements.Show_Hide_Password_Button);
	}
	public static boolean isShowHidePasswordButtonDiplayed() {
		return driver.findElement(CommonElements.Show_Hide_Password_Button).isDisplayed();
	}
	public static String getShowHidePasswordButtonText() {
		return driver.findElement(CommonElements.Show_Hide_Password_Button).getText();
	}	
	public static void clickOnShowHidePasswordButton() {
		driver.findElement(CommonElements.Show_Hide_Password_Button).click();		
	}
	
	public static MobileElement getEditNetworkButton()
	{
		return driver.findElement(CommonElements.Edit_Network_Button);
	}
	public static boolean isEditNetworkButtonDiplayed() {
		return driver.findElement(CommonElements.Edit_Network_Button).isDisplayed();
	}
	public static String getEditNetworkButtonText() {
		return driver.findElement(CommonElements.Edit_Network_Button).getText();
	}	
	public static void clickOnEditNetworkButton() {
		driver.findElement(CommonElements.Edit_Network_Button).click();		
	}

	
	public static MobileElement getEditNetworkTitlePage()
	{
		return driver.findElement(CommonElements.Edit_Network_TitlePage);
	}
	public static boolean isEditNetworkTitlePageDiplayed() {
		return driver.findElement(CommonElements.Edit_Network_TitlePage).isDisplayed();
	}
	public static String getEditNetworkTitlePageText() {
		return driver.findElement(CommonElements.Edit_Network_TitlePage).getText();
	}	

	public static MobileElement getSSIDNetworkElement()
	{
		return driver.findElement(CommonElements.SSID_Network_Element);
	}
	public static boolean isSSIDNetworkElementDiplayed() {
		return driver.findElement(CommonElements.SSID_Network_Element).isDisplayed();
	}
	public static String getSSIDNetworkElementText() {
		return driver.findElement(CommonElements.SSID_Network_Element).getText();
	}	
	public static void clickOnSSIDNetworkElement() {
		driver.findElement(CommonElements.SSID_Network_Element).click();		
	}
	
	public static MobileElement getSSIDNetworkField()
	{
		return driver.findElement(CommonElements.SSID_Network_Field);
	}
	public static boolean isSSIDNetworkFieldDiplayed() {
		return driver.findElement(CommonElements.SSID_Network_Field).isDisplayed();
	}
	public static String getSSIDNetworkFieldText() {
		return driver.findElement(CommonElements.SSID_Network_Field).getText();
	}	
	public static void clickOnSSIDNetworkField() {
		driver.findElement(CommonElements.SSID_Network_Field).click();		
	}
	public static void enterNewSSIDName() {
		driver.findElement(CommonElements.SSID_Network_Field).sendKeys("1");		
	}

	public static void clickOnClearSSIDField() {
		driver.findElement(CommonElements.Clear_Field).click();		
	}
	
	public static MobileElement getValidateButton()
	{
		return driver.findElement(CommonElements.Validate_Button);
	}
	public static boolean isValidateButtonDiplayed() {
		return driver.findElement(CommonElements.Validate_Button).isDisplayed();
	}
	public static String getValidateButtonText() {
		return driver.findElement(CommonElements.Validate_Button).getText();
	}	
	public static void clickOnValidateButton() {
		driver.findElement(CommonElements.Validate_Button).click();		
	}

	
	public static void enterOldSSIDName() {
		driver.findElement(CommonElements.SSID_Network_Field).sendKeys("#DeltaAutomation#");		
	}
	

	public static MobileElement getNetworkKeyElement()
	{
		return driver.findElement(CommonElements.Network_Key_Element);
	}
	public static boolean isNetworkKeyElementDiplayed() {
		return driver.findElement(CommonElements.Network_Key_Element).isDisplayed();
	}
	public static String getNetworkKeyElementText() {
		return driver.findElement(CommonElements.Network_Key_Element).getText();
	}	
	public static void clickOnNetworkKeyElement() {
		driver.findElement(CommonElements.Network_Key_Element).click();		
	}
	
	public static MobileElement getWiFiKeyTitlePage()
	{
		return driver.findElement(CommonElements.WiFiKey_Title_Page);
	}
	public static boolean isWiFiKeyTitlePageDiplayed() {
		return driver.findElement(CommonElements.WiFiKey_Title_Page).isDisplayed();
	}
	public static String getWiFiKeyTitlePageText() {
		return driver.findElement(CommonElements.WiFiKey_Title_Page).getText();
	}	
	
	public static MobileElement getWiFiKeyPageDesc()
	{
		return driver.findElement(CommonElements.WiFiKey_Page_Desc);
	}
	public static boolean isWiFiKeyPageDescDiplayed() {
		return driver.findElement(CommonElements.WiFiKey_Page_Desc).isDisplayed();
	}
	public static String getWiFiKeyPageDescText() {
		return driver.findElement(CommonElements.WiFiKey_Page_Desc).getText();
	}	
	


	public static MobileElement getWiFiKeyFieldName()
	{
		return driver.findElement(CommonElements.WiFiKey_Field_Name);
	}
	public static boolean isWiFiKeyFieldNameDiplayed() {
		return driver.findElement(CommonElements.WiFiKey_Field_Name).isDisplayed();
	}
	public static String getWiFiKeyFieldNameText() {
		return driver.findElement(CommonElements.WiFiKey_Field_Name).getText();
	}	
	
	//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

	public static MobileElement getWiFiKeyField()
	{
		return driver.findElement(CommonElements.WiFi_Key_Field);
	}
	public static boolean isWiFiKeyFieldDiplayed() {
		return driver.findElement(CommonElements.WiFi_Key_Field).isDisplayed();
	}
	public static String getWiFiKeyFieldText() {
		return driver.findElement(CommonElements.WiFi_Key_Field).getText();
	}	
	
	public static void enterWiFiKeyField(String WiFiKey) {
		driver.findElement(CommonElements.WiFi_Key_Field).sendKeys(WiFiKey);		
	}


	public static MobileElement getWiFiKeyStatus()
	{
		return driver.findElement(CommonElements.WiFi_Key_Status);
	}
	public static boolean isWiFiKeyStatusDiplayed() {
		return driver.findElement(CommonElements.WiFi_Key_Status).isDisplayed();
	}
	public static String getWiFiKeyStatusText() {
		return driver.findElement(CommonElements.WiFi_Key_Status).getText();
	}	

	
	public static MobileElement getSecurityWiFiElement()
	{
		return driver.findElement(CommonElements.Security_WiFi_Element);
	}
	public static boolean isSecurityWiFiElementDiplayed() {
		return driver.findElement(CommonElements.Security_WiFi_Element).isDisplayed();
	}
	public static String getSecurityWiFiElementText() {
		return driver.findElement(CommonElements.Security_WiFi_Element).getText();
	}	
	public static void clickOnSecurityWiFiElement() {
		driver.findElement(CommonElements.Security_WiFi_Element).click();		
	}

	
	public static MobileElement getNotRecommendedSecurityElement()
	{
		return driver.findElement(CommonElements.NotRecommended_Security_Element);
	}
	public static void clickOnNotRecommendedSecurityElement() {
		driver.findElement(CommonElements.NotRecommended_Security_Element).click();		
	}

	public static MobileElement getRecommendedSecurityElement()
	{
		return driver.findElement(CommonElements.Recommended_Security_Element);
	}
	public static void clickOnRecommendedSecurityElement() {
		driver.findElement(CommonElements.Recommended_Security_Element).click();		
	}

	
	public static MobileElement getPopupTitle()
	{
		return driver.findElement(CommonElements.Popup_Title);
	}
	public static boolean isPopupTitleDiplayed() {
		return driver.findElement(CommonElements.Popup_Title).isDisplayed();
	}
	public static String getPopupTitleText() {
		return driver.findElement(CommonElements.Popup_Title).getText();
	}	

	
	public static MobileElement getEditWiFiSecurityButton()
	{
		return driver.findElement(CommonElements.Edit_WiFiSecurity_Button);
	}
	public static boolean isEditWiFiSecurityButtonDiplayed() {
		return driver.findElement(CommonElements.Edit_WiFiSecurity_Button).isDisplayed();
	}
	public static String getEditWiFiSecurityButtonText() {
		return driver.findElement(CommonElements.Edit_WiFiSecurity_Button).getText();
	}	
	public static void clickOnEditWiFiSecurityButton() {
		driver.findElement(CommonElements.Edit_WiFiSecurity_Button).click();		
	}
	
	public static MobileElement getConfTypeElement()
	{
		return driver.findElement(CommonElements.Conf_Type_Element);
	}
	public static boolean isConfTypeElementDiplayed() {
		return driver.findElement(CommonElements.Conf_Type_Element).isDisplayed();
	}
	public static String getConfTypeElementText() {
		return driver.findElement(CommonElements.Conf_Type_Element).getText();
	}	
	public static void clickOnConfTypeElement() {
		driver.findElement(CommonElements.Conf_Type_Element).click();		
	}

	public static MobileElement getConfWiFiTypePage()
	{
		return driver.findElement(CommonElements.Conf_WiFiType_Page);
	}
	public static boolean isConfWiFiTypePageDiplayed() {
		return driver.findElement(CommonElements.Conf_WiFiType_Page).isDisplayed();
	}
	public static String getConfWiFiTypePageText() {
		return driver.findElement(CommonElements.Conf_WiFiType_Page).getText();
	}	

	public static MobileElement getSeparatedConfElement()
	{
		return driver.findElement(CommonElements.Separated_Conf_Element);
	}
	public static boolean isSeparatedConfElementDiplayed() {
		return driver.findElement(CommonElements.Separated_Conf_Element).isDisplayed();
	}
	public static String getSeparatedConfElementText() {
		return driver.findElement(CommonElements.Separated_Conf_Element).getText();
	}	
	public static void clickOnSeparatedConfElement() {
		driver.findElement(CommonElements.Separated_Conf_Element).click();		
	}

	public static MobileElement getUniqueConfElement()
	{
		return driver.findElement(CommonElements.Unique_Conf_Element);
	}
	public static boolean isUniqueConfElementDiplayed() {
		return driver.findElement(CommonElements.Unique_Conf_Element).isDisplayed();
	}
	public static String getUniqueConfElementText() {
		return driver.findElement(CommonElements.Unique_Conf_Element).getText();
	}	
	public static void clickOnUniqueConfElement() {
		driver.findElement(CommonElements.Unique_Conf_Element).click();		
	}

	public static MobileElement getWiFiInfoPag()
	{
		return driver.findElement(CommonElements.WiFi_Info_Pag);
	}
	public static boolean isWiFiInfoPagDiplayed() {
		return driver.findElement(CommonElements.WiFi_Info_Pag).isDisplayed();
	}
	public static String getWiFiInfoPagText() {
		return driver.findElement(CommonElements.WiFi_Info_Pag).getText();
	}	

	public static MobileElement getTab2GHz()
	{
		return driver.findElement(CommonElements.Tab_2GHz);
	}
	public static void clickOnTab2GHz() {
		driver.findElement(CommonElements.Tab_2GHz).click();		
	}

	public static MobileElement getConfirmTypeButton()
	{
		return driver.findElement(CommonElements.Confirm_Type_Button);
	}
	public static boolean isConfirmTypeButtonDiplayed() {
		return driver.findElement(CommonElements.Confirm_Type_Button).isDisplayed();
	}
	public static String getConfirmTypeButtonText() {
		return driver.findElement(CommonElements.Confirm_Type_Button).getText();
	}	
	public static void clickOnConfirmTypeButton() {
		driver.findElement(CommonElements.Confirm_Type_Button).click();		
	}

	public static MobileElement getPlannedWiFiOFFElement()
	{
		return driver.findElement(CommonElements.Planned_WiFiOFF_Element);
	}
	public static boolean isPlannedWiFiOFFElementDiplayed() {
		return driver.findElement(CommonElements.Planned_WiFiOFF_Element).isDisplayed();
	}
	public static String getPlannedWiFiOFFElementText() {
		return driver.findElement(CommonElements.Planned_WiFiOFF_Element).getText();
	}	
	public static void clickOnPlannedWiFiOFFElement() {
		driver.findElement(CommonElements.Planned_WiFiOFF_Element).click();		
	}

	public static void clickOnInAppValidateButton() {
		driver.findElement(CommonElements.InApp_Validate_Button).click();		
	}

	public static MobileElement getPlannedWiFiOFFPageName()
	{
		return driver.findElement(CommonElements.Planned_WiFiOFF_PageName);
	}
	public static boolean isPlannedWiFiOFFPageNameDiplayed() {
		return driver.findElement(CommonElements.Planned_WiFiOFF_PageName).isDisplayed();
	}
	public static String getPlannedWiFiOFFPageNameText() {
		return driver.findElement(CommonElements.Planned_WiFiOFF_PageName).getText();
	}	
	
	public static MobileElement getWifiPlanningMessage()
	{
		return driver.findElement(CommonElements.Wifi_Planning_Message);
	}
	public static boolean isWifiPlanningMessageDiplayed() {
		return driver.findElement(CommonElements.Wifi_Planning_Message).isDisplayed();
	}
	public static String getWifiPlanningMessageText() {
		return driver.findElement(CommonElements.Wifi_Planning_Message).getText();
	}	

	public static void clickOnWifiPlanningEnable() {
		driver.findElement(CommonElements.Wifi_Planning_Enable).click();		
	}

	public static void clickOnFirstTimeSlot() {
		driver.findElement(CommonElements.First_Time_Slot).click();		
	}

	public static MobileElement getWifiPlanningValidateButton()
	{
		return driver.findElement(CommonElements.Wifi_Planning_Validate_Button);
	}
	public static boolean isWifiPlanningValidateButtonDiplayed() {
		return driver.findElement(CommonElements.Wifi_Planning_Validate_Button).isDisplayed();
	}
	public static String getWifiPlanningValidateButtonText() {
		return driver.findElement(CommonElements.Wifi_Planning_Validate_Button).getText();
	}	
	public static void clickOnWifiPlanningValidateButton() {
		driver.findElement(CommonElements.Wifi_Planning_Validate_Button).click();		
	}

	public static MobileElement getWifiStateSubtitle()
	{
		return driver.findElement(CommonElements.Wifi_State_Subtitle);
	}
	public static boolean isWifiStateSubtitleDiplayed() {
		return driver.findElement(CommonElements.Wifi_State_Subtitle).isDisplayed();
	}
	public static String getWifiStateSubtitleText() {
		return driver.findElement(CommonElements.Wifi_State_Subtitle).getText();
	}	

	public static void clickOnMiddleTimeSlot() {
		driver.findElement(CommonElements.Middle_Time_Slot).click();		
	}

	public static MobileElement getWifiPlanningResetButton()
	{
		return driver.findElement(CommonElements.Wifi_Planning_Reset_Button);
	}
	public static boolean isWifiPlanningResetButtonDiplayed() {
		return driver.findElement(CommonElements.Wifi_Planning_Reset_Button).isDisplayed();
	}
	public static String getWifiPlanningResetButtonText() {
		return driver.findElement(CommonElements.Wifi_Planning_Reset_Button).getText();
	}	

	public static void clickOnWifiPlanningResetButton() {
		driver.findElement(CommonElements.Wifi_Planning_Reset_Button).click();		
	}

	public static MobileElement getWifiPlanningResetConfirmation()
	{
		return driver.findElement(CommonElements.Wifi_Planning_Reset_Confirmation);
	}
	public static boolean isWifiPlanningResetConfirmationDiplayed() {
		return driver.findElement(CommonElements.Wifi_Planning_Reset_Confirmation).isDisplayed();
	}
	public static String getWifiPlanningResetConfirmationText() {
		return driver.findElement(CommonElements.Wifi_Planning_Reset_Confirmation).getText();
	}	

	public static void clickOnWifiPlanningResetConfirmation() {
		driver.findElement(CommonElements.Wifi_Planning_Reset_Confirmation).click();		
	}

	public static void clickOnLastTimeSlot() {
		driver.findElement(CommonElements.Last_Time_Slot).click();		
	}

	
	
}
