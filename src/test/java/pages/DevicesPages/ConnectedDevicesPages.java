package pages.DevicesPages;



import elements.CommonElements;
import io.appium.java_client.MobileElement;
import tests.Base.BaseClass;

public class ConnectedDevicesPages extends BaseClass {

	

	public static MobileElement getChevronConnectedDevice()
	{
		return driver.findElement(CommonElements.Chevron_Connected_Device);
	}
	public static boolean isChevronConnectedDeviceDiplayed() {
		return driver.findElement(CommonElements.Chevron_Connected_Device).isDisplayed();
	}
	public static void clickOnChevronConnectedDevice() {
		driver.findElement(CommonElements.Chevron_Connected_Device).click();		
	}
	
	public static MobileElement getConnectedDevicesSection()
	{
		return driver.findElement(CommonElements.Connected_Devices_Section);
	}
	public static boolean isConnectedDevicesSectionDiplayed() {
		return driver.findElement(CommonElements.Connected_Devices_Section).isDisplayed();
	}
	public static String getConnectedDevicesSectionText() {
		return driver.findElement(CommonElements.Connected_Devices_Section).getText();
	}	

	
	public static MobileElement getDevicesConnectionType()
	{
		return driver.findElement(CommonElements.Devices_Connection_Type);
	}
	public static boolean isDevicesConnectionTypeDiplayed() {
		return driver.findElement(CommonElements.Devices_Connection_Type).isDisplayed();
	}

	public static MobileElement getDeviceUpRate()
	{
		return driver.findElement(CommonElements.Device_Up_Rate);
	}
	public static boolean isDeviceUpRateDiplayed() {
		return driver.findElement(CommonElements.Device_Up_Rate).isDisplayed();
	}

	public static MobileElement getDeviceDownRate()
	{
		return driver.findElement(CommonElements.Device_Down_Rate);
	}
	public static boolean isDeviceDownRateDiplayed() {
		return driver.findElement(CommonElements.Device_Down_Rate).isDisplayed();
	}

	public static MobileElement getDeviceRateUnit()
	{
		return driver.findElement(CommonElements.device_Rate_Unit);
	}
	public static boolean isDeviceRateUnitDiplayed() {
		return driver.findElement(CommonElements.device_Rate_Unit).isDisplayed();
	}

	public static MobileElement getFirstPictoDevice()
	{
		return driver.findElement(CommonElements.First_Picto_Device);
	}
	public static boolean isFirstPictoDeviceDiplayed() {
		return driver.findElement(CommonElements.First_Picto_Device).isDisplayed();
	}
	public static void clickOnFirstPictoDevice() {
		driver.findElement(CommonElements.First_Picto_Device).click();		
	}
	
	public static MobileElement getDeviceDetailImage()
	{
		return driver.findElement(CommonElements.Device_Detail_Image);
	}
	public static boolean isDeviceDetailImageDiplayed() {
		return driver.findElement(CommonElements.Device_Detail_Image).isDisplayed();
	}

	public static MobileElement getDeviceDetailStatus()
	{
		return driver.findElement(CommonElements.Device_Detail_Status);
	}
	public static boolean isDeviceDetailStatusDiplayed() {
		return driver.findElement(CommonElements.Device_Detail_Status).isDisplayed();
	}
	
	public static String getDeviceDetailStatusText() {
		return driver.findElement(CommonElements.Device_Detail_Status).getText();
	}	

	public static MobileElement getDeviceDetailUpRate()
	{
		return driver.findElement(CommonElements.DeviceDetail_Up_Rate);
	}
	public static boolean isDeviceDetailUpRateDiplayed() {
		return driver.findElement(CommonElements.DeviceDetail_Up_Rate).isDisplayed();
	}
	
	public static MobileElement getDeviceDetailDownRate()
	{
		return driver.findElement(CommonElements.DeviceDetail_Down_Rate);
	}
	public static boolean isDeviceDetailDownRateDiplayed() {
		return driver.findElement(CommonElements.DeviceDetail_Down_Rate).isDisplayed();
	}
	
	public static MobileElement getDeviceDetailDownRateUnit()
	{
		return driver.findElement(CommonElements.DeviceDetail_DownRate_Unit);
	}
	public static boolean isDeviceDetailDownRateUnitDiplayed() {
		return driver.findElement(CommonElements.DeviceDetail_DownRate_Unit).isDisplayed();
	}

	public static MobileElement getDeviceDetailGatewayIcon()
	{
		return driver.findElement(CommonElements.DeviceDetail_Gateway_Icon);
	}
	public static boolean isDeviceDetailGatewayIconDiplayed() {
		return driver.findElement(CommonElements.DeviceDetail_Gateway_Icon).isDisplayed();
	}

	public static MobileElement getDeviceDetailGatewayName()
	{
		return driver.findElement(CommonElements.DeviceDetail_Gateway_Name);
	}
	public static boolean isDeviceDetailGatewayNameDiplayed() {
		return driver.findElement(CommonElements.DeviceDetail_Gateway_Name).isDisplayed();
	}
	public static String getDeviceDetailGatewayNameText() {
		return driver.findElement(CommonElements.DeviceDetail_Gateway_Name).getText();
	}	

	public static MobileElement getDeviceDetailName()
	{
		return driver.findElement(CommonElements.Device_Detail_Name);
	}
	public static boolean isDeviceDetailNameDiplayed() {
		return driver.findElement(CommonElements.Device_Detail_Name).isDisplayed();
	}
	public static String getDeviceDetailNameText() {
		return driver.findElement(CommonElements.Device_Detail_Name).getText();
	}	

	public static MobileElement getDeviceDetailConstructor()
	{
		return driver.findElement(CommonElements.Device_Detail_Constructor);
	}
	public static boolean isDeviceDetailConstructorDiplayed() {
		return driver.findElement(CommonElements.Device_Detail_Constructor).isDisplayed();
	}
	public static String getDeviceDetailConstructorText() {
		return driver.findElement(CommonElements.Device_Detail_Constructor).getText();
	}	

	public static MobileElement getDeviceDetailAssociatedProfile()
	{
		return driver.findElement(CommonElements.DeviceDetail_Associated_Profile);
	}
	public static boolean isDeviceDetailAssociatedProfileDiplayed() {
		return driver.findElement(CommonElements.DeviceDetail_Associated_Profile).isDisplayed();
	}
	public static String getDeviceDetailAssociatedProfileText() {
		return driver.findElement(CommonElements.DeviceDetail_Associated_Profile).getText();
	}	

	public static MobileElement getDeviceDetailOthersInformations()
	{
		return driver.findElement(CommonElements.DeviceDetail_Others_Informations);
	}
	public static boolean isDeviceDetailOthersInformationsDiplayed() {
		return driver.findElement(CommonElements.DeviceDetail_Others_Informations).isDisplayed();
	}
	public static String getDeviceDetailOthersInformationsText() {
		return driver.findElement(CommonElements.DeviceDetail_Others_Informations).getText();
	}	
	public static void clickOnDeviceDetailOthersInformations() {
		driver.findElement(CommonElements.DeviceDetail_Others_Informations).click();		
	}

	
	public static MobileElement getDeviceIPV4Adress()
	{
		return driver.findElement(CommonElements.Device_IPV4_Adress);
	}
	public static boolean isDeviceIPV4AdressDiplayed() {
		return driver.findElement(CommonElements.Device_IPV4_Adress).isDisplayed();
	}
	public static String getDeviceIPV4AdressText() {
		return driver.findElement(CommonElements.Device_IPV4_Adress).getText();
	}	
	
	public static MobileElement getDeviceIPV6Adress()
	{
		return driver.findElement(CommonElements.Device_IPV6_Adress);
	}
	public static boolean isDeviceIPV6AdressDiplayed() {
		return driver.findElement(CommonElements.Device_IPV6_Adress).isDisplayed();
	}
	public static String getDeviceIPV6AdressText() {
		return driver.findElement(CommonElements.Device_IPV6_Adress).getText();
	}	

	public static MobileElement getDeviceMACAdress()
	{
		return driver.findElement(CommonElements.Device_MAC_Adress);
	}
	public static boolean isDeviceMACAdressDiplayed() {
		return driver.findElement(CommonElements.Device_MAC_Adress).isDisplayed();
	}
	public static String getDeviceMACAdressText() {
		return driver.findElement(CommonElements.Device_MAC_Adress).getText();
	}	

	public static MobileElement getModifyConnectedDeviceButton()
	{
		return driver.findElement(CommonElements.Modify_ConnectedDevice_Button);
	}
	public static boolean isModifyConnectedDeviceButtonDiplayed() {
		return driver.findElement(CommonElements.Modify_ConnectedDevice_Button).isDisplayed();
	}
	public static String getModifyConnectedDeviceButtonText() {
		return driver.findElement(CommonElements.Modify_ConnectedDevice_Button).getText();
	}	
	public static void clickOnModifyConnectedDeviceButton() {
		driver.findElement(CommonElements.Modify_ConnectedDevice_Button).click();		
	}

	public static MobileElement getModifyConnectedDevicePageName()
	{
		return driver.findElement(CommonElements.Modify_ConnectedDevice_PageName);
	}
	public static boolean isModifyConnectedDevicePageNameDiplayed() {
		return driver.findElement(CommonElements.Modify_ConnectedDevice_PageName).isDisplayed();
	}
	public static String getModifyConnectedDevicePageNameText() {
		return driver.findElement(CommonElements.Modify_ConnectedDevice_PageName).getText();
	}	

	public static MobileElement getFieldModifyDeviceDesc()
	{
		return driver.findElement(CommonElements.Field_ModifyDevice_Desc);
	}
	public static boolean isFieldModifyDeviceDescDiplayed() {
		return driver.findElement(CommonElements.Field_ModifyDevice_Desc).isDisplayed();
	}
	public static String getFieldModifyDeviceDescText() {
		return driver.findElement(CommonElements.Field_ModifyDevice_Desc).getText();
	}	

	public static MobileElement getTypeConnectedDeviceDesc()
	{
		return driver.findElement(CommonElements.Type_ConnectedDevice_Desc);
	}
	public static boolean isTypeConnectedDeviceDescDiplayed() {
		return driver.findElement(CommonElements.Type_ConnectedDevice_Desc).isDisplayed();
	}
	public static String getTypeConnectedDeviceDescText() {
		return driver.findElement(CommonElements.Type_ConnectedDevice_Desc).getText();
	}	

	public static MobileElement getModifyConnectedDeviceField()
	{
		return driver.findElement(CommonElements.Modify_ConnectedDevice_Field);
	}
	public static boolean isModifyConnectedDeviceFieldDiplayed() {
		return driver.findElement(CommonElements.Modify_ConnectedDevice_Field).isDisplayed();
	}
	public static String getModifyConnectedDeviceFieldText() {
		return driver.findElement(CommonElements.Modify_ConnectedDevice_Field).getText();
	}	
	public static void clickOnModifyConnectedDeviceField() {
		driver.findElement(CommonElements.Modify_ConnectedDevice_Field).click();		
	}
	public static void enterDeviceName(String DeviceName) {
		driver.findElement(CommonElements.Modify_ConnectedDevice_Field).sendKeys(DeviceName);		
	}

	public static MobileElement getValidateModifButton()
	{
		return driver.findElement(CommonElements.Validate_Modif_Button);
	}
	public static boolean isValidateModifButtonDiplayed() {
		return driver.findElement(CommonElements.Validate_Modif_Button).isDisplayed();
	}
	public static String getValidateModifButtonText() {
		return driver.findElement(CommonElements.Validate_Modif_Button).getText();
	}	
	public static void clickOnValidateModifButton() {
		driver.findElement(CommonElements.Validate_Modif_Button).click();		
	}
	
	public static MobileElement getDeviceNameValue()
	{
		return driver.findElement(CommonElements.Device_Name_Value);
	}
	public static boolean isDeviceNameValueDiplayed() {
		return driver.findElement(CommonElements.Device_Name_Value).isDisplayed();
	}
	public static String getDeviceNameValueText() {
		return driver.findElement(CommonElements.Device_Name_Value).getText();
	}	


	
}
