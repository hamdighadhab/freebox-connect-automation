package pages.DevicesPages;


import elements.CommonElements;
import io.appium.java_client.MobileElement;
import tests.Base.BaseClass;

public class RecentlyLoggedDevicesPages extends BaseClass {

	public static String RecentlyLoggedDevice = "Récemment connectés";

	public static MobileElement getRecentlyLoggedWithoutDeviceConnected()
	{
		return driver.findElement(CommonElements.recently_Logged_Without_Device_Connected);
	}
	public static boolean isRecentlyLoggedWithoutDeviceConnectedDiplayed() {
		return driver.findElement(CommonElements.recently_Logged_Without_Device_Connected).isDisplayed();
	}
	public static String getRecentlyLoggedWithoutDeviceConnectedText() {
		return driver.findElement(CommonElements.recently_Logged_Without_Device_Connected).getText();
	}	

	public static MobileElement getChevronRecentlyLoggedWithoutDeviceConnected()
	{
		return driver.findElement(CommonElements.Chevron_Recently_Logged_Without_Device_Connected);
	}
	public static boolean isChevronRecentlyLoggedWithoutDeviceConnectedDiplayed() {
		return driver.findElement(CommonElements.Chevron_Recently_Logged_Without_Device_Connected).isDisplayed();
	}
	public static void clickOnChevronRecentlyLoggedWithoutDeviceConnected() {
		driver.findElement(CommonElements.Chevron_Recently_Logged_Without_Device_Connected).click();		
	}

	public static MobileElement getRecentlyLoggedWithDeviceConnected()
	{
		return driver.findElement(CommonElements.recently_Logged_With_Device_Connected);
	}
	public static boolean isRecentlyLoggedWithDeviceConnectedDiplayed() {
		return driver.findElement(CommonElements.recently_Logged_With_Device_Connected).isDisplayed();
	}
	public static String getRecentlyLoggedWithDeviceConnectedText() {
		return driver.findElement(CommonElements.recently_Logged_With_Device_Connected).getText();
	}	

	public static MobileElement getChevronRecentlyLoggedWithDeviceConnected()
	{
		return driver.findElement(CommonElements.Chevron_Recently_Logged_With_Device_Connected);
	}
	public static boolean isChevronRecentlyLoggedWithDeviceConnectedDiplayed() {
		return driver.findElement(CommonElements.Chevron_Recently_Logged_With_Device_Connected).isDisplayed();
	}
	public static void clickOnChevronRecentlyLoggedWithDeviceConnected() {
		driver.findElement(CommonElements.Chevron_Recently_Logged_With_Device_Connected).click();		
	}
	
	public static MobileElement getFirstDeviceList()
	{
		return driver.findElement(CommonElements.First_Device_List);
	}
	public static boolean isFirstDeviceListDiplayed() {
		return driver.findElement(CommonElements.First_Device_List).isDisplayed();
	}
	public static void clickOnFirstDeviceList() {
		driver.findElement(CommonElements.First_Device_List).click();		
	}

	public static MobileElement getSecondDeviceList()
	{
		return driver.findElement(CommonElements.Second_Device_List);
	}
	public static boolean isSecondDeviceListDiplayed() {
		return driver.findElement(CommonElements.Second_Device_List).isDisplayed();
	}
	public static void clickOnSecondDeviceList() {
		driver.findElement(CommonElements.Second_Device_List).click();		
	}

	public static MobileElement getThirdDeviceList()
	{
		return driver.findElement(CommonElements.Third_Device_List);
	}
	public static boolean isThirdDeviceListDiplayed() {
		return driver.findElement(CommonElements.Third_Device_List).isDisplayed();
	}
	public static void clickOnThirdDeviceList() {
		driver.findElement(CommonElements.Third_Device_List).click();		
	}

	public static MobileElement getFourthDeviceList()
	{
		return driver.findElement(CommonElements.Fourth_Device_List);
	}
	public static boolean isFourthDeviceListDiplayed() {
		return driver.findElement(CommonElements.Fourth_Device_List).isDisplayed();
	}
	public static void clickOnFourthDeviceList() {
		driver.findElement(CommonElements.Fourth_Device_List).click();		
	}

	public static MobileElement getDeviceDetailRemove()
	{
		return driver.findElement(CommonElements.Device_Detail_Remove);
	}
	public static boolean isDeviceDetailRemoveDiplayed() {
		return driver.findElement(CommonElements.Device_Detail_Remove).isDisplayed();
	}
	public static String getDeviceDetailRemoveText() {
		return driver.findElement(CommonElements.Device_Detail_Remove).getText();
	}	
	public static void clickOnDeviceDetailRemove() {
		driver.findElement(CommonElements.Device_Detail_Remove).click();		
	}


	public static MobileElement getAlertDeviceTitle()
	{
		return driver.findElement(CommonElements.Alert_Device_Title);
	}
	public static boolean isAlertDeviceTitleDiplayed() {
		return driver.findElement(CommonElements.Alert_Device_Title).isDisplayed();
	}
	public static String getAlertDeviceTitleText() {
		return driver.findElement(CommonElements.Alert_Device_Title).getText();
	}	
	
	public static MobileElement getAlertDeviceDesc()
	{
		return driver.findElement(CommonElements.Alert_Device_Desc);
	}
	public static boolean isAlertDeviceDescDiplayed() {
		return driver.findElement(CommonElements.Alert_Device_Desc).isDisplayed();
	}
	public static String getAlertDeviceDescText() {
		return driver.findElement(CommonElements.Alert_Device_Desc).getText();
	}	

	public static MobileElement getAlertDeviceDecline()
	{
		return driver.findElement(CommonElements.Alert_Device_Decline);
	}
	public static boolean isAlertDeviceDeclineDiplayed() {
		return driver.findElement(CommonElements.Alert_Device_Decline).isDisplayed();
	}
	public static String getAlertDeviceDeclineText() {
		return driver.findElement(CommonElements.Alert_Device_Decline).getText();
	}	
	public static void clickOnAlertDeviceDecline() {
		driver.findElement(CommonElements.Alert_Device_Decline).click();		
	}



}
