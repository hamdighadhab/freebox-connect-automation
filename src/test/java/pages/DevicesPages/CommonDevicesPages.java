package pages.DevicesPages;

import elements.CommonElements;
import io.appium.java_client.MobileElement;
import tests.Base.BaseClass;


public class CommonDevicesPages extends BaseClass {

	public static MobileElement getDevicesTabBarButton()
	{
		return driver.findElement(CommonElements.Devices_TabBar_Button);
	}
	public static boolean isDevicesTabBarButtonDiplayed() {
		return driver.findElement(CommonElements.Devices_TabBar_Button).isDisplayed();
	}
	public static void clickOnDevicesTabBarButton() {
		driver.findElement(CommonElements.Devices_TabBar_Button).click();		
	}
	public static String getDevicesTabBarButtonText() {
		return driver.findElement(CommonElements.Devices_TabBar_Button).getText();
	}	


	public static MobileElement getConnectedDevice()
	{
		return driver.findElement(CommonElements.Connected_Device);
	}
	public static boolean isConnectedDeviceDiplayed() {
		return driver.findElement(CommonElements.Connected_Device).isDisplayed();
	}
	public static String getConnectedDeviceText() {
		return driver.findElement(CommonElements.Connected_Device).getText();
	}	
	
	
}
