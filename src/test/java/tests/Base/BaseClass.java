package tests.Base;

import java.net.URL;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import tests.ExtentReport.ExtentReportsFree;

public class BaseClass extends ExtentReportsFree{

	public static URL url;
	public static DesiredCapabilities cap;
	public static AppiumDriver<MobileElement> driver=null;

	@BeforeTest
	public void setUp() throws Exception {

		try {
			
			final String URL_STRING = "http://127.0.0.1:4723/wd/hub";
			url = new URL(URL_STRING);
			cap = new DesiredCapabilities();
			//Device Name
			cap.setCapability("deviceName", "SM-G998B");
			//Device udid
			cap.setCapability("udid", "R5CNC16D81E");
			//Device OS
			cap.setCapability("platformName", "Android");
			//Device OS Version
			cap.setCapability("platformVersion", "11");
			//Freebox-Connect App Definition
			cap.setCapability("appPackage", "fr.freebox.network.debug");
			cap.setCapability("appActivity", "networkapp.presentation.main.ui.MainActivity");
			cap.setCapability("autoGrantPermissions", "true");
			cap.setCapability("noReset", "true");
			cap.setCapability("fullReset", "false");

		}
		catch(Exception exp) {
			
			System.out.println("Cause is : "+exp.getCause());
			System.out.println("Message is : "+exp.getCause());
			exp.printStackTrace();	
		}
	}
	
	@AfterTest
	public void teardown() {
		driver.quit();
	}

}
