package tests.AndroidTests;

import java.io.IOException;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import Strings.CommonStrings;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import tests.Base.BaseClass;

public class AndroidDeepLink extends BaseClass {

    @Test
    public void testDeepLinkForDirectNavAndroid1 () throws IOException, InterruptedException {
		driver = new AppiumDriver<MobileElement>(url,cap);
		ExtentTest test = extent.createTest("Deeplink 1 : https://free.fr/app/connect", "https://free.fr/app/connect");
		Thread.sleep(3000);
		driver.navigate().back();
		try {

		driver.get("https://free.fr/app/connect ");
		Thread.sleep(3000);
		//Home page verification
		CommonStrings.homePageCheck(test, driver);
		
		} catch (Exception e) {
			test.fail(e);
		}

    }
    
    @Test
    public void testDeepLinkForDirectNavAndroid2 () throws IOException, InterruptedException {
		driver = new AppiumDriver<MobileElement>(url,cap);
		ExtentTest test = extent.createTest("Deeplink 2 : https://free.fr/apps/free-connect-redirect-mobile-app", "https://free.fr/apps/free-connect-redirect-mobile-app ");
		Thread.sleep(3000);
		driver.navigate().back();
		try {

		driver.get("https://free.fr/apps/free-connect-redirect-mobile-app");
		Thread.sleep(3000);
		//Home page verification
		CommonStrings.homePageCheck(test, driver);
		
		} catch (Exception e) {
			test.fail(e);
		}

    }

}
