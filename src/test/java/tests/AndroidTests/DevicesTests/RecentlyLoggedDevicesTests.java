package tests.AndroidTests.DevicesTests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import Strings.CommonStrings;
import Strings.DevicesStrings.CommonDevicesStrings;
import Strings.DevicesStrings.ConnectedDevicesStrings;
import Strings.DevicesStrings.RecentlyLoggedDevicesStrings;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.DevicesPages.CommonDevicesPages;
import pages.DevicesPages.ConnectedDevicesPages;
import pages.DevicesPages.RecentlyLoggedDevicesPages;
import pages.HomePagePages.HomeConnectionStatus;
import tests.Base.BaseClass;

public class RecentlyLoggedDevicesTests extends BaseClass{


	@Test 
	(priority=1, enabled=true)
	public void recentlyLoggedDevicesList() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(RecentlyLoggedDevicesStrings.TestName1, RecentlyLoggedDevicesStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Device tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(CommonDevicesPages.getDevicesTabBarButton()));
			CommonDevicesStrings.devicesTabBarButtonsCheck(test,driver);
			//Click on Device tab-bar
			CommonDevicesStrings.devicesTabBarButtonsClick(test,driver);
			//Number Of connected device
			wait.until(ExpectedConditions.visibilityOf(CommonDevicesPages.getConnectedDevice()));
			if (CommonDevicesPages.getConnectedDeviceText().equals("0 appareil connecté")) {
				//Header without connected device check 
				wait.until(ExpectedConditions.visibilityOf(RecentlyLoggedDevicesPages.getRecentlyLoggedWithoutDeviceConnected()));
				RecentlyLoggedDevicesStrings.recentlyLoggedWithoutDeviceConnectedCheck(test,driver);
				//chevron without connected device click 
				wait.until(ExpectedConditions.visibilityOf(RecentlyLoggedDevicesPages.getRecentlyLoggedWithoutDeviceConnected()));
				RecentlyLoggedDevicesStrings.chevronRecentlyLoggedWithoutDeviceConnectedCheck(test,driver);
				//chevron without connected device click 
				RecentlyLoggedDevicesStrings.chevronRecentlyLoggedWithoutDeviceConnectedClick(test,driver);
				Thread.sleep(1000);
				RecentlyLoggedDevicesStrings.chevronRecentlyLoggedWithoutDeviceConnectedClick(test,driver);
			} else {
				//Header with connected device check 
				wait.until(ExpectedConditions.visibilityOf(RecentlyLoggedDevicesPages.getRecentlyLoggedWithDeviceConnected()));
				RecentlyLoggedDevicesStrings.recentlyLoggedWithDeviceConnectedCheck(test,driver);
				//chevron without connected device click 
				wait.until(ExpectedConditions.visibilityOf(RecentlyLoggedDevicesPages.getRecentlyLoggedWithDeviceConnected()));
				RecentlyLoggedDevicesStrings.chevronRecentlyLoggedWithDeviceConnectedCheck(test,driver);
				//chevron without connected device click 
				RecentlyLoggedDevicesStrings.chevronRecentlyLoggedWithDeviceConnectedClick(test,driver);
				Thread.sleep(1000);
				RecentlyLoggedDevicesStrings.chevronRecentlyLoggedWithDeviceConnectedClick(test,driver);

			}
		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}
	
	
	@Test 
	(priority=2, enabled=true)
	public void recentlyLoggedDevicesDetails() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(RecentlyLoggedDevicesStrings.TestName2, RecentlyLoggedDevicesStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Device tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(CommonDevicesPages.getDevicesTabBarButton()));
			CommonDevicesStrings.devicesTabBarButtonsCheck(test,driver);
			//Click on Device tab-bar
			CommonDevicesStrings.devicesTabBarButtonsClick(test,driver);
			//Number Of connected device
			wait.until(ExpectedConditions.visibilityOf(CommonDevicesPages.getConnectedDevice()));
			
			if (CommonDevicesPages.getConnectedDeviceText().equals("0 appareil connecté")) {
				//First device check 
				wait.until(ExpectedConditions.visibilityOf(RecentlyLoggedDevicesPages.getFirstDeviceList()));
				RecentlyLoggedDevicesStrings.firstDeviceListCheck(test,driver);
				//Click on the first device 
				RecentlyLoggedDevicesStrings.firstDeviceListClick(test,driver);
			} else if (CommonDevicesPages.getConnectedDeviceText().equals("1 appareil connecté")) {
				//Second device check
				wait.until(ExpectedConditions.visibilityOf(RecentlyLoggedDevicesPages.getSecondDeviceList()));
				RecentlyLoggedDevicesStrings.secondDeviceListCheck(test,driver);
				//Click on the Second device 
				RecentlyLoggedDevicesStrings.secondDeviceListClick(test,driver);
			} else if (CommonDevicesPages.getConnectedDeviceText().equals("2 appareils connectés")) {
				//Third device check 
				wait.until(ExpectedConditions.visibilityOf(RecentlyLoggedDevicesPages.getThirdDeviceList()));
				RecentlyLoggedDevicesStrings.thirdDeviceListCheck(test,driver);
				//Click on the Third device 
				RecentlyLoggedDevicesStrings.thirdDeviceListClick(test,driver);
			} else if (CommonDevicesPages.getConnectedDeviceText().equals("3 appareils connectés")) {
				//Fourth device check 
				wait.until(ExpectedConditions.visibilityOf(RecentlyLoggedDevicesPages.getFourthDeviceList()));
				RecentlyLoggedDevicesStrings.fourthDeviceListCheck(test,driver);
				//Click on the Fourth device 
				RecentlyLoggedDevicesStrings.fourthDeviceListClick(test,driver);
			} 

			//Device Detail Image check
			wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getDeviceDetailImage()));
			ConnectedDevicesStrings.deviceDetailImagecheck(test,driver);
			//Device Detail Status check 
			wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getDeviceDetailStatus()));
			ConnectedDevicesStrings.recentlyDeviceDetailStatusCheck(test,driver);
			//Device Detail Name check 
			wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getDeviceDetailName()));
			ConnectedDevicesStrings.deviceDetailNameCheck(test,driver);
			//Device Detail Constructor check 
			wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getDeviceDetailConstructor()));
			ConnectedDevicesStrings.deviceDetailConstructorCheck(test,driver);
			//Device associated profile check 
			wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getDeviceDetailAssociatedProfile()));
			ConnectedDevicesStrings.deviceDetailAssociatedProfileCheck(test,driver);

			
		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test 
	(priority=3, enabled=true)
	public void recentlyLoggedDevicesMoreDetails() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(RecentlyLoggedDevicesStrings.TestName3, RecentlyLoggedDevicesStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Device tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(CommonDevicesPages.getDevicesTabBarButton()));
			CommonDevicesStrings.devicesTabBarButtonsCheck(test,driver);
			//Click on Device tab-bar
			CommonDevicesStrings.devicesTabBarButtonsClick(test,driver);
			//Number Of connected device
			wait.until(ExpectedConditions.visibilityOf(CommonDevicesPages.getConnectedDevice()));
			
			if (CommonDevicesPages.getConnectedDeviceText().equals("0 appareil connecté")) {
				//First device check 
				wait.until(ExpectedConditions.visibilityOf(RecentlyLoggedDevicesPages.getFirstDeviceList()));
				RecentlyLoggedDevicesStrings.firstDeviceListCheck(test,driver);
				//Click on the first device 
				RecentlyLoggedDevicesStrings.firstDeviceListClick(test,driver);
			} else if (CommonDevicesPages.getConnectedDeviceText().equals("1 appareil connecté")) {
				//Second device check
				wait.until(ExpectedConditions.visibilityOf(RecentlyLoggedDevicesPages.getSecondDeviceList()));
				RecentlyLoggedDevicesStrings.secondDeviceListCheck(test,driver);
				//Click on the Second device 
				RecentlyLoggedDevicesStrings.secondDeviceListClick(test,driver);
			} else if (CommonDevicesPages.getConnectedDeviceText().equals("2 appareils connectés")) {
				//Third device check 
				wait.until(ExpectedConditions.visibilityOf(RecentlyLoggedDevicesPages.getThirdDeviceList()));
				RecentlyLoggedDevicesStrings.thirdDeviceListCheck(test,driver);
				//Click on the Third device 
				RecentlyLoggedDevicesStrings.thirdDeviceListClick(test,driver);
			} else if (CommonDevicesPages.getConnectedDeviceText().equals("3 appareils connectés")) {
				//Fourth device check 
				wait.until(ExpectedConditions.visibilityOf(RecentlyLoggedDevicesPages.getFourthDeviceList()));
				RecentlyLoggedDevicesStrings.fourthDeviceListCheck(test,driver);
				//Click on the Fourth device 
				RecentlyLoggedDevicesStrings.fourthDeviceListClick(test,driver);
			} 

			//Device Detail Others Informations check 
			wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getDeviceDetailOthersInformations()));
			ConnectedDevicesStrings.deviceDetailOthersInformationsCheck(test,driver);
			//Click on Others Informations
			ConnectedDevicesStrings.deviceDetailOthersInformationsClick(test,driver);
			//IPV4 element check 
			wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getDeviceIPV4Adress()));
			ConnectedDevicesStrings.deviceIPV4AdressCheck(test,driver);
			//IPV6 element check 
			wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getDeviceIPV6Adress()));
			ConnectedDevicesStrings.deviceIPV6AdressCheck(test,driver);
			//MAC element check 
			wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getDeviceMACAdress()));
			ConnectedDevicesStrings.deviceMACAdressCheck(test,driver);

			
		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test 
	(priority=4, enabled=true)
	public void modifyRecentlyLoggedDevices() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(RecentlyLoggedDevicesStrings.TestName4, RecentlyLoggedDevicesStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Device tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(CommonDevicesPages.getDevicesTabBarButton()));
			CommonDevicesStrings.devicesTabBarButtonsCheck(test,driver);
			//Click on Device tab-bar
			CommonDevicesStrings.devicesTabBarButtonsClick(test,driver);
			//Number Of connected device
			wait.until(ExpectedConditions.visibilityOf(CommonDevicesPages.getConnectedDevice()));
			
			if (CommonDevicesPages.getConnectedDeviceText().equals("0 appareil connecté")) {
				//First device check 
				wait.until(ExpectedConditions.visibilityOf(RecentlyLoggedDevicesPages.getFirstDeviceList()));
				RecentlyLoggedDevicesStrings.firstDeviceListCheck(test,driver);
				//Click on the first device 
				RecentlyLoggedDevicesStrings.firstDeviceListClick(test,driver);
			} else if (CommonDevicesPages.getConnectedDeviceText().equals("1 appareil connecté")) {
				//Second device check
				wait.until(ExpectedConditions.visibilityOf(RecentlyLoggedDevicesPages.getSecondDeviceList()));
				RecentlyLoggedDevicesStrings.secondDeviceListCheck(test,driver);
				//Click on the Second device 
				RecentlyLoggedDevicesStrings.secondDeviceListClick(test,driver);
			} else if (CommonDevicesPages.getConnectedDeviceText().equals("2 appareils connectés")) {
				//Third device check 
				wait.until(ExpectedConditions.visibilityOf(RecentlyLoggedDevicesPages.getThirdDeviceList()));
				RecentlyLoggedDevicesStrings.thirdDeviceListCheck(test,driver);
				//Click on the Third device 
				RecentlyLoggedDevicesStrings.thirdDeviceListClick(test,driver);
			} else if (CommonDevicesPages.getConnectedDeviceText().equals("3 appareils connectés")) {
				//Fourth device check 
				wait.until(ExpectedConditions.visibilityOf(RecentlyLoggedDevicesPages.getFourthDeviceList()));
				RecentlyLoggedDevicesStrings.fourthDeviceListCheck(test,driver);
				//Click on the Fourth device 
				RecentlyLoggedDevicesStrings.fourthDeviceListClick(test,driver);
			} 

			//Modify button check 
			wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getModifyConnectedDeviceButton()));
			ConnectedDevicesStrings.modifyConnectedDeviceButtonCheck(test,driver);
			//Click on Modify button
			ConnectedDevicesStrings.modifyConnectedDeviceButtonClick(test,driver);
			//Modify Connected Device Page Name check 	
			wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getModifyConnectedDevicePageName()));
			ConnectedDevicesStrings.modifyConnectedDevicePageNameCheck(test,driver);
			//Field Modify Device Description check 
			wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getFieldModifyDeviceDesc()));
			ConnectedDevicesStrings.fieldModifyDeviceDescCheck(test,driver);
			//Field Modify Device Description check 
			wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getTypeConnectedDeviceDesc()));
			ConnectedDevicesStrings.typeConnectedDeviceDescCheck(test,driver);
			//Modify Connected Device Field check 	
			wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getModifyConnectedDeviceField()));
			ConnectedDevicesStrings.modifyConnectedDeviceFieldCheck(test,driver);
			//Click on Modify Connected Device Field
			ConnectedDevicesStrings.modifyConnectedDeviceFieldClick(test,driver);
			//Save initial device name
			ConnectedDevicesStrings.initialDeviceName();
			//Set new device name
			ConnectedDevicesPages.enterDeviceName("RecentlyConnectedDeviceAutomatedName");
			//Validate Button check
			wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getValidateModifButton()));
			ConnectedDevicesStrings.validateModifButtonCheck(test,driver);
			//Click on VALIDATE button
			ConnectedDevicesStrings.validateModifButtonClick(test,driver);
			//Modify button check 
			wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getModifyConnectedDeviceButton()));
			ConnectedDevicesStrings.modifyConnectedDeviceButtonCheck(test,driver);
			//Click on Modify button
			ConnectedDevicesStrings.modifyConnectedDeviceButtonClick(test,driver);
			//Modify Connected Device Field check 	
			wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getModifyConnectedDeviceField()));
			ConnectedDevicesStrings.modifyConnectedDeviceFieldCheck(test,driver);
			//Click on Modify Connected Device Field
			ConnectedDevicesStrings.modifyConnectedDeviceFieldClick(test,driver);
			//Set initial device name
			ConnectedDevicesPages.enterDeviceName(ConnectedDevicesStrings.InitialDeviceName);
			//Validate Button check
			wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getValidateModifButton()));
			ConnectedDevicesStrings.validateModifButtonCheck(test,driver);
			//Click on VALIDATE button
			ConnectedDevicesStrings.validateModifButtonClick(test,driver);
			//Modify button check 
			wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getModifyConnectedDeviceButton()));
			ConnectedDevicesStrings.modifyConnectedDeviceButtonCheck(test,driver);

			
		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test 
	(priority=5, enabled=true)
	public void removeRecentlyLoggedDevices() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(RecentlyLoggedDevicesStrings.TestName5, RecentlyLoggedDevicesStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Device tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(CommonDevicesPages.getDevicesTabBarButton()));
			CommonDevicesStrings.devicesTabBarButtonsCheck(test,driver);
			//Click on Device tab-bar
			CommonDevicesStrings.devicesTabBarButtonsClick(test,driver);
			//Number Of connected device
			wait.until(ExpectedConditions.visibilityOf(CommonDevicesPages.getConnectedDevice()));
			
			if (CommonDevicesPages.getConnectedDeviceText().equals("0 appareil connecté")) {
				//First device check 
				wait.until(ExpectedConditions.visibilityOf(RecentlyLoggedDevicesPages.getFirstDeviceList()));
				RecentlyLoggedDevicesStrings.firstDeviceListCheck(test,driver);
				//Click on the first device 
				RecentlyLoggedDevicesStrings.firstDeviceListClick(test,driver);
			} else if (CommonDevicesPages.getConnectedDeviceText().equals("1 appareil connecté")) {
				//Second device check
				wait.until(ExpectedConditions.visibilityOf(RecentlyLoggedDevicesPages.getSecondDeviceList()));
				RecentlyLoggedDevicesStrings.secondDeviceListCheck(test,driver);
				//Click on the Second device 
				RecentlyLoggedDevicesStrings.secondDeviceListClick(test,driver);
			} else if (CommonDevicesPages.getConnectedDeviceText().equals("2 appareils connectés")) {
				//Third device check 
				wait.until(ExpectedConditions.visibilityOf(RecentlyLoggedDevicesPages.getThirdDeviceList()));
				RecentlyLoggedDevicesStrings.thirdDeviceListCheck(test,driver);
				//Click on the Third device 
				RecentlyLoggedDevicesStrings.thirdDeviceListClick(test,driver);
			} else if (CommonDevicesPages.getConnectedDeviceText().equals("3 appareils connectés")) {
				//Fourth device check 
				wait.until(ExpectedConditions.visibilityOf(RecentlyLoggedDevicesPages.getFourthDeviceList()));
				RecentlyLoggedDevicesStrings.fourthDeviceListCheck(test,driver);
				//Click on the Fourth device 
				RecentlyLoggedDevicesStrings.fourthDeviceListClick(test,driver);
			} 
			//Device remove button check 
			wait.until(ExpectedConditions.visibilityOf(RecentlyLoggedDevicesPages.getDeviceDetailRemove()));
			RecentlyLoggedDevicesStrings.deviceDetailRemoveCheck(test,driver);
			//Click on device remove 
			RecentlyLoggedDevicesStrings.deviceDetailRemoveClick(test,driver);
			//Alert Device Title check 
			wait.until(ExpectedConditions.visibilityOf(RecentlyLoggedDevicesPages.getAlertDeviceTitle()));
			RecentlyLoggedDevicesStrings.alertDeviceTitleCheck(test,driver);
			//Alert Device Description check 
			wait.until(ExpectedConditions.visibilityOf(RecentlyLoggedDevicesPages.getAlertDeviceDesc()));
			RecentlyLoggedDevicesStrings.alertDeviceDescCheck(test,driver);
			//Alert decline button check 
			wait.until(ExpectedConditions.visibilityOf(RecentlyLoggedDevicesPages.getAlertDeviceDecline()));
			RecentlyLoggedDevicesStrings.alertDeviceDeclineCheck(test,driver);
			//Click on alert decline button
			RecentlyLoggedDevicesStrings.alertDeviceDeclineClick(test,driver);
			//Modify button check 
			wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getModifyConnectedDeviceButton()));
			ConnectedDevicesStrings.modifyConnectedDeviceButtonCheck(test,driver);


			
		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

}
