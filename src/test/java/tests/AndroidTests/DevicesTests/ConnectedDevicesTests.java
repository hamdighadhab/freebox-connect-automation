package tests.AndroidTests.DevicesTests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import Strings.CommonStrings;
import Strings.DevicesStrings.CommonDevicesStrings;
import Strings.DevicesStrings.ConnectedDevicesStrings;
import Strings.DevicesStrings.RecentlyLoggedDevicesStrings;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.DevicesPages.CommonDevicesPages;
import pages.DevicesPages.ConnectedDevicesPages;
import pages.HomePagePages.HomeConnectionStatus;
import tests.Base.BaseClass;

public class ConnectedDevicesTests extends BaseClass {

	@Test 
	(priority=1, enabled=true)
	public void connectedDevicesList() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(ConnectedDevicesStrings.TestName1, RecentlyLoggedDevicesStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Device tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(CommonDevicesPages.getDevicesTabBarButton()));
			CommonDevicesStrings.devicesTabBarButtonsCheck(test,driver);
			//Click on Device tab-bar
			CommonDevicesStrings.devicesTabBarButtonsClick(test,driver);
			//Number Of connected device
			wait.until(ExpectedConditions.visibilityOf(CommonDevicesPages.getConnectedDevice()));
			if (CommonDevicesPages.getConnectedDeviceText().equals("0 appareil connecté")) {
				test.log(Status.SKIP, "Aucun appareil connecté");
			} else {
				//connected list
				wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getConnectedDevicesSection()));
				ConnectedDevicesStrings.connectedDevicesSectionCheck(test,driver);
				//Chevron check
				wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getChevronConnectedDevice()));
				ConnectedDevicesStrings.chevronConnectedDeviceCheck(test,driver);
				//Click on Chevron
				ConnectedDevicesStrings.chevronConnectedDeviceClick(test,driver);
				Thread.sleep(1000);
				//Click on Chevron
				ConnectedDevicesStrings.chevronConnectedDeviceClick(test,driver);

			}
		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test 
	(priority=2, enabled=true)
	public void connectedDevicesListElement() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(ConnectedDevicesStrings.TestName2, RecentlyLoggedDevicesStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Device tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(CommonDevicesPages.getDevicesTabBarButton()));
			CommonDevicesStrings.devicesTabBarButtonsCheck(test,driver);
			//Click on Device tab-bar
			CommonDevicesStrings.devicesTabBarButtonsClick(test,driver);
			//Number Of connected device
			wait.until(ExpectedConditions.visibilityOf(CommonDevicesPages.getConnectedDevice()));
			if (CommonDevicesPages.getConnectedDeviceText().equals("0 appareil connecté")) {
				test.log(Status.SKIP, "Aucun appareil connecté");
			} else {
				//Devices Connection Type check
				wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getDevicesConnectionType()));
				ConnectedDevicesStrings.devicesConnectionTypeCheck(test,driver);
				//Device Up Rate check
				wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getDeviceUpRate()));
				ConnectedDevicesStrings.deviceUpRateCheck(test,driver);
				//Device Down Rate check
				wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getDeviceDownRate()));
				ConnectedDevicesStrings.deviceDownRateCheck(test,driver);
				//Device Rate Unit check
				wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getDeviceRateUnit()));
				ConnectedDevicesStrings.deviceRateUnitCheck(test,driver);
				//First Picto Device check
				wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getFirstPictoDevice()));
				ConnectedDevicesStrings.firstPictoDeviceCheck(test,driver);

			}
		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	
	@Test 
	(priority=3, enabled=true)
	public void connectedDevicesDetails() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(ConnectedDevicesStrings.TestName3, RecentlyLoggedDevicesStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Device tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(CommonDevicesPages.getDevicesTabBarButton()));
			CommonDevicesStrings.devicesTabBarButtonsCheck(test,driver);
			//Click on Device tab-bar
			CommonDevicesStrings.devicesTabBarButtonsClick(test,driver);
			//Number Of connected device
			wait.until(ExpectedConditions.visibilityOf(CommonDevicesPages.getConnectedDevice()));
			if (CommonDevicesPages.getConnectedDeviceText().equals("0 appareil connecté")) {
				test.log(Status.SKIP, "Aucun appareil connecté");
			} else {
				//First Picto Device check
				wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getFirstPictoDevice()));
				ConnectedDevicesStrings.firstPictoDeviceCheck(test,driver);
				//Click On connected device
				ConnectedDevicesStrings.firstPictoDeviceClick(test,driver);
				//Device Detail Image check
				wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getDeviceDetailImage()));
				ConnectedDevicesStrings.deviceDetailImagecheck(test,driver);
				//Device Detail Status check 
				wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getDeviceDetailStatus()));
				ConnectedDevicesStrings.deviceDetailStatusCheck(test,driver);
				//Device Detail Up Rate check
				wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getDeviceDetailUpRate()));
				ConnectedDevicesStrings.deviceDetailUpRatecheck(test,driver);
				//Device Detail Down Rate check
				wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getDeviceDetailDownRate()));
				ConnectedDevicesStrings.deviceDetailDownRatecheck(test,driver);
				//Device Detail Down Rate Unit check
				wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getDeviceDetailDownRateUnit()));
				ConnectedDevicesStrings.deviceDetailDownRateUnitcheck(test,driver);
				//Device Detail Gateway Icon check
				wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getDeviceDetailGatewayIcon()));
				ConnectedDevicesStrings.deviceDetailGatewayIconcheck(test,driver);
				//Device Detail Gateway Name check 
				wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getDeviceDetailGatewayName()));
				ConnectedDevicesStrings.deviceDetailGatewayNameCheck(test,driver);
				//Device Detail Name check 
				wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getDeviceDetailName()));
				ConnectedDevicesStrings.deviceDetailNameCheck(test,driver);
				//Device Detail Constructor check 
				wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getDeviceDetailConstructor()));
				ConnectedDevicesStrings.deviceDetailConstructorCheck(test,driver);
				//Device associated profile check 
				wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getDeviceDetailAssociatedProfile()));
				ConnectedDevicesStrings.deviceDetailAssociatedProfileCheck(test,driver);

			}
		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}
	

	@Test 
	(priority=4, enabled=true)
	public void connectedDevicesMoreDetails() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(ConnectedDevicesStrings.TestName4, RecentlyLoggedDevicesStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Device tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(CommonDevicesPages.getDevicesTabBarButton()));
			CommonDevicesStrings.devicesTabBarButtonsCheck(test,driver);
			//Click on Device tab-bar
			CommonDevicesStrings.devicesTabBarButtonsClick(test,driver);
			//Number Of connected device
			wait.until(ExpectedConditions.visibilityOf(CommonDevicesPages.getConnectedDevice()));
			if (CommonDevicesPages.getConnectedDeviceText().equals("0 appareil connecté")) {
				test.log(Status.SKIP, "Aucun appareil connecté");
			} else {
				//First Picto Device check
				wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getFirstPictoDevice()));
				ConnectedDevicesStrings.firstPictoDeviceCheck(test,driver);
				//Click On connected device
				ConnectedDevicesStrings.firstPictoDeviceClick(test,driver);
				//Device Detail Others Informations check 
				wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getDeviceDetailOthersInformations()));
				ConnectedDevicesStrings.deviceDetailOthersInformationsCheck(test,driver);
				//Click on Others Informations
				ConnectedDevicesStrings.deviceDetailOthersInformationsClick(test,driver);
				//IPV4 element check 
				wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getDeviceIPV4Adress()));
				ConnectedDevicesStrings.deviceIPV4AdressCheck(test,driver);
				//IPV6 element check 
				wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getDeviceIPV6Adress()));
				ConnectedDevicesStrings.deviceIPV6AdressCheck(test,driver);
				//MAC element check 
				wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getDeviceMACAdress()));
				ConnectedDevicesStrings.deviceMACAdressCheck(test,driver);


			}
		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test 
	(priority=5, enabled=true)
	public void ModifyConnectedDevices() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(ConnectedDevicesStrings.TestName5, RecentlyLoggedDevicesStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Device tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(CommonDevicesPages.getDevicesTabBarButton()));
			CommonDevicesStrings.devicesTabBarButtonsCheck(test,driver);
			//Click on Device tab-bar
			CommonDevicesStrings.devicesTabBarButtonsClick(test,driver);
			//Number Of connected device
			wait.until(ExpectedConditions.visibilityOf(CommonDevicesPages.getConnectedDevice()));
			if (CommonDevicesPages.getConnectedDeviceText().equals("0 appareil connecté")) {
				test.log(Status.SKIP, "Aucun appareil connecté");
			} else {
				//First Picto Device check
				wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getFirstPictoDevice()));
				ConnectedDevicesStrings.firstPictoDeviceCheck(test,driver);
				//Click On connected device
				ConnectedDevicesStrings.firstPictoDeviceClick(test,driver);
				//Modify button check 
				wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getModifyConnectedDeviceButton()));
				ConnectedDevicesStrings.modifyConnectedDeviceButtonCheck(test,driver);
				//Click on Modify button
				ConnectedDevicesStrings.modifyConnectedDeviceButtonClick(test,driver);
				//Modify Connected Device Page Name check 	
				wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getModifyConnectedDevicePageName()));
				ConnectedDevicesStrings.modifyConnectedDevicePageNameCheck(test,driver);
				//Field Modify Device Description check 
				wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getFieldModifyDeviceDesc()));
				ConnectedDevicesStrings.fieldModifyDeviceDescCheck(test,driver);
				//Field Modify Device Description check 
				wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getTypeConnectedDeviceDesc()));
				ConnectedDevicesStrings.typeConnectedDeviceDescCheck(test,driver);
				//Modify Connected Device Field check 	
				wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getModifyConnectedDeviceField()));
				ConnectedDevicesStrings.modifyConnectedDeviceFieldCheck(test,driver);
				//Click on Modify Connected Device Field
				ConnectedDevicesStrings.modifyConnectedDeviceFieldClick(test,driver);
				//Save initial device name
				ConnectedDevicesStrings.initialDeviceName();
				//Set new device name
				ConnectedDevicesPages.enterDeviceName("ConnectedDeviceAutomatedName");
				//Validate Button check
				wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getValidateModifButton()));
				ConnectedDevicesStrings.validateModifButtonCheck(test,driver);
				//Click on VALIDATE button
				ConnectedDevicesStrings.validateModifButtonClick(test,driver);
				//Modify button check 
				wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getModifyConnectedDeviceButton()));
				ConnectedDevicesStrings.modifyConnectedDeviceButtonCheck(test,driver);
				//Click on Modify button
				ConnectedDevicesStrings.modifyConnectedDeviceButtonClick(test,driver);
				//Modify Connected Device Field check 	
				wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getModifyConnectedDeviceField()));
				ConnectedDevicesStrings.modifyConnectedDeviceFieldCheck(test,driver);
				//Click on Modify Connected Device Field
				ConnectedDevicesStrings.modifyConnectedDeviceFieldClick(test,driver);
				//Set initial device name
				ConnectedDevicesPages.enterDeviceName(ConnectedDevicesStrings.InitialDeviceName);
				//Validate Button check
				wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getValidateModifButton()));
				ConnectedDevicesStrings.validateModifButtonCheck(test,driver);
				//Click on VALIDATE button
				ConnectedDevicesStrings.validateModifButtonClick(test,driver);
				//Modify button check 
				wait.until(ExpectedConditions.visibilityOf(ConnectedDevicesPages.getModifyConnectedDeviceButton()));
				ConnectedDevicesStrings.modifyConnectedDeviceButtonCheck(test,driver);

			}
		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

}
