package tests.AndroidTests.NetworkTests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import Strings.CommonStrings;
import Strings.HomePageStrings.ShareWifiAccessStrings;
import Strings.NetworkStrings.NetworkStrings;
import Strings.ProfilesStrings.ProfilesStrings;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.HomePagePages.HomeConnectionStatus;
import pages.HomePagePages.ShareWifiAccessPage;
import pages.NetworkPages.NetworkPages;
import pages.ProfilesPages.ProfilesPages;
import tests.Base.BaseClass;

public class WiFiRestrictionsTests extends BaseClass {
	@Test 
	(priority=1, enabled=true)
	public void BlockAccesswithoutDevice() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(NetworkStrings.TestName7, NetworkStrings.FonctionalityName2);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Network tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getNetworkTabBarButton()));
			NetworkStrings.networkTabBarButtonsCheck(test,driver);
			//Click on Network tab-bar
			NetworkStrings.networkTabBarButtonsClick(test,driver);
			//Restriction WiFi button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getRestrictionsWiFiButton()));
			NetworkStrings.restrictionsWiFiButtonCheck(test,driver);
			//Click on Restriction WiFi button
			NetworkStrings.RestrictionsWiFiButtonClick(test,driver);
			//Restriction WiFi page name Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getRestrictionsWiFiPageName()));
			NetworkStrings.restrictionsWiFiPageNameCheck(test,driver);
			//Restriction WiFi description Check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getRestrictionsWiFiDesc()));
			NetworkStrings.restrictionsWiFiDescCheck(test,driver);
			//Activate Restriction 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getRestrictionsTypeElement()));
			NetworkStrings.restrictionsTypeElementCheck(test,driver);
			//Check Restriction Type Value
			if ((NetworkPages.getRestrictionsTypeValueText().equals("Autoriser l'accès"))) {
				//Click on Restriction Type Value
				NetworkStrings.restrictionsTypeValueClick(test,driver);
				//Select "Bloquer l'accès" Restriction Type Value
				NetworkStrings.accessBlockElementClick(test,driver);
			}
			//Restriction Type Value check
			NetworkStrings.BlockAcessTypeValueCheck(test,driver);
			// Activation Restriction element Check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getActivationRestrictionsElement()));
			NetworkStrings.activationRestrictionsElementCheck(test,driver);
			//Switch WiFi Restriction Click
			NetworkStrings.restrictionsWiFiSwitchClick(test,driver);
			// No Device Restriction desc Check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getRestrictionsNoDeviceDesc()));
			NetworkStrings.restrictionsNoDeviceDescCheck(test,driver);
			//Validate button check
			NetworkStrings.restrictionsValidatebuttonCheck(test,driver);
			//Click on Validate button
			NetworkStrings.restrictionsValidatebuttonClick(test,driver);
			//Restriction WiFi button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getRestrictionsWiFiButton()));
			NetworkStrings.restrictionsWiFiButtonCheck(test,driver);

		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test 
	(priority=2, enabled=true)
	public void BlockAccessAddDevice() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(NetworkStrings.TestName8, NetworkStrings.FonctionalityName2);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Network tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getNetworkTabBarButton()));
			NetworkStrings.networkTabBarButtonsCheck(test,driver);
			//Click on Network tab-bar
			NetworkStrings.networkTabBarButtonsClick(test,driver);
			//Restriction WiFi button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getRestrictionsWiFiButton()));
			NetworkStrings.restrictionsWiFiButtonCheck(test,driver);
			//Click on Restriction WiFi button
			NetworkStrings.RestrictionsWiFiButtonClick(test,driver);
			//Restriction WiFi page name Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getRestrictionsWiFiPageName()));
			NetworkStrings.restrictionsWiFiPageNameCheck(test,driver);
			//Check Restriction Type Value
			if ((NetworkPages.getRestrictionsTypeValueText().equals("Autoriser l'accès"))) {
				//Click on Restriction Type Value
				NetworkStrings.restrictionsTypeValueClick(test,driver);
				//Select "Bloquer l'accès" Restriction Type Value
				NetworkStrings.accessBlockElementClick(test,driver);
			}
			//Restriction Type Value check
			NetworkStrings.BlockAcessTypeValueCheck(test,driver);
			// Activation Restriction element Check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getActivationRestrictionsElement()));
			NetworkStrings.activationRestrictionsElementCheck(test,driver);
			//Switch WiFi Restriction Click
			NetworkStrings.restrictionsWiFiSwitchClick(test,driver);
			// No Device Restriction desc Check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getRestrictionsNoDeviceDesc()));
			NetworkStrings.restrictionsNoDeviceDescCheck(test,driver);
			//Add Device button check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getAddDeviceButton()));
			NetworkStrings.addDeviceButtonCheck(test,driver);
			//Click on Add Device button
			NetworkStrings.addDeviceButtonClick(test,driver);	
			//Select Device List button check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getSelectListDevicesButton()));
			NetworkStrings.selectListDevicesButtonCheck(test,driver);
			//Click on Select Device List button
			NetworkStrings.selectListDevicesButtonClick(test,driver);
			//Select first device
			wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getFirstDeviceProfile()));
			ProfilesStrings.firstDeviceProfileSelect(test,driver);
			//Validate button check
			NetworkStrings.restrictionsValidatebuttonCheck(test,driver);
			//Click on Validate button
			NetworkStrings.restrictionsValidatebuttonClick(test,driver);
			Thread.sleep(2000);
			//Validate button check
			NetworkStrings.restrictionsValidatebuttonCheck(test,driver);
			//Click on Validate button
			NetworkStrings.restrictionsValidatebuttonClick(test,driver);
			Thread.sleep(2000);
			//Restriction WiFi button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getRestrictionsWiFiButton()));
			NetworkStrings.restrictionsWiFiButtonCheck(test,driver);


		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test 
	(priority=3, enabled=true)
	public void BlockAccessDeleteDevice() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(NetworkStrings.TestName9, NetworkStrings.FonctionalityName2);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Network tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getNetworkTabBarButton()));
			NetworkStrings.networkTabBarButtonsCheck(test,driver);
			//Click on Network tab-bar
			NetworkStrings.networkTabBarButtonsClick(test,driver);
			//Restriction WiFi button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getRestrictionsWiFiButton()));
			NetworkStrings.restrictionsWiFiButtonCheck(test,driver);
			//Click on Restriction WiFi button
			NetworkStrings.RestrictionsWiFiButtonClick(test,driver);
			//Restriction WiFi page name Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getRestrictionsWiFiPageName()));
			NetworkStrings.restrictionsWiFiPageNameCheck(test,driver);
			// Activation Restriction element Check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getActivationRestrictionsElement()));
			NetworkStrings.activationRestrictionsElementCheck(test,driver);
			//Switch WiFi Restriction Click
			NetworkStrings.restrictionsWiFiSwitchClick(test,driver);
			//Click to delete device
			NetworkStrings.deleteDeviceButtonClick(test,driver);
			//Confirm Delete
			wait.until(ExpectedConditions.visibilityOf(ShareWifiAccessPage.getComfirmationDeleteGuestWiFiAccess()));
			ShareWifiAccessStrings.comfirmationDeleteGuestWiFiAccessButtonClick(test, driver);
			// No Device Restriction desc Check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getRestrictionsNoDeviceDesc()));
			NetworkStrings.restrictionsNoDeviceDescCheck(test,driver);
			//Validate button check
			NetworkStrings.restrictionsValidatebuttonCheck(test,driver);
			//Click on Validate button
			NetworkStrings.restrictionsValidatebuttonClick(test,driver);
			//Restriction WiFi button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getRestrictionsWiFiButton()));
			NetworkStrings.restrictionsWiFiButtonCheck(test,driver);


		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test 
	(priority=4, enabled=true)
	public void AllowAccesswithoutDevice() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(NetworkStrings.TestName10, NetworkStrings.FonctionalityName2);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Network tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getNetworkTabBarButton()));
			NetworkStrings.networkTabBarButtonsCheck(test,driver);
			//Click on Network tab-bar
			NetworkStrings.networkTabBarButtonsClick(test,driver);
			//Restriction WiFi button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getRestrictionsWiFiButton()));
			NetworkStrings.restrictionsWiFiButtonCheck(test,driver);
			//Click on Restriction WiFi button
			NetworkStrings.RestrictionsWiFiButtonClick(test,driver);
			//Restriction WiFi page name Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getRestrictionsWiFiPageName()));
			NetworkStrings.restrictionsWiFiPageNameCheck(test,driver);
			//Restriction WiFi description Check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getRestrictionsWiFiDesc()));
			NetworkStrings.restrictionsWiFiDescCheck(test,driver);
			//Activate Restriction 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getRestrictionsTypeElement()));
			NetworkStrings.restrictionsTypeElementCheck(test,driver);
			//Check Restriction Type Value
			if ((NetworkPages.getRestrictionsTypeValueText().equals("Bloquer l'accès"))) {
				//Click on Restriction Type Value
				NetworkStrings.restrictionsTypeValueClick(test,driver);
				//Select "Bloquer l'accès" Restriction Type Value
				NetworkStrings.accessAlowElementClick(test,driver);
			}
			//Restriction Type Value check
			NetworkStrings.AllowAcessTypeValueCheck(test,driver);
			// Activation Restriction element Check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getActivationRestrictionsElement()));
			NetworkStrings.activationRestrictionsElementCheck(test,driver);
			//Switch WiFi Restriction Click
			NetworkStrings.restrictionsWiFiSwitchClick(test,driver);
			// No Device Restriction desc Check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getRestrictionsNoDeviceDesc()));
			NetworkStrings.restrictionsNoDeviceDescCheck(test,driver);
			//Validate button check
			NetworkStrings.restrictionsValidatebuttonCheck(test,driver);
			//Click on Validate button
			NetworkStrings.restrictionsValidatebuttonClick(test,driver);
			//Restriction WiFi button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getRestrictionsWiFiButton()));
			NetworkStrings.restrictionsWiFiButtonCheck(test,driver);

		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test 
	(priority=5, enabled=true)
	public void AllowAccessAddDevice() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(NetworkStrings.TestName11, NetworkStrings.FonctionalityName2);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Network tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getNetworkTabBarButton()));
			NetworkStrings.networkTabBarButtonsCheck(test,driver);
			//Click on Network tab-bar
			NetworkStrings.networkTabBarButtonsClick(test,driver);
			//Restriction WiFi button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getRestrictionsWiFiButton()));
			NetworkStrings.restrictionsWiFiButtonCheck(test,driver);
			//Click on Restriction WiFi button
			NetworkStrings.RestrictionsWiFiButtonClick(test,driver);
			//Restriction WiFi page name Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getRestrictionsWiFiPageName()));
			NetworkStrings.restrictionsWiFiPageNameCheck(test,driver);
			//Check Restriction Type Value
			if ((NetworkPages.getRestrictionsTypeValueText().equals("Bloquer l'accès"))) {
				//Click on Restriction Type Value
				NetworkStrings.restrictionsTypeValueClick(test,driver);
				//Select "Bloquer l'accès" Restriction Type Value
				NetworkStrings.accessAlowElementClick(test,driver);
			}
			//Restriction Type Value check
			NetworkStrings.AllowAcessTypeValueCheck(test,driver);
			// Activation Restriction element Check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getActivationRestrictionsElement()));
			NetworkStrings.activationRestrictionsElementCheck(test,driver);
			//Switch WiFi Restriction Click
			NetworkStrings.restrictionsWiFiSwitchClick(test,driver);
			// No Device Restriction desc Check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getRestrictionsNoDeviceDesc()));
			NetworkStrings.restrictionsNoDeviceDescCheck(test,driver);
			//Add Device button check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getAddDeviceButton()));
			NetworkStrings.addDeviceButtonCheck(test,driver);
			//Click on Add Device button
			NetworkStrings.addDeviceButtonClick(test,driver);	
			//Select Device List button check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getSelectListDevicesButton()));
			NetworkStrings.selectListDevicesButtonCheck(test,driver);
			//Click on Select Device List button
			NetworkStrings.selectListDevicesButtonClick(test,driver);
			//Select first device
			wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getFirstDeviceProfile()));
			ProfilesStrings.firstDeviceProfileSelect(test,driver);
			//Validate button check
			NetworkStrings.restrictionsValidatebuttonCheck(test,driver);
			//Click on Validate button
			NetworkStrings.restrictionsValidatebuttonClick(test,driver);
			Thread.sleep(2000);
			//Validate button check
			NetworkStrings.restrictionsValidatebuttonCheck(test,driver);
			//Click on Validate button
			NetworkStrings.restrictionsValidatebuttonClick(test,driver);
			//Restriction WiFi button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getRestrictionsWiFiButton()));
			NetworkStrings.restrictionsWiFiButtonCheck(test,driver);


		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test 
	(priority=6, enabled=true)
	public void AllowAccessDeleteDevice() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(NetworkStrings.TestName12, NetworkStrings.FonctionalityName2);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Network tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getNetworkTabBarButton()));
			NetworkStrings.networkTabBarButtonsCheck(test,driver);
			//Click on Network tab-bar
			NetworkStrings.networkTabBarButtonsClick(test,driver);
			//Restriction WiFi button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getRestrictionsWiFiButton()));
			NetworkStrings.restrictionsWiFiButtonCheck(test,driver);
			//Click on Restriction WiFi button
			NetworkStrings.RestrictionsWiFiButtonClick(test,driver);
			//Restriction WiFi page name Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getRestrictionsWiFiPageName()));
			NetworkStrings.restrictionsWiFiPageNameCheck(test,driver);
			// Activation Restriction element Check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getActivationRestrictionsElement()));
			NetworkStrings.activationRestrictionsElementCheck(test,driver);
			//Switch WiFi Restriction Click
			NetworkStrings.restrictionsWiFiSwitchClick(test,driver);
			//Click to delete device
			NetworkStrings.deleteDeviceButtonClick(test,driver);
			//Confirm Delete
			wait.until(ExpectedConditions.visibilityOf(ShareWifiAccessPage.getComfirmationDeleteGuestWiFiAccess()));
			ShareWifiAccessStrings.comfirmationDeleteGuestWiFiAccessButtonClick(test, driver);
			// No Device Restriction desc Check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getRestrictionsNoDeviceDesc()));
			NetworkStrings.restrictionsNoDeviceDescCheck(test,driver);
			//Validate button check
			NetworkStrings.restrictionsValidatebuttonCheck(test,driver);
			//Click on Validate button
			NetworkStrings.restrictionsValidatebuttonClick(test,driver);
			//Restriction WiFi button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getRestrictionsWiFiButton()));
			NetworkStrings.restrictionsWiFiButtonCheck(test,driver);


		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

}
