package tests.AndroidTests.NetworkTests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import Strings.CommonStrings;
import Strings.HomePageStrings.ShareWifiAccessStrings;
import Strings.NetworkStrings.NetworkStrings;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.HomePagePages.HomeConnectionStatus;
import pages.NetworkPages.NetworkPages;
import tests.Base.BaseClass;

public class MyNetwork extends BaseClass {
	
	@Test 
	(priority=1, enabled=true)
	public void ShowWiFiPassword() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(NetworkStrings.TestName17, NetworkStrings.FonctionalityName4);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Network tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getNetworkTabBarButton()));
			NetworkStrings.networkTabBarButtonsCheck(test,driver);
			//Click on Network tab-bar
			NetworkStrings.networkTabBarButtonsClick(test,driver);
			//Show password button check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getShowHidePasswordButton()));
			NetworkStrings.ShowPasswordButtonCheck(test,driver);
			//Click on Show Password button
			NetworkStrings.showHidePasswordButtonClick(test,driver);
			//Hide password button check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getShowHidePasswordButton()));
			NetworkStrings.HidePasswordButtonCheck(test,driver);
			//Click on Hide Password button
			NetworkStrings.showHidePasswordButtonClick(test,driver);


		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}
	
	

	@Test 
	(priority=2, enabled=true)
	public void EditSSID() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(NetworkStrings.TestName18, NetworkStrings.FonctionalityName4);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Network tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getNetworkTabBarButton()));
			NetworkStrings.networkTabBarButtonsCheck(test,driver);
			//Click on Network tab-bar
			NetworkStrings.networkTabBarButtonsClick(test,driver);
			//Edit network button check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getEditNetworkButton()));
			NetworkStrings.editNetworkButtonCheck(test,driver);
			//Click on Edit network button
			NetworkStrings.editNetworkButtonClick(test,driver);
			//Edit network Title page check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getEditNetworkTitlePage()));
			NetworkStrings.editNetworkTitlePageCheck(test,driver);
			//Network WiFi element check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getSSIDNetworkElement()));
			NetworkStrings.SSIDNetworkElementCheck(test,driver);
			//Select SSID
			NetworkStrings.SSIDNetworkElementClick(test,driver);
			//SSID name field check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getSSIDNetworkField()));
			NetworkStrings.SSIDNetworkFieldCheck(test,driver);
			//Select name field
			NetworkStrings.SSIDNetworkFieldClick(test,driver);
			//New SSID
			NetworkPages.enterNewSSIDName();
			driver.navigate().back();
			//Validate button check
			NetworkStrings.validateButtonCheck(test,driver);
			//Click on Edit network button
			NetworkStrings.validateButtonClick(test,driver);
			Thread.sleep(2000);
			//Validate button check
			NetworkStrings.restrictionsValidatebuttonCheck(test,driver);
			//Click on Validate button
			NetworkStrings.restrictionsValidatebuttonClick(test,driver);
			Thread.sleep(2000);
			//Confirmation
			ShareWifiAccessStrings.comfirmationDeleteGuestWiFiAccessButtonClick(test,driver);
			Thread.sleep(4000);
			//Click on Edit network button
			NetworkStrings.editNetworkButtonClick(test,driver);
			//Select SSID
			NetworkStrings.SSIDNetworkElementClick(test,driver);
			//SSID name field check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getSSIDNetworkField()));
			NetworkStrings.SSIDNetworkFieldCheck(test,driver);
			//Select name field
			NetworkStrings.SSIDNetworkFieldClick(test,driver);
			//Old SSID
			NetworkStrings.ClearSSIDFieldClick(test,driver);
			NetworkPages.enterOldSSIDName();
			driver.navigate().back();
			//Validate button check
			NetworkStrings.validateButtonCheck(test,driver);
			//Click on Edit network button
			NetworkStrings.validateButtonClick(test,driver);
			Thread.sleep(2000);
			//Validate button check
			NetworkStrings.restrictionsValidatebuttonCheck(test,driver);
			//Click on Validate button
			NetworkStrings.restrictionsValidatebuttonClick(test,driver);
			Thread.sleep(4000);
			//Confirmation
			ShareWifiAccessStrings.comfirmationDeleteGuestWiFiAccessButtonClick(test,driver);
			Thread.sleep(5000);
			//Show password button check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getShowHidePasswordButton()));
			NetworkStrings.ShowPasswordButtonCheck(test,driver);
			//Click on Show Password button
			NetworkStrings.showHidePasswordButtonClick(test,driver);
			Thread.sleep(2000);



			} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test 
	(priority=3, enabled=true)
	public void EditWiFiPwd() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(NetworkStrings.TestName19, NetworkStrings.FonctionalityName4);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Network tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getNetworkTabBarButton()));
			NetworkStrings.networkTabBarButtonsCheck(test,driver);
			//Click on Network tab-bar
			NetworkStrings.networkTabBarButtonsClick(test,driver);
			//Edit network button check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getEditNetworkButton()));
			NetworkStrings.editNetworkButtonCheck(test,driver);
			//Click on Edit network button
			NetworkStrings.editNetworkButtonClick(test,driver);
			//Edit network Title page check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getEditNetworkTitlePage()));
			NetworkStrings.editNetworkTitlePageCheck(test,driver);
			//WiFi Key element check
			NetworkStrings.networkKeyElementCheck(test,driver);
			//Click on WiFi Key button
			NetworkStrings.networkKeyElementClick(test,driver);
			//WiFi Key Page name check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getWiFiKeyTitlePage()));
			NetworkStrings.wiFiKeyTitlePageCheck(test,driver);
			//WiFi Key field name check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getWiFiKeyFieldName()));
			NetworkStrings.wiFiKeyFieldNameCheck(test,driver);
			//WiFi Key Description check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getWiFiKeyPageDesc()));
			NetworkStrings.wiFiKeyPageDescCheck(test,driver);
			//Edit WiFi Key
			NetworkStrings.ClearSSIDFieldClick(test,driver);
			NetworkStrings.WiFiKeyFieldEdit(test,driver,"AutomationKey");
			//WiFi Key Status check
			NetworkStrings.wiFiKeyStatusCheck(test,driver,"La clé doit faire au moins 8 caractères");
			driver.navigate().back();
			//Validate button check
			NetworkStrings.validateButtonCheck(test,driver);
			//Click on Edit network button
			NetworkStrings.validateButtonClick(test,driver);
			Thread.sleep(2000);
			//Validate button check
			NetworkStrings.restrictionsValidatebuttonCheck(test,driver);
			//Click on Validate button
			NetworkStrings.restrictionsValidatebuttonClick(test,driver);
			Thread.sleep(2000);
			//Confirmation
			ShareWifiAccessStrings.comfirmationDeleteGuestWiFiAccessButtonClick(test,driver);
			Thread.sleep(4000);
			//Show password button check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getShowHidePasswordButton()));
			NetworkStrings.ShowPasswordButtonCheck(test,driver);
			//Click on Show Password button
			NetworkStrings.showHidePasswordButtonClick(test,driver);
			//Edit network button check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getEditNetworkButton()));
			NetworkStrings.editNetworkButtonCheck(test,driver);
			//Click on Edit network button
			NetworkStrings.editNetworkButtonClick(test,driver);
			//Click on WiFi Key button
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getNetworkKeyElement()));
			NetworkStrings.networkKeyElementClick(test,driver);
			//Edit WiFi Key
			NetworkStrings.ClearSSIDFieldClick(test,driver);
			NetworkStrings.WiFiKeyFieldEdit(test,driver,"freeboxqa90");
			driver.navigate().back();
			//Validate button check
			NetworkStrings.validateButtonCheck(test,driver);
			//Click on Edit network button
			NetworkStrings.validateButtonClick(test,driver);
			Thread.sleep(2000);
			//Validate button check
			NetworkStrings.restrictionsValidatebuttonCheck(test,driver);
			//Click on Validate button
			NetworkStrings.restrictionsValidatebuttonClick(test,driver);
			Thread.sleep(2000);
			//Confirmation
			ShareWifiAccessStrings.comfirmationDeleteGuestWiFiAccessButtonClick(test,driver);
			Thread.sleep(4000);
			//Show password button check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getShowHidePasswordButton()));
			NetworkStrings.ShowPasswordButtonCheck(test,driver);
			//Click on Show Password button
			NetworkStrings.showHidePasswordButtonClick(test,driver);


			} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test 
	(priority=4, enabled=true)
	public void EditNotRecommendedSecurity() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(NetworkStrings.TestName20, NetworkStrings.FonctionalityName4);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Network tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getNetworkTabBarButton()));
			NetworkStrings.networkTabBarButtonsCheck(test,driver);
			//Click on Network tab-bar
			NetworkStrings.networkTabBarButtonsClick(test,driver);
			//Edit network button check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getEditNetworkButton()));
			NetworkStrings.editNetworkButtonCheck(test,driver);
			//Click on Edit network button
			NetworkStrings.editNetworkButtonClick(test,driver);
			//Edit network Title page check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getEditNetworkTitlePage()));
			NetworkStrings.editNetworkTitlePageCheck(test,driver);
			//WiFi Key element check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getSecurityWiFiElement()));
			NetworkStrings.securityWiFiElementCheck(test,driver);
			//Click on WiFi Key button
			NetworkStrings.securityWiFiElementClick(test,driver);
			//Click on Not recommended WiFi security element
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getRecommendedSecurityElement()));
			NetworkStrings.notrecommendedSecurityElementClick(test,driver);
			//Validate button check
			NetworkStrings.restrictionsValidatebuttonCheck(test,driver);
			//Click on Validate button
			NetworkStrings.restrictionsValidatebuttonClick(test,driver);
			Thread.sleep(5000);
			//Pop-up WiFi Security check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getPopupTitle()));
			NetworkStrings.popupTitleCheck(test,driver);
			//WiFi Key element check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getEditWiFiSecurityButton()));
			NetworkStrings.editWiFiSecurityButtonCheck(test,driver);
			//Click on WiFi Key button
			NetworkStrings.editWiFiSecurityButtonClick(test,driver);
			//WiFi Key element check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getSecurityWiFiElement()));
			NetworkStrings.securityWiFiElementCheck(test,driver);
			//Click on WiFi Key button
			NetworkStrings.securityWiFiElementClick(test,driver);
			//Click on recommended WiFi security element
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getNotRecommendedSecurityElement()));
			NetworkStrings.recommendedSecurityElementClick(test,driver);
			//Validate button check
			NetworkStrings.restrictionsValidatebuttonCheck(test,driver);
			//Click on Validate button
			NetworkStrings.restrictionsValidatebuttonClick(test,driver);
			Thread.sleep(5000);
			//Edit network button check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getEditNetworkButton()));
			NetworkStrings.editNetworkButtonCheck(test,driver);


			} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}
	
	@Test 
	(priority=5, enabled=true)
	public void EditConfWiFiType() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(NetworkStrings.TestName21, NetworkStrings.FonctionalityName4);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Network tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getNetworkTabBarButton()));
			NetworkStrings.networkTabBarButtonsCheck(test,driver);
			//Click on Network tab-bar
			NetworkStrings.networkTabBarButtonsClick(test,driver);
			//Edit network button check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getEditNetworkButton()));
			NetworkStrings.editNetworkButtonCheck(test,driver);
			//Click on Edit network button
			NetworkStrings.editNetworkButtonClick(test,driver);
			//WiFi Conf Type element check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getConfTypeElement()));
			NetworkStrings.confTypeElementCheck(test,driver);
			//Click on WiFi Conf Type
			NetworkStrings.confTypeElementClick(test,driver);
			//WiFi Conf Type Page check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getConfWiFiTypePage()));
			NetworkStrings.confWiFiTypePageCheck(test,driver);
			//Separated Conf Type element check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getSeparatedConfElement()));
			NetworkStrings.separatedConfElementCheck(test,driver);
			//Click On Separated
			NetworkStrings.separatedConfElementClick(test,driver);
			//Validate button check
			NetworkStrings.restrictionsValidatebuttonCheck(test,driver);
			//Click on Validate button
			NetworkStrings.restrictionsValidatebuttonClick(test,driver);
			Thread.sleep(5000);
			//Label element check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getWiFiInfoPag()));
			NetworkStrings.wiFiInfoPagCheck(test,driver,"5GHz-2");
			//Edit network button check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getEditNetworkButton()));
			NetworkStrings.editNetworkButtonCheck(test,driver);
			//Click on Edit network button
			NetworkStrings.editNetworkButtonClick(test,driver);
			//Click On 2.4GHz
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getTab2GHz()));
			NetworkStrings.tab2GHzClick(test,driver);
			//WiFi Conf Type element check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getConfTypeElement()));
			NetworkStrings.confTypeElementCheck(test,driver);
			//Click on WiFi Conf Type
			NetworkStrings.confTypeElementClick(test,driver);
			//Separated Conf Type element check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getUniqueConfElement()));
			NetworkStrings.uniqueConfElementCheck(test,driver);
			//Click On Separated
			NetworkStrings.uniqueConfElementClick(test,driver);
			//Confirm conf check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getConfirmTypeButton()));
			NetworkStrings.confirmTypeButtonCheck(test,driver);
			//Click On Confirm button
			NetworkStrings.confirmTypeButtonClick(test,driver);
			//Validate button check
			NetworkStrings.restrictionsValidatebuttonCheck(test,driver);
			//Click on Validate button
			NetworkStrings.restrictionsValidatebuttonClick(test,driver);
			//Confirmation
			ShareWifiAccessStrings.comfirmationDeleteGuestWiFiAccessButtonClick(test,driver);
			Thread.sleep(5000);
			//Edit network button check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getEditNetworkButton()));
			NetworkStrings.editNetworkButtonCheck(test,driver);

			} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}


}
