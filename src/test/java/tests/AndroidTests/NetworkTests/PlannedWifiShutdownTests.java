package tests.AndroidTests.NetworkTests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import Strings.CommonStrings;
import Strings.NetworkStrings.NetworkStrings;
import elements.CommonElements;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.HomePagePages.HomeConnectionStatus;
import pages.NetworkPages.NetworkPages;
import tests.Base.BaseClass;

public class PlannedWifiShutdownTests extends BaseClass {

	@Test 
	(priority=1, enabled=true)
	public void activateWiFi() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(NetworkStrings.TestName13, NetworkStrings.FonctionalityName3);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Network tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getNetworkTabBarButton()));
			NetworkStrings.networkTabBarButtonsCheck(test,driver);
			//Click on Network tab-bar
			NetworkStrings.networkTabBarButtonsClick(test,driver);
			if ((NetworkPages.getWifiStateSubtitleText().equals("Activé"))) {
				test.log(Status.PASS, "Le WiFi est Activé ");
			} else {
				//Planned WiFi OFF Element check
				wait.until(ExpectedConditions.visibilityOf(NetworkPages.getPlannedWiFiOFFElement()));
				NetworkStrings.plannedWiFiOFFElementCheck(test,driver);
				//Click On Planned WiFi OFF Element
				NetworkStrings.plannedWiFiOFFElementClick(test,driver);
				Thread.sleep(3000);
				if (driver.findElements(CommonElements.InApp_Validate_Button).size() > 0) {
					NetworkPages.clickOnInAppValidateButton();
				} 
				//WiFi Planning Page check
				wait.until(ExpectedConditions.visibilityOf(NetworkPages.getPlannedWiFiOFFPageName()));
				NetworkStrings.plannedWiFiOFFPageNameCheck(test,driver);
				//Wifi Planning Message check
				wait.until(ExpectedConditions.visibilityOf(NetworkPages.getWifiPlanningMessage()));
				NetworkStrings.wifiPlanningMessageCheck(test,driver);
				if (!(driver.findElements(CommonElements.Wifi_Planning_Header).size() > 0)) {
					NetworkPages.clickOnWifiPlanningEnable();
				} 
				CommonElements.scrollTo("Réinitialiser les plages horaires");
				//Wifi Planning Reset Button check
				wait.until(ExpectedConditions.visibilityOf(NetworkPages.getWifiPlanningResetButton()));
				NetworkStrings.wifiPlanningResetButtonCheck(test,driver);
				//Click On Wifi Planning Reset Button
				NetworkStrings.wifiPlanningResetButtonClick(test,driver);
				//WifiPlanningResetConfirmation check
				wait.until(ExpectedConditions.visibilityOf(NetworkPages.getWifiPlanningResetConfirmation()));
				NetworkStrings.wifiPlanningResetConfirmationCheck(test,driver);
				//Click On WifiPlanningResetConfirmation
				NetworkStrings.wifiPlanningResetConfirmationClick(test,driver);
				//Wifi Planning Validate Button check
				wait.until(ExpectedConditions.visibilityOf(NetworkPages.getWifiPlanningValidateButton()));
				NetworkStrings.wifiPlanningValidateButtonCheck(test,driver);
				//Click On Wifi Planning Validate Button
				NetworkStrings.wifiPlanningValidateButtonClick(test,driver);

				//Wifi State Subtitle check
				Thread.sleep(3000);
				NetworkStrings.wifiStateSubtitleCheck(test,driver,"Activé");
			}
		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}


	@Test 
	(priority=2, enabled=true)
	public void wiFiPlanningFirstSlot() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(NetworkStrings.TestName14, NetworkStrings.FonctionalityName3);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Network tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getNetworkTabBarButton()));
			NetworkStrings.networkTabBarButtonsCheck(test,driver);
			//Click on Network tab-bar
			NetworkStrings.networkTabBarButtonsClick(test,driver);
			//Planned WiFi OFF Element check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getPlannedWiFiOFFElement()));
			NetworkStrings.plannedWiFiOFFElementCheck(test,driver);
			//Click On Planned WiFi OFF Element
			NetworkStrings.plannedWiFiOFFElementClick(test,driver);
			Thread.sleep(3000);
			if (driver.findElements(CommonElements.InApp_Validate_Button).size() > 0) {
				NetworkPages.clickOnInAppValidateButton();
			} 
			//WiFi Planning Page check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getPlannedWiFiOFFPageName()));
			NetworkStrings.plannedWiFiOFFPageNameCheck(test,driver);
			//Wifi Planning Message check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getWifiPlanningMessage()));
			NetworkStrings.wifiPlanningMessageCheck(test,driver);
			if (!(driver.findElements(CommonElements.Wifi_Planning_Header).size() > 0)) {
				NetworkPages.clickOnWifiPlanningEnable();
			} 
			NetworkPages.clickOnFirstTimeSlot();
			CommonElements.scrollTo("Valider les plages horaires");
			//Wifi Planning Validate Button check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getWifiPlanningValidateButton()));
			NetworkStrings.wifiPlanningValidateButtonCheck(test,driver);
			//Click On Wifi Planning Validate Button
			NetworkStrings.wifiPlanningValidateButtonClick(test,driver);
			//Wifi State Subtitle check
			Thread.sleep(3000);
			NetworkStrings.wifiStateSubtitleCheck(test,driver,"Activé jusqu'à demain 00h00");

		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test 
	(priority=3, enabled=true)
	public void resetWiFiPlanning() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(NetworkStrings.TestName15, NetworkStrings.FonctionalityName3);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Network tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getNetworkTabBarButton()));
			NetworkStrings.networkTabBarButtonsCheck(test,driver);
			//Click on Network tab-bar
			NetworkStrings.networkTabBarButtonsClick(test,driver);
			//Planned WiFi OFF Element check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getPlannedWiFiOFFElement()));
			NetworkStrings.plannedWiFiOFFElementCheck(test,driver);
			//Click On Planned WiFi OFF Element
			NetworkStrings.plannedWiFiOFFElementClick(test,driver);
			Thread.sleep(3000);
			if (driver.findElements(CommonElements.InApp_Validate_Button).size() > 0) {
				NetworkPages.clickOnInAppValidateButton();
			} 
			//WiFi Planning Page check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getPlannedWiFiOFFPageName()));
			NetworkStrings.plannedWiFiOFFPageNameCheck(test,driver);
			//Wifi Planning Message check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getWifiPlanningMessage()));
			NetworkStrings.wifiPlanningMessageCheck(test,driver);
			if (!(driver.findElements(CommonElements.Wifi_Planning_Header).size() > 0)) {
				NetworkPages.clickOnWifiPlanningEnable();
			} 
			CommonElements.scrollTo("Réinitialiser les plages horaires");
			//Wifi Planning Reset Button check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getWifiPlanningResetButton()));
			NetworkStrings.wifiPlanningResetButtonCheck(test,driver);
			//Click On Wifi Planning Reset Button
			NetworkStrings.wifiPlanningResetButtonClick(test,driver);
			//WifiPlanningResetConfirmation check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getWifiPlanningResetConfirmation()));
			NetworkStrings.wifiPlanningResetConfirmationCheck(test,driver);
			//Click On WifiPlanningResetConfirmation
			NetworkStrings.wifiPlanningResetConfirmationClick(test,driver);
			//Wifi Planning Validate Button check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getWifiPlanningValidateButton()));
			NetworkStrings.wifiPlanningValidateButtonCheck(test,driver);
			//Click On Wifi Planning Validate Button
			NetworkStrings.wifiPlanningValidateButtonClick(test,driver);

			//Wifi State Subtitle check
			Thread.sleep(3000);
			NetworkStrings.wifiStateSubtitleCheck(test,driver,"Activé");

		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test 
	(priority=4, enabled=true)
	public void wiFiPlanningLastSlot() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(NetworkStrings.TestName16, NetworkStrings.FonctionalityName3);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Network tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getNetworkTabBarButton()));
			NetworkStrings.networkTabBarButtonsCheck(test,driver);
			//Click on Network tab-bar
			NetworkStrings.networkTabBarButtonsClick(test,driver);
			//Planned WiFi OFF Element check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getPlannedWiFiOFFElement()));
			NetworkStrings.plannedWiFiOFFElementCheck(test,driver);
			//Click On Planned WiFi OFF Element
			NetworkStrings.plannedWiFiOFFElementClick(test,driver);
			Thread.sleep(3000);
			if (driver.findElements(CommonElements.InApp_Validate_Button).size() > 0) {
				NetworkPages.clickOnInAppValidateButton();
			} 
			//WiFi Planning Page check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getPlannedWiFiOFFPageName()));
			NetworkStrings.plannedWiFiOFFPageNameCheck(test,driver);
			//Wifi Planning Message check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getWifiPlanningMessage()));
			NetworkStrings.wifiPlanningMessageCheck(test,driver);
			if (!(driver.findElements(CommonElements.Wifi_Planning_Header).size() > 0)) {
				NetworkPages.clickOnWifiPlanningEnable();
			} 
			NetworkPages.clickOnLastTimeSlot();
			CommonElements.scrollTo("Valider les plages horaires");
			//Wifi Planning Validate Button check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getWifiPlanningValidateButton()));
			NetworkStrings.wifiPlanningValidateButtonCheck(test,driver);
			//Click On Wifi Planning Validate Button
			NetworkStrings.wifiPlanningValidateButtonClick(test,driver);
			//Wifi State Subtitle check
			Thread.sleep(3000);
			NetworkStrings.wifiStateSubtitleCheck(test,driver,"Activé jusqu'à 23h30");

		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

}
