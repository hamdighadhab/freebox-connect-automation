package tests.AndroidTests.NetworkTests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import Strings.CommonStrings;
import Strings.NetworkStrings.NetworkStrings;
import Strings.ProfilesStrings.ProfilesStrings;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.HomePagePages.HomeConnectionStatus;
import pages.NetworkPages.NetworkPages;
import tests.Base.BaseClass;

public class DiagWiFiTests extends BaseClass {
	
	@Test 
	(priority=1, enabled=true)
	public void DiagWifiSecurity() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(NetworkStrings.TestName1, ProfilesStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Network tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getNetworkTabBarButton()));
			NetworkStrings.networkTabBarButtonsCheck(test,driver);
			//Click on Network tab-bar
			NetworkStrings.networkTabBarButtonsClick(test,driver);
			//Diag WiFi button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagWiFiButton()));
			NetworkStrings.diagWiFiButtonCheck(test,driver);
			//Click on Diag WiFi
			NetworkStrings.diagWiFiButtonClick(test,driver);
			//Start Diag button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagStartButton()));
			NetworkStrings.diagStartButtonCheck(test,driver);
			//Click on Start Diag button
			NetworkStrings.diagStartButtonClick(test,driver);
			// Diag page name Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagWiFiTitle()));
			NetworkStrings.diagWiFiTitleCheck(test,driver);
			// Diag page name Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagWiFiDesc()));
			NetworkStrings.diagWiFiDescCheck(test,driver);
			// Diag Params Check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagParamVerif()));
			NetworkStrings.diagParamVerifCheck(test,driver);
			// Diag status Check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagVerifStatus()));
			NetworkStrings.diagVerifStatusCheck(test,driver);
			//Click on detail Diag setting button
			NetworkStrings.detailsSettingsButtonClick(test,driver);
			// Diag Settings page name Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getVerifedSettingsPage()));
			NetworkStrings.verifedSettingsPageCheck(test,driver);
			// Setting Secutity WiFi Check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getSettingSecutityWiFi()));
			NetworkStrings.settingSecutityWiFiCheck(test,driver);
			// Click on Setting Secutity WiFi
			NetworkStrings.settingSecutityWiFiButtonClick(test,driver);
			// Diag Secutity Detail Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagSecutityDetail()));
			NetworkStrings.diagSecutityDetailCheck(test,driver);
			// Diag Secutity Desc Check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagSecutityDesc()));
			NetworkStrings.diagSecutityDescCheck(test,driver);
			// OK button Check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagnosticDetailButton()));
			NetworkStrings.diagnosticDetailButtonCheck(test,driver);
			//Click on OK button
			NetworkStrings.diagnosticDetailButtonClick(test,driver);
			// Setting Secutity WiFi Check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getSettingSecutityWiFi()));
			NetworkStrings.settingSecutityWiFiCheck(test,driver);

		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test 
	(priority=2, enabled=true)
	public void DiagWiFiVisibility() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(NetworkStrings.TestName2, ProfilesStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Network tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getNetworkTabBarButton()));
			NetworkStrings.networkTabBarButtonsCheck(test,driver);
			//Click on Network tab-bar
			NetworkStrings.networkTabBarButtonsClick(test,driver);
			//Diag WiFi button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagWiFiButton()));
			NetworkStrings.diagWiFiButtonCheck(test,driver);
			//Click on Diag WiFi
			NetworkStrings.diagWiFiButtonClick(test,driver);
			//Start Diag button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagStartButton()));
			NetworkStrings.diagStartButtonCheck(test,driver);
			//Click on Start Diag button
			NetworkStrings.diagStartButtonClick(test,driver);
			// Diag page name Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagWiFiTitle()));
			NetworkStrings.diagWiFiTitleCheck(test,driver);
			// Diag page name Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagWiFiDesc()));
			NetworkStrings.diagWiFiDescCheck(test,driver);
			// Diag Params Check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagParamVerif()));
			NetworkStrings.diagParamVerifCheck(test,driver);
			// Diag status Check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagVerifStatus()));
			NetworkStrings.diagVerifStatusCheck(test,driver);
			//Click on detail Diag setting button
			NetworkStrings.detailsSettingsButtonClick(test,driver);
			// Diag Settings page name Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getVerifedSettingsPage()));
			NetworkStrings.verifedSettingsPageCheck(test,driver);
			// Visibility WiFi element Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getSettingVisibilityWiFi()));
			NetworkStrings.settingVisibilityWiFiCheck(test,driver);
			//Click on Visibility WiFi button
			NetworkStrings.settingVisibilityWiFiClick(test,driver);
			// Diag Visibility Detail Check 
			NetworkStrings.diagVisibilityDetailCheck(test,driver);
			// Diag Visibility Desc Check 
			NetworkStrings.diagVisibilityDescCheck(test,driver);
			// OK button Check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagnosticDetailButton()));
			NetworkStrings.diagnosticDetailButtonCheck(test,driver);
			//Click on OK button
			NetworkStrings.diagnosticDetailButtonClick(test,driver);

		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test 
	(priority=3, enabled=true)
	public void Diagbandwidth() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(NetworkStrings.TestName3, ProfilesStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Network tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getNetworkTabBarButton()));
			NetworkStrings.networkTabBarButtonsCheck(test,driver);
			//Click on Network tab-bar
			NetworkStrings.networkTabBarButtonsClick(test,driver);
			//Diag WiFi button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagWiFiButton()));
			NetworkStrings.diagWiFiButtonCheck(test,driver);
			//Click on Diag WiFi
			NetworkStrings.diagWiFiButtonClick(test,driver);
			//Start Diag button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagStartButton()));
			NetworkStrings.diagStartButtonCheck(test,driver);
			//Click on Start Diag button
			NetworkStrings.diagStartButtonClick(test,driver);
			// Diag page name Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagWiFiTitle()));
			NetworkStrings.diagWiFiTitleCheck(test,driver);
			// Diag page name Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagWiFiDesc()));
			NetworkStrings.diagWiFiDescCheck(test,driver);
			// Diag Params Check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagParamVerif()));
			NetworkStrings.diagParamVerifCheck(test,driver);
			// Diag status Check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagVerifStatus()));
			NetworkStrings.diagVerifStatusCheck(test,driver);
			//Click on detail Diag setting button
			NetworkStrings.detailsSettingsButtonClick(test,driver);
			// Diag Settings page name Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getVerifedSettingsPage()));
			NetworkStrings.verifedSettingsPageCheck(test,driver);
			// bandwidth element Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getSettingBandwidthWiFi()));
			NetworkStrings.settingBandwidthWiFiCheck(test,driver);
			//Click on bandwidth Element
			NetworkStrings.settingBandwidthWiFiClick(test,driver);
			// Diag bandwidth detail Check 
			NetworkStrings.diagBandwidthDetailCheck(test,driver);
			// Diag bandwidth desc Check 
			NetworkStrings.diagBandwidthDescCheck(test,driver);
			// OK button Check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagnosticDetailButton()));
			NetworkStrings.diagnosticDetailButtonCheck(test,driver);
			//Click on OK button
			NetworkStrings.diagnosticDetailButtonClick(test,driver);

		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test 
	(priority=4, enabled=true)
	public void DiagCanals() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(NetworkStrings.TestName4, ProfilesStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Network tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getNetworkTabBarButton()));
			NetworkStrings.networkTabBarButtonsCheck(test,driver);
			//Click on Network tab-bar
			NetworkStrings.networkTabBarButtonsClick(test,driver);
			//Diag WiFi button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagWiFiButton()));
			NetworkStrings.diagWiFiButtonCheck(test,driver);
			//Click on Diag WiFi
			NetworkStrings.diagWiFiButtonClick(test,driver);
			//Start Diag button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagStartButton()));
			NetworkStrings.diagStartButtonCheck(test,driver);
			//Click on Start Diag button
			NetworkStrings.diagStartButtonClick(test,driver);
			// Diag page name Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagWiFiTitle()));
			NetworkStrings.diagWiFiTitleCheck(test,driver);
			// Diag page name Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagWiFiDesc()));
			NetworkStrings.diagWiFiDescCheck(test,driver);
			// Diag Params Check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagParamVerif()));
			NetworkStrings.diagParamVerifCheck(test,driver);
			// Diag status Check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagVerifStatus()));
			NetworkStrings.diagVerifStatusCheck(test,driver);
			//Click on detail Diag setting button
			NetworkStrings.detailsSettingsButtonClick(test,driver);
			// Diag Settings page name Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getVerifedSettingsPage()));
			NetworkStrings.verifedSettingsPageCheck(test,driver);
			// Canals element Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getSettingCanalsWiFi()));
			NetworkStrings.settingCanalsWiFiCheck(test,driver);
			//Click on Canals Element
			NetworkStrings.settingCanalsWiFiClick(test,driver);
			// Diag Canals detail Check 
			NetworkStrings.diagCanalsDetailCheck(test,driver);
			// Diag Canals desc Check 
			NetworkStrings.diagCanalsDescCheck(test,driver);
			// OK button Check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagnosticDetailButton()));
			NetworkStrings.diagnosticDetailButtonCheck(test,driver);
			//Click on OK button
			NetworkStrings.diagnosticDetailButtonClick(test,driver);

		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test 
	(priority=5, enabled=true)
	public void DiagWiFiStatus() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(NetworkStrings.TestName5, ProfilesStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Network tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getNetworkTabBarButton()));
			NetworkStrings.networkTabBarButtonsCheck(test,driver);
			//Click on Network tab-bar
			NetworkStrings.networkTabBarButtonsClick(test,driver);
			//Diag WiFi button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagWiFiButton()));
			NetworkStrings.diagWiFiButtonCheck(test,driver);
			//Click on Diag WiFi
			NetworkStrings.diagWiFiButtonClick(test,driver);
			//Start Diag button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagStartButton()));
			NetworkStrings.diagStartButtonCheck(test,driver);
			//Click on Start Diag button
			NetworkStrings.diagStartButtonClick(test,driver);
			// Diag page name Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagWiFiTitle()));
			NetworkStrings.diagWiFiTitleCheck(test,driver);
			// Diag page name Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagWiFiDesc()));
			NetworkStrings.diagWiFiDescCheck(test,driver);
			// Diag Params Check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagParamVerif()));
			NetworkStrings.diagParamVerifCheck(test,driver);
			// Diag status Check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagVerifStatus()));
			NetworkStrings.diagVerifStatusCheck(test,driver);
			//Click on detail Diag setting button
			NetworkStrings.detailsSettingsButtonClick(test,driver);
			// Diag Settings page name Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getVerifedSettingsPage()));
			NetworkStrings.verifedSettingsPageCheck(test,driver);
			// WiFiStatus element Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getStatusWiFi()));
			NetworkStrings.settingStatusWiFiCheck(test,driver);
			//Click on WiFiStatus Element
			NetworkStrings.settingStatusWiFiClick(test,driver);
			// Diag WiFiStatus detail Check 
			NetworkStrings.diagStatusDetailCheck(test,driver);
			// Diag WiFiStatus desc Check 
			NetworkStrings.diagStatusDescCheck(test,driver);
			// OK button Check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagnosticDetailButton()));
			NetworkStrings.diagnosticDetailButtonCheck(test,driver);
			//Click on OK button
			NetworkStrings.diagnosticDetailButtonClick(test,driver);

		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test 
	(priority=6, enabled=true)
	public void DiagWiFiRange() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(NetworkStrings.TestName6, ProfilesStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Network tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getNetworkTabBarButton()));
			NetworkStrings.networkTabBarButtonsCheck(test,driver);
			//Click on Network tab-bar
			NetworkStrings.networkTabBarButtonsClick(test,driver);
			//Diag WiFi button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagWiFiButton()));
			NetworkStrings.diagWiFiButtonCheck(test,driver);
			//Click on Diag WiFi
			NetworkStrings.diagWiFiButtonClick(test,driver);
			//Start Diag button Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagStartButton()));
			NetworkStrings.diagStartButtonCheck(test,driver);
			//Click on Start Diag button
			NetworkStrings.diagStartButtonClick(test,driver);
			// Diag page name Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagWiFiTitle()));
			NetworkStrings.diagWiFiTitleCheck(test,driver);
			// Diag page name Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagWiFiDesc()));
			NetworkStrings.diagWiFiDescCheck(test,driver);
			// Diag Params Check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagParamVerif()));
			NetworkStrings.diagParamVerifCheck(test,driver);
			// Diag status Check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagVerifStatus()));
			NetworkStrings.diagVerifStatusCheck(test,driver);
			//Click on detail Diag setting button
			NetworkStrings.detailsSettingsButtonClick(test,driver);
			// Diag Settings page name Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getVerifedSettingsPage()));
			NetworkStrings.verifedSettingsPageCheck(test,driver);
			// WiFiRange element Check 
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getRangeWiFi()));
			NetworkStrings.settingRangeWiFiCheck(test,driver);
			//Click on WiFiRange Element
			NetworkStrings.settingRangeWiFiClick(test,driver);
			// Diag WiFiRange detail Check 
			NetworkStrings.diagRangeDetailCheck(test,driver);
			// Diag WiFiRange desc Check 
			NetworkStrings.diagRangeDescCheck(test,driver);
			// OK button Check
			wait.until(ExpectedConditions.visibilityOf(NetworkPages.getDiagnosticDetailButton()));
			NetworkStrings.diagnosticDetailButtonCheck(test,driver);
			//Click on OK button
			NetworkStrings.diagnosticDetailButtonClick(test,driver);

		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}


}
