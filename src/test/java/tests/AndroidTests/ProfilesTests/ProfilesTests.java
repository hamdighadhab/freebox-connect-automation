package tests.AndroidTests.ProfilesTests;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import Strings.CommonStrings;
import Strings.DevicesStrings.CommonDevicesStrings;
import Strings.ProfilesStrings.ProfilesStrings;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.DevicesPages.CommonDevicesPages;
import pages.HomePagePages.HomeConnectionStatus;
import pages.ProfilesPages.ProfilesPages;
import tests.Base.BaseClass;

public class ProfilesTests extends BaseClass{


	@Test 
	(priority=1, enabled=true)
	public void emptyProfilePage() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(ProfilesStrings.TestName1, ProfilesStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Device tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(CommonDevicesPages.getDevicesTabBarButton()));
			CommonDevicesStrings.devicesTabBarButtonsCheck(test,driver);
			//Profiles tab-bar check
			wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfilesTabBarButton()));
			ProfilesStrings.profilesTabBarButtonCheck(test,driver);
			//Click on Profiles tab-bar
			ProfilesStrings.profilesTabBarButtonClick(test,driver);
			if ((ProfilesPages.getProfileNumberText().equals(ProfilesPages.EmptyProfile))) {
				//Profiles Number check
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfileNumber()));
				ProfilesStrings.profileNumberCheck(test,driver);
				//Profile empty list image check
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfilesEmptyListImage()));
				ProfilesStrings.profilesEmptyListImageCheck(test,driver);
				//Profile empty list description check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfilesEmptyListDesc()));
				ProfilesStrings.profilesEmptyListDescCheck(test,driver);
				//Profile empty list button check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfilesEmptyListButton()));
				ProfilesStrings.profilesEmptyListButtonCheck(test,driver);
			} else {
				test.log(Status.SKIP, "La page des profiles n'est pas vide");
			}
		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test 
	(priority=2, enabled=true)
	public void createProfileWithoutDevice() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(ProfilesStrings.TestName2, ProfilesStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Device tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(CommonDevicesPages.getDevicesTabBarButton()));
			CommonDevicesStrings.devicesTabBarButtonsCheck(test,driver);
			//Profiles tab-bar check
			wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfilesTabBarButton()));
			ProfilesStrings.profilesTabBarButtonCheck(test,driver);
			//Click on Profiles tab-bar
			ProfilesStrings.profilesTabBarButtonClick(test,driver);
			if ((ProfilesPages.getProfileNumberText().equals(ProfilesPages.EmptyProfile))) {
				//Profile empty list button check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfilesEmptyListButton()));
				ProfilesStrings.profilesEmptyListButtonCheck(test,driver);
				//Profile empty list button click 
				ProfilesStrings.profilesEmptyListButtonClick(test,driver);
				//New Profile page name check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getNewProfilePage()));
				ProfilesStrings.newProfilePageCheck(test,driver);
				//Profile icon description check
				ProfilesStrings.profileNameDescCheck(test,driver);
				//Profile icon description check
				ProfilesStrings.profileIconDescCheck(test,driver);
				//Enter first profile name
				ProfilesStrings.enterProfileName(test,driver, "AutomationProfile");
				//Profile icon check 
				ProfilesStrings.profileSecondIconSelectCheck(test,driver);
				//Profile icon select 
				ProfilesStrings.profileSecondIconSelectSelect(test,driver);
				//Profile empty list button check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getNextButtonNewProfiles()));
				ProfilesStrings.nextButtonNewProfilesCheck(test,driver);	
				//Profile empty list button click 
				ProfilesStrings.nextButtonNewProfilesClick(test,driver);
				Thread.sleep(2000);
				//Associate Devices page check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getAssociateDevicesPage()));
				ProfilesStrings.associateDevicesPageCheck(test,driver);
				//Close associate Devices page 
				driver.navigate().back();
				//Profile page number check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfileNumber()));
				ProfilesStrings.oneProfileNumberCheck(test,driver);
				//Profile name check 
				ProfilesStrings.profileListItemNameCheck(test,driver, "AutomationProfile");
				//Device associated to profile number check 
				ProfilesStrings.associetedDeviceNumberCheck(test,driver);
				//Profile Status check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfilStatus()));
				ProfilesStrings.profilStatusCheck(test,driver);
			} else {
				test.log(Status.SKIP, "La page des profiles n'est pas vide");
			}
		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}


	@Test 
	(priority=3, enabled=true)
	public void addDeviceToProfileWithoutDevice() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(ProfilesStrings.TestName3, ProfilesStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Device tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(CommonDevicesPages.getDevicesTabBarButton()));
			CommonDevicesStrings.devicesTabBarButtonsCheck(test,driver);
			//Profiles tab-bar check
			wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfilesTabBarButton()));
			ProfilesStrings.profilesTabBarButtonCheck(test,driver);
			//Click on Profiles tab-bar
			ProfilesStrings.profilesTabBarButtonClick(test,driver);
			if ((ProfilesPages.getProfileNumberText().equals(ProfilesPages.OneProfile))) {
				//Detail Profile button check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getDetailsProfilButton()));
				ProfilesStrings.detailsProfilButtonCheck(test,driver);
				//Detail Profile button click 
				ProfilesStrings.detailsProfilButtonClick(test,driver);
				//Element associated device check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfilesAssociatedDeviceElement()));
				ProfilesStrings.profilesAssociatedDeviceElementCheck(test,driver);
				//Number of associated device to profile check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfilesAssociatedDeviceNumber()));
				ProfilesStrings.profilesAssociatedDeviceNumberCheck(test,driver);
				//Element associated device click 
				ProfilesStrings.profilesAssociatedDeviceElementClick(test,driver);
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getNoDeviceButton()));
				ProfilesStrings.noDeviceButtonCheck(test,driver);
				//Add associated device click 
				ProfilesStrings.noDeviceButtonClick(test,driver);
				//Select first device
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getFirstDeviceProfile()));
				ProfilesStrings.firstDeviceProfileSelect(test,driver);
				//Validate Associated Device
				ProfilesStrings.validateDeviceAssociated(test,driver);
				Thread.sleep(1500);
				driver.navigate().back();
				Thread.sleep(1500);
				//One associated device to profile check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfilesAssociatedDeviceNumber()));
				ProfilesStrings.profilesAssociatedOneDeviceCheck(test,driver);
			} else {
				test.log(Status.SKIP, "La page des profiles n'affiche pas 1 profil");
			}
		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test 
	(priority=4, enabled=true)
	public void addHolidayToProfile() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(ProfilesStrings.TestName4, ProfilesStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Device tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(CommonDevicesPages.getDevicesTabBarButton()));
			CommonDevicesStrings.devicesTabBarButtonsCheck(test,driver);
			//Profiles tab-bar check
			wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfilesTabBarButton()));
			ProfilesStrings.profilesTabBarButtonCheck(test,driver);
			//Click on Profiles tab-bar
			ProfilesStrings.profilesTabBarButtonClick(test,driver);
			if ((ProfilesPages.getProfileNumberText().equals(ProfilesPages.OneProfile))) {
				//Detail Profile button check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getDetailsProfilButton()));
				ProfilesStrings.detailsProfilButtonCheck(test,driver);
				//Detail Profile button click 
				ProfilesStrings.detailsProfilButtonClick(test,driver);
				//School holiday element check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfileHolidaySchoolElement()));
				ProfilesStrings.profileHolidaySchoolElementCheck(test,driver);
				//School holiday Value check 
				ProfilesStrings.profileHolidaySchoolValueCheck(test,driver,"Non");
				//School holiday element click 
				ProfilesStrings.profileHolidaySchoolElementClick(test,driver);
				//Holiday Zone A element check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getHolidayZoneAProfile()));
				ProfilesStrings.holidayZoneAProfileCheck(test,driver);
				//Holiday Zone A element click 
				ProfilesStrings.holidayZoneAProfileClick(test,driver);
				//School holiday Value check 
				Thread.sleep(1000);
				ProfilesStrings.profileHolidaySchoolValueCheck(test,driver,"Zone A");
				//School holiday element click 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfileHolidaySchoolElement()));
				ProfilesStrings.profileHolidaySchoolElementClick(test,driver);
				//Holiday Zone B element check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getHolidayZoneBProfile()));
				ProfilesStrings.holidayZoneBProfileCheck(test,driver);
				//Holiday Zone B element click 
				ProfilesStrings.holidayZoneBProfileClick(test,driver);
				//School holiday Value check 
				Thread.sleep(1000);
				ProfilesStrings.profileHolidaySchoolValueCheck(test,driver,"Zone B");
				//School holiday element click 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfileHolidaySchoolElement()));
				ProfilesStrings.profileHolidaySchoolElementClick(test,driver);
				//Holiday Zone C element check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getHolidayZoneCProfile()));
				ProfilesStrings.holidayZoneCProfileCheck(test,driver);
				//Holiday Zone B element click 
				ProfilesStrings.holidayZoneCProfileClick(test,driver);
				//School holiday Value check
				Thread.sleep(1000);
				ProfilesStrings.profileHolidaySchoolValueCheck(test,driver,"Zone C");
				//School holiday element click 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfileHolidaySchoolElement()));
				ProfilesStrings.profileHolidaySchoolElementClick(test,driver);
				//Holiday Zone Corse element check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getHolidayZoneCProfile()));
				ProfilesStrings.holidayCorseProfileCheck(test,driver);
				//Holiday Zone Corse element click 
				ProfilesStrings.holidayCorseProfileClick(test,driver);
				//School holiday Value check 
				Thread.sleep(1000);
				ProfilesStrings.profileHolidaySchoolValueCheck(test,driver,"Corse");
				//School holiday element click 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfileHolidaySchoolElement()));
				ProfilesStrings.profileHolidaySchoolElementClick(test,driver);
				//None Holiday element check 
				ProfilesStrings.holidayAucuneProfileCheck(test,driver);
				//None Holiday element click 
				ProfilesStrings.holidayAucuneProfileClick(test,driver);
				//School holiday Value check 
				Thread.sleep(1000);
				ProfilesStrings.profileHolidaySchoolValueCheck(test,driver,"Non");

			} else {
				test.log(Status.SKIP, "La page des profiles n'affiche pas 1 profil");
			}
		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test 
	(priority=5, enabled=true)
	public void editProfile() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(ProfilesStrings.TestName5, ProfilesStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Device tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(CommonDevicesPages.getDevicesTabBarButton()));
			CommonDevicesStrings.devicesTabBarButtonsCheck(test,driver);
			//Profiles tab-bar check
			wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfilesTabBarButton()));
			ProfilesStrings.profilesTabBarButtonCheck(test,driver);
			//Click on Profiles tab-bar
			ProfilesStrings.profilesTabBarButtonClick(test,driver);
			if ((ProfilesPages.getProfileNumberText().equals(ProfilesPages.OneProfile))) {
				//Detail Profile button check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getDetailsProfilButton()));
				ProfilesStrings.detailsProfilButtonCheck(test,driver);
				//Detail Profile button click 
				ProfilesStrings.detailsProfilButtonClick(test,driver);
				//Edit Profile button check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getEditProfileButton()));
				ProfilesStrings.editProfileButtonCheck(test,driver);
				//Edit Profile button click 
				ProfilesStrings.editProfileButtonClick(test,driver);
				//Enter first profile name
				ProfilesStrings.enterProfileName(test,driver, "AutoProfileEdited");
				//Validate button click 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getNextButtonNewProfiles()));
				ProfilesStrings.nextButtonNewProfilesClick(test,driver);
				Thread.sleep(2000);
				driver.navigate().back();
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfileListItemName()));
				//Profile name check 
				ProfilesStrings.profileListItemNameCheck(test,driver, "AutoProfileEdited");
			} else {
				test.log(Status.SKIP, "La page des profiles n'affiche pas 1 profil");
			}
		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test 
	(priority=6, enabled=true)
	public void pauseInfiniteFromProfileList() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(ProfilesStrings.TestName6, ProfilesStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Device tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(CommonDevicesPages.getDevicesTabBarButton()));
			CommonDevicesStrings.devicesTabBarButtonsCheck(test,driver);
			//Profiles tab-bar check
			wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfilesTabBarButton()));
			ProfilesStrings.profilesTabBarButtonCheck(test,driver);
			//Click on Profiles tab-bar
			ProfilesStrings.profilesTabBarButtonClick(test,driver);
			if ((ProfilesPages.getProfileNumberText().equals(ProfilesPages.OneProfile))) {
				//Profile Status check
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfileStatus()));
				ProfilesStrings.profileStatusCheck(test,driver, "Aucune pause à venir");
				//Pause from List Profile button check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getPauseProfileButton()));
				ProfilesStrings.pauseProfileButtonCheck(test,driver);
				//Pause from List Profile button click 
				ProfilesStrings.pauseProfileButtonClick(test,driver, "Mettre en pause");
				//Select & Submit Pause 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getApplicatePauseButton()));
				ProfilesStrings.selectPause(test,driver,"4");
				//Profile Status check
				Thread.sleep(3000);
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfileStatus()));
				ProfilesStrings.profileStatusCheck(test,driver, "En pause");
				//Stop Pause
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getPauseProfileButton()));
				ProfilesStrings.pauseProfileButtonClick(test,driver, "Arrêter la pause");
				//Profile Status check
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfileStatus()));
				Thread.sleep(2000);
				ProfilesStrings.profileStatusCheck(test,driver, "Aucune pause à venir");
			} else {
				test.log(Status.SKIP, "La page des profiles n'affiche pas 1 profil");
			}
		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test 
	(priority=7, enabled=true)
	public void pause30MinFromProfileList() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(ProfilesStrings.TestName7, ProfilesStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Device tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(CommonDevicesPages.getDevicesTabBarButton()));
			CommonDevicesStrings.devicesTabBarButtonsCheck(test,driver);
			//Profiles tab-bar check
			wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfilesTabBarButton()));
			ProfilesStrings.profilesTabBarButtonCheck(test,driver);
			//Click on Profiles tab-bar
			ProfilesStrings.profilesTabBarButtonClick(test,driver);
			if ((ProfilesPages.getProfileNumberText().equals(ProfilesPages.OneProfile))) {
				//Profile Status check
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfileStatus()));
				ProfilesStrings.profileStatusCheck(test,driver, "Aucune pause à venir");
				//Pause from List Profile button check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getPauseProfileButton()));
				ProfilesStrings.pauseProfileButtonCheck(test,driver);
				//Pause from List Profile button click 
				ProfilesStrings.pauseProfileButtonClick(test,driver, "Mettre en pause");
				//Select & Submit Pause 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getApplicatePauseButton()));
				ProfilesStrings.selectPause(test,driver,"1");
				//Profile Status check
				Calendar now = Calendar.getInstance();
				now.add(Calendar.MINUTE, 30);
				// 24 hours format
				SimpleDateFormat df = new SimpleDateFormat("HH'h'mm");
				Thread.sleep(2000);
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfileStatus()));
				ProfilesStrings.profileStatusCheck(test,driver, "Pause jusqu'à "+df.format(now.getTime()));
				//Stop Pause
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getPauseProfileButton()));
				ProfilesStrings.pauseProfileButtonClick(test,driver, "Arrêter la pause");
				//Profile Status check
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfileStatus()));
				Thread.sleep(2000);
				ProfilesStrings.profileStatusCheck(test,driver, "Aucune pause à venir");
			} else {
				test.log(Status.SKIP, "La page des profiles n'affiche pas 1 profil");
			}
		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test 
	(priority=8, enabled=true)
	public void pause1HourFromProfileList() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(ProfilesStrings.TestName8, ProfilesStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Device tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(CommonDevicesPages.getDevicesTabBarButton()));
			CommonDevicesStrings.devicesTabBarButtonsCheck(test,driver);
			//Profiles tab-bar check
			wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfilesTabBarButton()));
			ProfilesStrings.profilesTabBarButtonCheck(test,driver);
			//Click on Profiles tab-bar
			ProfilesStrings.profilesTabBarButtonClick(test,driver);
			if ((ProfilesPages.getProfileNumberText().equals(ProfilesPages.OneProfile))) {
				//Profile Status check
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfileStatus()));
				ProfilesStrings.profileStatusCheck(test,driver, "Aucune pause à venir");
				//Pause from List Profile button check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getPauseProfileButton()));
				ProfilesStrings.pauseProfileButtonCheck(test,driver);
				//Pause from List Profile button click 
				ProfilesStrings.pauseProfileButtonClick(test,driver, "Mettre en pause");
				//Select & Submit Pause 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getApplicatePauseButton()));
				ProfilesStrings.selectPause(test,driver,"2");
				//Profile Status check
				Calendar now = Calendar.getInstance();
				now.add(Calendar.MINUTE, 60);
				// 24 hours format
				SimpleDateFormat df = new SimpleDateFormat("HH'h'mm");
				Thread.sleep(2000);
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfileStatus()));
				ProfilesStrings.profileStatusCheck(test,driver, "Pause jusqu'à "+df.format(now.getTime()));
				//Stop Pause
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getPauseProfileButton()));
				ProfilesStrings.pauseProfileButtonClick(test,driver, "Arrêter la pause");
				//Profile Status check
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfileStatus()));
				Thread.sleep(2000);
				ProfilesStrings.profileStatusCheck(test,driver, "Aucune pause à venir");
			} else {
				test.log(Status.SKIP, "La page des profiles n'affiche pas 1 profil");
			}
		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test 
	(priority=9, enabled=true)
	public void pause2HoursFromProfileList() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(ProfilesStrings.TestName9, ProfilesStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Device tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(CommonDevicesPages.getDevicesTabBarButton()));
			CommonDevicesStrings.devicesTabBarButtonsCheck(test,driver);
			//Profiles tab-bar check
			wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfilesTabBarButton()));
			ProfilesStrings.profilesTabBarButtonCheck(test,driver);
			//Click on Profiles tab-bar
			ProfilesStrings.profilesTabBarButtonClick(test,driver);
			if ((ProfilesPages.getProfileNumberText().equals(ProfilesPages.OneProfile))) {
				//Profile Status check
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfileStatus()));
				ProfilesStrings.profileStatusCheck(test,driver, "Aucune pause à venir");
				//Pause from List Profile button check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getPauseProfileButton()));
				ProfilesStrings.pauseProfileButtonCheck(test,driver);
				//Pause from List Profile button click 
				ProfilesStrings.pauseProfileButtonClick(test,driver, "Mettre en pause");
				//Select & Submit Pause 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getApplicatePauseButton()));
				ProfilesStrings.selectPause(test,driver,"3");
				//Profile Status check
				Calendar now = Calendar.getInstance();
				now.add(Calendar.MINUTE, 120);
				// 24 hours format
				SimpleDateFormat df = new SimpleDateFormat("HH'h'mm");
				Thread.sleep(2000);
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfileStatus()));
				ProfilesStrings.profileStatusCheck(test,driver, "Pause jusqu'à "+df.format(now.getTime()));
				//Stop Pause
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getPauseProfileButton()));
				ProfilesStrings.pauseProfileButtonClick(test,driver, "Arrêter la pause");
				//Profile Status check
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfileStatus()));
				Thread.sleep(2000);
				ProfilesStrings.profileStatusCheck(test,driver, "Aucune pause à venir");
			} else {
				test.log(Status.SKIP, "La page des profiles n'affiche pas 1 profil");
			}
		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}


	@Test 
	(priority=10, enabled=true)
	public void pauseInfiniteFromProfile() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(ProfilesStrings.TestName10, ProfilesStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Device tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(CommonDevicesPages.getDevicesTabBarButton()));
			CommonDevicesStrings.devicesTabBarButtonsCheck(test,driver);
			//Profiles tab-bar check
			wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfilesTabBarButton()));
			ProfilesStrings.profilesTabBarButtonCheck(test,driver);
			//Click on Profiles tab-bar
			ProfilesStrings.profilesTabBarButtonClick(test,driver);
			if ((ProfilesPages.getProfileNumberText().equals(ProfilesPages.OneProfile))) {
				//Profile Status check
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfileStatus()));
				ProfilesStrings.profileStatusCheck(test,driver, "Aucune pause à venir");
				//Detail Profile button check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getDetailsProfilButton()));
				ProfilesStrings.detailsProfilButtonCheck(test,driver);
				//Detail Profile button click 
				ProfilesStrings.detailsProfilButtonClick(test,driver);
				//Detail Profile Status check
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getDetailProfileStatus()));
				ProfilesStrings.detailProfileStatusCheck(test,driver, "Aucune pause à venir");
				//Pause from Profile button click
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getSetPauseProfileButton()));
				ProfilesStrings.setPauseProfileButtonClick(test,driver,"Mettre en pause");
				//Select & Submit Pause 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getApplicatePauseButton()));
				ProfilesStrings.selectPause(test,driver,"4");
				//Detail Profile Status check
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getDetailProfileStatus()));
				Thread.sleep(3000);
				ProfilesStrings.detailProfileStatusCheck(test,driver, "En pause");
				//Pause from Profile button click
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getSetPauseProfileButton()));
				ProfilesStrings.setPauseProfileButtonClick(test,driver,"Arrêter la pause");
				//Detail Profile Status check
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getDetailProfileStatus()));
				Thread.sleep(2000);
				ProfilesStrings.detailProfileStatusCheck(test,driver, "Aucune pause à venir");			
			} else {
				test.log(Status.SKIP, "La page des profiles n'affiche pas 1 profil");
			}
		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test 
	(priority=11, enabled=true)
	public void pause30minFromProfile() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(ProfilesStrings.TestName11, ProfilesStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Device tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(CommonDevicesPages.getDevicesTabBarButton()));
			CommonDevicesStrings.devicesTabBarButtonsCheck(test,driver);
			//Profiles tab-bar check
			wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfilesTabBarButton()));
			ProfilesStrings.profilesTabBarButtonCheck(test,driver);
			//Click on Profiles tab-bar
			ProfilesStrings.profilesTabBarButtonClick(test,driver);
			if ((ProfilesPages.getProfileNumberText().equals(ProfilesPages.OneProfile))) {
				//Profile Status check
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfileStatus()));
				ProfilesStrings.profileStatusCheck(test,driver, "Aucune pause à venir");
				//Detail Profile button check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getDetailsProfilButton()));
				ProfilesStrings.detailsProfilButtonCheck(test,driver);
				//Detail Profile button click 
				ProfilesStrings.detailsProfilButtonClick(test,driver);
				//Detail Profile Status check
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getDetailProfileStatus()));
				ProfilesStrings.detailProfileStatusCheck(test,driver, "Aucune pause à venir");
				//Pause from Profile button click
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getSetPauseProfileButton()));
				ProfilesStrings.setPauseProfileButtonClick(test,driver,"Mettre en pause");
				//Select & Submit Pause 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getApplicatePauseButton()));
				ProfilesStrings.selectPause(test,driver,"1");
				//Profile Status check
				Calendar now = Calendar.getInstance();
				now.add(Calendar.MINUTE, 30);
				// 24 hours format
				SimpleDateFormat df = new SimpleDateFormat("HH'h'mm");
				Thread.sleep(2000);
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getDetailProfileStatus()));
				ProfilesStrings.detailProfileStatusCheck(test,driver, "En pause jusqu'à "+df.format(now.getTime()));
				//Stop Pause from Profile button click
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getSetPauseProfileButton()));
				ProfilesStrings.setPauseProfileButtonClick(test,driver,"Arrêter la pause");
				//Detail Profile Status check
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getDetailProfileStatus()));
				Thread.sleep(2000);
				ProfilesStrings.detailProfileStatusCheck(test,driver, "Aucune pause à venir");			
			} else {
				test.log(Status.SKIP, "La page des profiles n'affiche pas 1 profil");
			}
		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test 
	(priority=12, enabled=true)
	public void pause1HourFromProfile() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(ProfilesStrings.TestName12, ProfilesStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Device tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(CommonDevicesPages.getDevicesTabBarButton()));
			CommonDevicesStrings.devicesTabBarButtonsCheck(test,driver);
			//Profiles tab-bar check
			wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfilesTabBarButton()));
			ProfilesStrings.profilesTabBarButtonCheck(test,driver);
			//Click on Profiles tab-bar
			ProfilesStrings.profilesTabBarButtonClick(test,driver);
			if ((ProfilesPages.getProfileNumberText().equals(ProfilesPages.OneProfile))) {
				//Profile Status check
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfileStatus()));
				ProfilesStrings.profileStatusCheck(test,driver, "Aucune pause à venir");
				//Detail Profile button check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getDetailsProfilButton()));
				ProfilesStrings.detailsProfilButtonCheck(test,driver);
				//Detail Profile button click 
				ProfilesStrings.detailsProfilButtonClick(test,driver);
				//Detail Profile Status check
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getDetailProfileStatus()));
				ProfilesStrings.detailProfileStatusCheck(test,driver, "Aucune pause à venir");
				//Pause from Profile button click
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getSetPauseProfileButton()));
				ProfilesStrings.setPauseProfileButtonClick(test,driver,"Mettre en pause");
				//Select & Submit Pause 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getApplicatePauseButton()));
				ProfilesStrings.selectPause(test,driver,"2");
				//Profile Status check
				Calendar now = Calendar.getInstance();
				now.add(Calendar.MINUTE, 60);
				// 24 hours format
				SimpleDateFormat df = new SimpleDateFormat("HH'h'mm");
				Thread.sleep(2000);
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getDetailProfileStatus()));
				ProfilesStrings.detailProfileStatusCheck(test,driver, "En pause jusqu'à "+df.format(now.getTime()));
				//Stop Pause from Profile button click
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getSetPauseProfileButton()));
				ProfilesStrings.setPauseProfileButtonClick(test,driver,"Arrêter la pause");
				//Detail Profile Status check
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getDetailProfileStatus()));
				Thread.sleep(2000);
				ProfilesStrings.detailProfileStatusCheck(test,driver, "Aucune pause à venir");			
			} else {
				test.log(Status.SKIP, "La page des profiles n'affiche pas 1 profil");
			}
		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test 
	(priority=13, enabled=true)
	public void pause2HoursFromProfile() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(ProfilesStrings.TestName13, ProfilesStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Device tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(CommonDevicesPages.getDevicesTabBarButton()));
			CommonDevicesStrings.devicesTabBarButtonsCheck(test,driver);
			//Profiles tab-bar check
			wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfilesTabBarButton()));
			ProfilesStrings.profilesTabBarButtonCheck(test,driver);
			//Click on Profiles tab-bar
			ProfilesStrings.profilesTabBarButtonClick(test,driver);
			if ((ProfilesPages.getProfileNumberText().equals(ProfilesPages.OneProfile))) {
				//Profile Status check
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfileStatus()));
				ProfilesStrings.profileStatusCheck(test,driver, "Aucune pause à venir");
				//Detail Profile button check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getDetailsProfilButton()));
				ProfilesStrings.detailsProfilButtonCheck(test,driver);
				//Detail Profile button click 
				ProfilesStrings.detailsProfilButtonClick(test,driver);
				//Detail Profile Status check
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getDetailProfileStatus()));
				ProfilesStrings.detailProfileStatusCheck(test,driver, "Aucune pause à venir");
				//Pause from Profile button click
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getSetPauseProfileButton()));
				ProfilesStrings.setPauseProfileButtonClick(test,driver,"Mettre en pause");
				//Select & Submit Pause 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getApplicatePauseButton()));
				ProfilesStrings.selectPause(test,driver,"3");
				//Profile Status check
				Calendar now = Calendar.getInstance();
				now.add(Calendar.MINUTE, 120);
				// 24 hours format
				SimpleDateFormat df = new SimpleDateFormat("HH'h'mm");
				Thread.sleep(2000);
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getDetailProfileStatus()));
				ProfilesStrings.detailProfileStatusCheck(test,driver, "En pause jusqu'à "+df.format(now.getTime()));
				//Stop Pause from Profile button click
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getSetPauseProfileButton()));
				ProfilesStrings.setPauseProfileButtonClick(test,driver,"Arrêter la pause");
				//Detail Profile Status check
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getDetailProfileStatus()));
				Thread.sleep(2000);
				ProfilesStrings.detailProfileStatusCheck(test,driver, "Aucune pause à venir");			
			} else {
				test.log(Status.SKIP, "La page des profiles n'affiche pas 1 profil");
			}
		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test 
	(priority=14, enabled=true)
	public void addScheduledPause() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(ProfilesStrings.TestName14, ProfilesStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Device tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(CommonDevicesPages.getDevicesTabBarButton()));
			CommonDevicesStrings.devicesTabBarButtonsCheck(test,driver);
			//Profiles tab-bar check
			wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfilesTabBarButton()));
			ProfilesStrings.profilesTabBarButtonCheck(test,driver);
			//Click on Profiles tab-bar
			ProfilesStrings.profilesTabBarButtonClick(test,driver);
			if ((ProfilesPages.getProfileNumberText().equals(ProfilesPages.OneProfile))) {
				//Profile Status check
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfileStatus()));
				ProfilesStrings.profileStatusCheck(test,driver, "Aucune pause à venir");
				//Detail Profile button check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getDetailsProfilButton()));
				ProfilesStrings.detailsProfilButtonCheck(test,driver);
				//Detail Profile button click 
				ProfilesStrings.detailsProfilButtonClick(test,driver);
				//Detail Profile Status check
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getDetailProfileStatus()));
				ProfilesStrings.detailProfileStatusCheck(test,driver, "Aucune pause à venir");
				//Scheduled Break Button check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getScheduledBreakButton()));
				ProfilesStrings.scheduledBreakButtonCheck(test,driver);
				//Scheduled Break Button click 
				ProfilesStrings.scheduledBreakButtonClick(test,driver);
				//New Break Page Title check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getNewBreakPageTitle()));
				ProfilesStrings.newBreakPageTitleCheck(test,driver);
				//New Break Page Title check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getBreakNameElement()));
				ProfilesStrings.breakNameElementCheck(test,driver);
				//Enter first profile name
				ProfilesStrings.enterScheduledPauseName(test,driver, "AutomationScheduledPause");
				//Hour Start Element check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getHourStartElement()));
				ProfilesStrings.hourStartElementCheck(test,driver);
				//Hour Start Element check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getHourEndElement()));
				ProfilesStrings.hourEndElementCheck(test,driver);
				//Week Days Header check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getWeekDaysElement()));
				ProfilesStrings.weekDaysElementCheck(test,driver);
				//Profile Pause Edit Summary check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfilePauseEditSummary()));
				ProfilesStrings.profilePauseEditSummaryCheck(test,driver);
				//Pause Edit Friday check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getPauseEditFriday()));
				ProfilesStrings.pauseEditFridayCheck(test,driver);
				//Pause Edit Friday click 
				ProfilesStrings.pauseEditFridayClick(test,driver);
				//Pause Edit Friday check
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getPauseEditSaturday()));
				ProfilesStrings.pauseEditSaturdayCheck(test,driver);
				//Pause Edit Friday click 
				ProfilesStrings.pauseEditSaturdayClick(test,driver);
				//Validate Scheduled Pause button click 
				Thread.sleep(2000);
				ProfilesStrings.validateScheduledPauseClick(test,driver);
				//Detail Profile Status check
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getDetailProfileStatus()));
				Thread.sleep(3000);
				ProfilesStrings.detailProfileStatusCheck(test,driver, "Prochaine pause : aujourd’hui à 20h00");			
				
			} else {
				test.log(Status.SKIP, "La page des profiles n'affiche pas 1 profil");
			}
		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test 
	(priority=15, enabled=true)
	public void disableScheduledPause() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(ProfilesStrings.TestName15, ProfilesStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Device tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(CommonDevicesPages.getDevicesTabBarButton()));
			CommonDevicesStrings.devicesTabBarButtonsCheck(test,driver);
			//Profiles tab-bar check
			wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfilesTabBarButton()));
			ProfilesStrings.profilesTabBarButtonCheck(test,driver);
			//Click on Profiles tab-bar
			ProfilesStrings.profilesTabBarButtonClick(test,driver);
			if ((ProfilesPages.getProfileNumberText().equals(ProfilesPages.OneProfile))) {
				//Detail Profile button check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getDetailsProfilButton()));
				ProfilesStrings.detailsProfilButtonCheck(test,driver);
				//Detail Profile button click 
				ProfilesStrings.detailsProfilButtonClick(test,driver);
				Thread.sleep(1000);
				ProfilesStrings.detailProfileStatusCheck(test,driver, "Prochaine pause : aujourd’hui à 20h00");	
				//Scheduled pause switch click 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getScheduledPauseOFF()));
				ProfilesStrings.scheduledPauseOFFClick(test,driver);
				Thread.sleep(1000);
				ProfilesStrings.detailProfileStatusCheck(test,driver, "Aucune pause à venir");			

			} else {
				test.log(Status.SKIP, "La page des profiles n'affiche pas 1 profil");
			}
		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test 
	(priority=16, enabled=true)
	public void deleteScheduledPause() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(ProfilesStrings.TestName16, ProfilesStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Device tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(CommonDevicesPages.getDevicesTabBarButton()));
			CommonDevicesStrings.devicesTabBarButtonsCheck(test,driver);
			//Profiles tab-bar check
			wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfilesTabBarButton()));
			ProfilesStrings.profilesTabBarButtonCheck(test,driver);
			//Click on Profiles tab-bar
			ProfilesStrings.profilesTabBarButtonClick(test,driver);
			if ((ProfilesPages.getProfileNumberText().equals(ProfilesPages.OneProfile))) {
				//Profile Status check
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfileStatus()));
				ProfilesStrings.profileStatusCheck(test,driver, "Aucune pause à venir");
				//Detail Profile button check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getDetailsProfilButton()));
				ProfilesStrings.detailsProfilButtonCheck(test,driver);
				//Detail Profile button click 
				ProfilesStrings.detailsProfilButtonClick(test,driver);
				//Detail Profile Status check
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getDetailProfileStatus()));
				ProfilesStrings.detailProfileStatusCheck(test,driver, "Aucune pause à venir");
				Thread.sleep(1000);
				ProfilesStrings.detailProfileStatusCheck(test,driver, "Aucune pause à venir");	
				//Scheduled Pause Item click 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getScheduledPauseItem()));
				ProfilesStrings.scheduledPauseItemClick(test,driver);
				//Scheduled Pause Delete Button check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getScheduledPauseDeleteButton()));
				ProfilesStrings.scheduledPauseDeleteButtonCheck(test,driver);
				//Scheduled Pause Delete Button click 
				ProfilesStrings.scheduledPauseDeleteButtonClick(test,driver);
				//Delete confirmation 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getScheduledPauseDeleteConfirmationButton()));
				ProfilesStrings.scheduledPauseDeleteConfirmationButtonClick(test,driver);
				Thread.sleep(1000);
				ProfilesStrings.detailProfileStatusCheck(test,driver, "Aucune pause à venir");	
				//Scheduled Break Button check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getScheduledBreakButton()));
				ProfilesStrings.scheduledBreakButtonCheck(test,driver);

			} else {
				test.log(Status.SKIP, "La page des profiles n'affiche pas 1 profil");
			}
		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	
	@Test 
	(priority=17, enabled=true)
	public void deleteProfile() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(ProfilesStrings.TestName17, ProfilesStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Device tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(CommonDevicesPages.getDevicesTabBarButton()));
			CommonDevicesStrings.devicesTabBarButtonsCheck(test,driver);
			//Profiles tab-bar check
			wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfilesTabBarButton()));
			ProfilesStrings.profilesTabBarButtonCheck(test,driver);
			//Click on Profiles tab-bar
			ProfilesStrings.profilesTabBarButtonClick(test,driver);
			if ((ProfilesPages.getProfileNumberText().equals(ProfilesPages.OneProfile))) {
				//Profile Status check
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfileStatus()));
				ProfilesStrings.profileStatusCheck(test,driver, "Aucune pause à venir");
				//Detail Profile button check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getDetailsProfilButton()));
				ProfilesStrings.detailsProfilButtonCheck(test,driver);
				//Detail Profile button click 
				ProfilesStrings.detailsProfilButtonClick(test,driver);
				//Edit Profile button check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getEditProfileButton()));
				ProfilesStrings.editProfileButtonCheck(test,driver);
				//Edit Profile button click 
				ProfilesStrings.editProfileButtonClick(test,driver);
				//Profile Delete Button check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfileDeleteButton()));
				ProfilesStrings.profileDeleteButtonCheck(test,driver);
				//Profile Delete Button click 
				ProfilesStrings.profileDeleteButtonClick(test,driver);
				//Profile Delete Button check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfileConfirmDeleteButton()));
				ProfilesStrings.profileConfirmDeleteButtonCheck(test,driver);
				//Profile Delete Button click 
				ProfilesStrings.profileConfirmDeleteButtonClick(test,driver);
				//Profile empty list description check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfilesEmptyListDesc()));
				ProfilesStrings.profilesEmptyListDescCheck(test,driver);
				//Profile empty list button check 
				wait.until(ExpectedConditions.visibilityOf(ProfilesPages.getProfilesEmptyListButton()));
				ProfilesStrings.profilesEmptyListButtonCheck(test,driver);

			} else {
				test.log(Status.SKIP, "La page des profiles n'affiche pas 1 profil");
			}
		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

}

