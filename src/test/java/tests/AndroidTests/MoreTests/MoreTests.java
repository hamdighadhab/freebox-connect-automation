
package tests.AndroidTests.MoreTests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;
import Strings.CommonStrings;
import Strings.DevicesStrings.RecentlyLoggedDevicesStrings;
import Strings.MoreStrings.MoreStrings;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.HomePagePages.HomeConnectionStatus;
import pages.MorePages.MorePages;
import tests.Base.BaseClass;

public class MoreTests extends BaseClass{
	
	@Test 
	(priority=1, enabled=true)
	public void lockConnectApp() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(MoreStrings.TestName1, RecentlyLoggedDevicesStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//More tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(MorePages.getMoreTabBarButton()));
			MoreStrings.moreTabBarButtonCheck(test, driver);
			//Click on More tab-bar
			MoreStrings.moreTabBarButtonClick(test, driver);
			//More page name Check 
			wait.until(ExpectedConditions.visibilityOf(MorePages.getMorePageTitle()));
			MoreStrings.morePageTitleCheck(test, driver);
			//Ma Freebox section Check 
			wait.until(ExpectedConditions.visibilityOf(MorePages.getFreeboxSectionTitle()));
			MoreStrings.freeboxSectionTitleCheck(test, driver);
			//Informations section Check 
			wait.until(ExpectedConditions.visibilityOf(MorePages.getInfosSectionTitle()));
			MoreStrings.infosSectionTitleCheck(test, driver);
			//Lock element Check 
			wait.until(ExpectedConditions.visibilityOf(MorePages.getLockElementTitle()));
			MoreStrings.lockElementTitleCheck(test, driver);
			//Click on lock element
			MoreStrings.lockElementTitleClick(test, driver);
			//Lock Page Check 
			wait.until(ExpectedConditions.visibilityOf(MorePages.getLockPageName()));
			MoreStrings.lockPageNameCheck(test, driver);
			//Lock Page Description Check 
			wait.until(ExpectedConditions.visibilityOf(MorePages.getLockPageDesc()));
			MoreStrings.lockPageDescCheck(test, driver);
			//Activate Lock Button Check 
			wait.until(ExpectedConditions.visibilityOf(MorePages.getActivateLockButton()));
			MoreStrings.activateLockButtonCheck(test, driver);
			//Click on Activate Lock Button  
			wait.until(ExpectedConditions.visibilityOf(MorePages.getActivateLockButton()));
			MoreStrings.activateLockButtonClick(test, driver);

		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test 
	(priority=2, enabled=true)
	public void oursAppFiles() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(MoreStrings.TestName2, RecentlyLoggedDevicesStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//More tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(MorePages.getMoreTabBarButton()));
			MoreStrings.moreTabBarButtonCheck(test, driver);
			//Click on More tab-bar
			MoreStrings.moreTabBarButtonClick(test, driver);
			//More page name Check 
			wait.until(ExpectedConditions.visibilityOf(MorePages.getMorePageTitle()));
			MoreStrings.morePageTitleCheck(test, driver);
			//Ma Freebox section Check 
			wait.until(ExpectedConditions.visibilityOf(MorePages.getFreeboxSectionTitle()));
			MoreStrings.freeboxSectionTitleCheck(test, driver);
			//Informations section Check 
			wait.until(ExpectedConditions.visibilityOf(MorePages.getInfosSectionTitle()));
			MoreStrings.infosSectionTitleCheck(test, driver);
			//Ours App Element Check 
			wait.until(ExpectedConditions.visibilityOf(MorePages.getOursAppElement()));
			MoreStrings.oursAppElementCheck(test, driver);
			//Click on Ours App Element
			MoreStrings.oursAppElementClick(test, driver);
			//Ours Apps Page Title Check 
			wait.until(ExpectedConditions.visibilityOf(MorePages.getOursAppsPageTitle()));
			MoreStrings.oursAppsPageTitleCheck(test, driver);
			//Files App Name Check 
			wait.until(ExpectedConditions.visibilityOf(MorePages.getFilesAppName()));
			MoreStrings.filesAppNameCheck(test, driver);
			//Files App Name Check 
			MoreStrings.filesAppDescCheck(test, driver);
			
		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test 
	(priority=3, enabled=true)
	public void oursAppHome() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(MoreStrings.TestName3, RecentlyLoggedDevicesStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//More tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(MorePages.getMoreTabBarButton()));
			MoreStrings.moreTabBarButtonCheck(test, driver);
			//Click on More tab-bar
			MoreStrings.moreTabBarButtonClick(test, driver);
			//More page name Check 
			wait.until(ExpectedConditions.visibilityOf(MorePages.getMorePageTitle()));
			MoreStrings.morePageTitleCheck(test, driver);
			//Ma Freebox section Check 
			wait.until(ExpectedConditions.visibilityOf(MorePages.getFreeboxSectionTitle()));
			MoreStrings.freeboxSectionTitleCheck(test, driver);
			//Informations section Check 
			wait.until(ExpectedConditions.visibilityOf(MorePages.getInfosSectionTitle()));
			MoreStrings.infosSectionTitleCheck(test, driver);
			//Ours App Element Check 
			wait.until(ExpectedConditions.visibilityOf(MorePages.getOursAppElement()));
			MoreStrings.oursAppElementCheck(test, driver);
			//Click on Ours App Element
			MoreStrings.oursAppElementClick(test, driver);
			//Ours Apps Page Title Check 
			wait.until(ExpectedConditions.visibilityOf(MorePages.getOursAppsPageTitle()));
			MoreStrings.oursAppsPageTitleCheck(test, driver);
			//Home App Name Check 
			wait.until(ExpectedConditions.visibilityOf(MorePages.getHomeAppName()));
			MoreStrings.homeAppNameCheck(test, driver);
			//Home App Name Check 
			MoreStrings.homeAppDescCheck(test, driver);

			
		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test 
	(priority=4, enabled=true)
	public void aboutApp() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(MoreStrings.TestName4, RecentlyLoggedDevicesStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//More tab-bar button Check 
			wait.until(ExpectedConditions.visibilityOf(MorePages.getMoreTabBarButton()));
			MoreStrings.moreTabBarButtonCheck(test, driver);
			//Click on More tab-bar
			MoreStrings.moreTabBarButtonClick(test, driver);
			//More page name Check 
			wait.until(ExpectedConditions.visibilityOf(MorePages.getMorePageTitle()));
			MoreStrings.morePageTitleCheck(test, driver);
			//Ma Freebox section Check 
			wait.until(ExpectedConditions.visibilityOf(MorePages.getFreeboxSectionTitle()));
			MoreStrings.freeboxSectionTitleCheck(test, driver);
			//Informations section Check 
			wait.until(ExpectedConditions.visibilityOf(MorePages.getInfosSectionTitle()));
			MoreStrings.infosSectionTitleCheck(test, driver);
			//À propos Check 
			wait.until(ExpectedConditions.visibilityOf(MorePages.getAboutAppElement()));
			MoreStrings.aboutAppElementCheck(test, driver);
			//Click on À propos
			MoreStrings.aboutAppElementClick(test, driver);
			//À propos page name Check
			wait.until(ExpectedConditions.visibilityOf(MorePages.getAboutPageName()));
			MoreStrings.aboutPageNameCheck(test, driver);
			//Legal Notice Element Check
			wait.until(ExpectedConditions.visibilityOf(MorePages.getLegalNoticeElement()));
			MoreStrings.legalNoticeElementCheck(test, driver);
			//Used Library Element Check
			wait.until(ExpectedConditions.visibilityOf(MorePages.getUsedLibraryElement()));
			MoreStrings.usedLibraryElementCheck(test, driver);
			//Click on Used Library Element
			MoreStrings.usedLibraryElementClick(test, driver);
			//Used Library Page name Check
			wait.until(ExpectedConditions.visibilityOf(MorePages.getUsedLibraryPage()));
			MoreStrings.usedLibraryPageCheck(test, driver);
			/*
			if (driver.findElements(By.xpath("//*[@resource-id='com.app.android:id/prelogin_signup']")).size() > 0) {
	            System.out.println("FOUND");
	        } else {
	            System.out.println("NOT FOUND!");
	        }
			*/
			
		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

}
