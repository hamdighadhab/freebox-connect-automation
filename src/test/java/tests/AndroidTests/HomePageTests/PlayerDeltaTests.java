package tests.AndroidTests.HomePageTests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import Strings.CommonStrings;
import Strings.HomePageStrings.PlayersStrings;
import Strings.HomePageStrings.RepeaterStrings;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.HomePagePages.HomeConnectionStatus;
import pages.HomePagePages.PlayerPage;
import pages.HomePagePages.RepeaterPage;
import pages.HomePagePages.ServerPage;
import tests.Base.BaseClass;

public class PlayerDeltaTests extends BaseClass {

	@Test
	(priority=1, enabled=true)
	public void connectedDeltaPlayer() throws InterruptedException {
		Thread.sleep(3000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(PlayersStrings.TestName1, PlayersStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Second Equipment Check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getSecondEquipment()));
			CommonStrings.secondEquipmentCheck(test, driver);
			//Click on Second Equipment
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getSecondEquipment()));
			CommonStrings.secondEquipmentClick(test, driver);
			//Delta player Status Check
			wait.until(ExpectedConditions.visibilityOf(PlayerPage.getDeltaPlayerStatus()));
			PlayersStrings.deltaPlayerStatusCheck(test, driver);
			//Delta player Image Check
			wait.until(ExpectedConditions.visibilityOf(PlayerPage.getDeltaPlayerPicto()));
			PlayersStrings.deltaPlayerPictoCheck(test, driver);	

		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}


	@Test
	(priority=2, enabled=true)
	public void informationsDeltaPlayer() throws InterruptedException {
		Thread.sleep(3000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(PlayersStrings.TestName2, PlayersStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Second Equipment Check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getSecondEquipment()));
			CommonStrings.secondEquipmentCheck(test, driver);
			//Click on Second Equipment
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getSecondEquipment()));
			CommonStrings.secondEquipmentClick(test, driver);
			//Information Section name Check
			wait.until(ExpectedConditions.visibilityOf(PlayerPage.getIndormationsPlayerSection()));
			PlayersStrings.indormationsPlayerSectionCheck(test, driver);						
			/*Player Connection Element check
			wait.until(ExpectedConditions.visibilityOf(PlayerPage.getPlayerConnectionElement()));
			PlayersStrings.playerConnectionElementCheck(test, driver);*/
			//Player Connection Type check
			wait.until(ExpectedConditions.visibilityOf(PlayerPage.getPlayerConnectionType()));
			PlayersStrings.playerConnectionTypeCheck(test, driver);

		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test
	(priority=3, enabled=true)
	public void rebootDeltaPlayer() throws InterruptedException {
		Thread.sleep(3000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(PlayersStrings.TestName3, PlayersStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));
			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Second Equipment Check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getSecondEquipment()));
			CommonStrings.secondEquipmentCheck(test, driver);
			//Click on Second Equipment
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getSecondEquipment()));
			CommonStrings.secondEquipmentClick(test, driver);
			//Information Section name Check
			wait.until(ExpectedConditions.visibilityOf(PlayerPage.getRebootPlayerButton()));
			PlayersStrings.rebootPlayerButtonCheck(test, driver);	
			//Delta player reboot Click
			PlayersStrings.rebootPlayerButtonClick(test, driver);
			//Alert reboot player Title check
			wait.until(ExpectedConditions.visibilityOf(PlayerPage.getRebootPlayerAlertTitle()));
			PlayersStrings.rebootPlayerAlertTitleCheck(test, driver);
			//Cancel Reboot Repeater 
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getCancelRebootButton()));
			RepeaterStrings.cancelRebootButtonTextCheck(test, driver);
			//Cancel Reboot
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getCancelRebootButton()));
			RepeaterStrings.cancelRebootButtonClick(test, driver);

		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}


	@Test
	(priority=4, enabled=true)
	public void otherInformationsDeltaPlayer() throws InterruptedException {
		Thread.sleep(3000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(PlayersStrings.TestName4, PlayersStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));
			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Second Equipment Check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getSecondEquipment()));
			CommonStrings.secondEquipmentCheck(test, driver);
			//Click on Second Equipment
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getSecondEquipment()));
			CommonStrings.secondEquipmentClick(test, driver);
			//Other informations player check
			wait.until(ExpectedConditions.visibilityOf(PlayerPage.getOthersInformationsDeltaPlayer()));
			PlayersStrings.othersInformationsDeltaPlayerCheck(test, driver);
			//Other informations player Click
			PlayersStrings.OthersInformationsDeltaPlayerClick(test, driver);
			//Other informations player page title check
			wait.until(ExpectedConditions.visibilityOf(PlayerPage.getPlayerOtherInformationsTitle()));
			PlayersStrings.playerOtherInformationsTitleCheck(test, driver);
			//Software Version Element check
			PlayersStrings.playerLogicielVersionElementCheck(test, driver);
			//Serial Number Element check
			PlayersStrings.playerSerielNumberElementCheck(test, driver);
			//MAC Address Element check
			PlayersStrings.playerMACAdressElementCheck(test, driver);
			//Turned on Element check
			PlayersStrings.playerTurnedONElementCheck(test, driver);


		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

}
