package tests.AndroidTests.HomePageTests;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import Strings.CommonStrings;
import Strings.HomePageStrings.ConnexionStatusStrings;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.HomePagePages.HomeConnectionStatus;
import tests.Base.BaseClass;



public class ConnectionStatusTests extends BaseClass {

	@Test 
	(priority=1, enabled=true)
	public void connectedStatus() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(ConnexionStatusStrings.TestName1, ConnexionStatusStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Click on Connection status button
			HomeConnectionStatus.clickOnConnectionStatusButton();
			//Status Page Check
			ConnexionStatusStrings.connectionStatusPageCheck(test, driver);
			//Status Check
			ConnexionStatusStrings.connectionStatusCheck(test, driver);
			//DELTA or POP check
			ConnexionStatusStrings.deltaBoxCheck(test, driver);
			//Wan rate Download Check
			ConnexionStatusStrings.wanRateDownloadCheck(test, driver);
			//Wan rate upload Check
			ConnexionStatusStrings.wanRateUploadCheck(test, driver);

		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}
}
