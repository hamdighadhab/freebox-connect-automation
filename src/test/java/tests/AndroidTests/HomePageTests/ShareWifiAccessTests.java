package tests.AndroidTests.HomePageTests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import Strings.CommonStrings;
import Strings.HomePageStrings.ShareWifiAccessStrings;
import elements.CommonElements;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.HomePagePages.HomeConnectionStatus;
import pages.HomePagePages.ShareWifiAccessPage;
import tests.Base.BaseClass;

public class ShareWifiAccessTests extends BaseClass{

	public static String freeBoxName1;
	public ExtentTest test;

	@Test
	//http://192.168.108.101/Testlink/linkto.php?tprojectPrefix=Connect-Android&item=testcase&id=Connect-Android-6
	(priority=1, enabled=true)
	public void shareWifiAccessKey() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		test = extent.createTest(ShareWifiAccessStrings.TestName1 , ShareWifiAccessStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {
			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));
			//Home page verification
			wait.until(ExpectedConditions.visibilityOf(CommonElements.getLogoFreeHome()));
			CommonStrings.homePageCheck(test, driver);
			//Click on share WiFi access button
			wait.until(ExpectedConditions.visibilityOf(CommonElements.getShareWiFibutton()));
			ShareWifiAccessStrings.shareWiFibuttonclick(test, driver);
			//My WiFi access WiFi page display check
			ShareWifiAccessStrings.accessPageCheck(test, driver);
			//Share button display check
			ShareWifiAccessStrings.shareKeyWifiButtonCheck(test, driver);
			//Click on Share button
			wait.until(ExpectedConditions.visibilityOf(CommonElements.getShareWifiKey()));
			ShareWifiAccessStrings.shareWifiKeyclick(test, driver);
			//Chooser_header check
			synchronized (driver)
			{
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			}
			driver.context("NATIVE_APP");
			if ((driver.findElement(By.id("android:id/chooser_header")).isDisplayed() == true)){
				test.log(Status.PASS, "Le Chooser_header s'affiche");
			} else {
				test.log(Status.FAIL, "La Chooser_header ne s'affiche pas");
			}
			//Back Android
			driver.navigate().back();	

		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test
	//http://192.168.108.101/Testlink/linkto.php?tprojectPrefix=Connect-Android&item=testcase&id=Connect-Android-7
	(priority=2, enabled=true)
	public void shareWifiAccessQRCode() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		test = extent.createTest(ShareWifiAccessStrings.TestName2, ShareWifiAccessStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(1000, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));
			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Click on share WiFi access button
			ShareWifiAccessStrings.shareWiFibuttonclick(test, driver);
			//My WiFi access WiFi page verification
			ShareWifiAccessStrings.accessPageCheck(test, driver);
			//Display QR Code button display check
			ShareWifiAccessStrings.QRCodebuttonCheck(test, driver);
			//Click on Display QR Code button
			ShareWifiAccessStrings.shareWiFiQRCodeButtonclick(test, driver);
			//Display QR Code button display check
			ShareWifiAccessStrings.QRCodeCheck(test, driver);
			//Back Android
			driver.navigate().back();	

		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test
	//http://192.168.108.101/Testlink/linkto.php?tprojectPrefix=Connect-Android&item=testcase&id=Connect-Android-8
	(priority=3, enabled=true)
	public void unlimitedGuestAccessInternetOnlyAutoKey() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		test = extent.createTest(ShareWifiAccessStrings.TestName3, ShareWifiAccessStrings.FonctionalityName);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		driver.manage().timeouts().implicitlyWait(1000, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));
			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Click on share WiFi access button
			ShareWifiAccessStrings.shareWiFibuttonclick(test, driver);
			//My WiFi access page verification
			ShareWifiAccessStrings.accessPageCheck(test, driver);
			//WiFi Guest Tab check
			ShareWifiAccessStrings.wiFiGuestTabCheck(test, driver);
			//Click on WiFi Guest Tab
			ShareWifiAccessStrings.wiFiGuestTabClick(test, driver);
			//Create Guest Access button display check
			ShareWifiAccessStrings.createGuestWifiAccessCheck(test, driver);
			//Click on Create Guest Access button
			ShareWifiAccessStrings.createGuestWifiAccessTabclick(test, driver);
			Thread.sleep(2000);
			//WiFi Guest Note check
			ShareWifiAccessStrings.wiFiGuestNoteCheck(test, driver);
			//Click on WiFi Guest Note
			ShareWifiAccessStrings.WiFiGuestNoteClick(test, driver);
			//WiFi Guest Note Field check
			ShareWifiAccessStrings.wiFiGuestNoteFieldCheck(test, driver);
			//Edit Note
			//ShareWifiAccessPage.enterGuestWiFiAccessNote();
			driver.navigate().back();
			//Wifi Note Validate check
			ShareWifiAccessStrings.wifiNoteValidateCheck(test, driver);
			//Click on Wifi Note Validate
			ShareWifiAccessStrings.wifiNoteValidateClick(test, driver);
			//Guest WiFi Access Name Field display check
			ShareWifiAccessStrings.validateGuestWiFiAccessButtonCheck(test, driver);
			//Click on Validate Guest WiFi Access Button
			ShareWifiAccessStrings.validateGuestWiFiAccessButtonclick(test, driver);
			//verification of the addition of guest WiFi access 
			//ShareWifiAccessStrings.GuestWiFiNameCheck(test, driver);

		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}		

	@Test
	//http://192.168.108.101/Testlink/linkto.php?tprojectPrefix=Connect-Android&item=testcase&id=Connect-Android-14
	(priority=4, enabled=true)
	public void guestWiFiSystemSharing() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		test = extent.createTest(ShareWifiAccessStrings.TestName4, ShareWifiAccessStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));
			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Click on share WiFi access button
			ShareWifiAccessStrings.shareWiFibuttonclick(test, driver);
			//My WiFi access page verification
			ShareWifiAccessStrings.accessPageCheck(test, driver);
			//WiFi Guest Tab check
			ShareWifiAccessStrings.wiFiGuestTabCheck(test, driver);
			//Click on WiFi Guest Tab
			ShareWifiAccessStrings.wiFiGuestTabClick(test, driver);
			//Share button display check
			ShareWifiAccessStrings.shareWifiGuestKeyButtonCheck(test, driver);
			//Click on Share button
			ShareWifiAccessStrings.shareWifiGuestKeyButtonclick(test, driver);

			synchronized (driver)
			{
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			}
			driver.context("NATIVE_APP");
			if ((driver.findElement(By.id("android:id/chooser_header")).isDisplayed() == true)){
				test.log(Status.PASS, "Le Chooser_header s'affiche");
			} else {
				test.log(Status.FAIL, "La Chooser_header ne s'affiche pas");
			}

			//Back Android
			driver.navigate().back();	

		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test
	//http://192.168.108.101/Testlink/linkto.php?tprojectPrefix=Connect-Android&item=testcase&id=Connect-Android-15
	(priority=5, enabled=true)
	public void guestWiFiSystemQRCodeSharing() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		test = extent.createTest(ShareWifiAccessStrings.TestName5, ShareWifiAccessStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));
			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Click on share WiFi access button
			ShareWifiAccessStrings.shareWiFibuttonclick(test, driver);
			//My WiFi access page verification
			ShareWifiAccessStrings.accessPageCheck(test, driver);
			//WiFi Guest Tab check
			ShareWifiAccessStrings.wiFiGuestTabCheck(test, driver);
			//Click on WiFi Guest Tab
			ShareWifiAccessStrings.wiFiGuestTabClick(test, driver);
			//Display QR Code button display check
			ShareWifiAccessStrings.shareWifiGuestQRCodeButtonCheck(test, driver);
			//Click on Display QR Code button
			test.log(Status.INFO, "Clique sur le bouton Afficher le QR Code");
			ShareWifiAccessPage.clickOnShareWifiGuestQRCodeButton();
			//Display QR Code button display check
			ShareWifiAccessStrings.QRCodeCheck(test, driver);
			//Back Android
			driver.navigate().back();	

		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test
	//http://192.168.108.101/Testlink/linkto.php?tprojectPrefix=Connect-Android&item=testcase&id=Connect-Android-13
	(priority=6, enabled=true)
	public void deleteGuestAccess() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		test = extent.createTest(ShareWifiAccessStrings.TestName6, ShareWifiAccessStrings.FonctionalityName);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));
			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Click on share WiFi access button
			ShareWifiAccessStrings.shareWiFibuttonclick(test, driver);
			//My WiFi access page verification
			ShareWifiAccessStrings.accessPageCheck(test, driver);
			//WiFi Guest Tab check
			ShareWifiAccessStrings.wiFiGuestTabCheck(test, driver);
			//Click on WiFi Guest Tab
			ShareWifiAccessStrings.wiFiGuestTabClick(test, driver);
			Thread.sleep(1000);
			//Delete button display check
			wait.until(ExpectedConditions.visibilityOf(ShareWifiAccessPage.getDeleteGuestWiFiAccessButton()));
			ShareWifiAccessStrings.DeleteGuestWiFiAccessButtonCheck(test, driver);
			//Click on Delete Guest WiFi Access button
			wait.until(ExpectedConditions.visibilityOf(ShareWifiAccessPage.getDeleteGuestWiFiAccessButton()));
			ShareWifiAccessStrings.deleteGuestWiFiAccessButtonclick(test, driver);
			//Display QR Code button display check
			wait.until(ExpectedConditions.visibilityOf(ShareWifiAccessPage.getComfirmationDeleteGuestWiFiAccess()));
			ShareWifiAccessStrings.ComfirmationDeleteGuestWiFiAccessCheck(test, driver);
			//Click on Delete Guest WiFi Access button
			wait.until(ExpectedConditions.visibilityOf(ShareWifiAccessPage.getComfirmationDeleteGuestWiFiAccess()));
			ShareWifiAccessStrings.comfirmationDeleteGuestWiFiAccessButtonClick(test, driver);
			Thread.sleep(3000);

		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

}
