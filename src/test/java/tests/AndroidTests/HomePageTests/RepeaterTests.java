package tests.AndroidTests.HomePageTests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import Strings.CommonStrings;
import Strings.HomePageStrings.RepeaterStrings;
import elements.CommonElements;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.HomePagePages.HomeConnectionStatus;
import pages.HomePagePages.RepeaterPage;
import pages.HomePagePages.ServerPage;
import tests.Base.BaseClass;


public class RepeaterTests extends BaseClass{
	@Test
	(priority=1, enabled=true)
	public void connectedRepeater() throws InterruptedException {
		Thread.sleep(3000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(RepeaterStrings.TestName1, RepeaterStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Third Equipment Check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getSecondEquipment()));
			CommonStrings.thirdEquipmentCheck(test, driver);
			//Click on Third Equipment
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getSecondEquipment()));
			CommonStrings.thirdEquipmentClick(test, driver);
			//Repeater Status Check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getEquipmentDetailStatus()));
			RepeaterStrings.equipmentDetailStatusCheck(test, driver);
			//Repeater Status Text Check
			RepeaterStrings.EquipmentStatusCheck(test, driver);
			//Repeater Image Check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getEquipmentDetailImage()));
			RepeaterStrings.equipmentDetailImageCheck(test, driver);

		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test
	(priority=2, enabled=true)
	public void repeaterInformations() throws InterruptedException {
		Thread.sleep(3000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(RepeaterStrings.TestName2, RepeaterStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Third Equipment Check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getSecondEquipment()));
			CommonStrings.thirdEquipmentCheck(test, driver);
			//Click on Third Equipment
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getSecondEquipment()));
			CommonStrings.thirdEquipmentClick(test, driver);
			//Information Item Title Check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getServerInformationsTitle()));
			RepeaterStrings.serverInformationsTitleCheck(test, driver);
			//Type Information Check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getServerInformationsTitle()));
			RepeaterStrings.typeInfomationsElementCheck(test, driver);
			//Server Type Check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getServerType()));
			RepeaterStrings.serverTypeValueCheck(test, driver);

		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test
	(priority=3, enabled=true)
	public void connectedDevicesToRepeater() throws InterruptedException {
		Thread.sleep(3000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(RepeaterStrings.TestName3, RepeaterStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Third Equipment Check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getSecondEquipment()));
			CommonStrings.thirdEquipmentCheck(test, driver);
			//Click on Third Equipment
			CommonStrings.thirdEquipmentClick(test, driver);
			//Connected devices element text Check
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getConnectedRepeaterDevices()));
			RepeaterStrings.connectedRepeaterDevicesTextCheck(test, driver);
			//Number of Connected devices element Check
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getRepeaterConnectedDevicesNumber()));
			RepeaterStrings.repeaterConnectedDevicesNumberCheck(test, driver);
			//Number devices connected
			test.log(Status.INFO, "Le Nombre des appareils connectés est :" + RepeaterPage.getRepeaterConnectedDevicesValue());


			if (!RepeaterPage.getRepeaterConnectedDevicesValue().equals("0")) {
				//Click on Connected Devices
				wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getRepeaterConnectedDevicesNumber()));
				RepeaterStrings.connectedRepeaterDevicesClick(test, driver);
				//Connected Devices Page Title
				wait.until(ExpectedConditions.visibilityOf(ServerPage.getConnectedDevicesPageTitle()));
				RepeaterStrings.connectedDevicesPageTitleTextCheck(test, driver);
				//Device name Check
				wait.until(ExpectedConditions.visibilityOf(ServerPage.getDeviceName()));
				RepeaterStrings.deviceNameCheck(test, driver);
				//Device Vendor Check
				wait.until(ExpectedConditions.visibilityOf(ServerPage.getDeviceVendor()));
				RepeaterStrings.deviceVendorCheck(test, driver);
				//Device Image Check
				wait.until(ExpectedConditions.visibilityOf(ServerPage.getDeviceImg()));
				RepeaterStrings.deviceImgCheck(test, driver);
				//Device Connexion Type Check
				wait.until(ExpectedConditions.visibilityOf(ServerPage.getDeviceConnectionType()));
				RepeaterStrings.DeviceConnectionTypeCheck(test, driver);
			} else {
				//No connected device 
				test.log(Status.INFO, "Pas d'appareils connéctés a la box'");
				//Click on Connected Devices
				wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getRepeaterConnectedDevicesNumber()));
				RepeaterStrings.connectedRepeaterDevicesClick(test, driver);
				//Connected Devices Page Title
				wait.until(ExpectedConditions.visibilityOf(ServerPage.getConnectedDevicesPageTitle()));
				RepeaterStrings.connectedDevicesPageTitleTextCheck(test, driver);
				//No connected device Description
				wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getNoDeviceDesc()));
				RepeaterStrings.noDeviceDescTextCheck(test, driver);
			}

		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test
	(priority=4, enabled=true)
	public void indicatorLight() throws InterruptedException {
		Thread.sleep(4000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(RepeaterStrings.TestName4, RepeaterStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Third Equipment Check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getSecondEquipment()));
			CommonStrings.thirdEquipmentCheck(test, driver);
			//Click on Third Equipment
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getSecondEquipment()));
			CommonStrings.thirdEquipmentClick(test, driver);
			//Scroll To
			CommonElements.scrollTo("Redémarrer le répéteur");
			//Light Indicator element
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getLightIndicator()));
			RepeaterStrings.lightIndicatorTextCheck(test, driver);
			//Indicator light Switch
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getLightIndicatorSwitch()));
			RepeaterStrings.lightIndicatorSwitchCheck(test, driver);

			//Switch State
			if (RepeaterPage.getLightIndicatorSwitchChecked().equals("true")) {
				RepeaterStrings.lightIndicatorSwitchONCheck(test, driver);
			} else {
				RepeaterStrings.lightIndicatorSwitchOFFCheck(test, driver);
			}

		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test
	(priority=5, enabled=true)
	public void RepeaterReboot() throws InterruptedException {
		Thread.sleep(3000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(RepeaterStrings.TestName5, RepeaterStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Third Equipment Check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getSecondEquipment()));
			CommonStrings.thirdEquipmentCheck(test, driver);
			//Click on Third Equipment
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getSecondEquipment()));
			CommonStrings.thirdEquipmentClick(test, driver);
			//Scroll To
			CommonElements.scrollTo("Redémarrer le répéteur");
			//Reboot button check
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getRebootRepeaterButton()));
			RepeaterStrings.rebootRepeaterButtonTextCheck(test, driver);
			//Click on reboot repeater button
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getRebootRepeaterButton()));
			RepeaterStrings.rebootRepeaterButtonClick(test, driver);
			//Reboot Repeater Alert Title check
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getRebootRepeaterAlertTitle()));
			RepeaterStrings.rebootRepeaterAlertTitleTextCheck(test, driver);
			//Reboot Repeater Alert Message check
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getRebootRepeaterAlertMessage()));
			RepeaterStrings.rebootRepeaterAlertMessageTextCheck(test, driver);
			//Cancel Reboot Repeater 
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getCancelRebootButton()));
			RepeaterStrings.cancelRebootButtonTextCheck(test, driver);
			//Cancel Reboot
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getCancelRebootButton()));
			RepeaterStrings.cancelRebootButtonClick(test, driver);
			//Back to informations page check
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getRebootRepeaterButton()));
			RepeaterStrings.rebootRepeaterButtonTextCheck(test, driver);

		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test
	(priority=6, enabled=true)
	public void DeleteRepeater() throws InterruptedException {
		Thread.sleep(3000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(RepeaterStrings.TestName6, RepeaterStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));
			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Third Equipment Check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getSecondEquipment()));
			CommonStrings.thirdEquipmentCheck(test, driver);
			//Click on Third Equipment
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getSecondEquipment()));
			CommonStrings.thirdEquipmentClick(test, driver);
			//Scroll To
			CommonElements.scrollTo("Supprimer de mes équipements");
			//Delete Repeater button check
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getDeleteRepeaterButton()));
			RepeaterStrings.deleteRepeaterButtonTextCheck(test, driver);
			//Click on delete repeater button
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getDeleteRepeaterButton()));
			RepeaterStrings.deleteRepeaterButtonClick(test, driver);
			//Cancel delete
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getCancelRebootButton()));
			RepeaterStrings.cancelRebootButtonClick(test, driver);
			//Back to informations page check
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getRebootRepeaterButton()));
			RepeaterStrings.rebootRepeaterButtonTextCheck	(test, driver);	

		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test
	(priority=7, enabled=true)
	public void MoveYourRepeaterAway() throws InterruptedException {
		Thread.sleep(3000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(RepeaterStrings.TestName7, RepeaterStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Third Equipment Check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getSecondEquipment()));
			CommonStrings.thirdEquipmentCheck(test, driver);
			//Click on Third Equipment
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getSecondEquipment()));
			CommonStrings.thirdEquipmentClick(test, driver);
			//Move your Repeater button check
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getrepeaterWarningButton()));
			RepeaterStrings.repeaterWarningButtonTextCheck(test, driver);
			//Click on move your repeater button
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getrepeaterWarningButton()));
			RepeaterStrings.repeaterWarningButtonClick(test, driver);
			//Move your Repeater title page check
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getMoveRepeaterPageTitle()));
			RepeaterStrings.moveRepeaterPageTitleTextCheck(test, driver);
			//Move repeater Image check Check
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getRepeaterWarningImage()));
			RepeaterStrings.repeaterWarningImageCheck(test, driver);
			//Warning title check
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getRepeaterWarningTitle()));
			RepeaterStrings.repeaterWarningTitleTextCheck(test, driver);
			//Close Warning button check
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getRepeaterWarningCloseButton()));
			RepeaterStrings.repeaterWarningCloseButtonTextCheck(test, driver);
			//Close Warning page
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getRepeaterWarningCloseButton()));
			RepeaterStrings.deleteRepeaterButtonTextClick(test, driver);
			//Move your Repeater button check
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getrepeaterWarningButton()));
			RepeaterStrings.repeaterWarningButtonTextCheck(test, driver);

		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}
	@Test
	(priority=8, enabled=true)
	public void repeaterOthersInformations() throws InterruptedException {
		Thread.sleep(3000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(RepeaterStrings.TestName8, RepeaterStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));
			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Third Equipment Check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getSecondEquipment()));
			CommonStrings.thirdEquipmentCheck(test, driver);
			//Click on Third Equipment
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getSecondEquipment()));
			CommonStrings.thirdEquipmentClick(test, driver);
			//Scroll To
			CommonElements.scrollTo("Redémarrer le répéteur");
			//Others Information Element Check 
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getRepeaterOthersInformation()));
			RepeaterStrings.repeaterOthersInformationTextCheck(test, driver);
			//Click on Others Informations
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getRepeaterOthersInformation()));
			RepeaterStrings.repeaterOthersInformationClick(test, driver);
			//Software version Check 
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getRepeaterLogicielVersion()));
			RepeaterStrings.repeaterLogicielVersionTextCheck(test, driver);
			//Serial number Check 
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getRepeaterSerialNumber()));
			RepeaterStrings.repeaterSerialNumberTextCheck(test, driver);
			//MAC adress Check 
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getRepeaterMACAdress()));
			RepeaterStrings.repeaterMACAdressTextCheck(test, driver);
			//IPV4 adress Check 
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getRepeaterIPV4Adress()));
			RepeaterStrings.repeaterIPV4AdressTextCheck(test, driver);
			//IPV6 adress Check 
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getRepeaterIPV6Adress()));
			RepeaterStrings.repeaterIPV6AdressTextCheck(test, driver);
			//WiFi 2.4GHz Check 
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getRepeaterWiFi2GHz()));
			RepeaterStrings.repeaterWiFi2GHzTextCheck(test, driver);
			//WiFi 5GHz Check 
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getRepeaterWiFi5GHz()));
			RepeaterStrings.repeaterWiFi5GHzTextCheck(test, driver);
			//Turn on from check 
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getRepeaterTurnOn()));
			RepeaterStrings.repeaterTurnOnTextCheck(test, driver);

		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test
	(priority=9, enabled=true)
	public void changeRepeaterName() throws InterruptedException {
		Thread.sleep(000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(RepeaterStrings.TestName9, RepeaterStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Third Equipment Check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getSecondEquipment()));
			CommonStrings.thirdEquipmentCheck(test, driver);
			//Click on Third Equipment
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getSecondEquipment()));
			CommonStrings.thirdEquipmentClick(test, driver);
			//Room element check 
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getRoomrepeaterElement()));
			RepeaterStrings.roomrepeaterElementTextCheck(test, driver);	
			//Room Name check 
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getRepeaterRoomName()));
			RepeaterStrings.repeaterRoomNameTextCheck(test, driver);
			//Click on Room Element
			RepeaterStrings.roomrepeaterElementClick(test, driver);
			//Select Chambre
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getNewRepeaterName()));
			RepeaterStrings.newRepeaterNameClick(test, driver);
			Thread.sleep(5000);
			//New Name check 
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getRepeaterRoomName()));
			RepeaterStrings.roomrepeaterElementTextCheck(test, driver);		
			//Click on Room Element
			RepeaterStrings.roomrepeaterElementClick(test, driver);
			//Select Chambre		
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getOldRepeaterName()));
			RepeaterStrings.oldRepeaterNameClick(test, driver);
			Thread.sleep(7000);
			//Room Name check 
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getRepeaterRoomName()));
			if ((RepeaterPage.getRepeaterRoomNameText().equals(RepeaterPage.RepeaterRoomName)) && (RepeaterPage.isRepeaterRoomNameDiplayed() == true)) {
				test.log(Status.PASS, "Le nom de Pièce est bien changé ");
			} else {
				test.log(Status.FAIL, "Le nom de Pièce n'est pas bien changé");
			}

		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test
	(priority=10, enabled=true)
	public void improveMyConnection() throws InterruptedException {
		Thread.sleep(3000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(RepeaterStrings.TestName10, RepeaterStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//Third Equipment Check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getSecondEquipment()));
			CommonStrings.thirdEquipmentCheck(test, driver);
			//Click on Third Equipment
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getSecondEquipment()));
			CommonStrings.thirdEquipmentClick(test, driver);
			//Advice button check 
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getRepeaterWarningHelp()));
			RepeaterStrings.repeaterWarningHelpTextCheck(test, driver);
			//Click on Advice button
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getRepeaterWarningHelp()));
			RepeaterStrings.repeaterWarningHelpClick(test, driver);
			//Onboarding first page check 
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getImproveMyConnectionTitlePage()));
			RepeaterStrings.onboardingFirstPageCheck(test, driver);
			//Click Next Advice button
			RepeaterStrings.nextAdviceButtonClick(test, driver);
			//Onboarding Second page check 
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getImproveMyConnectionTitlePage()));
			RepeaterStrings.onboardingSecondPageCheck(test, driver);
			//Click Next Advice button
			RepeaterStrings.nextAdviceButtonClick(test, driver);
			//Onboarding Third page check 
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getImproveMyConnectionTitlePage()));
			RepeaterStrings.onboardingThirdPageCheck(test, driver);
			//Click Next Advice button
			RepeaterStrings.nextAdviceButtonClick(test, driver);
			//Onboarding Fourth page check 
			wait.until(ExpectedConditions.visibilityOf(RepeaterPage.getImproveMyConnectionTitlePage()));
			RepeaterStrings.onboardingFourthPageCheck(test, driver);
			//Click Next Advice button
			RepeaterPage.clickOnNextAdviceButton();
			test.log(Status.INFO, "Clique sur le bouton 'Terminer'");
			//Informations repeater page Check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getEquipmentDetailImage()));
			RepeaterStrings.equipmentDetailImageCheck(test, driver);
		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

}
