package tests.AndroidTests.HomePageTests;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import Strings.CommonStrings;
import io.appium.java_client.AppiumDriver;
import Strings.HomePageStrings.FreeBoxDeltaStrings;
import io.appium.java_client.MobileElement;
import pages.HomePagePages.HomeConnectionStatus;
import pages.HomePagePages.ServerPage;
import tests.Base.BaseClass;

public class FreeBoxDeltaTests extends BaseClass {

	@Test
	//http://192.168.108.101/Testlink/linkto.php?tprojectPrefix=Connect-Android&item=testcase&id=Connect-Android-16
	(priority=1, enabled=true)
	public void connectedServer() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(FreeBoxDeltaStrings.TestName1, FreeBoxDeltaStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//First Equipment Check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getFirstEquipment()));
			CommonStrings.firstEquipmentCheck(test, driver);
			//Click on First Equipment
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getFirstEquipment()));
			CommonStrings.firstEquipmentClick(test, driver);
			//Server Status Check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getEquipmentDetailStatus()));
			FreeBoxDeltaStrings.equipmentDetailStatusCheck(test, driver);
			//Server Status Check
			FreeBoxDeltaStrings.equipmentStatusCheck(test, driver);
			//Server Image Check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getEquipmentDetailImage()));
			FreeBoxDeltaStrings.equipmentDetailImageCheck(test, driver);

		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test
	(priority=2, enabled=true)
	public void deltaServerInformations() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(FreeBoxDeltaStrings.TestName2, FreeBoxDeltaStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//First Equipment Check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getFirstEquipment()));
			CommonStrings.firstEquipmentCheck(test, driver);
			//Click on First Equipment
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getFirstEquipment()));
			CommonStrings.firstEquipmentClick(test, driver);
			//Information Item Title Check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getServerInformationsTitle()));
			FreeBoxDeltaStrings.serverInformationsTitleCheck(test, driver);
			//Type Information Check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getServerInformationsTitle()));
			FreeBoxDeltaStrings.typeInfomationsElemenCheck(test, driver);
			//Server Type Check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getServerType()));
			FreeBoxDeltaStrings.serverTypeValueCheck(test, driver);
			//Connected Device element check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getConnectedDeviceInfoElement()));
			FreeBoxDeltaStrings.connectedDeviceInfoElementCheck(test, driver);

		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test
	(priority=3, enabled=true)
	public void deltaOthersInformations() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(FreeBoxDeltaStrings.TestName3, FreeBoxDeltaStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//First Equipment Check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getFirstEquipment()));
			CommonStrings.firstEquipmentCheck(test, driver);
			//Click on First Equipment
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getFirstEquipment()));
			CommonStrings.firstEquipmentClick(test, driver);
			//Others Information Element Check 
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getDeltaOtherInformations()));
			FreeBoxDeltaStrings.deltaOtherInformationsCheck(test, driver);			
			//Click on Others Informations
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getDeltaOtherInformations()));
			FreeBoxDeltaStrings.deltaOtherInformationsClick(test, driver);
			//Delta Model Check 
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getModeleDelta()));
			FreeBoxDeltaStrings.modeleDeltaCheck(test, driver);
			//Delta Model Check
			//wait.until(ExpectedConditions.visibilityOf(ServerPage.getModeleDeltaValue()));
			//FreeBoxDeltaStrings.modeleDeltaTextCheck(test, driver);
			//Software Version Check 
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getVersionLogicielleDelta()));
			FreeBoxDeltaStrings.versionLogicielleDeltaCheck(test, driver);
			//Serial Number Check 
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getNumSerieDelta()));
			FreeBoxDeltaStrings.numSerieDeltaCheck(test, driver);
			//MAC Address Check 
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getAdresseMACDelta()));
			FreeBoxDeltaStrings.adresseMACDeltaCheck(test, driver);
			//IPV4 Address Check 
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getIPV4Delta()));
			FreeBoxDeltaStrings.IPV4DeltaCheck(test, driver);
			//IPV4 Address Check 
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getIPV6Delta()));
			FreeBoxDeltaStrings.IPV6DeltaCheck(test, driver);
			//Allumé Depuis Check 
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getAllumeDepuisDelta()));
			FreeBoxDeltaStrings.allumeDepuisDeltaCheck(test, driver);

		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test
	(priority=4, enabled=true)
	public void rebootServer() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(FreeBoxDeltaStrings.TestName4, FreeBoxDeltaStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//First Equipment Check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getFirstEquipment()));
			CommonStrings.firstEquipmentCheck(test, driver);
			//Click on First Equipment
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getFirstEquipment()));
			CommonStrings.firstEquipmentClick(test, driver);
			//Server Status Check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getEquipmentDetailStatus()));
			FreeBoxDeltaStrings.equipmentDetailStatusCheck(test, driver);	
			//Reboot button check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getRebootButton()));
			FreeBoxDeltaStrings.rebootButtonaCheck(test, driver);
			//Click on Reboot button
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getRebootButton()));
			FreeBoxDeltaStrings.rebootButtonClick(test, driver);
			//Reboot Page Title check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getServerRebootTitle()));
			FreeBoxDeltaStrings.serverRebootTitleTextCheck(test, driver);
			//Reboot Page Header check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getServerRebootHeader()));
			FreeBoxDeltaStrings.serverRebootHeaderTextCheck(test, driver);
			//Reboot Page Header check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getServerRebootDescription()));
			FreeBoxDeltaStrings.serverRebootDescriptionTextCheck(test, driver);
			//Reboot Page Image check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getServerRebootImage()));
			FreeBoxDeltaStrings.serverRebootImageCheck(test, driver);
			//Reboot Button2 check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getServerRebootButton2()));
			FreeBoxDeltaStrings.rebootButton2Check(test, driver);

		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}

	@Test
	(priority=5, enabled=true)
	public void connectedDevicesToDeltaBox() throws InterruptedException {
		Thread.sleep(2000);
		driver = new AppiumDriver<MobileElement>(url,cap);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		ExtentTest test = extent.createTest(FreeBoxDeltaStrings.TestName5, FreeBoxDeltaStrings.FonctionalityName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {

			Thread.sleep(2000);
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(HomeConnectionStatus.getQuatriemeFreeboxAssocie()));

			//Home page verification
			CommonStrings.homePageCheck(test, driver);
			//First Equipment Check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getFirstEquipment()));
			CommonStrings.firstEquipmentCheck(test, driver);
			//Click on First Equipment
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getFirstEquipment()));
			CommonStrings.firstEquipmentClick(test, driver);
			//Connected devices element text Check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getConnectedDeltaDevices()));
			FreeBoxDeltaStrings.connectedDeltaDevicesTextCheck(test, driver);
			//Number of Connected devices element Check
			wait.until(ExpectedConditions.visibilityOf(ServerPage.getConnectedDevicesNumber()));
			FreeBoxDeltaStrings.connectedDevicesNumberCheck(test, driver);
			//Number devices connected
			test.log(Status.INFO, "Le Nombre des appareils connectés est :" + ServerPage.getConnectedDevicesValue());

			if (!ServerPage.getConnectedDevicesValue().equals("0")) {
				//Click on Connected Devices
				wait.until(ExpectedConditions.visibilityOf(ServerPage.getConnectedDevicesNumber()));
				FreeBoxDeltaStrings.connectedDeltaDeviceClick(test, driver);
				//Connected Devices Page Title
				wait.until(ExpectedConditions.visibilityOf(ServerPage.getConnectedDevicesPageTitle()));
				FreeBoxDeltaStrings.connectedDevicesPageTitleTextCheck(test, driver);
				//Device name Check
				wait.until(ExpectedConditions.visibilityOf(ServerPage.getDeviceName()));
				FreeBoxDeltaStrings.deviceNameCheck(test, driver);
				//Device Vendor Check
				wait.until(ExpectedConditions.visibilityOf(ServerPage.getDeviceVendor()));
				FreeBoxDeltaStrings.deviceVendorCheck(test, driver);
				//Device Image Check
				wait.until(ExpectedConditions.visibilityOf(ServerPage.getDeviceImg()));
				FreeBoxDeltaStrings.deviceImgCheck(test, driver);
				//Device Connexion Type Check
				wait.until(ExpectedConditions.visibilityOf(ServerPage.getDeviceConnectionType()));
				FreeBoxDeltaStrings.deviceConnectionTypeCheck(test, driver);
			} else {
				test.log(Status.INFO, "Pas d'appareils connéctés a la box'");
			}

		} catch (Exception e) {
			test.fail(e);
		}
		CommonStrings.endTest(driver);
	}


}
